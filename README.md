# XSections Analysis for h025 dataset

[![pipeline status](https://gitlab.cern.ch/lamberti/h025_xsections/badges/master/pipeline.svg)](https://gitlab.cern.ch/lamberti/h025_xsections/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)


Code targeting H→γγ differential cross section analysis. Using h025 dataset from `/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025/`

The analysis uses [xmlAnaWSBuilder](https://gitlab.cern.ch/atlas-hgam-sw/xmlAnaWSBuilder) and [quickFit](https://gitlab.cern.ch/atlas_higgs_combination/software/quickFit). The analysis provides Docker images for running or for developing.

## Installing the code

The code is organized as a python package. You can decide to fully install it (in a virtual environment, a in docker image, or in your system), for example, from the main folder:

    pip install .

### Using Docker to run the analysis

A Docker image is available at https://gitlab.cern.ch/lamberti/h025_xsections/container_registry, it contains all the code, already installed with all the dependencies.

## Getting Started

Here what I think the best way to work. Suppose the first part of the analysis (production of many histograms) have been already made. Then you can work mostly on a single machine.

1. Download the package with git, you will get a `h025_xsections` folder with the code (and only the code)
2. Download the data folder (`/eos/user/t/turra/h025_xsections_data/v0.6.1`). This is a large folder (3 GB). It contains all the needed input (the merged histograms (200 MB)), and the output of all the intermediate and final steps, including plots (100 MB) and the data (280MB). The largest directory is the one containing the workspace (900 MB) and the fitted workspace. The bare minimum you need is the directory with the merged histograms (`output_histo_norm_merged`), the values of the cross-sections (`xsections` directory) and the data. If you are using cernbox it is very suggested to copy the directory outside cernbox, in a scratch directory, so that every time you make a change you don't have to upload tons of GB and you can play freely without loosing any important checkpoint. Here it is assumed that the code is in `/home/turra/h025_xsections` and the data in `/home/turra/h025_xsection-tmp-data`
3. Configure Docker and run the dev container (instead of Docker you can use a virtual env but you also have to install ROOT, XMLAnabuilder, quickFit and probably other software). The images are on gitlab-registry.cern.ch, so you have to authenticate:

        docker login gitlab-registry.cern.ch

    and the run:

        docker run -it --rm -v/home/turra/h025_xsections:/h025_xsections:Z -v/home/turra/h025_xsection-tmp-data:/data:Z gitlab-registry.cern.ch/lamberti/h025_xsections/dev

    the `-v` options are mounting local directory inside the container. The `:Z` is needed if you are running an operating system with SElinux. The first directory we mount is the one with the code, the second the one with the data.
4. Test everything is working, for example Internet connection (e.g. `ping cern.ch`) and you have write permission in the scratch directory `/data`. Install the code:

        cd /h025_xsections; pip install -e .

5. Try run the first steps
From the main directory `/h025_xsections` run

        ./run_analysis.sh /data

    By default all the cross sections are run.

This is the main interface to run all the steps (it will be remake soon...). Since we already have merged histogram we can start from the option `[3]`. All the output will be saved in `/data`.

## Developing

If instead you want to change the code and to be able to run your latest changes, you can install the code in a development mode, from the main folder:

    pip install -e .

It is always suggested to work in a virtual environment that should be created and setup before the installation. For example, to create it for the first time:

    virtualenv ~/venv_xsection --system-site-packages

The `--system-site-packages` is needed to use ROOT which must be already installed in your system. Then you need to activate it (change according to your shell):

    . ~/venv_xsection/bin/activate

### Using Docker

Using docker is the best option since you already have all the dependencies properly installed.

A dev Docker image with all the needed dependencies is available at https://gitlab.cern.ch/lamberti/h025_xsections/container_registry. It doesn't contain the code so that you can modify it. You can mount it, together with an external folder for data, for example:

    docker run -it --rm -v/home/turra/h025_xsections:/h025_xsections:Z -v/home/turra/cernbox_h025_xsections/v0.6.0:/data:Z gitlab-registry.cern.ch/lamberti/h025_xsections/dev

and the use it (inside the image):

    cd /h025_xsections
    pip install -e .
    ./run_analysis.sh /data

### Using vscode

If you open the repository with vscode it should tell you that a container is available. If you confirm the editor is reopened and you can use the integrated terminal which is running in the docker container. Also other features, as linters are already configured.

## Description of the steps

The analysis can be run automatically, or step by steps. Actually the first steps should be run manually since you have to wait for the results from condor. The background modelization (choice of the background function and computation of the value of the spurious signal are taken from external sources)

* create a catalogue of samples (umxAOD samples)
* loop on all the sample, to create the needed histograms. This can be easily done on condor.
* compute fiducial cross sections for each true-bin
* normalize the sample and merge them (e.g. merge mc16/a/d/e and the production modes)
* compute quantities relative to the yields: expected true-events, expected reco-events, c-factors, migration matrix, ...
* compute signal shape
* run on data, to dump myy for each categories
* make sidebands fit to get the normalization of the background
* create xml needed to create the workspace
* create workspace and Asimov dataset
* run checks on the workspace
* fit Asimov
* plot final results, tables, report


## Dump the histograms

The first task is to compute the yields from the mxAOD or uxAOD samples. The main place for the sample is on eos:

* MxAOD: /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h025
* uxAOD: /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025

Some of the last one are also replicate in Milan (proof.mi.infn.it): `/storage_tmp/atlas/turra/uxAOD/h025`.

If you haven't installed the package you can just do (change the path):

    export PYTHONPATH=$PYTHONPATH:/users2/{USER_NAME}/h025_xsections

To run a single file locally (from the `h025_xsections/root_files_analysis` folder):

    ./Analysis_Run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025/mc16a/NoSys/PowhegPy8EG_NNPDF30_VBFH125.root --mc-sample mc16a --output-name test.root

To run multiple files with condor:

    python condor_run_histograms.py --production-mode bbH --submission-folder sub
    cd sub
    condor_submit submit.condor

In this way we are running on all the bbH sample. If you remove this option you will run on all the samples. If you don't specify the submission-folder one will be create with the present timestamp. There are other options, for example if you run in Milan the input samples should be searched in another location:

    python condor_run_histograms.py --mc mc16a --submission-folder sub_mc16a --base-dir=/storage_tmp/atlas/turra/uxAOD/h025

## Scan the dataset

The second task is to create the data files from the dataset.
Run interactively the code (from the `h025_xsections/scan_dataset` folder)

    ./read_data.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025/data/data1*.root --split-folders

with `--split-folders`, sub-directories are created. All the output will be in the `output` directory.
Merge the files

    python merging.py output
