import pandas as pd
import re
import ROOT
import os
import logging

from h025_xsections.core.utils import iter_collection
from h025_xsections.fit_data.fit import fit_workspace, FitArgs
from h025_xsections.core.definitions import luminosity, get_region_quantities
from h025_xsections.make_workspace.make_xml import create_xmls
from h025_xsections.make_workspace.make_workspace import make_workspace_from_xml
from h025_xsections.make_workspace.hybrid_asimov import patch_workspace_filename
from h025_xsections.core.naming import FileHistoInfo
from h025_xsections.reader import Reader

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def get_mu_values(fn, region: str, quantity: str):
    f = ROOT.TFile.Open(fn)
    fit_result = f.Get("fitResult")
    ROOT.SetOwnership(fit_result, True)
    parameters = fit_result.floatParsFinal()
    results = {}
    r = re.compile(r"mu_xsec_{region}_{quantity}_([0-9]+)".format(region=region, quantity=quantity))
    for parameter in iter_collection(parameters):
        m = r.match(parameter.GetName())
        if not m:
            continue
        ibin = int(m.group(1))
        results[ibin] = [parameter.getVal(), parameter.getError()]
    return pd.DataFrame(results, index=['value', 'error']).sort_index().T


def get_NP_list_from_RooArgSet_selection(ws, name_selection):
    NP_list = []
    RooArgSet_selection = ws.allVars().selectByName(name_selection)
    for item in iter_collection(RooArgSet_selection):
        NP_list.append("%s" % item.GetName())
    return NP_list


def check_signal_bias(basedir: str, region: str, quantity: str, method: str):
    results = {}

    # nominal workspace

    xml_folder = "xml_nominal_%s_%s_%s" % (method, region, quantity)

    create_xmls(
        region,
        quantity,
        basedir,
        os.path.join(basedir, "data"),
        xml_folder,
        unfolding=method,
        blinded=True,
        lumi=luminosity["mcAll"],
        do_fit=False,
        copy_data=True,
        mH_value=125,
    )

    workspace_fn_nominal = make_workspace_from_xml(xml_folder)

    patch_workspace_filename(
        workspace_fn_nominal,
        basedir,
        region,
        quantity,
        dataset_name="AsimovSB_with_signal_template",
    )

    # ttH shape

    tags_histogram_ttH = FileHistoInfo("mcAll", "ttH", "isPass_or_isFiducial", "Nominal", "NOMINAL")
    reader = Reader(basedir)
    reco_normalization = reader.get_nreco(region, quantity)
    logging.info(
        "creating dataset from ttH signal shape with normalization from nominal: %s",
        reco_normalization,
    )

    patch_workspace_filename(
        workspace_fn_nominal,
        basedir,
        region,
        quantity,
        tags_histogram_ttH,
        abs_normalization=reco_normalization.values,
        dataset_name="AsimovSB_with_signal_template_ttHshape",
    )

    # gaussian workspace

    xml_folder = "xml_gaus_%s_%s_%s" % (method, region, quantity)

    create_xmls(
        region,
        quantity,
        basedir,
        os.path.join(basedir, "data"),
        xml_folder,
        unfolding=method,
        blinded=True,
        lumi=luminosity["mcAll"],
        do_fit=False,
        copy_data=True,
        signal_model_type="gaus",
        mH_value=125,
    )

    workspace_fn_gaus = make_workspace_from_xml(xml_folder)
    patch_workspace_filename(workspace_fn_gaus, basedir, region, quantity)

    # get NP list

    f = ROOT.TFile.Open(workspace_fn_nominal)
    ws = f.Get("combWS")

    NP_list = get_NP_list_from_RooArgSet_selection(ws, "ATLAS*")

    prefix_output = "fit_{region}_{quantity}_{method}".format(
        region=region, quantity=quantity, method=method
    )

    # fit nominal workspace, asimov

    output_fname = "%s.statonly.nominal_model.asimov.root" % prefix_output
    fit_args = FitArgs(
        workspace_fn_nominal, "combWS", "AsimovSB", output_fname, None, savews=False, fix_np=NP_list
    )
    fit_workspace(fit_args)
    fitted_mu = get_mu_values(output_fname, region, quantity)
    results[("DSCB", "AsimovSB")] = fitted_mu['value']

    # fit norminal workspace, asimov with signal template

    output_fname = "%s.statonly.nominal_model.hybrid.root" % prefix_output
    fit_args = FitArgs(
        workspace_fn_nominal,
        "combWS",
        "AsimovSB_with_signal_template",
        output_fname,
        None,
        savews=False,
        fix_np=NP_list,
    )
    fit_workspace(fit_args)

    fitted_mu = get_mu_values(output_fname, region, quantity)
    results[("DSCB", "signal_injection")] = fitted_mu['value']

    # fit nominal workspace, asimov with signal template, only ttH
    output_fname = "%s.statonly.nominal_model.hybrid_ttHshape.root" % prefix_output
    fit_args = FitArgs(
        workspace_fn_nominal,
        "combWS",
        "AsimovSB_with_signal_template_ttHshape",
        output_fname,
        None,
        savews=False,
        fix_np=NP_list,
    )
    fit_workspace(fit_args)

    fitted_mu = get_mu_values(output_fname, region, quantity)
    logging.info("fitting mu (DSCB model, injection ttH shape) = %s" % fitted_mu)
    results[("DSCB", "signal_injection_ttH")] = fitted_mu['value']

    # fit gaus workspace, asimov with signal template

    output_fname = "%s.statonly.gaus_model.hybrid.root" % prefix_output
    fit_args = FitArgs(
        workspace_fn_gaus,
        "combWS",
        "AsimovSB_with_signal_template",
        output_fname,
        None,
        savews=False,
        fix_np=NP_list,
    )
    fit_workspace(fit_args)

    fitted_mu = get_mu_values(output_fname, region, quantity)
    results[("gaus", "signal_injection")] = fitted_mu['value']
    results = pd.concat(results, names=["signal_model", "dataset", "bin"]) - 1
    results.to_csv("result.%s.%s.%s.csv" % (region, quantity, method))
    return results


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Check bias due to signal model")
    parser.add_argument("basedir")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--quantities", nargs="*")
    parser.add_argument("--method", choices=["bin_by_bin", "matrix"])
    args = parser.parse_args()

    region_quantities = get_region_quantities(args.regions, args.quantities)

    results = {}
    for region, quantity in region_quantities:
        logging.info("computing bias for %s %s %s", region, quantity, args.method)
        results[(region, quantity)] = check_signal_bias(args.basedir, region, quantity, args.method)
