from glob import glob
import pandas as pd
from h025_xsections.core.latex import Document, Slide, RawLatex, Figure, Graphics
from h025_xsections.core import definitions
from h025_xsections.core.utils import extract_info_from_fitresult
from typing import Dict
import logging
import ROOT

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def make_figures():
    import ROOT

    f = ROOT.TFile.Open("xml_nominal_matrix_Diphoton_pT_yy/XSectionWS_Diphoton_pT_yy.root")
    ws = f.Get("combWS")
    obs = ws.var("atlas_invMass_category_Diphoton_pT_yy_1")
    canvas = ROOT.TCanvas()
    args = ROOT.RooFit.Cut("channellist==channellist::category_Diphoton_pT_yy_1")
    frame = obs.frame()
    ws.data("AsimovSB").plotOn(frame, args)
    ws.data("AsimovSB_with_signal_template").plotOn(frame, args, ROOT.RooFit.MarkerColor(ROOT.kRed))
    ws.data("AsimovSB_with_signal_template_ttHshape").plotOn(frame, args, ROOT.RooFit.MarkerColor(ROOT.kBlue))
    frame.Draw()
    canvas.SaveAs("fig1.pdf")

    frame.SetAxisRange(122, 128)
    frame.SetAxisRange(300, 380, "y")
    frame.Draw()
    canvas.SaveAs("fig2.pdf")


def make_intro(document: Document):
    slide1 = Slide("Introduction")
    slide1.add_content(
        RawLatex(
            r"""
Check the impact of the accuracy of the signal shape model on the fitted results ($\mu$s)\\
\begin{itemize}
    \item for simplicity use only matrix method (1 step)
    \item assume $m_H=\SI{125.00}{\GeV}$ (not 125.09)
    \item fit with stat-only since we want a rigid model of the signal to test the bias (i.e. not changed by resolution/scale systematics)
\end{itemize}
Check three cases\footnote{S here includes all the resonant components}:
\begin{itemize}
    \item fit nominal model (with $m_H=\SI{125.00}{\GeV}$ and Double-Sided-Crystall-Ball) to Asimov S($\mu=1$)+B generate from the pdf
    \item fit nominal model to S+B dataset with S from the MC and B from the Asimov (signal-injection)
    \item fit gaus model (as the nominal one, but signal shape is a simple gaussian) to the signal-injection dataset
\end{itemize}
First case is to check if we have a perfect closure (e.g. no problem in the fit). Second case is to check if the signal-model is accurate enough. Third case is to check how much we are sensitive to the detail description of the signal shape
    """
        )
    )
    document.add_slide(slide1)

    slide2 = Slide("Injections")
    slide2.add_content(
        RawLatex(
            r"""
The signal-injection sample is made from AsimovB (from the background pdf with sidebands parameters), adding the signal from MC samples. All the samples (mc-periods, production modes) are used, with their proper weights
    """
        )
    )
    slide2.add_content(
        Figure(
            [
                Graphics("fig1.png", options=r"width=0.45\textwidth"),
                Graphics("fig2.png", options=r"width=0.45\textwidth"),
            ]
        )
    )
    slide2.add_content(
        RawLatex("Red point are the injection dataset, blue points the same but with only ttH sample, normalized to total signal yield, black points are the usual s+b Asimov")
    )
    document.add_slide(slide2)

    slide4 = Slide("Signal models")
    opt = r"width=0.4\textwidth"
    slide4.add_content(RawLatex("DSCB (nominal) model"))
    slide4.add_content(
        Figure(
            [
                Graphics(
                    "../tmp-results/plots/signal/signal_parameters_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/DSCB/Diphoton/constant/bin_1/fit_Diphoton_constant_bin_1.png",
                    options=opt,
                ),
                Graphics(
                    "../tmp-results/plots/signal/signal_parameters_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/DSCB/Diphoton/pT_yy/bin_19/fit_Diphoton_pT_yy_bin_19.png",
                    options=opt,
                ),
            ]
        )
    )
    slide4.add_content(RawLatex("Gaussian (bad) model"))
    slide4.add_content(
        Figure(
            [
                Graphics(
                    "../tmp-results/plots/signal/signal_parameters_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/gaus/Diphoton/constant/bin_1/fit_Diphoton_constant_bin_1.png",
                    options=opt,
                ),
                Graphics(
                    "../tmp-results/plots/signal/signal_parameters_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/gaus/Diphoton/pT_yy/bin_19/fit_Diphoton_pT_yy_bin_19.png",
                    options=opt,
                ),
            ]
        )
    )

    document.add_slide(slide4)

    slide_results = Slide("Results")
    slide_results.add_content(
        RawLatex(
            r"""
    In the following slides almost all the variables are investigated.
    \begin{itemize}
    \item Nominal model (DSCB) on Asimov: bias much less than 0.1\%
    \item Nominal model (DSCB) on injection (hybrid dataset): usually less than 0.1\%. At least two order of magnitude less than the stat-error. Quite similar in all the binds, larger on low-stat bin. In low stat-bins the injection may be not reliable since the MC sample has non-neglibigle fluctuations
    \item Gaussian model on injection: quite large, around 6-7\%, but smaller than the stat-error
    \end{itemize}
    Conclusions:
    \begin{itemize}
    \item The modellization of the $m_{\gamma\gamma}$ shape in this analysis is very accurate and does not create a significant bias on the results.
    \item In principle we could use a much less accurate model (simple gaussian) and we will still be dominated by stat-error
    \end{itemize}
    """
        )
    )
    document.add_slide(slide_results)


def make_report(results: pd.DataFrame, fn: str):
    document = Document("Signal bias tests", authors="Ruggero Turra")
    make_intro(document)
    g = results.reset_index().groupby(["region", "quantity", "method"])
    for k, v in g:
        region, quantity, method = k
        v = (
            v.set_index(["signal_model", "dataset", "bin"])
            .drop("region", axis=1)
            .drop("quantity", axis=1)
            .drop("method", axis=1)
        )
        values = v["value"].unstack("bin") - 1
        errors = v["error"].unstack("bin")

        values_latex = (values * 100).to_latex(float_format="%.2f")
        errors_latex = (errors * 100).to_latex(float_format="%.2f")

        slide = Slide(title="%s %s %s" % (region, definitions.latex_labels[quantity], method))
        width = "0.8" if len(v) > 4 else "0.5"
        slide.add_content(
            RawLatex(
                r"""
    Bias=$\mu-1$ (\%)
    \begin{table}
    \centering
    \resizebox{<width>\textwidth}{!}{
        <table>
    }
    \end{table}
    """.replace(
                    "<table>", values_latex
                ).replace(
                    "<width>", width
                )
            )
        )
        slide.add_content(
            RawLatex(
                r"""
    \vspace{1em}
    Stat errors (\%):
    \begin{table}
    \centering
    \resizebox{<width>\textwidth}{!}{
        <table>
    }
    \end{table}
    """.replace(
                    "<table>", errors_latex
                ).replace(
                    "<width>", width
                )
            )
        )

        document.add_slide(slide)
    latex = document.render()
    with open(fn, "w") as f:
        f.write(latex)


if __name__ == "__main__":
    all_fns = glob("fit*.root")
    results = {}
    for fn in all_fns:
        f = ROOT.TFile.Open(fn)
        fit_result = f.Get("fitResult")
        ROOT.SetOwnership(fit_result, True)
        fr = {
            int(k.split("_")[-1]): v
            for k, v in extract_info_from_fitresult(fit_result).items()
            if k.startswith("mu_xsec")
        }
        fr = pd.DataFrame(fr, index=["value", "error"]).T
        fr.index.name = "bin"
        suffix = fn.split(".")[0].split("_", 1)[1]
        region = suffix.split("_", 1)[0]
        method = suffix.rsplit("_", 1)[1]
        quantity = suffix.rsplit("_", 1)[0].replace(region, "")[1:]
        dataset = fn.split(".")[3]
        dataset = {
            "hybrid": "S injection",
            "hybrid_ttHshape": "S injection (ttH shape)",
            "asimov": "Asimov S+B",
        }.get(dataset, dataset)
        model = fn.split(".")[2]
        model = {"nominal_model": "DSCB", "gaus_model": "Gaus"}.get(model, model)
        key = region, quantity, method, model, dataset
        results[key] = fr
    results = pd.concat(results, names=["region", "quantity", "method", "signal_model", "dataset"])

    make_figures()
    make_report(results, "prova.tex")
