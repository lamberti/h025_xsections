import pytest

from h025_xsections.core.folders import get_path


class TestFolders:
    def test_folders(self):
        with pytest.raises(ValueError):
            get_path("tmp-results", "workspace", region="Diphoton", quantity="pT_yy")

        assert (
            get_path(
                "tmp-results", "workspace", region="Diphoton", quantity="pT_yy", method="bin_by_bin", blinded=True
            )
            == "tmp-results/workspace/bin_by_bin/Diphoton/pT_yy/blinded/XSectionWS_Diphoton_pT_yy.root"
        )
        assert (
            get_path(
                "tmp-results",
                "workspace_fitted",
                region="Diphoton",
                quantity="pT_yy",
                method="bin_by_bin",
                blinded=True,
                dataset="AsimovSB",
                sys="statonly",
            )
            == "tmp-results/workspace_fitted/bin_by_bin/Diphoton/pT_yy/XSectionWS_Diphoton_pT_yy-fitted-AsimovSB-statonly.root"
        )
