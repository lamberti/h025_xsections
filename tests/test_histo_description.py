from h025_xsections.core.histo_description import (
    HistoDescription,
    histo_description_region_quantity,
)
from h025_xsections.core.predicate import Predicate


class Test(object):
    def test_histo(self):
        h = HistoDescription(
            "pT_yy",
            "reco",
            isFiducial=Predicate(Predicate.ANY),
            isDalitz=Predicate(Predicate.ANY),
            isPassed=Predicate(Predicate.ANY),
        )
        assert h.name() == "pT_yy_reco_ALL"

    def test_from_name(self):
        h = HistoDescription.from_name("histo1D_m_jj_30_true_isFiducialMET_isDalitz")
        assert h.isFiducial.value is True
        assert h.isDalitz.value is True
        assert h.quantity == "m_jj_30"
        assert h.recotrue == "true"

    def test_partial(self):
        HD = histo_description_region_quantity("VBF", "pT_yy")
        h = HD(
            recotrue="reco",
            isFiducial=Predicate(Predicate.ANY),
            isDalitz=Predicate(Predicate.ANY),
            isPassed=Predicate(Predicate.ANY),
        )
        assert h.name() == "pT_yy_reco_ALL"

        h = HD(recotrue="reco", isFiducial=True, isDalitz=Predicate(Predicate.ANY), isPassed=False,)

        assert h.name() == "pT_yy_reco_isFiducialVBF_notPassedVBF"
