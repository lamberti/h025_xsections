from h025_xsections.core.naming import FileHistoInfo, Catalogue


class TestNaming(object):
    def test_FileHistoInfo(self):
        h1 = FileHistoInfo(
            mc="mc16a", prod="prodAll", skim="myskim", sysgroup="mysysgroup", sys="mysys"
        )
        h2 = FileHistoInfo(
            mc="mc16a", prod="prodAll", skim="myskim", sysgroup="mysysgroup", sys="mysys"
        )
        h3 = FileHistoInfo(
            mc="mc16a", prod="prodAll", skim="myskim", sysgroup="mysysgroup", sys="mysys1"
        )
        assert h1 == h2
        assert h1 == h1
        assert h3 == h3
        assert h1 != h3
        assert h2 != h3

        l = [h1, h2]
        assert h1 in l
        assert h2 in l
        assert h3 not in l

    def test_catalogue(self):
        files = [
            "histo_mc16a.ggZH.isPass_or_isFiducial.Nominal.NOMINAL.name.root",
            "histo_mc16a.ggH.isPass_or_isFiducial.Nominal.NOMINAL.name.root",
            "histo_mc16d.ggZH.isPass_or_isFiducial.Nominal.NOMINAL.name.root",
            "histo_mc16d.ggH.isPass_or_isFiducial.Nominal.NOMINAL.name.root",
        ]

        catalogue = Catalogue.create_catalogue(files)
        assert len(catalogue) == 4
        assert set(catalogue.all_mc()) == set(["mc16a", "mc16d"])
        assert set(catalogue.all_production()) == set(["ggH", "ggZH"])
        catalogue_mc16a = catalogue.sub_catalogue(mc="mc16a")
        assert len(catalogue_mc16a) == 2
        assert catalogue_mc16a.common_tags() == FileHistoInfo(
            mc="mc16a", prod=None, skim="isPass_or_isFiducial", sysgroup="Nominal", sys="NOMINAL"
        )
        catalogue_ggH = catalogue.sub_catalogue(prod="ggH")
        assert len(catalogue_ggH) == 2
        assert catalogue_ggH.common_tags() == FileHistoInfo(
            mc=None, prod="ggH", skim="isPass_or_isFiducial", sysgroup="Nominal", sys="NOMINAL"
        )

        assert catalogue.missings(mc=["mc16a", "mc16d", "mc16e"])
        assert not catalogue.missings(mc=["mc16a", "mc16d"])
