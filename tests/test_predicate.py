from h025_xsections.core.predicate import Predicate, PredicateSet


class Test(object):
    def test_predicate(self):
        Predicate(True, "name")

        assert Predicate(True, "name").value is True
        assert Predicate(Predicate.ANY).value == Predicate.ANY
        assert (Predicate(True, "name") & Predicate(False, "name")).value is False
        assert (Predicate(True, "name") & Predicate(Predicate.ANY)).value is True
        assert Predicate(True, "isFiducial") != Predicate(True, "isDalitz")
        assert Predicate(True, "isFiducial") == Predicate(True, "isFiducial")
        assert hash(Predicate(True, "isFiducial")) == hash(Predicate(True, "isFiducial"))
        assert Predicate(True, "name").is_any() is False
        assert Predicate(Predicate.ANY, "isFiducial1") == Predicate(Predicate.ANY, "isFiducial2")
        assert hash(Predicate(Predicate.ANY, "isFiducial1")) == hash(Predicate(Predicate.ANY, "isFiducial2"))
        assert Predicate(Predicate.ANY, "isFiducial1") != Predicate(True, "isFiducial2")

    def test_predicate_set(self):

        assert PredicateSet(isFiducial=Predicate(Predicate.ANY)) == PredicateSet(
            isFiducial=Predicate(Predicate.ANY)
        )
        assert PredicateSet(isFiducial=Predicate(Predicate.ANY)) == PredicateSet(
            isPassed=Predicate(Predicate.ANY)
        )
        assert PredicateSet(isFiducial=Predicate(Predicate.ANY)) == PredicateSet()
        assert PredicateSet(isFiducial=Predicate(Predicate.ANY, "isFiducial")) == PredicateSet()
        assert PredicateSet(isFiducial=Predicate(Predicate.ANY, "isFiducial")) == PredicateSet(
            isFiducial=Predicate(Predicate.ANY, "isPassed")
        )
        assert PredicateSet(isFiducial=Predicate(True, "isFiducial")) != PredicateSet(
            isFiducial=Predicate(True, "isPassed")
        )
        assert PredicateSet(
            isFiducial=Predicate(True, "isFiducial"), isPassed=Predicate(Predicate.ANY)
        ) != PredicateSet(isFiducial=Predicate(True, "isPassed"), isPassed=Predicate(Predicate.ANY))

        assert hash(PredicateSet(isFiducial=Predicate(Predicate.ANY))) == hash(
            PredicateSet(isFiducial=Predicate(Predicate.ANY))
        )
        assert hash(PredicateSet(isFiducial=Predicate(Predicate.ANY))) == hash(
            PredicateSet(isPassed=Predicate(Predicate.ANY))
        )
        assert hash(PredicateSet(isFiducial=Predicate(Predicate.ANY))) == hash(PredicateSet())
        assert hash(PredicateSet(isFiducial=Predicate(Predicate.ANY, "isFiducial"))) == hash(PredicateSet())
        assert hash(PredicateSet(isFiducial=Predicate(Predicate.ANY, "isFiducial"))) == hash(
            PredicateSet(isFiducial=Predicate(Predicate.ANY, "isPassed"))
        )
        assert hash(PredicateSet(isFiducial=Predicate(True, "isFiducial"))) != hash(
            PredicateSet(isFiducial=Predicate(True, "isPassed"))
        )
        assert hash(
            PredicateSet(isFiducial=Predicate(True, "isFiducial"), isPassed=Predicate(Predicate.ANY))
        ) != hash(PredicateSet(isFiducial=Predicate(True, "isPassed"), isPassed=Predicate(Predicate.ANY)))

    def test_predicate_set_and(self):
        is_fiducial1_any = PredicateSet(isFiducial=Predicate(Predicate.ANY, "isFiducial1"))
        is_fiducial2_any = PredicateSet(isFiducial=Predicate(Predicate.ANY, "isFiducial2"))
        r = is_fiducial1_any & is_fiducial2_any
        assert r == PredicateSet()
        assert hash(r) == hash(PredicateSet())

        is_fiducial1_true = PredicateSet(isFiducial=Predicate(True, "isFiducial1", formula="isFiducial1"))
        r = is_fiducial1_any & is_fiducial1_true
        assert r == PredicateSet(isFiducial=Predicate(True, "isFiducial1", "isFiducial1"))
        assert hash(r) == hash(PredicateSet(isFiducial=Predicate(True, "isFiducial1", "isFiducial1")))

        is_passed1_true = PredicateSet(isPassed=Predicate(True, "isPassed1", "isPassed1"))
        r = is_fiducial1_any & is_passed1_true
        assert r == is_passed1_true
        assert hash(r) == hash(is_passed1_true)

        p1 = PredicateSet(
            isFiducial=Predicate(True, name="FiducialDiphoton", formula="isFiducialDiphoton"),
            isDalitz=Predicate(False, name="Dalitz", formula="isDalitz"),
            isPassed=Predicate(True, name="PassedDiphoton", formula="isPassedDiphoton"),
        )

        p_dalitz = PredicateSet(isDalitz=Predicate(True, name="Dalitz", formula="isDalitz"))

        p2 = PredicateSet(
            isFiducial=Predicate(True, name="FiducialDiphoton", formula="isFiducialDiphoton"),
            isPassed=Predicate(True, name="PassedDiphoton", formula="isPassedDiphoton"),
        ) & (~p_dalitz)

        assert p1 == p2
        assert hash(p1) == hash(p2)

    def test_predicate_name(self):
        p = PredicateSet(isFiducial=Predicate(True, "FiducialMET"))
        assert PredicateSet.from_name(p.name()) == p

        p = PredicateSet(isFiducial=Predicate(False, "FiducialDiphoton"))
        assert PredicateSet.from_name(p.name()) == p

        p = PredicateSet(isFiducial=Predicate(True, "FiducialMET"), isPassed=Predicate(False, "PassedMET"))
        assert PredicateSet.from_name(p.name()) == p

        p = PredicateSet()
        assert PredicateSet.from_name(p.name()) == p

        p = PredicateSet(isFiducial=Predicate(True, "FiducialMET"), isPassed=Predicate(False, "PassedMET"), isDalitz=Predicate(True, 'Dalitz'))
        assert PredicateSet.from_name(p.name()) == p