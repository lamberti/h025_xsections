import os

import numpy as np
import pytest
from h025_xsections.reader import Reader
from h025_xsections.reader.LPHE import Reader as Reader_LPHE


class TestMilano(object):
    def test_cfactors(self):
        reader = Reader("tests/data/")
        df = reader.get_cfactors("Diphoton", "N_j_30")
        assert df.shape == (5,)
        assert np.isnan(df[0])

    def test_matrix(self):
        reader = Reader("tests/data/")
        df = reader.get_matrix("Diphoton", "N_j_30")
        assert df.shape == (7, 5)

    def test_xs(self):
        reader = Reader("tests/data/")
        df = reader.get_xsections_fiducial("Diphoton", "N_j_30")

        assert df.shape == (5,)
        assert df.min() == 0
        assert df[0] == 0

        df_ggH = reader.get_xsections_fiducial("Diphoton", "N_j_30", prod="ggH")
        assert df_ggH.shape == (5,)
        assert df_ggH.min() == 0
        assert df_ggH[0] == 0

        assert np.all(df.values >= df_ggH.values)
        assert df[1] > df_ggH[1]

        df_all = reader.get_xsections_fiducial("Diphoton", "N_j_30", prod=None)
        assert df_all.shape == (8, 5)

    def test_signal_pars(self):
        reader = Reader("tests/data")
        df = reader.get_signal_pars("Diphoton", "pT_yy")
        assert df.shape == (6, 22)

    def test_bkg_pars(self):
        reader = Reader("tests/data")
        df = reader.get_background_prefit_pars("Diphoton", "N_j_30", 1)
        assert len(df) == 3

    #def test_systematic_list(self):
    #    reader = Reader("tmp-results")
    #    assert reader.get_systematics()

    #def test_get_cfactors_sys(self):
    #    reader = Reader("tmp-results")
    #    sys = reader.get_cfactors_sys("Diphoton", "N_j_30")
    #    assert sys.shape[1] == 3 + 2



dir_LPHE = "/home/turra/FullRun2CrossSections"

@pytest.mark.skipif(not os.path.isdir(dir_LPHE), reason="folder %s does not exist" % dir_LPHE)
class TestLPHE(object):

    @pytest.mark.skip
    def test_xs_fiducial(self):
        reader = Reader_LPHE(dir_LPHE)
        xs = reader.get_xsections_fiducial("Diphoton", "N_j_30")
        assert xs.shape == (5,)

    def test_pes(self):
        reader = Reader_LPHE(dir_LPHE)
        pes = reader.get_pes("Diphoton", "N_j_30")
        assert pes.shape[1] == 5

        pes = reader.get_pes("Diphoton", "constant")
        assert pes.shape[1] == 3

        pes = reader.get_pes("VBF", "constant")
        assert pes.shape[1] == 4

    def test_per(self):
        reader = Reader_LPHE(dir_LPHE)
        per = reader.get_per("Diphoton", "N_j_30")
        assert per.shape[1] == 5

        per = reader.get_per("Diphoton", "constant")
        assert per.shape[1] == 3

        per = reader.get_per("VBF", "constant")
        assert per.shape[1] == 4

    def test_results_cfactor_errors(self):
        reader = Reader_LPHE(dir_LPHE)
        cfactors_errors = reader.get_result_cfactors_errors("Diphoton", "N_j_30")
        assert cfactors_errors.shape[1] == 5
