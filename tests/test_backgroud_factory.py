from h025_xsections.core.background import background_factory, VALID_BACKGROUND_FUNCTIONS

# pylint: disable=no-self-use


class TestBackgroundFactory:
    def test_general(self):
        examples = """
Exponential -> EXPR::myname('exp(@0 * @1)', myobs, mypar_1[0.000000,-20.000000,20.000000])
Pow         -> EXPR::myname('pow(@0/100., @1)', myobs, mypar_1[0.000000,-10.000000,0.000000])
ExpPoly2    -> EXPR::myname('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', myobs, mypar_1[-5.000000,-10.000000,5.000000], mypar_2[2.000000,0.001000,6.000000])
ExpPoly3    -> EXPR::myname('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100 + @3*(@0 - 100)/100*(@0 - 100)/100))', myobs, mypar_1[-5.000000,-10.000000,5.000000], mypar_2[2.000000,0.001000,6.000000], mypar_3[2.000000,0.001000,6.000000])
Bern3       -> RooBernsteinM::myname(myobs, {1, mypar_1[0.600000,-10.000000,10.000000], mypar_2[0.600000,-10.000000,10.000000], mypar_3[0.600000,-10.000000,10.000000]})
Bern4       -> RooBernsteinM::myname(myobs, {1, mypar_1[0.600000,-10.000000,10.000000], mypar_2[0.600000,-10.000000,10.000000], mypar_3[0.600000,-10.000000,10.000000], mypar_4[0.600000,-10.000000,10.000000]})
Bern5       -> RooBernsteinM::myname(myobs, {1, mypar_1[0.600000,-10.000000,10.000000], mypar_2[0.600000,-10.000000,10.000000], mypar_3[0.600000,-10.000000,10.000000], mypar_4[0.600000,-10.000000,10.000000], mypar_5[0.600000,-10.000000,10.000000]})
"""

        queries_results = {}
        for line in examples.split("\n"):
            if not line:
                continue
            functional_form, test_expression = line.split("->")
            queries_results[functional_form.strip()] = test_expression.strip()

        assert len(queries_results) == len(VALID_BACKGROUND_FUNCTIONS)

        for functional_form, result in queries_results.items():
            assert background_factory("myname", functional_form, "myobs", "mypar") == result

    def test_arguments(self):
        r = background_factory(
            "myname", "Exponential", "myobs", "mypar", par_values=[[0.0, 0.1, 0.2]]
        )
        assert "mypar_1[0.000000,0.100000,0.200000]" in r
