# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Physics changes

- Asymmetric uncertainty on yield and resolution
- Symmetric uncertainty on scale, but averaged between up and down
- Fit shape on systematic variation fixing all the parameters, except sigma_CB and mu_CB to nominal
- Update binnings, add pT_yy with jet veto
- Added spurious signal
- Fit from systematics use nominal parameters fixed except for sigma_CB and mu_CB

### Added

- Usage of a common Reader to read all the saved parameters
- Usage of a centralized folder naming
- New interface run.py with Fire
- Estimate impact of systematic by group of systematics
- using new cppyy ROOT to numpy
- better condor submission, proxy working
- condor submission auto-configure memory and time request

## [0.5.1]

### Physics changes

- Systematics on shape and yield. Only experimental systematics, no spurious signal
- Include VBF, previously missed
- Background parameters from the prefit

### Added

- More check on the file produced, to be sure we have everything
- Memory option for condor
- Factorized naming

### Fix

- Include VBF, previously missed

## [0.4.0]

### Physics changes

- Double differential cross sections, only the regular ones
- Correct final weights for jets and other objects
- Background parameters to build Asimov are from a prefit, done category by category. Only for the bin by bin by now.

### Added

- Changelog file
- Docker files, one for the full code, one (dev) only with dependencies
- vscode devcontainer
- Continuos integration using docker images
- Continuos integration producing histograms from an example uxAOD and compare with a reference
- Latex labels for bins now included by default the under/over-flows

### Fix

- Limit machines to Proof when running in Milan because of problems about cvmfs

## [0.0.3]

### Physics changes

- Other fiducial regions
- Compute chi2 wrt background-only
- Compute number of conditioning

### Added

- Full automatic pdf report
- Automatic tables
- First unit test with tox

[Unreleased]: https://gitlab.cern.ch/lamberti/h025_xsections/-/compare/v0.5.1...master
[0.5.1]: https://gitlab.cern.ch/lamberti/h025_xsections/-/compare/v0.4.0...v0.5.1
[0.4.0]: https://gitlab.cern.ch/lamberti/h025_xsections/-/compare/v0.0.3...v0.4.0
[0.0.3]: https://gitlab.cern.ch/lamberti/h025_xsections/-/compare/v0.0.2...v0.0.3
