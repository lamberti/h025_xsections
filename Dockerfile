FROM gitlab-registry.cern.ch/lamberti/h025_xsections/dev

COPY . /h025_xsection
RUN cd /h025_xsection && pip install .

# Set up the ATLAS user, and give it super user rights.
#RUN echo '%wheel        ALL=(ALL)       NOPASSWD: ALL' >> /etc/sudoers && \
RUN useradd -ms /bin/bash atlas && chmod 755 /home/atlas


COPY prompt.sh /etc/profile.d/
COPY motd /etc/
#USER atlas

WORKDIR /h025_xsection
ENTRYPOINT [ "sh", "-c" ]
CMD ["/bin/cat /etc/motd && /bin/bash"]
