#!/usr/bin/env python

import logging
import os
from typing import Callable, List
from time import time

import fire

from h025_xsections.core import definitions, folders
from h025_xsections.reader import Reader
from h025_xsections.core.utils import create_dir_if_not_exists

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


DIR_MERGED = "output_histo_norm_merged"
DIR_SIGNAL = "signal_parameters"
DIR_DATA = "data"
DIR_BKG_PARAMETERS = "bkg_prefit_parameters"


def loop(regions: List[str], quantities: List[str], script: Callable, do_raise: bool = True):
    regions_quantities = definitions.get_region_quantities(regions, quantities)
    errors = {}
    for region, quantity in regions_quantities:
        try:
            start = time()
            err = script(region, quantity)
            delta = time() - start
            errors[(region, quantity)] = err
            if delta > 0.2:
                logging.info("elapsed time: %.2f s", delta)
        except Exception as ex:
            logging.error("exception raised for %s %s" % (region, quantity))
            if do_raise:
                raise ex
        if any(errors.values()):
            logging.warning("there have been errors")
            for k, err in errors.items():
                if err is not None:
                    logging.info('%s %s: %s' % (k[0], k[1], err))
        else:
            logging.info("no errors")


class Analysis(object):
    def __init__(self, basedir=None, regions=None, quantities=None, debug=False):
        self.basedir = basedir
        self.regions = regions
        self.quantities = quantities
        self.debug = debug

    #        if self.basedir is None:
    #            raise ValueError("--basedir should not be none")

    def signal_model_nominal(self, do_plots=True, do_resolution=True, signal_model_type='DSCB'):
        """run the signal shape model for the nominal case"""
        from h025_xsections.make_signal.make_signal_all_sys import make_signal_all_sys

        def script(region, quantity):
            make_signal_all_sys(
                os.path.join(self.basedir, DIR_MERGED),
                os.path.join(self.basedir, DIR_SIGNAL),
                folders.get_path(self.basedir, "signal_plot"),
                region,
                quantity,
                False,
                do_plots,
                do_resolution,
                None,
                self.debug,
                signal_model_type=signal_model_type
            )

        loop(self.regions, self.quantities, script)

    def signal_model_systematic(self, do_plots=False):
        """run the signal shape model for the systematic case, fixing most of the parameters to the nominal ones"""
        from h025_xsections.make_signal.make_signal_all_sys import make_signal_all_sys

        def script(region, quantity):
            make_signal_all_sys(
                os.path.join(self.basedir, DIR_MERGED),
                os.path.join(self.basedir, DIR_SIGNAL),
                folders.get_path(self.basedir, "signal_plot"),
                region,
                quantity,
                True,
                do_plots,
                False,
                os.path.join(self.basedir),
                self.debug,
            )

        loop(self.regions, self.quantities, script)

    def cfactor_sys(self):
        from h025_xsections.efficiency.compute_systematics import compute_sys_cfactor

        reader = Reader(self.basedir)
        yield_variations = reader.get_variations(only_yield=True)
        output_dir_plot_cfactors = folders.get_path(self.basedir, "cfactor_sys_plot_folder")

        def script(region, quantity):
            output_directory = folders.get_path(
                self.basedir, "cfactor_sys_folder", region=region, quantity=quantity
            )
            compute_sys_cfactor(
                reader,
                output_directory,
                yield_variations,
                region,
                quantity,
                output_dir_plot_cfactors,
            )

        loop(self.regions, self.quantities, script)

    def matrix_sys(self):
        from h025_xsections.efficiency.compute_systematics import compute_sys_matrix

        reader = Reader(self.basedir)
        yield_variations = reader.get_variations(only_yield=True)

        def script(region, quantity):
            output_directory = folders.get_path(
                self.basedir, "matrix_sys_folder", region=region, quantity=quantity
            )
            compute_sys_matrix(reader, output_directory, yield_variations, region, quantity)

        loop(self.regions, self.quantities, script)

    def background_prefit(self, blinded=False):
        from h025_xsections.bkg_prefit import loop as loop_bkg_prefit

        blinded_str = "blinded" if blinded else "unblinded"
        input_dir = folders.get_path(self.basedir, "data_folder", blinded=blinded_str)
        output_dir = folders.get_path(self.basedir, "bkg_prefit_parameters_folder")
        def script(region, quantity):
            loop_bkg_prefit(input_dir, output_dir, [region], [quantity], verbose=self.debug)

        loop(self.regions, self.quantities, script)

    def make_xml(
        self, unfolding, blinded, do_fit=False, lumi=definitions.luminosity["mcAll"], copy_data=True
    ):
        from h025_xsections.make_workspace.make_xml import create_xmls

        def script(region, quantity):
            output_directory = folders.get_path(
                self.basedir,
                "xml_folder",
                region=region,
                quantity=quantity,
                method=unfolding,
                blinded=blinded,
            )
            create_xmls(
                region,
                quantity,
                self.basedir,
                os.path.join(self.basedir, DIR_DATA),
                output_directory,
                unfolding,
                blinded,
                lumi,
                do_fit,
                copy_data,
            )

        loop(self.regions, self.quantities, script)

    def make_workspace(self, unfolding, blinded):
        from h025_xsections.make_workspace.make_workspace import make_workspace_from_xml
        from h025_xsections.make_workspace.hybrid_asimov import patch_workspace

        def script(region, quantity):
            input_directory = folders.get_path(
                self.basedir,
                "xml_folder",
                region=region,
                quantity=quantity,
                method=unfolding,
                blinded=blinded,
            )
            make_workspace_from_xml(input_directory)
            patch_workspace(self.basedir, region, quantity, unfolding, blinded)

        loop(self.regions, self.quantities, script)

    def plot_workspace(self, unfolding, dataset="AsimovSB", blinded=False, nbins=220):
        from h025_xsections.plot_workspace import plot_workspace_from_file

        def script(region, quantity):
            blinded_str = "blinded" if blinded else "unblinded"
            args = dict(
                region=region,
                quantity=quantity,
                method=unfolding,
                blinded=blinded_str,
            )
            fn_ws = folders.get_path(self.basedir, "workspace", **args)
            output_dir = folders.get_path(
                self.basedir, "workspace_plots_folder", dataset=dataset, **args
            )
            plot_workspace_from_file(fn_ws, "combWS", dataset, nbins, output_dir)

        loop(self.regions, self.quantities, script)

    def fit_nominal(self, unfolding, dataset="AsimovSB", blinded=False, execution="direct"):
        self.fit(
            unfolding,
            dataset,
            blinded,
            execution,
            do_stat_and_full=True,
            do_sys_loop=False,
            do_sysgroup_loop=False,
        )

    def fit_sysgroup(self, unfolding, dataset="AsimovSB", blinded=False, execution="direct"):
        self.fit(
            unfolding,
            dataset,
            blinded,
            execution,
            do_stat_and_full=False,
            do_sys_loop=False,
            do_sysgroup_loop=True,
        )

    def fit(
        self,
        unfolding,
        dataset="AsimovSB",
        blinded=False,
        execution="direct",
        do_stat_and_full=True,
        do_sys_loop=False,
        do_sysgroup_loop=False,
    ):
        from h025_xsections.fit_data.run_quickFit_all_sys import loop_sys

        def script(region, quantity):
            ws_fn = folders.get_path(
                self.basedir,
                "workspace",
                region=region,
                quantity=quantity,
                method=unfolding,
                dataset=dataset,
                blinded=blinded,
            )
            output_dir = folders.get_path(
                self.basedir,
                "workspace_fitted_folder",
                region=region,
                quantity=quantity,
                method=unfolding,
            )
            logging.info("Fitting %s output in %s", ws_fn, output_dir)

            return loop_sys(
                ws_fn,
                "combWS",
                dataset,
                output_dir,
                do_stat_and_full=do_stat_and_full,
                do_sys_loop=do_sys_loop,
                do_sysgroup_loop=do_sysgroup_loop,
                execution=execution,
            )

        loop(self.regions, self.quantities, script)

    def plot_results(self, unfolding, blinded=True, dataset="AsimovSB"):
        from h025_xsections.plot_results import plot_results as pr

        reader = Reader(self.basedir)

        def script(region, quantity):
            common_args = dict(region=region, quantity=quantity, method=unfolding, dataset=dataset)

            ws_path_statonly = folders.get_path(
                self.basedir, "workspace_fitted", sys="statonly", **common_args
            )

            ws_path_fullfit = folders.get_path(
                self.basedir, "workspace_fitted", sys="full", **common_args
            )

            output_directory_plots = folders.get_path(
                self.basedir, "final_plots_folder", **common_args
            )
            output_directory = folders.get_path(self.basedir, "results", **common_args)

            create_dir_if_not_exists(output_directory)
            create_dir_if_not_exists(output_directory_plots)

            pr(
                ws_path_statonly,
                ws_path_fullfit,
                reader,
                region,
                quantity,
                unfolding,
                dataset,
                output_directory,
                output_directory_plots,
            )

        return loop(self.regions, self.quantities, script)

    def compare_fit_sys(self, unfolding, dataset="AsimovSB"):
        from h025_xsections.fit_data.compare_fit_sys import compare

        def script(region, quantity):
            args = dict(method=unfolding, region=region, quantity=quantity, dataset=dataset)
            input_dir = folders.get_path(
                self.basedir,
                "workspace_fitted_folder",
                method=unfolding,
                region=region,
                quantity=quantity,
            )
            output_dir = folders.get_path(self.basedir, "results", **args)
            output_dir_plot = folders.get_path(self.basedir, "final_plots_folder", **args)
            compare(region, quantity, unfolding, input_dir, output_dir, output_dir_plot, dataset)

        loop(self.regions, self.quantities, script)

    def check_workspace(self, unfolding, ws_name="combWS", dataset="AsimovSB"):
        from h025_xsections.make_workspace.check_workspace import check_workspace

        reader = Reader(self.basedir)

        def script(region, quantity):
            fn_workspace = folders.get_path(
                self.basedir,
                "workspace_fitted",
                region=region,
                quantity=quantity,
                method=unfolding,
                sys="full",
                dataset=dataset,
            )
            output_plot = folders.get_path(
                self.basedir,
                "check_workspace_plot",
                region=region,
                quantity=quantity,
                method=unfolding,
                dataset=dataset,
            )
            check_workspace(fn_workspace, region, quantity, unfolding, reader, output_plot, ws_name)

        loop(self.regions, self.quantities, script)


if __name__ == "__main__":
    fire.Fire(Analysis)
