import setuptools
import os.path


def get_version():
    g = {}
    exec(open(os.path.join("h025_xsections", "version.py")).read(), g)
    return g["__version__"]


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="h025_xsections",
    version=get_version(),
    author="Mario Lamberti, Ruggero Turra",
    author_email="mario.lamberti@studenti.unimi.it, ruggero.turra@cern.ch",
    description="ATLAS Analysis for differential cross section based on h025 HGam sample",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/lamberti/h025_xsections",
    packages=setuptools.find_packages(),
    install_requires=["numpy", "matplotlib", "seaborn", "pandas", "lxml", "pyyaml", "fire"],
    test_suite="tests",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 2",
        "Operating System :: OS Independent",
    ],
)
