import logging
import os

import numpy as np
from matplotlib import pyplot as plt

from h025_xsections.core import definitions
from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.histo_utils import plot_stack
from h025_xsections.core.utils import create_dir_if_not_exists
from h025_xsections.reader import Reader

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def plot_xs(reader, region, quantity, output_folder):
    xsections = reader.get_xsections_fiducial(region, quantity, prod=None)
    xsections = xsections.reindex(
        [prod for prod in definitions.production_modes if prod in xsections.index]
    )

    binning = definitions.binnings[quantity]
    if isinstance(binning, tuple) and len(binning) == 3:
        binning = np.linspace(binning[1], binning[2], binning[0] + 1)
    else:
        binning = np.array(binning)

    fig = plot_stack(
        binning,
        xsections.values,
        xsections.index,
        bin_labels_with_extraflow=definitions.bin_labels[quantity],
        latex_label=definitions.latex_labels[quantity],
        show_underflow=definitions.show_underflows[quantity],
        show_overflow=definitions.show_overflows[quantity],
        do_normalization=definitions.do_normalizations[quantity],
        do_log_x=definitions.do_logs_x[quantity],
        stacked=True,
        ylabel=r"$\sigma$",
    )
    fig.axes[0].set_title(
        "region: {region} quantity: {quantity}".format(
            region=region, quantity=definitions.latex_labels[quantity]
        )
    )

    fig.savefig(
        os.path.join(
            output_folder,
            "xsection_{region}_{quantity}.pdf".format(region=region, quantity=quantity),
        ),
        bbox_inches="tight",
    )
    plt.close(fig)

    fig = plot_stack(
        binning,
        (xsections / xsections.sum(axis=0)).fillna(0).values,
        xsections.index,
        bin_labels_with_extraflow=definitions.bin_labels[quantity],
        latex_label=definitions.latex_labels[quantity],
        show_underflow=definitions.show_underflows[quantity],
        show_overflow=definitions.show_overflows[quantity],
        do_normalization=False,
        do_log_x=definitions.do_logs_x[quantity],
        stacked=True,
        ylabel="fraction",
    )
    fig.axes[0].set_ylim(0, 1)
    fig.axes[0].set_title(
        "region: {region} quantity: {quantity}".format(
            region=region, quantity=definitions.latex_labels[quantity]
        )
    )

    fig.savefig(
        os.path.join(
            output_folder,
            "fraction_{region}_{quantity}.pdf".format(region=region, quantity=quantity),
        ),
        bbox_inches="tight",
    )
    plt.close(fig)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("data_folder", help="example: /home/turra/cernbox_h025_xsections/v0.6.0")
    parser.add_argument("output_folder", default="compare_plots")
    parser.add_argument("--prod-mode")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--quantities", nargs="*")

    args = parser.parse_args()

    create_dir_if_not_exists(args.output_folder)

    regions_quantities_analysis = get_region_quantities(args.regions, args.quantities)

    reader = Reader(args.data_folder)

    for region, quantity in regions_quantities_analysis:
        logging.info("plotting {region} {quantity}".format(region=region, quantity=quantity))
        plot_xs(reader, region, quantity, args.output_folder)
