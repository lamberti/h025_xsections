import logging
import os

from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np

from h025_xsections.core import definitions
from h025_xsections.reader import Reader
from h025_xsections.reader.LPHE import Reader as Reader_LPHE
from h025_xsections.core.utils import create_dir_if_not_exists

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def compare_signal(region, quantity, output_dir):
    LPHE = data_LPHE.get_signal_pars(region=region, quantity=quantity, parameter="sigma_offset")
    milano = data_Milano.get_signal_pars(region=region, quantity=quantity, parameter="sigma_cb")

    if not LPHE.index.equals(milano.index):
        logging.error("index are different for %s %s when looking at the signal", region, quantity)

    fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True)
    ax1.step(milano.index, milano, where="mid", label="Milano")
    ax1.plot(LPHE.index, LPHE, label="LPHE", marker="o", ls="", ms=4)
    ax1.set_ylabel(r"$\sigma_{CB}$ [GeV]")
    ax1.set_title("%s %s" % (region, definitions.latex_labels[quantity]))
    ax1.legend(loc=0)

    ratio = LPHE / milano - 1
    ax2.step(ratio.index, ratio * 100, where="mid")
    ax2.set_ylabel("ratio to Milano [%]")

    nbins = max(len(LPHE.index), len(milano.index))
    all_xlabels = definitions.bin_labels[quantity]
    all_xlabels = [all_xlabels[i] for i in milano.index]
    ax1.set_xticks(range(0, nbins))
    ax2.set_xticks(range(0, nbins))
    ax2.set_xticklabels(all_xlabels, rotation=90)

    fig.savefig(
        os.path.join(output_dir, "compare_sigmaCB_%s_%s.pdf" % (region, quantity)),
        bbox_inches="tight",
    )
    plt.close(fig)

    LPHE = data_LPHE.get_signal_pars(region=region, quantity=quantity, parameter="mu_offset")
    milano = data_Milano.get_signal_pars(region=region, quantity=quantity, parameter="mu_cb") - 125

    fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True)
    ax1.step(milano.index, milano, where="mid", label="Milano")
    ax1.plot(LPHE.index, LPHE, label="LPHE", marker="o", ls="", ms=4)
    ax1.set_ylabel(r"$\mu_{CB} - 125.00$ [GeV]")
    ax1.set_title("%s %s" % (region, definitions.latex_labels[quantity]))
    ax1.legend(loc=0)

    ratio = LPHE / milano - 1
    ax2.step(ratio.index, ratio * 100, where="mid")
    ax2.set_ylabel("ratio to Milano [%]")

    nbins = max(len(LPHE.index), len(milano.index))
    ax1.set_xticks(range(0, nbins))
    ax2.set_xticks(range(0, nbins))
    ax2.set_xticklabels(all_xlabels, rotation=90)
    fig.savefig(
        os.path.join(output_dir, "compare_muCB_%s_%s.pdf" % (region, quantity)), bbox_inches="tight"
    )
    plt.close(fig)


def compare_pes(region, quantity, output_dir):
    LPHE = data_LPHE.get_pes(region=region, quantity=quantity, direction="up")
    milano = data_Milano.get_pes(region=region, quantity=quantity, direction="up")

    common_sys = sorted(set(LPHE.index.tolist() + milano.index.tolist()))

    LPHE = LPHE.reindex(common_sys)
    milano = milano.reindex(common_sys)
    ratio = LPHE / milano - 1

    idx_small = ((LPHE.abs() < 1e-4) & (milano.abs() < 1e-4)).fillna(False)
    ratio[idx_small] = np.nan

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(15, 0.5 * milano.shape[1] + 5))
    sns.heatmap(
        milano.T * 100,
        center=0,
        ax=ax1,
        vmin=-0.25,
        vmax=0.25,
        square=False,
        xticklabels=["%2d" % d for d in range(1, milano.shape[0] + 1)],
    )

    ax1.set_title(
        "Milano | Systematic on %s [%%] | region=%s quantity=%s"
        % ("PES up", region, definitions.latex_labels[quantity])
    )
    ax1.set_xlabel("")

    sns.heatmap(
        LPHE.T * 100,
        center=0,
        ax=ax2,
        vmin=-0.25,
        vmax=0.25,
        square=False,
        xticklabels=["%2d" % d for d in range(1, LPHE.shape[0] + 1)],
    )
    ax2.set_title(
        "LPHE | Systematic on %s [%%] | region=%s quantity=%s"
        % ("PES up", region, definitions.latex_labels[quantity])
    )
    ax2.set_xlabel("")

    sns.heatmap(
        ratio.T * 100,
        center=0,
        vmin=-10,
        vmax=10,
        ax=ax3,
        square=False,
        xticklabels=["%2d. %s" % (i, s) for (i, s) in enumerate(milano.index, 1)],
    )
    ax3.set_title(
        "relative difference [%%] | Systematic on %s | region=%s quantity=%s"
        % ("PES up", region, definitions.latex_labels[quantity])
    )

    fig.tight_layout()
    fig.savefig(
        os.path.join(output_dir, "compare_PES_%s_%s.pdf" % (region, quantity)), bbox_inches="tight"
    )
    plt.close(fig)


def compare_per(region, quantity, output_dir):
    LPHE = data_LPHE.get_per(region=region, quantity=quantity, direction="up")
    milano = data_Milano.get_per(region=region, quantity=quantity, direction="up")

    common_sys = sorted(set(LPHE.index.tolist() + milano.index.tolist()))

    LPHE = LPHE.reindex(common_sys)
    milano = milano.reindex(common_sys)
    ratio = LPHE / milano - 1

    idx_small = ((LPHE.abs() < 1e-2) & (milano.abs() < 1e-2)).fillna(False)
    ratio[idx_small] = np.nan

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(10, 0.5 * milano.shape[1] + 5))
    sns.heatmap(
        milano.T * 100,
        center=0,
        ax=ax1,
        vmin=-8,
        vmax=8,
        square=False,
        xticklabels=["%2d" % d for d in range(1, milano.shape[0] + 1)],
    )

    ax1.set_title(
        "Milano | Systematic on %s [%%] | region=%s quantity=%s"
        % ("PER up", region, definitions.latex_labels[quantity])
    )
    ax1.set_xlabel("")

    sns.heatmap(
        LPHE.T * 100,
        center=0,
        ax=ax2,
        vmin=-8,
        vmax=8,
        square=False,
        xticklabels=["%2d" % d for d in range(1, LPHE.shape[0] + 1)],
    )
    ax2.set_title(
        "LPHE | Systematic on %s [%%] | region=%s quantity=%s"
        % ("PER up", region, definitions.latex_labels[quantity])
    )
    ax2.set_xlabel("")

    sns.heatmap(
        ratio.T * 100,
        center=0,
        vmin=-30,
        vmax=30,
        ax=ax3,
        square=False,
        xticklabels=["%2d. %s" % (i, s) for (i, s) in enumerate(milano.index, 1)],
    )
    ax3.set_title(
        "relative difference [%%] | Systematic on %s | region=%s quantity=%s"
        % ("PER up", region, definitions.latex_labels[quantity])
    )

    fig.tight_layout()
    fig.savefig(
        os.path.join(output_dir, "compare_PER_%s_%s.pdf" % (region, quantity)), bbox_inches="tight"
    )
    plt.close(fig)


def compare_xs_fiducial(region, quantity, prod, output_dir):
    LPHE = data_LPHE.get_xsections_fiducial(region=region, quantity=quantity, prod=prod)
    milano = data_Milano.get_xsections_fiducial(region=region, quantity=quantity, prod=prod)

    if len(milano) != len(LPHE):
        raise ValueError("binning is different, Milano %d LPHE %d" % (len(milano), len(LPHE)))

    ratio = LPHE / milano - 1
    ratio[(LPHE == 0) & (milano == 0)] = 0

    nbins = max(len(LPHE.index), len(milano.index))

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
    ax1.step(milano.index, milano, where="mid", label="Milano")
    ax1.plot(LPHE.index, LPHE, label="LPHE", marker="o", ls="", ms=4)
    ax1.set_xticks(range(0, nbins))

    ax1.set_ylabel(r"cross-section [pb]")
    ax1.set_title("%s %s %s" % (region, definitions.latex_labels[quantity], prod))
    ax1.legend(loc=0)

    ax2.step(ratio.index, ratio * 100, where="mid")
    ax2.set_ylabel("ratio to Milano - 1 [%]")
    ax2.set_ylim(-5, 5)

    ax2.set_xticks(range(0, nbins))
    ax2.set_xticklabels(definitions.bin_labels[quantity], rotation=90)

    fig.savefig(
        os.path.join(output_dir, "compare_xsections_%s_%s_%s.pdf" % (prod, region, quantity)),
        bbox_inches="tight",
    )

    plt.close(fig)


def compare_cfactors_sys(region, quantity, output_dir):
    LPHE = data_LPHE.get_result_cfactors_errors(region, quantity).fillna(0)
    milano = data_Milano.get_cfactors_sys(region, quantity).fillna(0)

    LPHE_minus_milano = set(LPHE.index.levels[0]) - set(milano.index.levels[0])
    milano_minus_LPHE = set(milano.index.levels[0]) - set(LPHE.index.levels[0])

    if LPHE_minus_milano:
        logging.warning("systematics in LPHE but not in Milano: %s", ", ".join(LPHE_minus_milano))
    if milano_minus_LPHE:
        logging.warning("systematic in milano not in LPHE: %s", ", ".join(milano_minus_LPHE))

    # consider only the up
    LPHE = LPHE.xs('up', level='direction')
    milano = milano.xs('up', level='direction')

    # remove zero rows (systematics)
    # LPHE = LPHE.reindex(LPHE.index[LPHE.abs().sum(axis=1) != 0])
    LPHE = LPHE.loc[~(LPHE.abs() < 0.001).all(axis=1)]
    milano = milano.loc[~(milano.abs() < 0.001).all(axis=1)]

    common_sys = sorted(set(LPHE.index.tolist() + milano.index.tolist()))

    milano = milano.reindex(common_sys)
    LPHE = LPHE.reindex(common_sys)

    ratio = milano.abs() / LPHE.abs() - 1
    ratio[(milano.abs() < 0.003) & (LPHE.abs() < 0.003)] = np.nan

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(15, 0.5 * milano.shape[1] + 5))
    sns.heatmap(
        milano.T.abs() * 100,
        ax=ax1,
        vmin=0,
        vmax=2,
        square=False,
        xticklabels=["%2d" % d for d in range(1, milano.shape[0] + 1)],
    )

    ax1.set_title(
        "Milano | Abs systematic on %s [%%] | region=%s quantity=%s"
        % ("cfactors UP", region, definitions.latex_labels[quantity])
    )
    ax1.set_xlabel("")

    sns.heatmap(
        LPHE.T.abs() * 100,
        ax=ax2,
        vmin=0,
        vmax=2,
        square=False,
        xticklabels=["%2d" % d for d in range(1, LPHE.shape[0] + 1)],
    )
    ax2.set_title(
        "LPHE | Abs Systematic on %s [%%] | region=%s quantity=%s"
        % ("cfactors UP", region, definitions.latex_labels[quantity])
    )
    ax2.set_xlabel("")

    sns.heatmap(
        ratio.T * 100,
        center=0,
        vmin=-10,
        vmax=10,
        ax=ax3,
        square=False,
        xticklabels=["%2d. %s" % (i, s) for (i, s) in enumerate(milano.index, 1)],
    )
    ax3.set_title(
        "relative difference [%%] | Systematic on %s | region=%s quantity=%s"
        % ("cfactors UP", region, definitions.latex_labels[quantity])
    )

    fig.tight_layout()
    fig.savefig(
        os.path.join(output_dir, "compare_cfactor_sys_%s_%s.pdf" % (region, quantity)), bbox_inches="tight"
    )
    plt.close(fig)


def loop_compare(function, kwargs, reraise=False):
    for kwarg in kwargs:
        try:
            function(**kwarg)
        except FileNotFoundError as ex:
            logging.warning("cannot find file for %s: %s", kwarg, ex)
        except Exception as ex:
            logging.error("strange exception for %s: %s", kwarg, ex)
            if reraise:
                raise


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("dir_milano", help="example: /home/turra/cernbox_h025_xsections/v0.5.1")
    parser.add_argument(
        "dir_LPHE",
        help="example: /home/turra/FullRun2CrossSections (from ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/h-yy-xsec/FullRun2CrossSections.git)",
    )
    parser.add_argument(
        "--action", choices=("all", "signal", "pes", "per", "xs", "cfactors_sys"), default="all"
    )
    parser.add_argument("--output-plot", default="compare_plots")
    parser.add_argument("--prod-mode")
    parser.add_argument("--region")
    parser.add_argument("--quantity")
    parser.add_argument("--doraise", action="store_true")

    args = parser.parse_args()

    create_dir_if_not_exists(args.output_plot)

    data_LPHE = Reader_LPHE(args.dir_LPHE)
    data_Milano = Reader(args.dir_milano)

    region_quantities = [
        (region, quantity)
        for (region, quantity) in definitions.regions_quantities_analysis
        if (
            (args.region is None or region == args.region)
            and (args.quantity is None or quantity == args.quantity)
        )
    ]

    args_region_quantity = [
        {"region": region, "quantity": quantity, "output_dir": args.output_plot}
        for region, quantity in region_quantities
    ]

    if args.action == "all" or args.action == "signal":
        loop_compare(compare_signal, args_region_quantity)

    if args.action == "all" or args.action == "pes":
        loop_compare(compare_pes, args_region_quantity)

    if args.action == "all" or args.action == "per":
        loop_compare(compare_per, args_region_quantity)

    if args.action == "all" or args.action == "cfactors_sys":
        loop_compare(compare_cfactors_sys, args_region_quantity)

    if args.action == "all" or args.action == "xs":
        kwargs = (
            {"region": region, "quantity": quantity, "prod": prod, "output_dir": args.output_plot}
            for region, quantity in region_quantities
            for prod in definitions.production_modes + ["prodAll"]
            if (args.prod_mode is None or prod == args.prod_mode)
        )
        loop_compare(compare_xs_fiducial, kwargs, reraise=args.doraise)
