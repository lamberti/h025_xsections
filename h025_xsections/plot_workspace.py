#!/usr/bin/env python

import logging
import os

import ROOT
from h025_xsections.core import utils
from h025_xsections.core.definitions import get_region_quantities

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")
ROOT.PyConfig.IgnoreCommandLineOptions = True

ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Plotting)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(1)

utils.silence_roofit(ROOT)


def plot_workspace_from_file(fn_ws:str, ws_name:str, data_name:str, nbins:int, output_dir:str):
    f = ROOT.TFile.Open(fn_ws)
    ws = f.Get(ws_name)
    ROOT.SetOwnership(ws, True)
    if not ws:
        logging.info("no workspace in file %s", fn_ws)
        return
    logging.info("Plotting workspace: %s", fn_ws)
    plot_workspace(ws, data_name, nbins, output_dir, prefix=os.path.splitext(os.path.split(fn_ws)[1])[0])


def plot_workspace(ws: ROOT.RooWorkspace, data_name: str, nbins:int, output_dir: str, prefix: str):
    utils.create_dir_if_not_exists(output_dir)
    cat = ws.obj("channellist")
    ncat = cat.numTypes()
    cat_names = [cat.lookupType(i).GetName() for i in range(ncat)]
    top_pdf = ws.obj("CombinedPdf")

    data = ws.data(data_name)
    if not data:
        raise ValueError("cannot find data %s in workspace")

    RFNorm = ROOT.RooFit.Normalization(1.0, ROOT.RooAbsReal.RelativeExpected)

    for icat in range(ncat):
        canvas = ROOT.TCanvas()
        frame = ws.obj("atlas_invMass_%s" % cat_names[icat]).frame(
            ROOT.RooFit.Title("category: %d" % icat), ROOT.RooFit.Bins(nbins)
        )
        data.plotOn(
            frame,
            ROOT.RooFit.DataError(ROOT.RooAbsData.Poisson),
            ROOT.RooFit.Cut("channellist==channellist::%s" % cat_names[icat]),
        )
        pdfi = top_pdf.getPdf(cat_names[icat])

        pdfi.plotOn(
            frame,
            ROOT.RooFit.Components("pdf__background_{catname}".format(catname=cat_names[icat])),
            RFNorm,
            ROOT.RooFit.LineStyle(ROOT.kDashed),
        )
        pdfi.plotOn(frame, RFNorm, ROOT.RooFit.LineColor(ROOT.kRed - 4))

        frame.GetXaxis().SetTitle("m_{#gamma#gamma} [GeV]")
        frame.GetYaxis().SetTitleOffset(1.4)
        frame.Draw()
        old_level = ROOT.gErrorIgnoreLevel
        ROOT.gErrorIgnoreLevel = ROOT.kWarning
        canvas.SaveAs(os.path.join(output_dir, "%s_%d.pdf" % (prefix, icat)))
        ROOT.gErrorIgnoreLevel = old_level


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Plot result of a fit")
    parser.add_argument("workspace")
    parser.add_argument("output_dir")
    parser.add_argument("--ws-name", default="combWS")
    parser.add_argument("--data", default="AsimovSB")
    parser.add_argument("--nbins", type=int, default=220)
    args = parser.parse_args()

    plot_workspace_from_file(args.workspace, args.ws_name, args.data, args.nbins, args.output_dir)
