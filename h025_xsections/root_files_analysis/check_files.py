import argparse
import logging
import os
from glob import glob

import pandas as pd
from h025_xsections.core import definitions
from h025_xsections.core.naming import parse_histo_filename

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def diff_set(exp, obs):
    if set(exp) == set(obs):
        return None
    else:
        return set(exp) - set(obs), set(obs) - set(exp)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Check that all the files are produced by condor")
    parser.add_argument("folder")

    args = parser.parse_args()

    all_fn = glob(os.path.join(args.folder, "*.root"))
    logging.info("found %d ROOT files", len(all_fn))

    all_info = []
    for fn in all_fn:
        info = parse_histo_filename(fn)
        all_info.append(info.asdict())

    df = pd.DataFrame(all_info)

    all_sys_found = df["sys"].unique()
    logging.info("found %s systematic variation (including nominal)", len(all_sys_found))

    # remove NOMINAL not from Nominal-group
    df = df[~((df["sys"] == "NOMINAL") & (df["sysgroup"] != "Nominal"))]

    # check there are no duplicated sys
    gr = df.groupby(["mc", "prod", "sys"])["sysgroup"].apply(lambda r: set(r))
    gr_duplicates = gr[gr.apply(lambda x: len(x) != 1)]
    if len(gr_duplicates) != 0:
        logging.warning("there are duplicated sys")
        print(gr_duplicates)

    # check we have all the mc
    d = diff_set(definitions.mc_samples, df["mc"].unique())
    if d is not None:
        if d[0]:
            logging.warning("missing mc: %s", ",".join(d[0]))
        if d[1]:
            logging.warning("spurious mc: %s", ",".join(d[1]))

    # check we have all the prod
    d = diff_set(definitions.production_modes, df["prod"].unique())
    if d is not None:
        if d[0]:
            logging.warning("missing prod: %s", ",".join(d[0]))
        if d[1]:
            logging.warning("spurious prod: %s", ",".join(d[1]))

    for period in definitions.mc_samples:
        df_period = df[df["mc"] == period][["prod", "sys"]]
        df_period["n"] = 1
        df_period = df_period.set_index(["prod", "sys"])

        table = df_period.pivot_table(values="n", columns="prod", index="sys", aggfunc="sum")
        table = table.reindex(
            index=all_sys_found, columns=definitions.production_modes, fill_value=0
        ).sort_index()
        t = table.stack(dropna=False).fillna(0)

        for row in t[t < 1].iteritems():
            if row[0][1] == "tHjb" or row[0][1] == "tWh":
                continue
            logging.warning("missing %s %s %s", period, row[0][1], row[0][0])

#        fig, ax = plt.subplots()
#        sns.heatmap(table)
#        fig.savefig('check_%s.png' % period, bbox_inches='tight')
#        plt.close(fig)
