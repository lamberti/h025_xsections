import re
from glob import glob
import os
import logging
import argparse

parser = argparse.ArgumentParser(description="Check logs")
parser.add_argument('folder')
args = parser.parse_args()

BASEDIR = args.folder
output = open("timing.txt", "w")
output.write('\t'.join(['nevents', 'config', 'run', 'total']) + '\n')

all_errs = glob(os.path.join(BASEDIR, "err/*"))


r_total = re.compile(r"total time: ([0-9]+)")
r_running = re.compile(r"running time: ([0-9]+)")
r_config = re.compile(r"config time: ([0-9]+)")
r_events = re.compile(r"Running on ([0-9]+) events")

for fn in all_errs:
    text = open(fn).read()
    m = r_total.search(text)
    total_time = ''
    if not m:
        logging.error("cannot find total time in file %s", fn)
    else:
        total_time = m.group(1)

    m = r_running.search(text)
    running_time = ''
    if not m:
        logging.error("cannot find running time in file: %s", fn)
    else:
        running_time = m.group(1)

    m = r_config.search(text)
    config_time = ''
    if not m:
        logging.error("cannot find runnig time in file: %s", fn)
    else:
        config_time = m.group(1)

    m = r_events.search(text)
    nevents = ''
    if not m:
        logging.error("cannot find number of events in file: %s", fn)
    else:
        nevents = m.group(1)

    output.write("%s\t%s\t%s\t%s\n" % (nevents, config_time, running_time, total_time))
    output.flush()

import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
df = pd.read_csv("timing.txt", sep='\t')
fig, axs = plt.subplots(2, 2)
df.plot.scatter(x='nevents', y='total', ax=axs[0][0])
df.plot.scatter(x='nevents', y='config', ax=axs[0][1])
df.plot.scatter(x='nevents', y='run', ax=axs[1][0])
fig.savefig('timing.png')

