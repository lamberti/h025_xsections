import argparse
from argparse import RawTextHelpFormatter
import logging
import os
import socket
import stat
from datetime import datetime
from shutil import copy

import h025_xsections
from h025_xsections.core import definitions
from h025_xsections.core.naming import FileHistoInfo
from h025_xsections.find_samples import (
    filter_samples,
    get_samples_from_list,
    get_samples_h025_uxAOD,
)

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def create_package():
    path_module = h025_xsections.__path__[0]
    os.system("cd %s/.. && python setup.py sdist --formats=gztar" % path_module)

    from h025_xsections.version import __version__  # pylint: disable=C0415

    path_tarball = os.path.normpath(
        os.path.join(path_module, "../dist/h025_xsections-%s.tar.gz" % __version__)
    )
    if not os.path.isfile(path_tarball):
        raise IOError("problem when creating package tarball")
    return path_tarball


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Create condor submission for histograms production.",
        formatter_class=RawTextHelpFormatter,
        epilog="""
    Example at Milan:

    python condor_run_histograms.py --list-sample ../samples/samples_h025_uxAOD-proof.txt --sys-group Nominal --skim isPass_or_isFiducial --guess-memory --guess-time --submission-folder=nominal

    Example for scale/resolution detailed systematics
    python condor_run_histograms.py --list-sample ../samples/samples_h025_uxAOD-proof.txt --skim isPass --sys "EG*" "PH_SCALE*" --extra-args="--only-reco" --guess-memory --guess-time --submission-folder=energy

    Example for other systematics
    python condor_run_histograms.py --list-sample ../samples/samples_h025_uxAOD-proof.txt --sys-group "JetSys*" LeptonMETSys FlavorSys PhotonSys --extra-args="--no-myy" --guess-memory --guess-time --submission-folder=others

    If the process get held check if this is because of memory usage, change the maximum with condor_qedit, for example:

        condor_q -hold
        condor_qedit 3596728 -constraint "JobStatus==5" RequestMemory 4000
        condor_release 3596728

    """,
    )
    parser.add_argument(
        "--submission-folder",
        default="submission_" + datetime.isoformat(datetime.now()).replace(":", "."),
    )
    parser.add_argument("--base-dir", help="path to the folder with input data")
    parser.add_argument("--list-samples", help="filename listing all the samples")
    parser.add_argument("--production-mode", nargs="*", default=definitions.production_modes)
    parser.add_argument("--mc-period", nargs="*", default=definitions.mc_samples)
    parser.add_argument("--sys-group", nargs="*", help="the sys group, accept wildcard")
    parser.add_argument("--sys", nargs="*", help="systematics, accept wildcard")
    parser.add_argument("--skim", nargs="*")
    parser.add_argument("--extra-args")
    parser.add_argument("--memory", type=int)
    parser.add_argument("--guess-memory", action='store_true')
    parser.add_argument("--time", type=int, help='time in seconds')
    parser.add_argument("--guess-time", action='store_true')
    args = parser.parse_args()

    try:
        os.mkdir(args.submission_folder)
    except OSError:
        logging.error(
            "the directory %s already exist, remove it before running", args.submission_folder
        )
        exit()

    package_tarball_path = create_package()
    package_tarball_basename = os.path.split(package_tarball_path)[1]

    copy(package_tarball_path, args.submission_folder)
    copy("Analysis_Run.py", args.submission_folder)

    condor_template = """
    universe                = vanilla
    executable              = wrapper_setup.sh
    arguments               = $(ClusterId).$(ProcId) $(input_name) --mc-sample $(mc_sample) --output-name $(output_name) --base-reco-branch $(reco_branch) --no-progress-bar {extra_args}
    transfer_input_files    = {transfer_input_files}
    output                  = {output_dir}/Analysis_Run.$(ClusterId).$(ProcId).out
    error                   = {error_dir}/Analysis_Run.$(ClusterId).$(ProcId).err
    log                     = {log_dir}/Analysis_Run.$(ClusterId).log

    stream_error            = True
    stream_output           = True

    {other_options}

    +MaxRuntime = $(time)

    request_memory          = $(memory)
    {request_memory}

    queue input_name,mc_sample,reco_branch,output_name,memory,time from (
    {queue}
    )
    """

    if args.base_dir is not None:
        samples_info = get_samples_h025_uxAOD(args.base_dir)
    elif args.list_samples is not None:
        samples_info = get_samples_from_list(args.list_samples)
    else:
        raise ValueError("you should specify --base-dir or --list-samples")
    for sample_info in sorted(samples_info):
        print(sample_info.path, sample_info.sys, sample_info.production_mode, sample_info.entries)

    samples_info = filter_samples(
        samples_info,
        sys=args.sys_group,
        skim=args.skim,
        mc=args.mc_period,
        production=args.production_mode,
    )
    if not samples_info:
        raise ValueError("no sample found matching the requests")

    for f in "log", "err", "out":
        os.mkdir(os.path.join(args.submission_folder, f))

    logging.info("number of samples: %d", len(samples_info))

    queue_txt = []
    for sample_info in sorted(samples_info):
        for reco_branch in sample_info.reco_branches:
            sys_name = reco_branch
            if reco_branch == "HGamEventInfo":
                sys_name = "NOMINAL"
            else:
                sys_name = reco_branch.replace("HGamEventInfo_", "")

            if args.sys is not None:
                import fnmatch

                if not any([fnmatch.fnmatch(sys_name, arg_sys) for arg_sys in args.sys]):
                    continue

            tags = FileHistoInfo(
                mc=sample_info.mc_period,
                prod=sample_info.production_mode,
                skim=sample_info.skim,
                sysgroup=sample_info.sys,
                sys=sys_name,
            )
            output_name = "histo_{tags}.{fn}".format(
                tags=tags.format_tags(), fn=os.path.split(sample_info.path)[1]
            )

            memory = 1024  # MB
            if args.memory:
                memory = args.memory
            elif args.guess_memory:
                if sample_info.entries != 0:
                    memory = int(300 + 0.0006 * sample_info.entries)
            time = 30 * 60
            if args.time:
                time = args.time
            elif args.guess_time:
                if sample_info.entries != 0:
                    time = int(700 + 0.001 * sample_info.entries)
            queue_txt += [
                "{input_path},{mc},{reco_branch},{output_path},{memory},{time}".format(
                    input_path=sample_info.path,
                    mc=sample_info.mc_period,
                    output_path=output_name,
                    reco_branch=reco_branch + "AuxDyn",
                    memory=memory,
                    time=time
                )
            ]
            logging.info(
                "{mc} {prod:4} {skim:20} {sysgroup} {sys} {path}".format(
                    mc=sample_info.mc_period,
                    prod=sample_info.production_mode,
                    skim=sample_info.skim,
                    sysgroup=sample_info.sys,
                    sys=sys_name,
                    path=sample_info.path
                )
            )

    logging.info("submitting %d jobs", (len(queue_txt)))
    queue_txt = "\n".join(queue_txt)
    other_options = ""

    hostname = socket.gethostname()
    if "mi.infn.it" in hostname:
        other_options = (
            'Requirements = TARGET.HasCVMFS && RegExp("^proof-.*", Machine) && OpSysMajorVer == 7'
        )

    transfer_input_files = [package_tarball_basename, 'Analysis_Run.py']
    proxy_path = os.getenv('X509_USER_PROXY')
    if proxy_path:
        copy(proxy_path, os.path.join(args.submission_folder, 'x509up'))
        transfer_input_files += ['x509up']

    condor_txt = condor_template.format(
        output_dir="out",
        bin_folder=os.getcwd(),
        log_dir="log",
        error_dir="err",
        other_options=other_options,
        queue=queue_txt,
        transfer_input_files = ','.join(transfer_input_files),
        extra_args=args.extra_args or "",
        request_memory="" if args.memory is None else "request_memory=%d" % args.memory,
    )

    with open(os.path.join(args.submission_folder, "submit.condor"), "w") as f:
        f.write(condor_txt)

    wrapper_env_template = open("wrapper_setup.sh.in").read()
    wrapper_env = wrapper_env_template.replace("<tarball>", package_tarball_basename).replace(
        "<package_path>", package_tarball_basename.replace(".tar.gz", "")
    )
    wrapper_fn = os.path.join(args.submission_folder, "wrapper_setup.sh")
    with open(wrapper_fn, "w") as f:
        f.write(wrapper_env)
    st = os.stat(wrapper_fn)
    os.chmod(wrapper_fn, st.st_mode | stat.S_IEXEC)
