#!/usr/bin/env python

"""
Produce yield histograms from a ROOT file (mxAOD, uxAOD).
Histograms are produced for all the fiducial regions (Diphoton, ...)
and all the requested quantities (including the inclusive case).

Histograms are usually normalized to xs x Br x lumi

These histograms are produced from the TTree in the ROOT file
   * w_initial: 1 bin histograms, with the sum of weight_initial
   * w_initial_xsec_br_lumi: 1 bin histograms, witht he sum of the
     product of weight_initial x Br x xsection
   * w_final: 1 bin histograms, with the sum of w_final
   * histo1D_{quantity}_true_{predicate}: histogram of the
     true quantity, weighted with w_initial x xs x Br x lumi
   * histo1D_{quantity}_reco{predicate}: histogram of the 
     reco quantity, weighted with w_final x xs x Br x lumi
   * histo2D_{quantity}_{predicate}: histogram with the
     reco quantity and true quantity weighted with
     w_final x xs x Br x lumi
   * histo2D_m_yy_{quantity}_{predicate}: histogram with
     the reco m_yy and teh reco quantity weighted with 
     w_final x xs x Br x lumi

In addition some values are copyied from the orignal file:
   * cutflow_weighted: cutflow histogram, for all and noDalitz
   * cutflow_weighted_xsec_Br_lumi: same but multiplied by
     xs x Br x lumi
"""

import argparse
import logging
from itertools import product
from argparse import RawTextHelpFormatter
from array import array
import os
import time

start_time = time.time()

import ROOT

ROOT.PyConfig.IgnoreCommandLineOptions = True
from ROOT.ROOT import RDataFrame as RDF

from h025_xsections.core import definitions
from h025_xsections.core.predicate import Predicate, PredicateSet

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=RawTextHelpFormatter,
    epilog="""
Example: python Analysis_Run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025/mc16a/NoSys/PowhegPy8EG_NNPDF30_VBFH125.root \
 --mc-sample mc16a \
 --output-name PowhegPy8EG_NNPDF30_VBFH125_sysNominal.root
""",
)

parser.add_argument("input_file", help="ROOT file to be processed")
parser.add_argument("--tree-name", default="CollectionTree")
parser.add_argument("--base-reco-branch", default="HGamEventInfoAuxDyn")
parser.add_argument("--base-truth-branch", default="HGamTruthEventInfoAuxDyn")
parser.add_argument("--isDalitz-branch", default="HGamEventInfoAuxDyn.isDalitz")
parser.add_argument("--mc-sample", help="mc sample name (mc16a, ...)")
parser.add_argument("--output-name", help="output_file name")
parser.add_argument("--no-progress-bar", action="store_true")
parser.add_argument(
    "--only-reco",
    action="store_true",
    help="don't store true histograms, useful for systematic case",
)
parser.add_argument(
    "--no-myy",
    action="store_true",
    help="don't store 2d histogram with myy, useful for systematic not related to PER/PES",
)
parser.add_argument(
    "--quantities",
    nargs="*",
    help="space separates, sublist of %s" % ",".join(definitions.quantities),
    default=definitions.quantities + ["constant"] + list(definitions.quantities_2d.keys()),
)
parser.add_argument("--verbose", action="store_true")
args = parser.parse_args()

do_truth = not args.only_reco

if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("turning on debug messages")

if args.output_name is None:
    args.output_name = "histo_%s.%s" % (args.mc_sample, os.path.basename(args.input_file))

tree_name = args.tree_name
fn = args.input_file

for q in args.quantities:
    if q not in definitions.quantities + ["constant"] + list(definitions.quantities_2d.keys()):
        raise ValueError(
            "invalid quantity %s, should be one of %s" % (q, ",".join(definitions.quantities))
        )
quantities = args.quantities

logging.info('Processing file:      "%s"', fn)
logging.info('Tree Name:            "%s"', tree_name)
logging.info('Base_Reco_Branch:     "%s"', args.base_reco_branch)
logging.info('Base_Truth_Branch:    "%s"', args.base_truth_branch)
logging.info('mc:                   "%s"', args.mc_sample)
logging.info('Output file:          "%s"', args.output_name)
logging.info("Quantities (%d):       %s", len(quantities), quantities)
logging.info("Only reco:             %s", args.only_reco)

# try DeclareCppCallable
ROOT.gInterpreter.Declare(
    """
class FoldedBinner
{
public:
    FoldedBinner(const TAxis& axis1, const TAxis& axis2)
        :m_axis1(axis1), m_axis2(axis2) { }
    int operator()(double x, double y)
    {
        const auto bin1 = m_axis1.FindBin(x);
        const auto bin2 = m_axis2.FindBin(y);

        return bin1 * (m_axis2.GetNbins() + 2) + bin2;
    }
private:
    TAxis m_axis1, m_axis2;
};

FoldedBinner binner_cpp(TAxis(10, 0, 100), TAxis(10, 0, 100));
"""
)


def get_branch_name(quantity, recotrue):
    if recotrue not in ("reco", "true"):
        raise ValueError("recotrue must be reco or true, got %s" % recotrue)
    if quantity == "constant":
        return "0"
    else:
        prefix = args.base_reco_branch if recotrue == "reco" else args.base_truth_branch
        return "%s.%s" % (prefix, definitions.branches[quantity])


LUMI = definitions.luminosity[args.mc_sample]

logging.info("Luminosity:           %s pb^-1", LUMI)

initial_weights_name = "%s.weightInitial" % args.base_reco_branch
logging.info('initial_weights_name: "%s"', initial_weights_name)

isFiducial_formulas = {
    "Diphoton": "{base}.isFiducial".format(base=args.base_truth_branch),
    "VBF": "{base}.isFiducial && {base}.catXS_VBF".format(base=args.base_truth_branch),
    "Lepton": "{base}.isFiducial && {base}.catXS_lepton".format(base=args.base_truth_branch),
    "MET": "{base}.isFiducial && {base}.catXS_MET".format(base=args.base_truth_branch),
}

isPassed_formulas = {
    "Diphoton": "{base}.isPassed".format(base=args.base_reco_branch),
    "VBF": "{base}.isPassed && {base}.catXS_VBF".format(base=args.base_reco_branch),
    "Lepton": "{base}.isPassed && {base}.catXS_lepton".format(base=args.base_reco_branch),
    "MET": "{base}.isPassed && {base}.catXS_MET".format(base=args.base_reco_branch),
}

isDalitz_name = args.isDalitz_branch

f = ROOT.TFile.Open(fn)
tree = f.Get(tree_name)
nevents = tree.GetEntries()
df = RDF(tree)

output_file = ROOT.TFile.Open("%s" % args.output_name, "RECREATE")

# ************************ Defining cuts ****************************

if do_truth:
    for key in isFiducial_formulas:
        formula = isFiducial_formulas[key]
        logging.debug("defining isFiducial%s := %s", key, formula)
        df = df.Define("isFiducial" + key, formula)

for key in isPassed_formulas:
    formula = isPassed_formulas[key]
    logging.debug("defining isPassed%s := %s", key, formula)
    df = df.Define("isPassed" + key, formula)

if do_truth:
    logging.debug("defining isDalitz := %s", isDalitz_name)
    df = df.Define("isDalitz", isDalitz_name)

# ************************ Defining a constant **********************

df = df.Define("zero", "0.")  # useful to fill single-bin histograms

# ******************* Defining initial weights ***********************

logging.debug("defining w_initial := %s", initial_weights_name)
df = df.Define("w_initial", initial_weights_name)

expression_w_initial_xsec_br_lumi = "(float)(%s * %s * %s)" % (
    "w_initial",
    "HGamEventInfoAuxDyn.crossSectionBRfilterEff",
    LUMI,
)
logging.debug("defining w_initial_xsec_br_lumi := %s", expression_w_initial_xsec_br_lumi)
df = df.Define("w_initial_xsec_br_lumi", expression_w_initial_xsec_br_lumi)

# ********************* Defining final weights ***********************
# Final weights are defined as xs * Br * weight_final
# weight_final includes the MC correction (including weight_initial)
# plus the reconstruction corrections. It depends on the final
# categorization

single_final_weights = set()

for region, quantity in definitions.regions_quantities_analysis:
    final_weights_name = definitions.weight_final.get((region, quantity), None)
    if final_weights_name is None:
        logging.warning(
            "region %s quantity %s has no final weight set in definitions.py! Using 'weight'. FIX!!!",
            region,
            quantity,
        )
        final_weights_name = "weight"
    if type(final_weights_name) is str:
        final_weights_name = [final_weights_name]
    for w in final_weights_name:
        if w not in single_final_weights:
            logging.debug("defining %s := %s", w, "%s.%s" % (args.base_reco_branch, w))
            df = df.Define(w, "%s.%s" % (args.base_reco_branch, w))
            single_final_weights.add(w)

    final_weight_name = "w_final_{region}_{quantity}".format(region=region, quantity=quantity)
    final_weights_expression = "(float)(%s)" % (" * ".join(final_weights_name))

    logging.debug("defining %s := %s", final_weight_name, final_weights_expression)
    df = df.Define(final_weight_name, final_weights_expression)

    name_w_final_xsec_br_lumi = "w_final_xsec_br_lumi_{region}_{quantity}".format(
        region=region, quantity=quantity
    )
    expression_w_final_xsec_br_lumi = "(float)(%s * %s * %s)" % (
        "w_final_{region}_{quantity}".format(region=region, quantity=quantity),
        "HGamEventInfoAuxDyn.crossSectionBRfilterEff",
        LUMI,
    )
    logging.debug("defining %s := %s", name_w_final_xsec_br_lumi, expression_w_final_xsec_br_lumi)
    df = df.Define(name_w_final_xsec_br_lumi, expression_w_final_xsec_br_lumi)

# *********************** Defining quantities ***********************

# True branches
if do_truth:
    for quantity in quantities + ["m_yy"]:
        if quantity in definitions.quantities_2d:
            continue
        branch_quantity_name = get_branch_name(quantity, "true")
        if definitions.units_root != definitions.units_final:
            unit_conversion = definitions.units_root[quantity] / float(
                definitions.units_final[quantity]
            )
            branch_quantity_name += " * %s" % unit_conversion
        alias = "%s_true" % quantity
        logging.debug("defining %s := %s", alias, branch_quantity_name)
        try:
            df = df.Define(alias, branch_quantity_name)
        except TypeError:
            raise ValueError("problem when defining %s := %s" % (alias, branch_quantity_name))

# Reco branches
for quantity in quantities + ["m_yy"]:
    if quantity in definitions.quantities_2d:
        continue
    branch_quantity_name = get_branch_name(quantity, "reco")
    if definitions.units_root != definitions.units_final:
        unit_conversion = definitions.units_root[quantity] / float(
            definitions.units_final[quantity]
        )
        branch_quantity_name += " * %s" % unit_conversion
    alias = "%s_reco" % quantity
    logging.debug("defining %s := %s", alias, branch_quantity_name)
    try:
        df = df.Define(alias, branch_quantity_name)
    except TypeError:
        raise ValueError("problem when defining %s := %s" % (alias, branch_quantity_name))

# ********************** Defining 2D quantities **********************

all_binner = []

# Reco and true branches
for q12, (q1, q2) in definitions.quantities_2d.items():

    # must be double ('d') since TAxis interface
    bins1 = array("d", definitions.binnings[q1])
    bins2 = array("d", definitions.binnings[q2])

    binner = ROOT.FoldedBinner(ROOT.TAxis(len(bins1) - 1, bins1), ROOT.TAxis(len(bins2) - 1, bins2))
    all_binner.append(binner)  # TODO: try to remove

    suffixes = ["_reco"]
    if do_truth:
        suffixes += ["_true"]
    for suffix in suffixes:
        cols = ROOT.std.vector("std::string")()
        cols.push_back(q1 + suffix)
        cols.push_back(q2 + suffix)
        name_folded_var = q12 + suffix
        logging.debug("defining %s := folding %s x %s", name_folded_var, q1 + suffix, q2 + suffix)
        try:
            df = df.Define(name_folded_var, binner, cols)
        except TypeError:
            raise ValueError(
                "problem when defining %s := folding %s x %s"
                % (name_folded_var, q1 + suffix, q2 + suffix)
            )


# ********************** Filtering the dataframe *************************
# all the events (no filter)
all_dfs = {PredicateSet(): df}

# isDalitz / notDalitz
is_dalitz_selection = PredicateSet(isDalitz=Predicate(True, name="Dalitz", formula="isDalitz"))
if do_truth:
    all_dfs.update(
        {
            is_dalitz_selection: df.Filter(is_dalitz_selection.formula()),
            ~is_dalitz_selection: df.Filter((~is_dalitz_selection).formula()),
        }
    )

# isFiducial / notFiducial, isPassed / not Passed, for all the definitions
for fiducial_type in isFiducial_formulas:

    is_fiducial_selection = PredicateSet(
        isFiducial=Predicate(True, "Fiducial" + fiducial_type, "isFiducial" + fiducial_type)
    )
    is_passed_selection = PredicateSet(
        isPassed=Predicate(True, "Passed" + fiducial_type, "isPassed" + fiducial_type)
    )

    # filter by basic predicate and their negation
    for predicate in (is_fiducial_selection, is_passed_selection):
        if not do_truth:
            if not predicate.isDalitz.is_any() or not predicate.isFiducial.is_any():
                continue
        filter_expression = predicate.formula()
        logging.debug(
            "defining df from predicate %s with expression %s", predicate, filter_expression
        )
        all_dfs[predicate] = df.Filter(filter_expression)
        filter_expression = (~predicate).formula()
        logging.debug(
            "defining df from predicate %s with expression %s",
            ~predicate,
            filter_expression,
        )
        all_dfs[~predicate] = df.Filter(filter_expression)

    # dfs for the reco: (isFiducial=0/1) x (isDalitz=0/1) x (isPassed=1)
    # dfs for the truth: (isFiducial=0/1) x (isDalitz=0/1)
    for is_fiducial, is_dalitz, is_passed in product(
        (is_fiducial_selection, ~is_fiducial_selection),
        (is_dalitz_selection, ~is_dalitz_selection),
        (is_passed_selection, PredicateSet()),
    ):

        predicate = is_fiducial & is_dalitz & is_passed
        if not do_truth:
            if not predicate.isDalitz.is_any() or not predicate.isFiducial.is_any():
                continue

        df_after_fiducial = all_dfs[is_fiducial]
        df_after_fiducial_dalitz = df_after_fiducial.Filter(is_dalitz.formula())

        if predicate.isPassed.is_any():
            all_dfs[predicate] = df_after_fiducial_dalitz
        else:
            all_dfs[predicate] = df_after_fiducial_dalitz.Filter(is_passed.formula())

logging.debug("all selections defined:\n  %s", "\n  ".join("%s" % s for s in all_dfs.keys()))

# ***************** Defining sum of initial weights histograms *******************

logging.debug("defining histograms for sum of initial weights")

all_histos_sum_weight_initial = {}

for predicate in all_dfs:
    if predicate.isPassed.value is False:
        continue
    d = all_dfs[predicate]  # python 2/3 compatibility
    histo_name = "w_initial_%s" % predicate.name()
    logging.debug(
        "defining histo %s from predicate %s with weight w_initial", histo_name, predicate
    )
    histo = d.Histo1D(
        (histo_name, "weight initial %s" % predicate.name(), 1, 0.0, 1.0), "zero", "w_initial"
    )

    all_histos_sum_weight_initial[predicate] = histo

# *********** Defining sum of initial weights x xsection histograms *************

logging.debug("defining histograms for sum of initial weights x xsection")

all_histos_sum_weight_initial_x_xsection_br_lumi = {}

for predicate in all_dfs:
    if predicate.isPassed.value is False:
        continue
    d = all_dfs[predicate]  # python 2/3 compatibility
    histo_name = "w_initial_xsec_br_lumi_%s" % predicate.name()
    logging.debug(
        "defining histo %s from predicate %s with weight w_initial_xsec_br_lumi",
        histo_name,
        predicate,
    )
    histo = d.Histo1D(
        (histo_name, "weight initial x xsection x br x lumi %s" % predicate.name(), 1, 0.0, 1.0),
        "zero",
        "w_initial_xsec_br_lumi",
    )

    all_histos_sum_weight_initial_x_xsection_br_lumi[predicate] = histo

# ***************** Defining sum of final weights histograms ********************

logging.debug("defining histograms for sum of final weights")

all_histos_sum_weight_final = {}

for quantity in quantities:
    for predicate in all_dfs:
        region = predicate.isPassed.name.replace("Passed", "")
        if (quantity, region) not in definitions.regions_quantities_analysis:
            continue
        if predicate.isPassed.is_any():
            region = "Diphoton"
        d = all_dfs[predicate]
        histo_name = "w_final_%s_%s" % (predicate.name(), quantity)
        logging.debug(
            "defining histo %s from predicate %s quantity %s with weight %s",
            histo_name,
            predicate,
            quantity,
            "w_final_%s_%s" % (region, quantity),
        )

        histo = d.Histo1D(
            (
                histo_name,
                "weight final %s %s" % (quantity, predicate.name()),
                1,
                0.0,
                1.0,
            ),
            "zero",
            "w_final_%s_%s" % (region, quantity),
        )

        all_histos_sum_weight_final[(quantity, predicate)] = histo

# ***************** Defining 1D true ****************************

all_histos1d_true = {}

if do_truth:
    for quantity in quantities:
        for predicate in all_dfs:
            # for the truth we want isPassed = ALL
            if not predicate.isPassed.is_any():
                continue

            d = all_dfs[predicate]
            histo_name = "histo1D_{quantity}_true_{predicate}".format(
                quantity=quantity, predicate=predicate.name()
            )
            histo_title = "true {quantity} {predicate}".format(
                quantity=quantity, predicate=predicate.name()
            )
            logging.debug(
                "defining histo %s from predicate %s quantity %s_true",
                histo_name,
                predicate,
                quantity,
            )
            bins = definitions.binnings[quantity]
            if quantity in definitions.quantities_2d:
                logging.debug(
                    "name: %s, title: %s, %s %s %s",
                    histo_name,
                    histo_title,
                    bins[0],
                    bins[1],
                    bins[2],
                )
                histo_description = ROOT.RDF.TH1DModel(
                    histo_name, histo_title, bins[0], bins[1], bins[2]
                )
            else:
                histo_description = ROOT.RDF.TH1DModel(histo_name, histo_title, len(bins) - 1, bins)
            histo = d.Histo1D(
                histo_description,
                "%s_true" % quantity,
                "w_initial_xsec_br_lumi",
            )

            all_histos1d_true[(quantity, predicate)] = histo

# ***************** Defining 1D reco ****************************

all_histo1d_reco = {}

for quantity in quantities:
    for predicate in all_dfs:
        # reco are only isPassed == TRUE
        if predicate.isPassed.value is not True:
            continue

        region = predicate.isPassed.name.replace("Passed", "")
        if (region, quantity) not in definitions.regions_quantities_analysis:
            continue

        d = all_dfs[predicate]
        histo_name = "histo1D_{quantity}_reco_{predicate}".format(
            quantity=quantity, predicate=predicate.name()
        )
        histo_title = "reco {quantity} {predicate}".format(
            quantity=quantity, predicate=predicate.name()
        )
        logging.debug(
            "defining histo %s from predicate %s quantity %s_reco with weight %s",
            histo_name,
            predicate,
            quantity,
            "w_final_xsec_br_lumi_%s_%s" % (region, quantity),
        )
        bins = definitions.binnings[quantity]
        if quantity in definitions.quantities_2d:
            histo_description = ROOT.RDF.TH1DModel(
                histo_name, histo_title, bins[0], bins[1], bins[2]
            )
        else:
            histo_description = ROOT.RDF.TH1DModel(histo_name, histo_title, len(bins) - 1, bins)
        histo = d.Histo1D(
            histo_description,
            "%s_reco" % quantity,
            "w_final_xsec_br_lumi_%s_%s" % (region, quantity),
        )

        all_histo1d_reco[(quantity, predicate)] = histo

# ***************** Defining 2D reco/true ****************************

all_histo2d = {}

if do_truth:
    # only one predicate (for each fiducial type) for 2d: isFiducial, isPassed, notDalitz
    predicates2d = [
        PredicateSet(
            isFiducial=Predicate(
                True, name="Fiducial" + fiducial_type, formula="isFiducial" + fiducial_type
            ),
            isPassed=Predicate(
                True, name="Passed" + fiducial_type, formula="isPassed" + fiducial_type
            ),
        )
        & (~is_dalitz_selection)
        for fiducial_type in isFiducial_formulas
    ]
    for predicate2d in predicates2d:
        for quantity in quantities:
            region = predicate2d.isPassed.name.replace("Passed", "")
            if (region, quantity) not in definitions.regions_quantities_analysis:
                continue

            histo_name = "histo2D_{quantity}_{predicate}".format(
                quantity=quantity, predicate=predicate2d.name()
            )
            histo_title = "reco/true {quantity} {predicate}".format(
                quantity=quantity, predicate=predicate2d.name()
            )
            logging.debug(
                "defining histo2D %s from predicate %s quantity %s with weight %s",
                histo_name,
                predicate2d,
                quantity,
                "w_final_xsec_br_lumi_%s_%s" % (region, quantity),
            )

            bins = definitions.binnings[quantity]
            if quantity in definitions.quantities_2d:
                histo_description = ROOT.RDF.TH2DModel(
                    histo_name,
                    histo_title,
                    bins[0],
                    bins[1],
                    bins[2],
                    bins[0],
                    bins[1],
                    bins[2],
                )
            else:
                histo_description = ROOT.RDF.TH2DModel(
                    histo_name, histo_title, len(bins) - 1, bins, len(bins) - 1, bins
                )

            histo = all_dfs[predicate2d].Histo2D(
                histo_description,
                "%s_true" % quantity,
                "%s_reco" % quantity,
                "w_final_xsec_br_lumi_%s_%s" % (region, quantity),
            )

            all_histo2d[(quantity, predicate2d)] = histo

# ***************** Defining 2D m_yy / quantity ****************************

all_histo_myy = {}

if not args.no_myy:
    # isPassed
    predicates_myy = [
        PredicateSet(
            isPassed=Predicate(
                True, name="Passed" + fiducial_type, formula="isPassed" + fiducial_type
            )
        )
        for fiducial_type in isPassed_formulas
    ]
    for predicate_myy in predicates_myy:
        for quantity in quantities:
            region = predicate_myy.isPassed.name.replace("Passed", "")
            if (region, quantity) not in definitions.regions_quantities_analysis:
                continue

            histo_name = "histo2D_m_yy_{quantity}_{predicate}".format(
                quantity=quantity, predicate=predicate_myy.name()
            )
            histo_title = "myy vs {quantity} {predicate}".format(
                quantity=quantity, predicate=predicate_myy.name()
            )
            logging.debug(
                "defining histo2D %s m_yy vs %s from predicate %s",
                histo_name,
                quantity,
                predicate_myy,
            )

            bins = definitions.binnings[quantity]
            bins_myy = definitions.binnings["m_yy"]
            if quantity in definitions.quantities_2d:
                histo_description = ROOT.RDF.TH2DModel(
                    histo_name,
                    histo_title,
                    bins[0],
                    bins[1],
                    bins[2],
                    len(bins_myy) - 1,
                    array("d", bins_myy),
                )
            else:
                histo_description = ROOT.RDF.TH2DModel(
                    histo_name, histo_title, len(bins) - 1, bins, len(bins_myy) - 1, bins_myy
                )

            histo = all_dfs[predicate_myy].Histo2D(
                histo_description,
                "%s_reco" % quantity,
                "m_yy_reco",
                "w_final_xsec_br_lumi_%s_%s" % (region, quantity),
            )
            all_histo_myy[(quantity, predicate_myy)] = histo

# ****************** Writing histograms to file *******************


ROOT.gInterpreter.Declare(
    r"""
    std::string progress;

    void ff_(TH1D &h) {
        progress.push_back('#');
        std::cout << "\r[" << std::left << std::setw(100) << progress << ']' << std::flush;
    }

    std::function<void(TH1D &)> ff = ff_; // can't use lambda since it cannot capture progress

    void ff2_(TH1D &h) { std::cerr << (int)h.GetEntries() << std::endl; }
    std::function<void(TH1D &)> ff2 = ff2_;
    """
)

if not args.no_progress_bar:
    all_histos_sum_weight_initial[PredicateSet()].OnPartialResult(int(nevents / 100), ROOT.ff)
else:
    all_histos_sum_weight_initial[PredicateSet()].OnPartialResult(int(nevents / 20), ROOT.ff2)

logging.info('Running on %d events and writing output file "%s"', nevents, args.output_name)


def write_histograms(d):
    for k in d:
        d[k].Write()


config_time = time.time()
logging.info("config time: %d s", config_time - start_time)

for collection in (
    all_histos_sum_weight_initial_x_xsection_br_lumi,
    all_histos_sum_weight_initial,
    all_histos_sum_weight_final,
    all_histos1d_true,
    all_histo1d_reco,
    all_histo2d,
    all_histo_myy,
):
    write_histograms(collection)

if not args.no_progress_bar:
    print()  # since progress bar bar don't put \n

logging.info("Copying original weight histograms")
for k in f.GetListOfKeys():
    name = k.GetName()
    if "CutFlow" in name and "weighted" in name:
        if "noDalitz" in name:
            h = f.Get(name)
            h.SetName("cutflow_weighted_noDalitz")
            h.Write()
        else:
            h = f.Get(name)
            h.SetName("cutflow_weighted")
            h.Write()

predicate_all = PredicateSet()
xsec_br_lumi = (
    all_histos_sum_weight_initial_x_xsection_br_lumi[predicate_all].GetSumOfWeights()
    / all_histos_sum_weight_initial[predicate_all].GetSumOfWeights()
)
logging.info("xsection x Bryy x lumi is: %f", xsec_br_lumi)

# TODO: remove Bryy

logging.info("Copying original weight histograms with xsection x Bryy x lumi")
for k in f.GetListOfKeys():
    name = k.GetName()
    if "CutFlow" in name and "weighted" in name:
        if "noDalitz" in name:
            h = f.Get(name)
            hh = h.Clone("cutflow_weighted_noDalitz_xsec_Br_lumi")
            hh.Scale(xsec_br_lumi)
            hh.Write()
        else:
            h = f.Get(name)
            hh = h.Clone("cutflow_weighted_xsec_Br_lumi")
            hh.Scale(xsec_br_lumi)
            hh.Write()

output_file.Close()

end_time = time.time()
logging.info("running time: %d", end_time - config_time)
logging.info("total time: %d", end_time - start_time)
logging.info("total time / nevents: %.4f", float(end_time - start_time) / nevents)
