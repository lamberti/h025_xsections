#!/usr/bin/env python

import logging
import os
from functools import total_ordering
from glob import glob
from multiprocessing.pool import ThreadPool

from h025_xsections.core import definitions

logging.basicConfig(
    level=logging.INFO,
    format="%(module)-25s %(threadName)s %(levelname)-10s %(message)s",
)


EOS_UXAOD_H025_BASEDIR = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025"
MILANO_UXAOD_H025_BASEDIR = "/storage_tmp/atlas/turra/uxAOD/h025"


def rchop(text, suffix):
    if suffix and text.endswith(suffix):
        return text[: -len(suffix)]
    return text


@total_ordering
class SampleFile:
    __slots__ = [
        "generator",
        "production_mode",
        "mc_period",
        "skim",
        "sys",
        "ifile",
        "reco_branches",
        "truth_branches",
        "path",
        "entries",
    ]

    def __init__(
        self,
        generator,
        production_mode,
        mc_period,
        skim,
        sys,
        ifile,
        path,
        truth_branches=None,
        reco_branches=None,
        entries=None,
    ):
        self.generator = generator
        self.production_mode = production_mode
        self.mc_period = mc_period
        self.sys = sys
        self.skim = skim
        self.ifile = ifile
        self.path = path
        if truth_branches is None or reco_branches is None or entries is None:
            meta_info = self.get_meta()
            if meta_info is None:
                raise IOError("cannot get meta from file %s" % self.path)
            self.truth_branches, self.reco_branches, self.entries = meta_info
        else:
            self.truth_branches = truth_branches
            self.reco_branches = reco_branches
            self.entries = entries

    def get_meta(self):
        import ROOT

        f = ROOT.TFile.Open(self.path)
        if not f:
            logging.error("problem with file %s", self.path)
            return None
        tree = f.Get("CollectionTree")
        if not tree:
            logging.error("problem with file %s, cannot find tree", self.path)
            return None
        branches_prefix = self.get_branch_prefix(tree)
        truth_branches = [b for b in branches_prefix if "HGamTruthEvent" in b]
        reco_branches = [b for b in branches_prefix if "HGamEvent" in b]
        entries = tree.GetEntries()

        return truth_branches, reco_branches, entries

    def __lt__(self, other):
        return [getattr(self, f) for f in self.__slots__] < [
            getattr(other, f) for f in self.__slots__
        ]

    def get_line_csv(self, sep="\t", fields=None, short=False):
        data = {
            "generator": self.generator,
            "production_mode": self.production_mode,
            "mc_period": self.mc_period,
            "skim": self.skim,
            "sys": self.sys,
            "ifile": self.ifile,
            "path": self.path,
            "entries": self.entries,
            "reco_branches": ",".join(self.reco_branches) if not short else len(self.reco_branches),
            "truth_branches": ",".join(self.truth_branches)
            if not short
            else len(self.truth_branches),
        }
        all_fields = (
            "generator",
            "production_mode",
            "mc_period",
            "skim",
            "sys",
            "ifile",
            "path",
            "entries",
            "reco_branches",
            "truth_branches",
        )
        if fields is not None:
            if not all(f in fields for f in all_fields):
                raise ValueError("some field are not ok")
        else:
            fields = all_fields

        return sep.join(map(str, [data[field] for field in fields]))

    def get_branch_prefix(self, tree):
        all_branches_names = [b.GetName() for b in tree.GetListOfBranches()]
        collection_names = set(
            rchop(rchop(b.split(".")[0], "AuxDyn"), "Aux") for b in all_branches_names
        )
        return collection_names


def split_xrootd_path(basedir):
    try:
        from urllib.parse import urlparse
    except ImportError:
        from urlparse import urlparse

    p = urlparse(basedir)
    domain = p.netloc
    path = p.path[1:]  # remove one / from //

    return domain, path


def get_root_files(basedir):
    all_root_files = []

    if "root://" in basedir:
        domain, path = split_xrootd_path(basedir)
        logging.info("searching samples on xrootd, you need to have token")
        import subprocess

        command = "xrdfs root://{domain} ls -R -u {path}".format(domain=domain, path=path)
        logging.info("executing %s", command)
        try:
            r = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
            pipe_result = r.communicate()[0]
        except OSError:
            raise IOError("problem running %s" % command)
        if r.returncode != 0:
            raise IOError(
                "cannot fetch from xrootd, do you have access? Maybe you need to setup a kerberos ticket"
            )
        if not isinstance(pipe_result, str):  # python 3
            pipe_result = pipe_result.decode("ascii")
        all_root_files = [line for line in pipe_result.split("\n") if ".root" in line]
    else:
        all_root_files = glob(os.path.join(basedir, "mc16?/**/*.root*"))

    return all_root_files


def get_production(fn):
    for prod in definitions.production_modes:
        # extra care for ggZH vs ZH, no protection about postfix, e.g. ZH1 vs ZH2
        # TODO: check just one matching, also for other functions
        if "_" + prod.upper() in fn.upper():
            return prod
    return "?"


def get_generator(fn):
    for gen, gen_syn in definitions.generators_synonyms.items():
        for syn in gen_syn:
            if syn in fn:
                return gen
    return "?"


def process_path(fn):
    path_parts = os.path.normpath(fn).split(os.sep)[-3:]
    if any("data" in p for p in path_parts):
        return
    if "mc16ade" in path_parts:
        return

    mc_name = path_parts[0]
    if mc_name not in definitions.mc_samples:
        logging.warning("cannot understand mc %s for file %s", mc_name, fn)
        mc_name = "?"

    if path_parts[1] == "NoSkim":
        skim_name = "NoSkim"
        sys_name = "Nominal"
    elif path_parts[1] == "Skim":
        skim_name = "isPass"
        sys_name = "Nominal"
    elif path_parts[1] == "NoSys":
        skim_name = "isPass_or_isFiducial"
        sys_name = "Nominal"
    else:
        sys_name = path_parts[1]
        if path_parts[1] in (
            "PhotonAllSys1",
            "PhotonAllSys2",
            "PhotonAllSys3",
            "PhotonAllSys4",
        ):
            skim_name = "isPass"
        else:
            skim_name = "isPass_or_isFiducial"

    production = get_production(os.path.split(fn)[1])
    generator = get_generator(os.path.split(fn)[1])
    try:
        sample = SampleFile(generator, production, mc_name, skim_name, sys_name, 0, fn)
    except IOError:
        logging.error("problem with sample %s", fn)
        return None
    logging.info(sample.get_line_csv(short=True))
    return sample


def get_samples_from_list(fn):
    samples = []

    with open(fn) as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            elements = line.split("\t")

            (
                generator,
                prod_modes,
                mc_period,
                skim,
                sys,
                ifile,
                path,
                entries,
                reco_branches,
                truth_branches,
            ) = elements
            entries = int(entries)
            reco_branches = reco_branches.split(",")
            truth_branches = truth_branches.split(",")

            sample = SampleFile(
                generator,
                prod_modes,
                mc_period,
                skim,
                sys,
                ifile,
                path,
                truth_branches,
                reco_branches,
                entries,
            )
            samples.append(sample)
    return samples


def get_samples_h025_uxAOD(basedir=EOS_UXAOD_H025_BASEDIR):

    all_root_files = get_root_files(basedir)

    if not all_root_files:
        logging.error("no root files found in %s", basedir)
    else:
        logging.info("found %d files", len(all_root_files))

    p = ThreadPool(3)

    results = list(p.map(process_path, all_root_files))
    results = [r for r in results if r is not None]

    return results


def filter_samples(samples_info, sys=None, generator=None, mc=None, production=None, skim=None):
    import fnmatch

    def normalize_arg(arg):
        if arg is None or arg == []:
            return None
        if isinstance(arg, str):
            return [arg]
        return arg

    result = samples_info

    generator, mc, production, skim = map(normalize_arg, (generator, mc, production, skim))

    if generator is not None:
        result = [sample for sample in result if sample.generator in generator]
    if mc is not None:
        result = [sample for sample in result if sample.mc_period in mc]
    if skim is not None:
        result = [sample for sample in result if sample.skim in skim]
    if production is not None:
        result = [sample for sample in result if sample.production_mode in production]
    if sys is not None:
        result = [sample for sample in result if any(fnmatch.fnmatch(sample.sys, s) for s in sys)]

    return result


def write(results, fn):
    with open(fn, "w") as f:
        for result in results:
            f.write(result.get_line_csv("\t") + "\n")


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        epilog="""example:
    ./find_samples root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025
    """
    )
    parser.add_argument("base_dir")
    args = parser.parse_args()

    samples_h025_uxAOD = get_samples_h025_uxAOD(args.base_dir)
    write(samples_h025_uxAOD, "samples_h025_uxAOD.txt")
