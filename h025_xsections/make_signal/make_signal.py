#!/usr/bin/env python

import logging
import os
from typing import List

import numpy as np
import ROOT
from h025_xsections.core import definitions, utils
from h025_xsections.core.naming import ALPHA_HI, ALPHA_LOW, MU_CB, NCB_HI, NCB_LOW, SIGMA_CB
from h025_xsections.core.utils import extract_info_from_fitresult, iter_collection
from scipy.optimize import minimize, root_scalar

ROOT.PyConfig.IgnoreCommandLineOptions = True

cb_cxx = os.path.join(os.path.dirname(os.path.abspath(__file__)), "RooTwoSidedCBShape.cxx")

ROOT.gROOT.ProcessLine(".L %s" % cb_cxx)
ROOT.gROOT.SetBatch(True)
utils.silence_roofit(ROOT)

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

SIGMA_DEF = (1.75, 0.5, 2.5)


def create_workspace(signal_model_type: str = "DSCB"):
    ws = ROOT.RooWorkspace("signalWS")
    ws.factory("mass[105, 140]")  # GeV

    if signal_model_type == "DSCB":
        ws.factory("%s[125, 123, 127]" % MU_CB)  # GeV
        ws.factory("%s[%f, %f, %f]" % (SIGMA_CB, SIGMA_DEF[0], SIGMA_DEF[1], SIGMA_DEF[2]))
        ws.factory("%s[1.5323, 0.6, 3]" % ALPHA_LOW)
        ws.factory("%s[9, 0.1, 100]" % NCB_LOW)
        ws.factory("%s[2.0264, 0.6, 3]" % ALPHA_HI)
        ws.factory("%s[5, 0.1, 100]" % NCB_HI)
        ws.factory(
            f"RooTwoSidedCBShape::signal(mass, {MU_CB}, {SIGMA_CB}, {ALPHA_LOW}, {NCB_LOW}, {ALPHA_HI}, {NCB_HI})"
        )
        return ws
    elif signal_model_type == "gaus":
        ws.factory("mu_G[125, 123, 127]")  # GeV
        ws.factory("sigma_G[%f, %f, %f]" % (SIGMA_DEF[0], SIGMA_DEF[1], SIGMA_DEF[2]))
        ws.factory("RooGaussian::signal(mass, mu_G, sigma_G)")
        return ws
    else:
        raise ValueError("cannot understand signal model type %s" % signal_model_type)


def get_histogram(f, region: str, quantity: str, ibin: int):
    hist_name = "histo2D_m_yy_{quantity}_isPassed{fid_region}".format(
        quantity=quantity, fid_region=region
    )
    logging.debug("Getting histogram 2d %s", hist_name)
    hist2d = f.Get(hist_name)
    if not hist2d:
        raise ValueError("cannot open histogram %s from file %s" % (hist_name, f.GetName()))
    hist1d = hist2d.ProjectionY("_projY", ibin, ibin)

    return hist1d


def fit_histo(histo, pdf, obs, debug: bool = False):
    rdh = ROOT.RooDataHist("root_data_hist", "root_data_hist", ROOT.RooArgList(obs), histo)
    if debug:
        fr = pdf.fitTo(rdh, ROOT.RooFit.SumW2Error(True), ROOT.RooFit.Save())
    else:
        fr = pdf.fitTo(
            rdh, ROOT.RooFit.PrintLevel(-1), ROOT.RooFit.SumW2Error(True), ROOT.RooFit.Save()
        )
    return fr


def eval_resolution(ws, fractions: List[float]):
    pdf = ws.pdf("signal")
    obs = ws.var("mass")

    roofit_cdf = pdf.createCdf(obs)

    def cdf(x: float) -> float:
        obs.setVal(x)
        return roofit_cdf.getVal()

    def invcdf(x: float) -> float:
        def f(xx: float) -> float:
            return cdf(xx) - x

        return root_scalar(f, bracket=[obs.getMin(), obs.getMax()])

    def fmin(xx: float, fraction: float) -> float:
        x1 = xx
        y1 = cdf(x1)
        y2 = y1 + fraction
        if y2 > 1:
            return np.inf
        invresult = invcdf(y2)
        x2 = invresult.root
        return x2 - x1

    widths = []
    for fraction in fractions:
        min_results = minimize(fmin, args=(fraction,), x0=0.5 * (obs.getMin() + obs.getMax()))
        widths.append(min_results.fun / 2.0)

    return widths


def fit(histo, ws, output_plots, title_plot: str = None, debug: bool = False):
    pdf = ws.pdf("signal")
    obs = ws.var("mass")
    fr = fit_histo(histo, pdf, obs, debug=debug)

    if output_plots is not None:
        frame = obs.frame()
        if title_plot is not None:
            frame.SetTitle(title_plot)
        rdh = ROOT.RooDataHist("root_data_hist", "root_data_hist", ROOT.RooArgList(obs), histo)
        canvas = ROOT.TCanvas()
        rdh.plotOn(frame)
        pdf.plotOn(frame)
        pdf.paramOn(frame, ROOT.RooFit.Layout(0.1, 0.4, 0.9))
        frame.Draw()
        old_level = ROOT.gErrorIgnoreLevel
        ROOT.gErrorIgnoreLevel = ROOT.kWarning
        for output_plot in output_plots:
            canvas.SaveAs(output_plot)
            if ".root" in output_plot:
                fout = ROOT.TFile.Open(output_plot, "update")
                histo.Write("histo")
        ROOT.gErrorIgnoreLevel = old_level

    return fr


def write_parameters(fr, output_dir: str):
    text = ""
    for par_name, par_value in extract_info_from_fitresult(fr).items():
        text += "%s %.10f\n" % (par_name, par_value[0])
    for par_name, par_value in extract_info_from_fitresult(fr, const=True).items():
        text += "%s %.10f\n" % (par_name, par_value[0])

    logging.debug("Signal parameters:")
    logging.debug(text)

    parameters_output = open("%s/resonance_paramList.txt" % output_dir, "w")
    parameters_output.write(text)


def write_resolutions(fractions: List[float], widths: List[float], output_dir: str):
    text = ""
    for fraction, width in zip(fractions, widths):
        text += "%.10f\t%.10f\n" % (fraction, width)
    widths_output = open("%s/resolution.txt" % output_dir, "w")
    widths_output.write(text)


def check_fit(fit_result: ROOT.RooFitResult):
    msg = []
    for var in iter_collection(fit_result.floatParsFinal()):
        if var.GetName() in (NCB_HI, NCB_LOW):
            continue
        tolerance = (var.getMax() - var.getMin()) * 0.01
        if (var.getVal() - var.getMin()) < tolerance or (var.getMax() - var.getVal()) < tolerance:
            msg.append(
                "variable %s very close to bondary: =%f (%f %f)"
                % (
                    var.GetName(),
                    var.getVal(),
                    var.getMin(),
                    var.getMax(),
                )
            )
    return msg


def fit_file(
    filename: str,
    output_basedir: str,
    output_basedir_plots: str,
    region: str,
    quantity: str,
    do_resolution: bool,
    icat=None,
    plot_ext=None,
    nominal_basedir: str = None,
    debug: bool = False,
    signal_model_type: str = "DSCB",
):

    plot_ext = plot_ext or []

    reader = None
    nominal_signal_par = None
    if nominal_basedir:
        from h025_xsections.reader import Reader

        reader = Reader(nominal_basedir)
        nominal_signal_par = reader.get_signal_pars(region, quantity, basedir=nominal_basedir)

    # generate signal workspace
    ws = create_workspace(signal_model_type)
    ws.saveSnapshot("initial", ws.allVars())

    f = ROOT.TFile.Open(filename)

    if icat is None:
        if "_vs_" in quantity:
            binning_info = definitions.binnings[quantity]
            bins = range(binning_info[0] + 1)
        else:
            bins = range(len(definitions.binnings[quantity]) + 1)
    else:
        bins = [icat]

    logging.info(
        "Fitting process for [Fiducial region: %s, Quantity: %s] for %d bins",
        region,
        quantity,
        len(bins),
    )
    for ibin in bins:
        logging.debug("fitting bin %s", ibin)
        histo = get_histogram(f, region, quantity, ibin)
        if histo.GetEntries() == 0:
            if (ibin != 0 or definitions.show_underflows[quantity]) and (
                ibin != len(bins) - 1 or definitions.show_overflows[quantity]
            ):
                logging.warning(
                    "Bin [Fiducial region: {region}, Quantity: {quantity}, Bin: {ibin}] is empty. Skipping".format(
                        region=region, quantity=quantity, ibin=ibin
                    )
                )
            logging.debug("skipping empty histogram")
            continue
        output_dir = os.path.join(
            output_basedir, signal_model_type, region, quantity, "bin_%s" % ibin
        )
        output_dir_plot = os.path.join(
            output_basedir_plots, signal_model_type, region, quantity, "bin_%s" % ibin
        )
        utils.create_dir_if_not_exists(output_dir)
        utils.create_dir_if_not_exists(output_dir_plot)

        output_plots = None
        if plot_ext:
            if isinstance(plot_ext, str):
                plot_ext = [plot_ext]
            output_plots = [
                "%s/fit_%s_%s_bin_%d.%s" % (output_dir_plot, region, quantity, ibin, ext)
                for ext in plot_ext
            ]

        ws.loadSnapshot("initial")
        if nominal_signal_par is not None:
            if ibin not in nominal_signal_par.columns:
                if (ibin != 0 or definitions.show_underflows[quantity]) and (
                    ibin != len(bins) - 1 or definitions.show_overflows[quantity]
                ):
                    logging.error("cannot find info for nominal fit for bin %s", ibin)
                continue
            ws.var(ALPHA_LOW).setVal(nominal_signal_par.loc[ALPHA_LOW, ibin])
            ws.var(ALPHA_LOW).setConstant(True)
            ws.var(ALPHA_HI).setVal(nominal_signal_par.loc[ALPHA_HI, ibin])
            ws.var(ALPHA_HI).setConstant(True)
            ws.var(NCB_LOW).setVal(nominal_signal_par.loc[NCB_LOW, ibin])
            ws.var(NCB_LOW).setConstant(True)
            ws.var(NCB_HI).setVal(nominal_signal_par.loc[NCB_HI, ibin])
            ws.var(NCB_HI).setConstant(True)

        fr = fit(
            histo,
            ws,
            output_plots,
            title_plot="model: %s, region: %s, quantity: %s, bin: %s"
            % (signal_model_type, region, quantity, ibin),
            debug=debug,
        )

        ROOT.SetOwnership(fr, True)  # otherwise memory leak
        msg = check_fit(fr)
        for m in msg:
            logging.warning("problem with %s %s %s: %s", region, quantity, ibin, msg)
        write_parameters(fr, output_dir)

        if do_resolution:
            fractions = [0.6827, 0.90, 0.95]
            resolutions = eval_resolution(ws, fractions)
            write_resolutions(fractions, resolutions, output_dir)
    f.Close()


if __name__ == "__main__":

    import argparse

    from h025_xsections.core.naming import parse_histo_filename

    parser = argparse.ArgumentParser(
        description="Fit signal shape", formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument("filename", help="file holding m_yy distribution histograms")
    parser.add_argument("--output-dir", default="output")
    parser.add_argument("--quantity", help="variable name")
    parser.add_argument("--region", help="fiducial region")
    parser.add_argument("--bin", type=int, help="variable bin")
    parser.add_argument(
        "--nominal-basedir", help="if specificed some parameters are fixed to the nominal one"
    )
    parser.add_argument("--do-plot", action="store_true")
    parser.add_argument("--save-root", action="store_true")
    parser.add_argument("--signal-model-type", choices=("DSCB", "gaus"), default="DSCB")
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--do-resolution", action='store_true', help='compute the resolution as half smaller window')

    args = parser.parse_args()

    if not os.path.isfile(args.filename):
        raise IOError("filename %s does not exists" % args.filename)

    histo_info = parse_histo_filename(os.path.basename(args.filename))
    output_folder = os.path.join(args.output_dir, "signal_parameters_" + histo_info.format_tags())

    logging.info("INPUT FILE:       %s", args.filename)
    logging.info("OUTPUT DIRECTORY: %s", args.output_dir)

    plot_ext = []
    if args.do_plot:
        plot_ext.append("png")
    if args.save_root:
        plot_ext.append("root")

    fit_file(
        args.filename,
        output_folder,
        output_folder,
        args.region,
        args.quantity,
        args.bin,
        plot_ext,
        args.nominal_basedir,
        debug=args.debug,
        signal_model_type=args.signal_model_type,
        do_resolution=args.do_resolution,
    )
