#!/usr/bin/env python

import argparse
import logging
import os
from glob import glob
from typing import Dict

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from h025_xsections.core import definitions
from h025_xsections.core.naming import parse_histo_filename, SIGMA_CB, MU_CB
from h025_xsections.core.utils import create_dir_if_not_exists

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

parser = argparse.ArgumentParser()
parser.add_argument("input_dir")
parser.add_argument("output_dir")
parser.add_argument("--output-dir-plots")
args = parser.parse_args()

create_dir_if_not_exists(args.output_dir)
create_dir_if_not_exists(args.output_dir_plots)

all_dirs = os.listdir(args.input_dir)
dict_sys_dirs = {}
for d in all_dirs:
    info = parse_histo_filename(d)
    if info.sys not in dict_sys_dirs:
        dict_sys_dirs[info.sys] = d
    else:
        raise ValueError(
            "multiple directories for the same systematic (%s):\n %s\n %s "
            % (info.sys, d, dict_sys_dirs[info.sys])
        )


all_params = {}
for isys, (sys, d) in enumerate(dict_sys_dirs.items(), 1):
    logging.info("%d/%d opening data for systematic %s", isys, len(dict_sys_dirs), sys)
    for region, quantity in definitions.regions_quantities_analysis:
        path = os.path.join(args.input_dir, d, "DSCB", region, quantity)
        bin_directories = glob(os.path.join(path, "*"))
        for bin_directory in bin_directories:
            ibin = int(bin_directory.split("/")[-1].replace("bin_", ""))
            fn = os.path.join(bin_directory, "resonance_paramList.txt")
            if os.path.exists(fn):
                params = pd.read_csv(fn, sep=" ", header=None, index_col=0)[1]
                all_params[(region, quantity, ibin, sys)] = params
        if not all_params:
            logging.error(
                "cannot find any parameter for %s %s %s in %s", sys, region, quantity, path
            )

df = (
    pd.concat(all_params, names=["region", "quantity", "bin", "sys", "param"])
    .unstack("param")
    .sort_index()
)

ranges = {MU_CB: (-0.25, 0.25), SIGMA_CB: (-6, 6)}
latex_parameters = {MU_CB: r"$\mu_{CB}$", SIGMA_CB: r"$\sigma_{CB}$"}

path_scale = os.path.join(args.output_dir, "sys_scale")
path_resolution = os.path.join(args.output_dir, "sys_resolution")
create_dir_if_not_exists(path_scale)
create_dir_if_not_exists(path_resolution)

for region, quantity in definitions.regions_quantities_analysis:
    logging.info("plotting sys for %s %s", region, quantity)

    create_dir_if_not_exists(os.path.join(path_scale, region))
    create_dir_if_not_exists(os.path.join(path_resolution, region))
    create_dir_if_not_exists(os.path.join(path_scale, region, quantity))
    create_dir_if_not_exists(os.path.join(path_resolution, region, quantity))

    if "_vs_" in quantity:
        bin_info = definitions.binnings[quantity]
        nbins_noextraflow = len(np.linspace(bin_info[1], bin_info[2], bin_info[0]))
    else:
        nbins_noextraflow = definitions.nbins_noextraflow[quantity]

    for parameter in MU_CB, SIGMA_CB:
        if parameter == MU_CB:
            path = os.path.join(path_scale, region, quantity)
        elif parameter == SIGMA_CB:
            path = os.path.join(path_resolution, region, quantity)
        import pdb

        df_parameter = df.xs([region, quantity])[parameter].unstack("bin")
        table_parameter = df_parameter / df_parameter.loc["NOMINAL"] - 1
        vmin, vmax = ranges[parameter]
        for direction in "up", "down":
            table_parameter_direction = table_parameter.loc[
                table_parameter.index.str.endswith("__1%s" % direction)
            ]

            if args.output_dir_plots is not None:
                fig, ax = plt.subplots(figsize=(15, 0.15 * table_parameter_direction.shape[1]))
                sns.heatmap(
                    table_parameter_direction.T * 100,
                    center=0,
                    ax=ax,
                    square=True,
                    vmin=vmin,
                    vmax=vmax,
                    xticklabels=table_parameter_direction.index.str.replace(
                        "__1%s" % direction, ""
                    ),
                )
                ax.set_title(
                    "Systematic on %s %s [%%] | region=%s quantity=%s"
                    % (
                        latex_parameters[parameter],
                        direction,
                        region,
                        definitions.latex_labels[quantity],
                    )
                )
                img_fn = os.path.join(
                    args.output_dir_plots,
                    "plotsys_{parameter}_{region}_{quantity}_{direction}.png".format(
                        parameter=parameter, region=region, quantity=quantity, direction=direction
                    ),
                )
                fig.savefig(img_fn, bbox_inches="tight")
                plt.close(fig)

            for sys, values in table_parameter_direction.reindex(
                columns=range(nbins_noextraflow + 2)
            ).iterrows():
                sys_name = "ATLAS_" + sys
                output_fn = os.path.join(path, sys_name + ".csv")
                values.to_csv(output_fn, header=False, na_rep="nan", index=False)
