#!/usr/bin/env python

import argparse
import logging
import os
from glob import glob
from time import time
from typing import List

from h025_xsections.core.naming import Catalogue, FileHistoInfo
from h025_xsections.make_signal import make_signal


logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def is_nominal(histo_info: FileHistoInfo):
    return histo_info.sys == "NOMINAL" and histo_info.sysgroup == "Nominal"


def is_shape_systematic(histo_info: FileHistoInfo):
    return histo_info.sysgroup != "PhotonSys" and (
        "EG_SCALE" in histo_info.sys
        or "EG_RESOLUTION" in histo_info.sys
        or "PH_SCALE" in histo_info.sys
        or "PH_RESOLUTION" in histo_info.sys
    )


def create_catalogue(input_dir: str, systematics: bool):
    all_files = glob(os.path.join(input_dir, "histo_mcAll.*"))

    catalogue = Catalogue.create_catalogue(all_files)
    filter_function = is_shape_systematic if systematics else is_nominal
    catalogue = catalogue.sub_catalogue(mc="mcAll", prod="prodAll", function=filter_function)
    return catalogue


def make_signal_all_sys(
    input_dir: str,
    output_dir: str,
    output_dir_plot: str,
    region: str,
    quantity: str,
    systematics: bool,
    do_plots: bool,
    do_resolution: bool,
    basedir_parameters: str,
    verbose: bool,
    signal_model_type:str="DSCB"
):
    catalogue = create_catalogue(input_dir, systematics)
    if not catalogue:
        logging.warning("cannot find any file to be fitted in %s", input_dir)
    all_files_to_be_fitted = sorted(catalogue.items(), key=lambda x: x[0].sys)

    for histo_info, fn in all_files_to_be_fitted:
        logging.info("{:>35}: {}".format(histo_info.sys, os.path.basename(fn)))

    if len(set(x[0].sys for x in all_files_to_be_fitted)) != len(all_files_to_be_fitted):
        raise ValueError("there are duplicate systematics")

    logging.info("found %d files to be fitted", len(all_files_to_be_fitted))

    for ifn, (histo_info, fn) in enumerate(all_files_to_be_fitted, 1):
        logging.info("(%d/%d) fitting %s", ifn, len(all_files_to_be_fitted), histo_info.sys)

        plot_ext = ["png", "root"] if do_plots else []
        if basedir_parameters is not None:
            nominal_basedir = basedir_parameters
        else:
            nominal_basedir = output_dir if systematics else None
        output_folder = os.path.join(output_dir, "signal_parameters_" + histo_info.format_tags())
        output_plot_folder = os.path.join(output_dir_plot, "signal_parameters_" + histo_info.format_tags())
        if not verbose:
            logging.getLogger().setLevel(logging.ERROR)
        start = time()
        make_signal.fit_file(
            fn,
            output_folder,
            output_plot_folder,
            region,
            quantity,
            do_resolution,
            None,
            plot_ext=plot_ext,
            nominal_basedir=nominal_basedir,
            signal_model_type=signal_model_type
        )
        if not verbose:
            logging.getLogger().setLevel(logging.INFO)
        logging.info("done in %.2f seconds", time() - start)
    logging.info(
        "signal model computed for all the %d systematic variations", len(all_files_to_be_fitted)
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir")
    parser.add_argument("output_dir", default="output")
    parser.add_argument("--region")
    parser.add_argument("--quantity")
    parser.add_argument("--systematics", action="store_true")
    parser.add_argument("--basedir-parameters")
    parser.add_argument("--do-plots", action="store_true")
    parser.add_argument("--verbose", action="store_true")
    parser.add_argument("--do-resolution", action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    make_signal_all_sys(
        args.input_dir,
        args.output_dir,
        args.output_dir,
        args.region,
        args.quantity,
        args.systematics,
        args.do_plots,
        args.do_resolution,
        args.basedir_parameters,
        args.verbose,
    )
