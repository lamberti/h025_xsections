#!/usr/bin/env python

import logging
import os
from typing import Dict, List

import matplotlib
import numpy as np
import ROOT

from h025_xsections.core import definitions
from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.histo_description import (
    HistoDescription,
    histo_description_region_quantity,
)
from h025_xsections.core.histo_utils import (
    TH2F2np,
    get_content_binning_from_file,
    plot1D,
    plot2D,
    plot_stack,
)
from h025_xsections.core.naming import parse_histo_filename
from h025_xsections.core.utils import create_dir_if_not_exists
from matplotlib import pyplot as plt

matplotlib.use("Agg")

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def get_all_histo1d(f, quantity: str, region: str, expected_binning=None) -> Dict:
    """
    return all the interesting 1D histogram from the file,
    in the form {HistoDescription: histogram, ...}
    """
    all_histo1d = {}
    for key in f.GetListOfKeys():
        histo_name = key.GetName()
        if "histo1D" not in histo_name:
            continue
        histo_description = HistoDescription.from_name(histo_name)

        # skip histograms not for this quantity
        if histo_description.quantity != quantity:
            continue
        # skip histograms requiring isFiducial but not for this region
        if (
            not histo_description.isFiducial.is_any()
            and histo_description.isFiducial.name != "Fiducial" + region
        ):
            continue
        # skip histogram requiring isPassed, but not for this region
        if (
            not histo_description.isPassed.is_any()
            and histo_description.isPassed.name != "Passed" + region
        ):
            continue
        # skip histogram not divided by isDalitz / noDalitz:
        # this is much more safe since in H7 there are no-Dalitz
        if histo_description.isDalitz.is_any():
            continue
        content, binning_from_h = get_content_binning_from_file(f, histo_name)
        if expected_binning is not None and not np.all(expected_binning == binning_from_h):
            raise ValueError(
                "ERROR: binning of the histogram %s (%s) different from the expected (%s)"
                % (histo_name, binning_from_h, expected_binning)
            )
        all_histo1d[histo_description] = content

    return all_histo1d


def add_missing_predicates(all_histo1d: Dict, HD):
    all_histo1d[HD(recotrue="reco", isDalitz=True, isPassed=True)] = (
        all_histo1d[HD(recotrue="reco", isDalitz=True, isPassed=True, isFiducial=False)]
        + all_histo1d[HD(recotrue="reco", isDalitz=True, isPassed=True, isFiducial=True)]
    )

    all_histo1d[HD(recotrue="reco", isPassed=True)] = (
        all_histo1d[HD(recotrue="reco", isFiducial=True, isPassed=True, isDalitz=True)]
        + all_histo1d[HD(recotrue="reco", isFiducial=True, isPassed=True, isDalitz=False)]
        + all_histo1d[HD(recotrue="reco", isFiducial=False, isPassed=True, isDalitz=True)]
        + all_histo1d[HD(recotrue="reco", isFiducial=False, isPassed=True, isDalitz=False)]
    )


def get_efficiency(
    f: str,
    output_basedir: str,
    region: str,
    quantity: str,
    sys: str = "NOMINAL",
    do_plots: bool = False,
):
    # convenient method, with HD it is easy to create HistoDescriptions
    if region not in definitions.fiducial_regions:
        raise ValueError("region '%s' unknown" % region)
    if (
        quantity not in definitions.quantities
        and quantity not in definitions.quantities_2d
        and quantity != "constant"
    ):
        raise ValueError("quantity '%s' unknown" % quantity)

    HD = histo_description_region_quantity(region=region, quantity=quantity)

    if not isinstance(f, ROOT.TFile):
        f = ROOT.TFile.Open(f)

    info = parse_histo_filename(f.GetName())

    mc = info.mc
    prod = info.prod

    suffix_output = "{region}_{obs}_{mc}_prod{prod}_{sys}".format(
        region=region, obs=quantity, mc=mc, prod=prod, sys=sys
    )

    luminosity_pb = definitions.luminosity[mc]

    latex_label = definitions.latex_labels[quantity]
    binning = definitions.binnings[quantity]  # without under/over-flow
    if isinstance(binning, tuple) and len(binning) == 3:
        binning = np.linspace(binning[1], binning[2], binning[0] + 1)
    else:
        binning = np.array(binning)
    bin_labels_extraflow = definitions.bin_labels[quantity]
    show_overflow = definitions.show_overflows[quantity]
    show_underflow = definitions.show_underflows[quantity]

    do_normalization = definitions.do_normalizations[quantity]
    do_log_x = definitions.do_logs_x[quantity]

    binning_with_extraflow = np.concatenate([[-np.inf], np.array(binning), [np.inf]])

    logging.info("SELECTED QUANTITY:               %s", quantity)
    logging.info("fiducial region:                 %s", region)
    logging.info("sys:                             %s", sys)
    logging.debug("label:                           %s", latex_label)
    logging.debug("show overflow:                   %s", show_overflow)
    logging.debug("show underflow:                  %s", show_underflow)
    logging.debug("do normalization:                %s", do_normalization)
    logging.debug("do log-x:                        %s", do_log_x)
    logging.info("binning:                         %s", binning)
    logging.debug("binning with extraflows          %s", binning_with_extraflow)
    logging.debug("bin labes with extraflows        %s", bin_labels_extraflow)

    # Create output directory
    output_dirs = {
        "folding_matrices": "{output_basedir}/folding_matrices/{region}/{quantity}/",
        "plot1D": "{output_basedir}/plot1D/{region}/{quantity}/",
        "folding_matrices_plot": "{output_basedir}/folding_matrices/{region}/{quantity}/plot/",
        "xsections": "{output_basedir}/xsections/{region}/{quantity}",
        "acceptances": "{output_basedir}/acceptances/{region}/{quantity}",
        "cfactors": "{output_basedir}/cfactors/{region}/{quantity}",
        "cfactors_plot": "{output_basedir}/cfactors/{region}/{quantity}/plot",
        "cond_number": "{output_basedir}/cond_number/{region}/{quantity}",
        "nreco_isPassed": "{output_basedir}/nreco_isPassed/{region}/{quantity}",
        "ntrue_isFiducial_noDalitz": "{output_basedir}/ntrue_isFiducial_noDalitz/{region}/{quantity}",
    }

    output_dirs = {
        k: v.format(output_basedir=output_basedir, quantity=quantity, region=region)
        for k, v in output_dirs.items()
    }

    for output_dir in output_dirs.values():
        create_dir_if_not_exists(output_dir)

    latex_label_with_unit = latex_label
    if definitions.units_final.get(quantity, 1) == definitions.GeV:
        latex_label_with_unit += " [GeV]"

    all_histo1d = get_all_histo1d(f, quantity, region, binning_with_extraflow)
    add_missing_predicates(all_histo1d, HD)

    if HD(recotrue="true") not in all_histo1d:
        logging.info("this is a skimmed sample")

    # Plots 1D
    if do_plots:
        logging.debug("Producing %d 1D plots", len(all_histo1d))
        for description in all_histo1d:
            content = all_histo1d[description]
            fig = plot1D(
                binning,
                content,
                bin_labels_extraflow,
                "{name}_{mc}_prod{prod}_LUMI_{lumi}".format(
                    name=description.name(),
                    mc=mc,
                    prod=prod,
                    lumi=luminosity_pb,
                ),
                latex_label_with_unit,
                show_underflow,
                show_overflow,
                do_normalization,
                do_log_x,
                color=description.get_color(),
            )
            for fmt in ("png",):  # no pdf to save space
                fig.savefig(
                    "{base_dir}/histo_{name}_sys_{sys}.{fmt}".format(
                        base_dir=output_dirs["plot1D"],
                        name=description.name(),
                        sys=sys,
                        fmt=fmt,
                    ),
                    bbox_inches="tight",
                )
            plt.close(fig)

    logging.debug("Yield (including under/over-flow):")
    for description in all_histo1d:
        logging.debug(
            "  {recotrue}  fiducial: {fiducial:9} dalitz: {dalitz:9} passed: {passed:9} events: {value:8.2f}".format(
                recotrue=description.recotrue,
                fiducial=str(description.isFiducial.value),
                dalitz=str(description.isDalitz.value),
                passed=str(description.isPassed.value),
                value=all_histo1d[description].sum(),
            )
        )

    # Get weights
    weight_initial_plusDalitz = f.Get("cutflow_weighted").GetBinContent(3)
    weight_initial_notDalitz = f.Get("cutflow_weighted_noDalitz").GetBinContent(3)
    weight_initial_isDalitz = weight_initial_plusDalitz - weight_initial_notDalitz

    dalitz_fraction = weight_initial_isDalitz / weight_initial_plusDalitz
    Br_isDalitz = weight_initial_isDalitz / weight_initial_notDalitz * definitions.Br

    logging.info("Dalitz fraction: %f", dalitz_fraction)
    logging.info("Dalitz Br: %f", Br_isDalitz)

    nevents_total_noDalitz = f.Get("cutflow_weighted_xsec_Br_lumi_normalized").GetBinContent(3)
    nevents_total_isDalitz = nevents_total_noDalitz * Br_isDalitz / definitions.Br

    acceptance = (
        all_histo1d[HD("true", isFiducial=True, isDalitz=False)].sum() / nevents_total_noDalitz
    )
    logging.info("acceptance: %f", acceptance)

    # Cross sections for WS, including under/overflow (not including Br)
    xsections_WS = all_histo1d[HD(recotrue="true", isFiducial=True, isDalitz=False)] / (
        luminosity_pb * definitions.Br
    )
    np.savetxt(
        os.path.join(
            output_dirs["xsections"],
            "XSections_isFiducial_notDalitz_{suffix}.csv".format(suffix=suffix_output),
        ),
        xsections_WS,
        delimiter=",",
    )

    logging.info("xs fiducial = %f pb", xsections_WS.sum())
    logging.info("xs fiducial x Br = %f pb", (xsections_WS.sum() * definitions.Br))

    logging.debug("all fiducial xsections:")
    for ibin, xs in enumerate(xsections_WS):
        logging.debug("    bin %d: %f", ibin, xs)

    bin_width = binning[1:] - binning[:-1]  # type: ignore

    logging.info("XSections for paper [xsections * Br / bin width]:")
    for ibin, xs in enumerate(xsections_WS[1:-1] * definitions.Br / bin_width * 1000, 1):
        logging.info("    bin %d: %f", ibin, xs)

    if do_plots:
        fig = plot_stack(
            binning,
            [
                all_histo1d[HD(recotrue="true", isFiducial=True, isDalitz=False)],
                all_histo1d[HD(recotrue="reco", isPassed=True)],
            ],
            (r"true $H\to\gamma\gamma$ fiducial", r"reco"),
            bin_labels_extraflow,
            latex_label_with_unit,
            show_underflow=show_underflow,
            show_overflow=show_overflow,
            do_normalization=do_normalization,
            do_log_x=do_log_x,
            stacked=False,
        )

        for fmt in "png", "pdf":
            fig.savefig(
                "{base_dir}/histo_reco_true_{suffix}.{fmt}".format(
                    base_dir=output_dirs["plot1D"], suffix=suffix_output, fmt=fmt
                ),
                bbox_inches="tight",
            )
        plt.close(fig)

    # Correction factors
    numerator_d = HD(recotrue="reco", isPassed=True)
    denominator_d = HD(recotrue="true", isFiducial=True, isDalitz=False)

    np.savetxt(
        os.path.join(
            output_dirs["nreco_isPassed"],
            "nreco_isPassed_{suffix}.csv".format(suffix=suffix_output),
        ),
        all_histo1d[numerator_d],
        delimiter=",",
    )

    np.savetxt(
        os.path.join(
            output_dirs["ntrue_isFiducial_noDalitz"],
            "ntrue_isFiducial_noDalitz_{suffix}.csv".format(suffix=suffix_output),
        ),
        all_histo1d[denominator_d],
        delimiter=",",
    )

    with np.errstate(invalid="ignore"):
        cfactors = all_histo1d[numerator_d] / all_histo1d[denominator_d]
    np.savetxt(
        os.path.join(
            output_dirs["cfactors"],
            "cfactors_{suffix}.csv".format(suffix=suffix_output),
        ),
        cfactors,
        delimiter=",",
    )

    logging.info("cfactors")
    for ibin, cfactor in enumerate(cfactors):
        logging.info(
            "    bin {}: {:.3f} / {:.3f} = {}".format(
                ibin,
                all_histo1d[numerator_d][ibin],
                all_histo1d[denominator_d][ibin],
                cfactor,
            )
        )

    if do_plots:
        fig, ax = plt.subplots()
        ax.bar(np.arange(len(cfactors)), cfactors)
        ax.set_xticks(np.arange(len(cfactors)))
        ax.set_xticklabels(bin_labels_extraflow, rotation=90)
        ax.set_title("{mc} {prod} {sys}".format(mc=mc, prod=prod, sys=sys))
        ax.set_xlabel(latex_label_with_unit)
        ax.set_ylabel("cfactor")

        for fmt in "png", "pdf":
            fig.savefig(
                "{base_dir}/cfactors_sys_{suffix}.{fmt}".format(
                    base_dir=output_dirs["cfactors_plot"],
                    suffix=suffix_output,
                    fmt=fmt,
                ),
                bbox_inches="tight",
            )
        plt.close(fig)

    histo2D_isFiducial_notDalitz_isPassed = f.Get(
        "histo2D_{quantity}_isFiducial{region}_notDalitz_isPassed{region}".format(
            region=region, quantity=quantity
        )
    )
    content2D_isFiducial_notDalitz_isPassed, binningx, binningy = TH2F2np(
        histo2D_isFiducial_notDalitz_isPassed
    )

    if not (
        np.all(binning_with_extraflow == binningy) and np.all(binning_with_extraflow == binningx)
    ):
        raise ValueError(
            "binning not as expected. Expected = %s, x = %s, y = %s"
            % (binning_with_extraflow, binningx, binningy)
        )

    if not np.isclose(
        content2D_isFiducial_notDalitz_isPassed.sum(),
        all_histo1d[HD(recotrue="reco", isFiducial=True, isDalitz=False, isPassed=True)].sum(),
    ):
        logging.warning("sum of 2D content isFiducial_noDalitz_isPassed different from 1D")

    if not np.isclose(
        all_histo1d[
            HD(
                recotrue="reco",
                isFiducial=True,
                isDalitz=False,
                isPassed=True,
            )
        ][1],
        content2D_isFiducial_notDalitz_isPassed.sum(axis=0)[1],
    ):
        logging.warning("sum of 2D content isFiducial_notDalitz_isPassed different from 1D")

    content2d_extended = np.vstack(
        [
            all_histo1d[HD(recotrue="reco", isDalitz=False, isFiducial=False, isPassed=True)],
            all_histo1d[
                HD(
                    recotrue="reco",
                    isDalitz=True,
                    isPassed=True,
                )
            ],
            content2D_isFiducial_notDalitz_isPassed,
        ]
    )

    notPassed_isFiducial_notDalitz = (
        all_histo1d[
            HD(
                recotrue="true",
                isFiducial=True,
                isDalitz=False,
            )
        ]
        - content2D_isFiducial_notDalitz_isPassed.sum(axis=1)
    )

    notPassed_noDalitz = (
        nevents_total_noDalitz - all_histo1d[HD(recotrue="reco", isPassed=True)].sum()
    )

    notPassed_notFiducial_notDalitz = notPassed_noDalitz - notPassed_isFiducial_notDalitz.sum()

    notPassed_isDalitz = (
        nevents_total_isDalitz - all_histo1d[HD("reco", isDalitz=True, isPassed=True)].sum()
    )
    if notPassed_isDalitz < 0:
        logging.warning("somethind WRONG setting to zero")
        notPassed_isDalitz = 0

    notPassed = np.concatenate(
        [
            [notPassed_notFiducial_notDalitz],
            [notPassed_isDalitz],
            notPassed_isFiducial_notDalitz,
        ]
    )

    content2d_extended = np.hstack([np.expand_dims(notPassed, axis=1), content2d_extended])

    bin_labels_x = ["notPassed"] + bin_labels_extraflow
    bin_labels_y = ["notFiducial & notDatlitz", "isDalitz"] + bin_labels_extraflow

    if do_plots:
        fig, ax = plot2D(
            content2d_extended,
            latex_label_with_unit,
            bin_labels_x,
            bin_labels_y,
            0.0,
            None,
            ".0f",
        )
        ax.set_title("Yield {mc} {prod} {sys}".format(sys=sys, prod=prod, mc=mc))
        ax.invert_yaxis()

        for fmt in "png", "pdf":
            fig.savefig(
                "{basedir}/yield2d_{mc}_{prod}_sys{sys}.{fmt}".format(
                    basedir=output_dirs["folding_matrices_plot"],
                    mc=mc,
                    prod=prod,
                    sys=sys,
                    fmt=fmt,
                ),
                bbox_inches="tight",
            )
        plt.close(fig)

    with np.errstate(invalid="ignore"):
        folding_matrix = (content2d_extended.T / content2d_extended.sum(axis=1)).T

    if do_plots:
        fig, ax = plot2D(
            folding_matrix * 100,
            latex_label_with_unit,
            bin_labels_x,
            bin_labels_y,
            0.0,
            None,
            ".1f",
        )
        ax.set_title(
            "Folding matrix $P[reco|true]$ {mc}_{prod}_sys{sys}_LUMI_{lumi} $[%]$".format(
                lumi=luminosity_pb, sys=sys, prod=prod, mc=mc
            )
        )
        ax.invert_yaxis()

        for fmt in "png", "pdf":
            fig.savefig(
                "{basedir}/folding_{mc}_{prod}_sys{sys}.{fmt}".format(
                    basedir=output_dirs["folding_matrices_plot"],
                    mc=mc,
                    prod=prod,
                    sys=sys,
                    fmt=fmt,
                ),
                bbox_inches="tight",
            )
        plt.close(fig)

    folding_matrix_is_passed = folding_matrix[:, 1:]

    np.savetxt(
        "{basedir}/folding_matrix_isPassed_for_workspace_{suffix}.csv".format(
            basedir=output_dirs["folding_matrices"], suffix=suffix_output
        ),
        folding_matrix_is_passed,
        delimiter=",",
    )

    empty_underflow = (content2D_isFiducial_notDalitz_isPassed.sum(axis=0)[0] == 0) and (
        content2D_isFiducial_notDalitz_isPassed.sum(axis=1)[0] == 0
    )
    empty_overflow = (content2D_isFiducial_notDalitz_isPassed.sum(axis=0)[-1] == 0) and (
        content2D_isFiducial_notDalitz_isPassed.sum(axis=1)[-1] == 0
    )

    a = 1 if empty_underflow else None
    b = -1 if empty_overflow else None

    try:
        cond_number = np.linalg.cond(folding_matrix_is_passed[2:, :][a:b, a:b])
        np.savetxt(
            "{basedir}/cond_{suffix}.csv".format(
                basedir=output_dirs["cond_number"],
                suffix=suffix_output,
            ),
            np.array([cond_number]),
            delimiter=",",
            fmt="%.5e",
        )
    except ValueError:
        logging.error("problem computing condition number with suffix %s", suffix_output)


def get_efficiency_all(
    fn: str,
    output_basedir: str,
    regions: List[str] = None,
    quantities: List[str] = None,
    sys: str = "NOMINAL",
    do_plots: bool = False,
):
    """
    run the efficiency computation for one file all the regions x quantities
    """
    create_dir_if_not_exists(output_basedir)
    regions_quantities_analysis = get_region_quantities(regions, quantities)
    for region, quantity in regions_quantities_analysis:
        logging.info("ask to efficiency for compute %s %s", region, quantity)

    f = ROOT.TFile.Open(fn)

    for region, quantity in regions_quantities_analysis:
        get_efficiency(f, output_basedir, region, quantity, sys, do_plots=do_plots)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        description="Produce xsections, c-factors & folding matrices from root files"
    )
    parser.add_argument("input", help="input ROOT files with all the histograms normalized")
    parser.add_argument("output_basedir")
    parser.add_argument("--quantities", nargs="*")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--do-plots", action="store_true")
    parser.add_argument("--verbose", action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.debug("turning on debug messages")

    info = parse_histo_filename(args.input)

    logging.info("mc_name :                        %s", info.mc)
    logging.info("luminosity:                      %.2f /pb", definitions.luminosity[info.mc])
    logging.info("production_mode:                 %s", info.prod)
    logging.info("prod xsection:                   %s pb", definitions.xsections[info.prod])

    get_efficiency_all(
        args.input,
        args.output_basedir,
        args.regions,
        args.quantities,
        sys=info.sys,
        do_plots=args.do_plots,
    )
