#!/usr/bin/env python

import logging
import os

import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
import pandas as pd

from h025_xsections.core import definitions
from h025_xsections.core.utils import create_dir_if_not_exists
from h025_xsections.reader import Reader

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def compute_sys_cfactor(
    reader: Reader,
    output_folder: str,
    yield_variations,
    region: str,
    quantity: str,
    output_dir_plots: str,
):
    create_dir_if_not_exists(output_folder)

    all_cfactors = {}
    for sysgroup, sys in yield_variations:
        cfactors = reader.get_cfactors(region, quantity, sysgroup=sysgroup, sys=sys)
        all_cfactors[(sysgroup, sys)] = cfactors

    all_cfactors = pd.DataFrame(all_cfactors).T.sort_index()

    all_cfactors_no_jet34 = all_cfactors[
        all_cfactors.index.map(lambda idx: idx[0] not in ("JetSys3", "JetSys4"))
    ]
    all_sys = all_cfactors_no_jet34 / all_cfactors_no_jet34.loc["Nominal", "NOMINAL"] - 1
    all_sys = all_sys.droplevel(0)

    all_cfactors_jet34 = all_cfactors[
        all_cfactors.index.map(lambda idx: idx[0] in ("JetSys3", "JetSys4"))
    ]
    all_sys34 = all_cfactors_jet34.loc["JetSys3"] / all_cfactors_jet34.loc["JetSys4"] - 1
    intersection = all_sys.index.intersection(all_sys34.index)
    assert len(intersection) == 0

    all_sys = pd.concat([all_sys, all_sys34])
    all_sys = all_sys.drop("NOMINAL")

    for sys, values in all_sys.iterrows():
        output_fn_binbybin = os.path.join(output_folder, "ATLAS_" + sys + ".csv")
        np.savetxt(output_fn_binbybin, values, delimiter=",")

    create_dir_if_not_exists(output_dir_plots)
    if output_dir_plots is not None:
        logging.info("producing cfactor plots for region: %s quantity: %s", region, quantity)
        err_cfactors = {}
        for direction in "up", "down":
            fig, ax = plt.subplots(figsize=(20, 0.20 * all_sys.shape[1]))
            df_direction = all_sys.loc[all_sys.index.str.endswith("__1%s" % direction)].sort_index()
            vmax = df_direction.abs().max().max()
            sns.heatmap(
                df_direction.T * 100,
                center=0,
                ax=ax,
                square=True,
                vmin=-vmax * 100,
                vmax=vmax * 100,
                xticklabels=df_direction.index.str.replace("__1%s" % direction, ""),
            )
            ax.set_title(
                "Systematic on cfactor %s [%%] | region=%s quantity=%s"
                % (direction, region, definitions.latex_labels[quantity])
            )
            img_fn = os.path.join(
                output_dir_plots,
                "plotsys_cfactor_{region}_{quantity}_{direction}.png".format(
                    region=region, quantity=quantity, direction=direction
                ),
            )
            fig.savefig(img_fn, bbox_inches="tight")
            plt.close(fig)

            err_cfactors[direction] = np.sqrt((df_direction ** 2).sum(axis=0))

        fig, ax = plt.subplots()
        latex_label = definitions.latex_labels[quantity]
        latex_label_with_unit = latex_label
        if definitions.units_final.get(quantity, 1) == definitions.GeV:
            latex_label_with_unit += " [GeV]"
        ax.errorbar(
            range(all_sys.shape[1]),
            all_cfactors.loc[("Nominal", "NOMINAL")],
            yerr=(err_cfactors["down"], err_cfactors["up"]),
            fmt=".",
        )
        bin_labels_extraflow = definitions.bin_labels[quantity]
        ax.set_xticks(range(all_sys.shape[1]))
        ax.set_xticklabels(bin_labels_extraflow, rotation=90)

        img_fn = os.path.join(
            output_dir_plots,
            "plot_cfactor_{region}_{quantity}.png".format(region=region, quantity=quantity),
        )
        ax.set_title("region: %s, quantity: %s" % (region, quantity))
        ax.set_xlabel(latex_label_with_unit)
        ax.set_ylabel("cfactor")

        fig.savefig(img_fn, bbox_inches="tight")
        plt.close()


def compute_sys_matrix(
    reader: Reader, output_folder: str, yield_variations, region: str, quantity: str
):
    create_dir_if_not_exists(output_folder)

    all_matrices = {}
    for sysgroup, sys in yield_variations:
        matrix = reader.get_matrix(region, quantity, sysgroup=sysgroup, sys=sys)
        all_matrices[(sysgroup, sys)] = matrix
    all_matrices = pd.concat(all_matrices).sort_index()

    all_matrix_no_jet34 = all_matrices[
        all_matrices.index.map(lambda idx: idx[0] not in ("JetSys3", "JetSys4"))
    ]
    all_sys = all_matrix_no_jet34 / all_matrix_no_jet34.loc["Nominal", "NOMINAL"] - 1
    all_sys = all_sys.droplevel(0)

    all_matrix_jet34 = all_matrices[
        all_matrices.index.map(lambda idx: idx[0] in ("JetSys3", "JetSys4"))
    ]
    all_sys_jet34 = all_matrix_jet34.loc["JetSys3"] / all_matrix_jet34.loc["JetSys4"] - 1

    all_sys = pd.concat([all_sys, all_sys_jet34])
    all_sys = all_sys.drop("NOMINAL")
    all_sys.index = all_sys.index.remove_unused_levels()

    for sys in all_sys.index.levels[0]:
        if sys == "NOMINAL":
            continue
        values = all_sys.loc[sys].values
        output_fn_matrix = os.path.join(output_folder, "ATLAS_" + sys + ".csv")
        np.savetxt(output_fn_matrix, values, delimiter=",")


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        epilog="""Example:
compute_systematics.py ../../tmp-results output --sys-names EG_RESOLUTION_ALL__1up")
"""
    )
    parser.add_argument("basedir")
    parser.add_argument("output_dir")
    parser.add_argument("--method", choices=["bin_by_bin", "matrix"])
    parser.add_argument("--region")
    parser.add_argument("--quantity")
    parser.add_argument("--output-dir-plots")
    parser.add_argument("--debug", action="store_true")
    args = parser.parse_args()

    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    reader = Reader(args.basedir)

    yield_variations = reader.get_variations(only_yield=True)
    logging.info("found %d systematic variation (including nominal)", len(yield_variations))

    logging.info("doing %s %s", args.region, args.quantity)

    if args.method == "bin_by_bin":
        compute_sys_cfactor(
            reader,
            args.output_dir,
            yield_variations,
            args.region,
            args.quantity,
            args.output_dir_plots,
        )
    elif args.method == "matrix":
        compute_sys_matrix(reader, args.output_dir, yield_variations, args.region, args.quantity)
