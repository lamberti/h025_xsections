#!/usr/bin/env python

import argparse
import logging
import os
from glob import glob
from time import time

from h025_xsections.core.naming import parse_histo_filename
from h025_xsections.efficiency import get_efficiency

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

parser = argparse.ArgumentParser()
parser.add_argument("input_dir")
parser.add_argument("output_dir", default="output")
args = parser.parse_args()

all_files = glob(os.path.join(args.input_dir, "histo_mcAll.*.isPass_or_isFiducial.*"))
if not all_files:
    raise IOError("cannot find any file in %s" % args.input_dir)

all_files_yield = []
for fname in all_files:
    histo_info = parse_histo_filename(fname)
    # prodAll mcAll all systematics except for PER/PES detailed
    to_save_prod_all = (
        histo_info.prod == "prodAll"
        and histo_info.mc == "mcAll"
        and histo_info.skim == "isPass_or_isFiducial"
    )
    to_save_prod_single = (
        histo_info.prod != "prodAll"
        and histo_info.mc == "mcAll"
        and histo_info.skim == "isPass_or_isFiducial"
        and histo_info.sys == "NOMINAL"
    )

    if to_save_prod_all or to_save_prod_single:
        logging.info(
            "{:<5} {:>50}: {}".format(histo_info.prod, histo_info.sys, os.path.basename(fname))
        )
        all_files_yield.append(os.path.basename(fname))

if not all_files_yield:
    raise IOError("cannot find proper files in directory %s" % args.input_dir)

logging.info("found %d files to be fitted", len(all_files_yield))
for ifn, fname in enumerate(all_files_yield, 1):
    histo_info = parse_histo_filename(fname)
    logging.info(
        "(%d/%d) computing yields for %s %s",
        ifn,
        len(all_files_yield),
        histo_info.prod,
        histo_info.sys,
    )
    do_plots = ("NOMINAL" in histo_info.sys and histo_info.prod == 'prodAll')
    output_folder = os.path.join(args.output_dir, "efficiencies_" + histo_info.format_tags())
    logging.getLogger().setLevel(logging.ERROR)
    start = time()
    get_efficiency.get_efficiency_all(
        os.path.join(args.input_dir, fname), output_folder, sys=histo_info.sys, do_plots=do_plots,
    )
    logging.getLogger().setLevel(logging.INFO)
    logging.info("done in %.2f seconds", time() - start)

logging.info("efficiencies computed for all the %d systematic variations", len(all_files_yield))
