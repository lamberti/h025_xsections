#!/usr/bin/env python

import logging
import os
import tempfile
from itertools import product
from shutil import copyfile, rmtree
import subprocess

import numpy as np
import pandas as pd

from h025_xsections.core import definitions
from h025_xsections.core.utils import create_dir_if_not_exists
from h025_xsections.core.definitions import regions_quantities_analysis
from h025_xsections.reader import Reader

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def check_latex(try_install=True):
    try:
        status = subprocess.call(
            "pdflatex --version".split(), stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w")
        )
        if status != 0:
            return False
    except FileNotFoundError:
        return False

    def is_sty_installed(sty_name):
        status = subprocess.call(
            ["kpsewhich", sty_name], stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w")
        )
        return not bool(status)

    for package in ("standalone", "inputenc", "booktabs", "siunitx", "graphicx"):
        if not is_sty_installed("%s.sty" % package):
            logging.error("package %s is not installed", package)
            install_latex_package(package)
            return False

    return True


def install_latex_package(package):
    if package == "standalone":
        print(
            """you can install package with the following:
cd
mkdir texmf/tex/latex/
cd texmf/tex/
wget http://mirrors.ctan.org/install/macros/latex/contrib/standalone.tds.zip
unzip standalone.tds.zip
texhash ~/texmf
"""
        )


def readall_wrapper_from_reader(reader, function_name):
    function = getattr(reader, function_name)

    def read_from_reader(_, region_quantities, mcs, prods):
        all_df = {}
        for (region, quantity), mc, prod in product(region_quantities, mcs, prods):
            try:
                df = function(region, quantity, mc=mc, prod=prod)
                all_df[(region, quantity, mc, prod)] = df
            except IOError:
                logging.warning(
                    "cannot evaluate function for region=%s, quantity=%s, mc=%s, prod=%",
                    region,
                    quantity,
                    mc,
                    prod,
                )
                continue
        df = pd.concat(all_df, names=["region", "quantity", "mc", "prod"])
        return df

    return read_from_reader


def read_all_csv(folder_scheme, region_quantities, mcs, prods):
    all_df = {}
    if "{bin}" not in folder_scheme:
        for (region, quantity), mc, prod in product(region_quantities, mcs, prods):
            fn = folder_scheme.format(region=region, quantity=quantity, mc=mc, prod=prod)
            try:
                df = pd.read_csv(fn, header=None)
                df.index.name = "bin"
                all_df[(region, quantity, mc, prod)] = df
            except IOError:
                logging.warning("cannot find file %s", fn)
                continue
        if not all_df:
            return None
        df = pd.concat(all_df, names=["region", "quantity", "mc", "prod"])
        return df
    else:
        raise NotImplementedError


def read_signal_parameters(folder_scheme, regions_quantities, mcs, prods):
    all_df = {}
    for (region, quantity), mc, prod in product(regions_quantities, mcs, prods):
        nbins = len(definitions.binnings[quantity])
        all_df_bins = {}
        for ibin in range(nbins + 1):
            fn = folder_scheme.format(region=region, quantity=quantity, mc=mc, prod=prod, bin=ibin)
            try:
                df = pd.read_csv(fn, header=None, sep=" ").set_index(0)[1]
                all_q = [
                    "muCBNom_SM",
                    "sigmaCBNom_SM",
                    "alphaCBLo_SM",
                    "alphaCBHi_SM",
                    "nCBLo_SM",
                    "nCBHi_SM",
                    "sigYield_SM",
                ]
                all_q_new = [
                    "mu_cb",
                    "sigma_cb",
                    "alpha_low",
                    "alpha_hi",
                    "ncb_low",
                    "ncb_hi",
                    "yield",
                ]
                m = {
                    "{q}_{quantity}_{bin}_m125000_c0".format(quantity=quantity, bin=ibin, q=q): qnew
                    for (q, qnew) in zip(all_q, all_q_new)
                }
                m.update(
                    {
                        "{q}_{quantity}_{bin}_c0".format(quantity=quantity, bin=ibin, q=q): qnew
                        for (q, qnew) in zip(all_q, all_q_new)
                    }
                )
                df = df.rename(m, axis=0)
                df.index.name = "parameter"
                all_df_bins[ibin] = df

            except FileNotFoundError:
                if (ibin != 0 or definitions.show_underflows[quantity] is True) and (
                    ibin != nbins or definitions.show_overflows[quantity] is True
                ):
                    logging.warning(
                        "cannot find file %s",
                        folder_scheme.format(
                            region=region, quantity=quantity, mc=mc, prod=prod, bin=ibin
                        ),
                    )
                continue
        if not all_df_bins:
            logging.error(
                "cannot find any info for %s %s %s %s for signal parameters",
                region,
                quantity,
                mc,
                prod,
            )
            continue
        df_all_bins = pd.concat(all_df_bins, names=["bin"])
        all_df[(region, quantity, mc, prod)] = df_all_bins
    df = pd.concat(all_df, names=["region", "quantity", "mc", "prod"])
    return df


def split_pages(df, pages):
    all_values = {}
    for el in pages:
        level = df.index.names.index(el)
        values = df.index.levels[level]
        all_values[el] = values.to_list()

    dfs = {}

    keys = list(all_values.keys())
    values_combinations = list(all_values.values())

    for values in product(*values_combinations):
        df_page = df.xs(values, level=keys)
        dfs[values] = df_page

    return dfs


def split_rotate_df(df, table_scheme):
    results = {}

    dfs = split_pages(df, table_scheme["pages"])
    for values in dfs:
        df_page = dfs[values]
        df_page = df_page.reset_index().pivot(
            index=table_scheme["index"], columns=table_scheme["columns"]
        )
        results[values] = df_page

    return results


def save_latex(tables, prefix, render=True):
    for table in tables:
        latex_caption = table["caption"]
        latex_table = table["latex_table"]
        key = table["key"]

        to_write = "{table}\n%{caption}".format(table=latex_table, caption=latex_caption)
        suffix_fn = "_".join(["%s%s" % (k, key[k]) for k in key])
        fn = "%s_%s.tex" % (prefix, suffix_fn)

        with open(fn, "w") as f:
            f.write(to_write)

        # convert={density=300 -alpha deactivate}
        if render:
            template = r"""
\documentclass[varwidth=\maxdimen, convert={png}]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{booktabs}
\usepackage{siunitx}
%\usepackage{graphicx}
\begin{document}
\begin{table}
\centering
%\resizebox{\textwidth}{!}{
<table>
%}
\caption{<caption>}
\end{table}
\end{document}
"""
            template = template.replace("<table>", latex_table).replace("<caption>", latex_caption)

            tmpdir = tempfile.mkdtemp()
            document_fn = os.path.join(tmpdir, "document.tex")
            with open(document_fn, "w") as f:
                f.write(template)
            logging.info("running latex for %s %s" % (prefix, suffix_fn))
            subprocess.call(
                ("pdflatex -shell-escape %s" % document_fn).split(),
                cwd=tmpdir,
                stdout=open(os.devnull, "w"),
                stderr=open(os.devnull, "w"),
            )
            # os.system("cd %s; pdflatex -shell-escape %s" % (tmpdir, document_fn))
            copyfile(os.path.join(tmpdir, "document.pdf"), "%s_%s.pdf" % (prefix, suffix_fn))
            try:
                copyfile(os.path.join(tmpdir, "document.png"), "%s_%s.png" % (prefix, suffix_fn))
            except FileNotFoundError:
                logging.error(
                    "Seems there are problem to produce the png. Do you have ImageMagick?"
                )

            rmtree(tmpdir)


def get_table(df, table_scheme, add_sum=True, formatting=None, label=""):

    result = []

    if formatting is None:
        formatting = lambda x: "-" if np.isnan(x) else "%.3f" % x
    elif type(formatting) is str:
        formatting = lambda x: "-" if np.isnan(x) else formatting % x

    dfs = split_rotate_df(df, table_scheme)

    for value_page in dfs:
        df_page = dfs[value_page]

        if len(df_page.columns.levels[0]) == 1:
            df_page.columns = df_page.columns.droplevel(0)
        if add_sum:
            df_page.loc["sum"] = df_page.sum(axis=0)

        df_page = df_page.applymap(formatting)
        latex_table = df_page.rename(columns=definitions.latex_labels).to_latex(escape=False)

        for v in definitions.latex_labels:
            latex_table = latex_table.replace(v.replace(r"_", r"\_"), definitions.latex_labels[v])

        value_page_text = " ".join(value_page)
        value_page_text = value_page_text.replace("_", r"\_")
        latex_caption = "{label_text} for {value_page}.".format(
            label_text=label, value_page=value_page_text
        )

        result.append(
            {
                "key": {k: v for (k, v) in zip(table_scheme["pages"], value_page)},
                "caption": latex_caption,
                "latex_table": latex_table,
            }
        )

    return result


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Produce LaTeX tables")
    parser.add_argument("--base-dir")
    parser.add_argument("--output-dir", default="output_latex")
    parser.add_argument("--jobs", nargs="*")
    args = parser.parse_args()

    create_dir_if_not_exists(args.output_dir)

    reader = Reader(args.base_dir)

    latex_ok = check_latex()
    if not latex_ok:
        logging.warning("LaTeX is not correctly working or some packages are missing")

    jobs = [
        {
            "name": "resolution68",
            "output_dir": "resolution68",
            "input_scheme": None,
            "label": "resolution of the signal, evaluated as half of the smallest window containing 68.3\% of the events using MC simulations.",
            "table_scheme": {
                "pages": ["mc", "prod", "region"],
                "index": "bin",
                "columns": "quantity",
            },
            "reading": readall_wrapper_from_reader(reader, "get_resolution"),
            "sum": False,
        },
        {
            "name": "xsection",
            "output_dir": "xsections",
            "input_scheme": "%s/efficiencies/efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/xsections/{region}/{quantity}/XSections_isFiducial_notDalitz_{region}_{quantity}_{mc}_prod{prod}_NOMINAL.csv"
            % args.base_dir,
            "label": "Cross section in pb",
            "table_scheme": {
                "pages": ["mc", "prod", "region"],
                "index": "bin",
                "columns": "quantity",
            },
            "reading": read_all_csv,
            "sum": True,
        },
        {
            "name": "nreco_isPassed",
            "output_dir": "nreco_isPassed",
            "input_scheme": "%s/efficiencies/efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/nreco_isPassed/{region}/{quantity}/nreco_isPassed_{region}_{quantity}_{mc}_prod{prod}_NOMINAL.csv"
            % args.base_dir,
            "label": "Number of signal reco-events",
            "table_scheme": {
                "pages": ["mc", "prod", "region"],
                "index": "bin",
                "columns": "quantity",
            },
            "reading": read_all_csv,
            "sum": True,
        },
        {
            "name": "ntrue_isFiducial_noDalitz",
            "output_dir": "ntrue_isFiducial_noDalitz",
            "input_scheme": "%s/efficiencies/efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/ntrue_isFiducial_noDalitz/{region}/{quantity}/ntrue_isFiducial_noDalitz_{region}_{quantity}_{mc}_prod{prod}_NOMINAL.csv"
            % args.base_dir,
            "label": "Number of signal truth-events (Fiducial and notDalitz)",
            "table_scheme": {
                "pages": ["mc", "prod", "region"],
                "index": "bin",
                "columns": "quantity",
            },
            "reading": read_all_csv,
            "sum": True,
        },
        {
            "name": "cfactors",
            "output_dir": "cfactors",
            "input_scheme": "%s/efficiencies/efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/cfactors/{region}/{quantity}/cfactors_{region}_{quantity}_{mc}_prod{prod}_NOMINAL.csv"
            % args.base_dir,
            "label": "C-factors",
            "table_scheme": {
                "pages": ["mc", "prod", "region"],
                "index": "bin",
                "columns": "quantity",
            },
            "reading": read_all_csv,
            "sum": False,
        },
        {
            "name": "signal parameters",
            "output_dir": "signal_pars",
            "input_scheme": "%s/signal_parameters/signal_parameters_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/{region}/{quantity}/bin_{bin}/resonance_paramList.txt"
            % args.base_dir,
            "label": "Signal parameter",
            "table_scheme": {
                "pages": ["mc", "prod", "region", "parameter"],
                "index": "bin",
                "columns": "quantity",
            },
            "reading": read_signal_parameters,
            "sum": False,
        },
        #        {
        #            "name": "relative_errors_matrix",
        #            "output_dir": "rel_errors_matrix",
        #            "input_scheme": "%s/plot_results/matrix/asimov/{region}/{quantity}/relative_errors_{region}_{quantity}.csv"
        #            % args.base_dir,
        #            "label": "Relative errors matrix method",
        #            "table_scheme": {
        #                "pages": ["mc", "prod", "region"],
        #                "index": "bin",
        #                "columns": "quantity",
        #            },
        #            "reading": read_all_csv,
        #            "sum": False,
        #        },
        #        {
        #            "name": "relative_errors_bin_by_bin",
        #            "output_dir": "rel_errors_binbybin",
        #            "input_scheme": "%s/plot_results/bin_by_bin/asimov/{region}/{quantity}/relative_errors_{region}_{quantity}.csv"
        #            % args.base_dir,
        #            "label": "Relative errors bin by bin",
        #            "table_scheme": {
        #                "pages": ["mc", "prod", "region"],
        #                "index": "bin",
        #                "columns": "quantity",
        #            },
        #            "reading": read_all_csv,
        #            "sum": False,
        #        },
        #        {
        #            "name": "condition number",
        #            "output_dir": "condition_number",
        #            "input_scheme": "%s/efficiencies/efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/cond_number/{region}/{quantity}/cond_{region}_{quantity}_{mc}_prod{prod}_Nominal.csv"
        #            % args.base_dir,
        #            "label": "Condition number of folding matrix fiducial ispassed noDalitz",
        #            "table_scheme": {"pages": ["prod", "mc"], "index": "region", "columns": "quantity"},
        #            "reading": read_all_csv,
        #            "sum": False,
        #        },
        #        {
        #            "name": "ratio matrix bin by bin",
        #            "output_dir": "ratio_methods",
        #            "input_scheme": None,
        #            "label": "Ratio of the the relative error between matrix and bin by bin method",
        #            "table_scheme": {
        #                "pages": ["mc", "prod", "region"],
        #                "index": "bin",
        #                "columns": "quantity",
        #            },
        #            "reading": "CUSTOM",
        #            "sum": False,
        #        },
        #        {
        #            "name": "chi2 bin by bin",
        #            "output_dir": "chi2/bin_by_bin",
        #            "input_scheme": "%s/plot_results/bin_by_bin/asimov/{region}/{quantity}/chi2_{region}_{quantity}.csv"
        #            % args.base_dir,
        #            "label": "$\chi^2$ values.",
        #            "table_scheme": {"pages": ["prod", "mc"], "index": "region", "columns": "quantity"},
        #            "reading": read_all_csv,
        #            "sum": False,
        #        },
        #        {
        #            "name": "chi2 matrix",
        #            "output_dir": "chi2/matrix",
        #            "input_scheme": "%s/plot_results/matrix/asimov/{region}/{quantity}/chi2_{region}_{quantity}.csv"
        #            % args.base_dir,
        #            "label": "$\chi^2$ values",
        #            "table_scheme": {"pages": ["prod", "mc"], "index": "region", "columns": "quantity"},
        #            "reading": read_all_csv,
        #            "sum": False,
        #        },
    ]

    all_dfs = {}

    if args.jobs is not None:
        valid_jobs_name = [job["name"] for job in jobs]
        for job_name in args.jobs:
            if job_name not in valid_jobs_name:
                raise ValueError("not valid job: %s" % job_name)
        jobs = [job for job in jobs if job["name"] in args.jobs]

    for job in jobs:
        all_dfs[job["name"]] = job["reading"](
            job["input_scheme"], regions_quantities_analysis, ["mcAll"], ["prodAll"]
        )
        if all_dfs[job["name"]] is None:
            logging.error(
                "Cannot find any input for job '%s' with scheme %s",
                job["name"],
                job["input_scheme"],
            )

    if "relative_errors matrix" in all_dfs and "relative_errors_bin_by_bin" in all_dfs:
        logging.info("doing ratio relative errors")
        if (
            all_dfs["relative_errors_matrix"] is None
            or all_dfs["relative_errors_bin_by_bin"] is None
        ):
            logging.warning("cannot compute relative error matrix / relative errors bin by bin")
            all_dfs["ratio matrix bin by bin"] = None
        else:
            all_dfs["ratio matrix bin by bin"] = (
                all_dfs["relative_errors_matrix"] / all_dfs["relative_errors_bin_by_bin"]
            )

    for job in jobs:
        logging.info("doing %s", job["name"])

        dfs = all_dfs[job["name"]]
        if dfs is None:
            logging.error("cannot run job %s", job["name"])
            continue

        output_dir = os.path.join(args.output_dir, job["output_dir"])
        create_dir_if_not_exists(output_dir)

        tables = get_table(
            dfs,
            label=job["label"],
            table_scheme=job["table_scheme"],
            add_sum=job["sum"],
        )

        save_latex(
            tables,
            prefix="{folder}/table_{name}".format(folder=output_dir, name=job["name"]),
            render=latex_ok,
        )
