import logging
import os

import numpy as np

from h025_xsections.core import definitions

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def extract_xsections(input_dir, prod, region, quantity):
    input_fname = os.path.join(
        input_dir,
        "efficiencies_mcAll.%s.isPass_or_isFiducial.Nominal.NOMINAL" % prod,
        "xsections",
        region,
        quantity,
        "XSections_isFiducial_notDalitz_{region}_{quantity}_mcAll_prod{prod}_NOMINAL.csv".format(
            region=region, quantity=quantity, prod=prod
        ),
    )
    xsec_array = np.genfromtxt(input_fname, delimiter=",")
    return xsec_array


def create_empty_array(input_dir, region, quantity):
    input_fname = os.path.join(
        input_dir,
        "efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL",
        "xsections",
        region,
        quantity,
        "XSections_isFiducial_notDalitz_{region}_{quantity}_mcAll_prodprodAll_NOMINAL.csv".format(
            region=region, quantity=quantity
        ),
    )
    xsec_array = np.genfromtxt(input_fname, delimiter=",")
    empty_array = np.zeros(len(xsec_array))
    return empty_array


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir")
    # parser.add_argument("output_dir")
    args = parser.parse_args()

    logging.info("ANALYSIS COMBINATIONS:")
    map(logging.info, definitions.regions_quantities_analysis)

    for group_name in definitions.xsection_groups.keys():
        xsection_group = definitions.xsection_groups[group_name]
        logging.info("grouping xsections for {}: {:>20}".format(group_name, xsection_group))
        for region, quantity in definitions.regions_quantities_analysis:
            if "Diphoton" not in region:
                continue
            logging.info("cross section for (%s, %s, %s)", group_name, region, quantity)
            xsec_sum_array = create_empty_array(args.input_dir, region, quantity)
            for prod in xsection_group:
                print("extracting xsections for %s" % prod)
                xsec = extract_xsections(args.input_dir, prod, region, quantity)
                xsec_sum_array += xsec
            logging.info("XSections for the %s group: %s", group_name, xsec_sum_array)
            xsec_sum = xsec_sum_array.sum()
            logging.info("Total fiducial xsection for the %s group: %s", group_name, xsec_sum)
