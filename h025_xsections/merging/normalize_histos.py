#!/usr/bin/env python

import logging
import os
from glob import glob

import ROOT

from h025_xsections.core import definitions
from h025_xsections.core.utils import create_dir_if_not_exists
from h025_xsections.core.predicate import PredicateSet

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def normalize(input_filename, output_filename):
    f = ROOT.TFile.Open(input_filename)
    output = ROOT.TFile.Open(output_fname, "RECREATE")
    if not output or output.IsZombie():
        raise IOError("cannot create outputfile %s" % output_filename)

    try:
        w_initial_plusDalitz = f.Get("cutflow_weighted").GetBinContent(3)
        w_initial_notDalitz = f.Get("cutflow_weighted_noDalitz").GetBinContent(3)
    except AttributeError:
        raise IOError(
            "Cannot find histogram cutflow_weighted or cutflow_weighted_noDalitz in file %s"
            % input_filename
        )
    # TODO: maybe better to do this computation for the fiducial
    #       because with the skimming we are getting too many isPassed (which are non-Dalitz usually)
    w_initial_isDalitz = w_initial_plusDalitz - w_initial_notDalitz
    Br_isDalitz = (w_initial_isDalitz / w_initial_notDalitz) * definitions.Br

    def is_less(a, b):
        # return a < b with 10% tolerance
        if a > 0 and b == 0:
            return False
        if a == 0 and b == 0:
            return True
        return (a - b) / b < -0.1

    is_skimmed = False
    is_skimmed_isFiducial_or_isPassed = False

    # check if sumW[ALL] is less to the one from the cutflow histogram
    # it means something is missing in sumW due to skimming
    histo_w_initial = f.Get("w_initial_ALL")
    if not histo_w_initial:
        raise IOError("Cannot find w_initial_ALL in file %s" % input_filename)
    if is_less(histo_w_initial.GetBinContent(1), w_initial_plusDalitz):
        is_skimmed = True
    logging.debug("file %s is skimmed", input_filename)

    try:
        # check if sumW[notDalitz AND (isFiducial OR isPassed)] < sumW[notDalitz]
        # if not this case it means the usual skimming has been applied
        if not is_less(
            f.Get("w_initial_isFiducialDiphoton_notDalitz").GetBinContent(1)
            + f.Get("w_initial_notFiducialDiphoton_notDalitz_isPassedDiphoton").GetBinContent(1),
            f.Get("w_initial_notDalitz").GetBinContent(1),
        ):
            is_skimmed_isFiducial_or_isPassed = True
            assert is_skimmed  # it must be skimmed
    except AttributeError:
        logging.warning("cannot determine the kind of skimming, maybe it is only-reco")

    if is_skimmed and is_skimmed_isFiducial_or_isPassed:
        logging.debug(
            "file %s is skimmed with isFiducial or isPassed. Some histograms will be removed",
            input_filename,
        )
    elif is_skimmed and not is_skimmed_isFiducial_or_isPassed:
        logging.warning("file %s is skimmed but not in the usual way", input_filename)

    for key in f.GetListOfKeys():
        if "histo" not in key.GetName():
            continue

        predicate_set = PredicateSet.from_name(key.GetName())
        # if it is skimmed (considering the usual skimming: isFiducial OR isPassed)
        # then do not save histograms which are incomplete
        if not (predicate_set.isFiducial.value is True or predicate_set.isPassed.value is True):
            if is_skimmed_isFiducial_or_isPassed:
                continue

        histo = key.ReadObj()
        histo_norm = histo.Clone(histo.GetName())
        if "isDalitz" in histo.GetName():
            histo_norm.Scale(Br_isDalitz / (w_initial_isDalitz * definitions.Br))
        else:
            histo_norm.Scale(1.0 / w_initial_notDalitz)
        histo_norm.Write()

    histo_cutflow_weighted = f.Get("cutflow_weighted")
    histo_cutflow_weighted_noDalitz = f.Get("cutflow_weighted_noDalitz")
    histo_cutflow_weighted.Write()
    histo_cutflow_weighted_noDalitz.Write()

    histo_xsec_br_lumi = f.Get("cutflow_weighted_xsec_Br_lumi").Clone(
        "cutflow_weighted_xsec_Br_lumi_normalized"
    )
    histo_xsec_br_lumi_noDalitz = f.Get("cutflow_weighted_noDalitz_xsec_Br_lumi").Clone(
        "cutflow_weighted_noDalitz_xsec_Br_lumi_normalized"
    )
    histo_xsec_br_lumi.Scale(1.0 / histo_cutflow_weighted.GetBinContent(3))
    histo_xsec_br_lumi_noDalitz.Scale(1.0 / histo_cutflow_weighted_noDalitz.GetBinContent(3))
    histo_xsec_br_lumi.Write()
    histo_xsec_br_lumi_noDalitz.Write()


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        description="Normalize histograms. This program should be applied to sample with defined normalization (e.g. no sub-file)"
    )
    parser.add_argument("input_folder", help="for example ../root_files_analysis/output_histo/")
    parser.add_argument("output_folder", nargs="?", default="output_dir_normalized")
    parser.add_argument('--verbose')

    args = parser.parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.debug("turning on debug messages")

    logging.info("Input folder           %s", args.input_folder)
    logging.info("Output folder          %s", args.output_folder)

    if not os.path.isdir(args.input_folder):
        raise IOError("cannot find directory %s" % args.input_folder)

    input_files = glob(os.path.join(args.input_folder, "histo*.root"))
    logging.info("found %d ROOT files", len(input_files))
    if not input_files:
        raise ValueError("no input files found")

    create_dir_if_not_exists(args.output_folder)

    for ifile, input_file in enumerate(input_files, 1):
        output_fname = os.path.join(
            args.output_folder, os.path.basename(input_file).replace(".root", ".norm.root")
        )
        logging.info("%d/%d normalizing %s -> %s", ifile, len(input_files), input_file, output_fname)
        normalize(input_file, output_fname)
