#!/usr/bin/env python

import logging
import os
import subprocess
from glob import glob
from itertools import groupby

from h025_xsections.core import definitions
from h025_xsections.core.naming import FileHistoInfo, Catalogue
from h025_xsections.core.utils import create_dir_if_not_exists

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def merge(input_files, output_file):
    command = "hadd -f {output_file} {input_files}".format(
        input_files=" ".join(input_files), output_file=output_file
    )
    status = subprocess.call(
        command.split(), stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w")
    )
    if status != 0:
        raise IOError("problem when merging with command %s" % command)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Merge files according to mc and production mode.")
    parser.add_argument("input_folder")
    parser.add_argument("output_folder")
    parser.add_argument("--allow-missing", action="store_true")
    parser.add_argument("--verbose", action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.debug("turning on debug messages")

    if not os.path.isdir(args.input_folder):
        raise ValueError("directory %s does not exist" % args.input_folder)
    all_fn = glob(os.path.join(args.input_folder, "*.root*"))
    if not all_fn:
        raise ValueError("cannot find any file in %s" % args.input_folder)

    for fn in all_fn:
        if "norm" not in fn:
            logging.warning("file %s seems not normalized", fn)

    catalogue = Catalogue.create_catalogue(all_fn)

    for k, v in catalogue.items():
        logging.debug("%s: %s", k.get_line_csv(), v)

    logging.info("all mc: %s", catalogue.all_mc())
    if catalogue.all_mc() != set(definitions.mc_samples):
        logging.warning(
            "mc found different from expected: %s != %s",
            catalogue.all_mc(),
            definitions.mc_samples,
        )
    logging.info("all productions: %s", catalogue.all_production())
    if catalogue.all_production() != definitions.production_modes:
        logging.warning(
            "prod modes different from extected: %s != %s",
            ", ".join(sorted(catalogue.all_production())),
            ", ".join(sorted(definitions.production_modes)),
        )

    # check that for a fixed mc-period, prod and sys there is only 1 file
    msgs = catalogue.check_mc_prod()
    for msg in msgs:
        if args.allow_missing:
            logging.warning(msg)
        else:
            raise IOError(msg)

    nominal_samples = catalogue.sub_catalogue(
        skim="isPass_or_isFiducial", sysgroup="Nominal", sys="NOMINAL"
    )

    nominal_missings = nominal_samples.missings(
        mc=catalogue.all_mc(), prod=catalogue.all_production()
    )
    for missing in nominal_missings:
        msg = "no nominal sample for prod = %s mc = %s" % (missing.prod, missing.mc)
        if args.allow_missing:
            logging.warning(msg)
        else:
            raise ValueError(msg)

    merging_tasks = []

    # nominal (no sys)
    # merge all -> prodAll, mcAll
    if nominal_samples:
        if len(nominal_samples) != len(catalogue.all_production()) * len(catalogue.all_mc()):
            logging.error("Number of nominal samples doesn't match prod x mc")
        tags = nominal_samples.common_tags()
        tags.prod = "prodAll"
        tags.mc = "mcAll"
        tags = tags.format_tags()
        merging_tasks.append(
            {
                "input_files": [catalogue[k] for k in nominal_samples],
                "output_file": "histo_{tags}.norm.merged.root".format(tags=tags),
            }
        )

    # merge by mc
    for mc_period in nominal_samples.all_mc():
        samples_mc = nominal_samples.sub_catalogue(mc=mc_period)
        if not samples_mc:
            continue
        tags = samples_mc.common_tags()
        tags.prod = "prodAll"
        output_fn = "histo_{tags}.norm.merged.root".format(tags=tags.format_tags())
        task = {"input_files": samples_mc.values(), "output_file": output_fn}
        logging.debug("merging %d files to %s", len(samples_mc), output_fn)
        merging_tasks.append(task)

    # merge by production mode
    for prod in nominal_samples.all_production():
        samples_prod = nominal_samples.sub_catalogue(prod=prod)
        if not samples_prod:
            continue
        tags = samples_prod.common_tags()
        tags.mc = "mcAll"
        output_fn = "histo_{tags}.norm.merged.root".format(tags=tags.format_tags())
        task = {"input_files": samples_prod.values(), "output_file": output_fn}
        logging.debug("merging %d files to %s", len(samples_prod), output_fn)
        merging_tasks.append(task)

    # sys
    all_sys_samples = sorted(
        [s for s in catalogue if s.sys != "NOMINAL"], key=lambda s: (s.skim, s.sysgroup, s.sys)
    )
    for k, g in groupby(all_sys_samples, lambda s: (s.skim, s.sysgroup, s.sys)):
        samples = [catalogue[_] for _ in g]
        if len(samples) != len(catalogue.all_production()) * len(catalogue.all_mc()):
            logging.error("number of samples for %s (%d) doesn't match mc x prod", k, len(samples))
        tags = FileHistoInfo(
            mc="mcAll", prod="prodAll", skim=k[0], sysgroup=k[1], sys=k[2]
        ).format_tags()
        output_fn = "histo_{tags}.norm.merged.root".format(tags=tags)
        task = {"input_files": samples, "output_file": output_fn}
        logging.debug("merging %d files to %s", len(samples), output_fn)
        merging_tasks.append(task)

    create_dir_if_not_exists(args.output_folder)

    for itask, task in enumerate(merging_tasks, 1):
        output_merged = os.path.join(args.output_folder, task["output_file"])
        logging.info(
            "merging %d/%d <%d files> -> %s",
            itask,
            len(merging_tasks),
            len(task["input_files"]),
            output_merged,
        )
        merge(task["input_files"], output_merged)
