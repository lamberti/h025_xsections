#!/usr/bin/env python

import logging
import os
from glob import glob

import numpy as np
import pandas as pd
import ROOT
from h025_xsections.core import definitions
from h025_xsections.core.histo_description import HistoDescription, Predicate
from h025_xsections.core.histo_utils import TH1F2np
from h025_xsections.core.naming import Catalogue
from h025_xsections.core.utils import create_dir_if_not_exists

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Compute cross sections.")
    parser.add_argument("input_folder")
    parser.add_argument("output_folder")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--quantities", nargs="*")
    parser.add_argument("--allow-missing", action="store_true")
    parser.add_argument("--verbose", action="store_true")
    args = parser.parse_args()

    create_dir_if_not_exists(args.output_folder)

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.debug("turning on debug messages")

    all_fn = glob(os.path.join(args.input_folder, "*.root*"))
    if not all_fn:
        raise ValueError("cannot find any file in %s" % args.input_folder)

    catalogue = Catalogue.create_catalogue(all_fn)
    catalogue_nominal = catalogue.sub_catalogue(sysgroup="Nominal")
    msg = catalogue_nominal.check_mc_prod()
    if msg:
        logging.error("problem with sample: %s", msg)
    if len(catalogue_nominal) != len(catalogue_nominal.all_production()) * len(
        catalogue_nominal.all_mc()
    ):
        logging.error(
            "problem with number of samples: the production modes are %d, the mc are %d, the samples are %d",
            len(catalogue_nominal.all_production()),
            len(catalogue_nominal.all_mc()),
            len(catalogue_nominal),
        )

    logging.info("reading %d files", len(catalogue_nominal))

    region_quantities = definitions.get_region_quantities(args.regions, args.quantities)
    logging.debug("running for region, quantities:")
    for region, quantity in region_quantities:
        logging.debug("%s, %s", region, quantity)

    numerators = {}
    denominators = {}
    problematic_region_quantities = []
    for file_info, fn in catalogue_nominal.items():
        logging.debug("reading file %s", fn)
        f = ROOT.TFile.Open(fn)
        for region, quantity in region_quantities:
            histo_description = HistoDescription(
                quantity=quantity,
                recotrue="true",
                isFiducial=Predicate(True, "Fiducial" + region),
                isDalitz=Predicate(False, "Dalitz"),
                isPassed=Predicate(Predicate.ANY),
            )
            histo_name = "histo1D_" + histo_description.name()
            h = f.Get(histo_name)
            if not h:
                logging.error("cannot find histogram %s in file %s", histo_name, fn)
                problematic_region_quantities.append((region, quantity))
                continue
            hdata = TH1F2np(h)[0]
            # numerators are the sum of (xs * Br * lumi * weight_initial | fiducial, noDalitz, mc, prod)
            logging.debug(
                "numerator sum(xs * Br * lumi * weight_initial) for %s %s %s: %s",
                file_info.format_tags(),
                region,
                quantity,
                hdata,
            )
            numerators[(file_info.mc, file_info.prod, region, quantity)] = hdata
        w_initial_notDalitz = f.Get("cutflow_weighted_noDalitz").GetBinContent(3)
        # denominator are the sum of (weight_initial | noDalitz, mc, prod)
        logging.debug(
            "denominator (sum(weight_initial) for %s: %s",
            file_info.format_tags(),
            w_initial_notDalitz,
        )
        denominators[(file_info.mc, file_info.prod)] = w_initial_notDalitz

    xsection_x_Br_per_prod_mode = {}

    for region, quantity in region_quantities:
        if (region, quantity) in problematic_region_quantities:
            continue
        dir_region = os.path.join(args.output_folder, region)
        create_dir_if_not_exists(dir_region)
        dir_region_quantity = os.path.join(dir_region, quantity)
        create_dir_if_not_exists(dir_region_quantity)
        for prod_mode in catalogue_nominal.all_production():
            sum_numerator = np.sum(
                [
                    numerators[mc, prod_mode, region, quantity] / definitions.luminosity[mc]
                    for mc in catalogue_nominal.all_mc()
                ],
                axis=0,
            )
            logging.debug(
                "sum numerator sum(xs * Br * weight_initial) for %s %s %s: %s",
                prod_mode,
                region,
                quantity,
                sum_numerator,
            )
            sum_denominator = np.sum(
                [denominators[mc, prod_mode] for mc in catalogue_nominal.all_mc()]
            )
            logging.debug(
                "sum denominator sum(xs * Br * weight_initial) for %s %s %s: %s",
                prod_mode,
                region,
                quantity,
                sum_denominator,
            )
            xsection_x_Br_per_prod_mode[prod_mode, region, quantity] = (
                sum_numerator / sum_denominator
            )
        df = {
            prod_mode: xsection_x_Br_per_prod_mode[prod_mode, region, quantity]
            for prod_mode in catalogue_nominal.all_production()
        }
        df = pd.DataFrame(df).T
        df.name = "xsection_x_Br"
        df.index.name = "prod"
        df.columns.name = "bin"
        suffix = "isFiducial_notDalitz_{region}_{quantity}_mcAll_NOMINAL.csv".format(
            region=region, quantity=quantity
        )
        df.to_csv(
            os.path.join(dir_region_quantity, "xsection_x_Br_per_prod_modes_%s" % suffix),
            header=True,
        )
        df_sum = df.sum(axis=0)
        df_sum.to_csv(
            os.path.join(dir_region_quantity, "xsection_x_Br_%s" % suffix),
            header=False,
            index=False,
        )
        (df_sum / definitions.Br).to_csv(
            os.path.join(dir_region_quantity, "xsection_%s" % suffix), header=False, index=False
        )
        (df / definitions.Br).to_csv(
            os.path.join(dir_region_quantity, "xsection_per_prod_modes_%s" % suffix), header=True
        )
