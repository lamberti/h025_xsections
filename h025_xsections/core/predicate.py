import re


class Predicate(object):
    ANY = "ANY"

    def __init__(self, value, name="", formula=""):
        if isinstance(value, Predicate):
            self.value = value.value
            self.formula = value.formula
            self.name = name
            return

        if value != Predicate.ANY and (name == ""):
            raise ValueError("name is missing for a non-ANY predicate")

        if not formula:
            formula = name

        if not isinstance(value, bool) and value != Predicate.ANY:
            raise ValueError("value should be True/False/'%s', got %s" % (Predicate.ANY, value))
        self.value = value
        self.formula = formula
        self.name = name

    def __and__(self, other):
        """
        In python it is not possible to overload && (and) since it is short-circuited.
        This is the overload of &.
        Usual boolean and, plus: True & ANY = True; False & ANY = False
        """
        if isinstance(self.value, bool) and isinstance(other.value, bool):
            if self.name != other.name or self.formula != other.formula:
                raise ValueError(
                    "cannot do and between %s and %s since they have inconsistent name or formula"
                    % (self, other)
                )
            value = self.value and other.value
            return Predicate(value, self.name, self.formula)
        elif self.value == Predicate.ANY and isinstance(other.value, bool):
            return Predicate(other.value, other.name, other.formula)
        elif other.value == Predicate.ANY and isinstance(self.value, bool):
            return Predicate(self.value, self.name, self.formula)
        else:
            return Predicate(Predicate.ANY)

    def __repr__(self):
        if self.is_any():
            return "<%s = ANY>" % (self.name)
        else:
            return "<%s = %s>" % (self.name, self.value)

    def __invert__(self):
        """
        Overload of ~. ~ANY = ANY
        """
        if not self.is_any():
            return Predicate(not self.value, self.name, self.formula)
        else:
            return Predicate(Predicate.ANY, self.name, self.formula)

    def __hash__(self):
        if self.is_any():
            return hash(Predicate.ANY)
        return hash((self.value, self.name, self.formula))

    def __eq__(self, other):
        if self.is_any() and other.is_any():
            return True

        return (
            self.value == other.value and self.name == other.name and self.formula == other.formula
        )

    def is_any(self):
        return self.value == Predicate.ANY


class PredicateSet(object):

    name_separator = "_"

    def __init__(
        self,
        isFiducial=Predicate(Predicate.ANY),
        isDalitz=Predicate(Predicate.ANY),
        isPassed=Predicate(Predicate.ANY),
    ):
        if not (
            isinstance(isFiducial, Predicate)
            and isinstance(isDalitz, Predicate)
            and isinstance(isPassed, Predicate)
        ):
            raise ValueError("arguments are not predicates")

        self.isFiducial = isFiducial
        self.isDalitz = isDalitz
        self.isPassed = isPassed

    def __repr__(self):
        return "<PredicateSet %s(%s): %s, %s(%s): %s, %s(%s): %s>" % (
            self.isFiducial.name or "<Fiducial>",
            self.isFiducial.formula or "",
            self.isFiducial.value,
            self.isDalitz.name or "<Dalitz>",
            self.isDalitz.formula or "",
            self.isDalitz.value,
            self.isPassed.name or "<Passed>",
            self.isPassed.formula or "",
            self.isPassed.value,
        )

    def __and__(self, other):
        return PredicateSet(
            isFiducial=self.isFiducial & other.isFiducial,
            isDalitz=self.isDalitz & other.isDalitz,
            isPassed=self.isPassed & other.isPassed,
        )

    def __hash__(self):
        return hash((hash(self.isFiducial), hash(self.isDalitz), hash(self.isPassed)))

    def __eq__(self, other):
        return (
            self.isFiducial == other.isFiducial
            and self.isDalitz == other.isDalitz
            and self.isPassed == other.isPassed
        )

    def __invert__(self):
        n_any = sum(
            map(int, [self.isFiducial.is_any(), self.isDalitz.is_any(), self.isPassed.is_any()])
        )
        if n_any != 2:
            raise ValueError("can invert only if all are any excet for one, now: %s" % self)
        return PredicateSet(
            isFiducial=~self.isFiducial, isDalitz=~self.isDalitz, isPassed=~self.isPassed,
        )

    def formula(self):
        sub_expressions = []
        if not self.isFiducial.is_any():
            sub_expressions.append(
                "(%s==%s)" % (self.isFiducial.formula, int(self.isFiducial.value))
            )
        if not self.isDalitz.is_any():
            sub_expressions.append("(%s==%s)" % (self.isDalitz.formula, int(self.isDalitz.value)))
        if not self.isPassed.is_any():
            sub_expressions.append("(%s==%s)" % (self.isPassed.formula, int(self.isPassed.value)))

        if not sub_expressions:
            return "1"
        else:
            return " && ".join(sub_expressions)

    def name(self):
        sub_names = []

        def boolean_prefix(value):
            return "is" if value else "not"

        if not self.isFiducial.is_any():
            sub_names.append(boolean_prefix(self.isFiducial.value) + self.isFiducial.name)
        if not self.isDalitz.is_any():
            sub_names.append(boolean_prefix(self.isDalitz.value) + self.isDalitz.name)
        if not self.isPassed.is_any():
            sub_names.append(boolean_prefix(self.isPassed.value) + self.isPassed.name)
        if sub_names:
            return self.name_separator.join(sub_names)
        else:
            return "ALL"

    @staticmethod
    def from_name(name):
        match = re.search(r"(is|not)Fiducial(MET|Diphoton|Lepton|VBF)", name)
        if not match:
            is_fiducial = Predicate.ANY
            name_fiducial = "Fiducial"
        else:
            is_fiducial = {"is": True, "not": False}[match.group(1)]
            name_fiducial = "Fiducial" + match.group(2)

        is_dalitz = Predicate.ANY
        if "_isDalitz" in name:
            is_dalitz = True
        elif "_notDalitz" in name:
            is_dalitz = False

        match = re.search(r"(is|not)Passed(MET|Diphoton|Lepton|VBF)", name)
        if not match:
            is_passed = Predicate.ANY
            name_passed = "Passed"
        else:
            is_passed = {"is": True, "not": False}[match.group(1)]
            name_passed = "Passed" + match.group(2)

        return PredicateSet(
            isFiducial=Predicate(is_fiducial, name_fiducial),
            isDalitz=Predicate(is_dalitz, "Dalitz"),
            isPassed=Predicate(is_passed, name_passed),
        )
