from h025_xsections.core.predicate import PredicateSet, Predicate


class HistoDescription(PredicateSet):
    def __init__(
        self,
        quantity,
        recotrue,
        isFiducial=Predicate(Predicate.ANY),
        isDalitz=Predicate(Predicate.ANY),
        isPassed=Predicate(Predicate.ANY),
    ):
        super(HistoDescription, self).__init__(isFiducial, isDalitz, isPassed)

        if recotrue not in ("reco", "true"):
            raise ValueError("recotrue value %s not valid" % recotrue)

        self.recotrue = recotrue
        self.quantity = quantity

    def __repr__(self):
        return "<HistoDescription quantity='{quantity}', recotrue='{recotrue}', isFiducial={fiducial}, isDalitz={dalitz}, isPassed={isPassed}>".format(
            quantity=self.quantity,
            recotrue=self.recotrue,
            fiducial=self.isFiducial,
            dalitz=self.isDalitz,
            isPassed=self.isPassed,
        )

    def __hash__(self):
        return hash((super(HistoDescription, self).__hash__(), self.recotrue, self.quantity))

    def __eq__(self, other):
        return (
            super(HistoDescription, self).__eq__(other)
            and self.recotrue == other.recotrue
            and self.quantity == other.quantity
        )

    def name(self):
        return self.name_separator.join(
            (self.quantity, self.recotrue, super(HistoDescription, self).name())
        )

    @staticmethod
    def from_name(name):
        predicate = PredicateSet.from_name(name)
        predicate_name = predicate.name()
        name = name.replace(predicate_name, "")
        if "_true_" in name:
            recotrue = "true"
        elif "_reco_" in name:
            recotrue = "reco"
        else:
            raise ValueError("cannot parse %s" % name)

        name = name.replace("_%s_" % recotrue, "")
        name = name.replace("histo1D_", "")
        quantity = name
        return HistoDescription(
            quantity=quantity,
            recotrue=recotrue,
            isFiducial=predicate.isFiducial,
            isDalitz=predicate.isDalitz,
            isPassed=predicate.isPassed,
        )

    def get_color(self):
        """
        # https://matplotlib.org/examples/color/named_colors.html
        color_dict = {('true', 'isFiducial', 'notDalitz'): 'springgreen',
                      ('true', 'ALL', 'ALL'): 'lightseagreen',
                      ('reco', 'isFiducial', 'isDalitz'): 'C1',
                      ('reco', 'notFiducial', 'isDalitz'): 'C2',
                     }
        try:
            color = color_dict[(self.recotrue, self.fiducial, self.dalitz)]
            return color
        except KeyError:
            print("color for %s %s %s not encoded, returning red" % (self.recotrue, self.fiducial, self.dalitz))
            return 'red'
        """
        return "springgreen"


def histo_description_region_quantity(region, quantity):
    def HD(recotrue, isFiducial=Predicate.ANY, isDalitz=Predicate.ANY, isPassed=Predicate.ANY):
        return HistoDescription(
            quantity=quantity,
            recotrue=recotrue,
            isFiducial=Predicate(isFiducial, "Fiducial" + region),
            isDalitz=Predicate(isDalitz, "Dalitz"),
            isPassed=Predicate(isPassed, "Passed" + region),
        )

    return HD
