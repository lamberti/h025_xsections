from array import array
from itertools import product
import numpy as np

from h025_xsections.core.utils import pairwise

mH = 125.09

production_modes = [
    "ggH",
    "VBF",
    "WmH",
    "WpH",
    "ZH",
    "ggZH",
    "ttH",
    "tHjb",
    "tWh",
    "bbH",
]

generators = ("PowhegPy8", "PowhegH7")
generators_synonyms = {"PowhegPy8": ("PowhegPy8",), "PowhegH7": ("PowhegH7", "PowhegH713", "PowhegHw7")}

xsection_groups = {
    "ggH_plus_bbH": ["ggH", "bbH"],
    "VBF": ["VBF"],
    "VH": ["WmH", "WpH", "ZH"],
    "ttH": ["ttH"],
}

generator_production = {
    "PowhegPy8EG_NNPDF30_VBFH125": "VBF",
    "PowhegPy8_NNLOPS_ggH125": "ggH",
    "PowhegPy8_WmH125J": "WmH",
    "PowhegPy8_WpH125J": "WpH",
    "PowhegPy8_ZH125J": "ZH",
    "PowhegPy8_bbH125": "bbH",
    "PowhegPy8_ggZH125": "ggZH",
    "PowhegPy8_ttH125_fixweight": "ttH",
}


mc_samples = "mc16a", "mc16d", "mc16e"
luminosity = {"mc16a": 36240, "mc16d": 44307, "mc16e": 58450}
luminosity["mcAll"] = sum(luminosity.values())

xsections = {  # pb
    "ggH": 48.52,
    "VBF": 3.779,
    "WmH": 0.5313,
    "WpH": 0.838,
    "ZH": 0.7596,
    "ggZH": 0.1226,
    "ttH": 0.5065,
    # "tHjb": 0.001881, s-ch or t-ch ?
    # "tWH": 0.01517,
    "bbH": 0.4863,
}

xsections["prodAll"] = sum(xsections.values())

Br = 2.270e-3

fiducial_regions = ["Diphoton", "VBF", "Lepton", "MET"]

quantities = [
    "pT_yy",
    "pT_yy_reduced",
    "pT_yy_JV_30",
    "pT_yy_JV_40",
    "pT_yy_JV_50",
    "pT_yy_JV_60",
    "yAbs_yy",
    "yAbs_yy_reduced",
    "N_j_30",
    "rel_pT_y1",
    "rel_pT_y2",
    "pT_j1_30",
    "VBF_pT_j1_30",
    "pT_yyj_30",
    "pT_yyj_30_reduced",
    "pT_yyjj_30",
    "VBF_pT_yyjj_30",
    "HT_30",
    "maxTau_yyj_30",
    "sumTau_yyj_30",
    "m_yyj_30",
    "m_jj_30",
    "Dphi_yy_jj_30",
    "Dphi_j_j_30_signed",
    "Dphi_j_j_30",
    "abs_Zepp",
]

quantities_2d = {
    "VBF_pT_j1_30_vs_Dphi_j_j_30": ("VBF_pT_j1_30", "Dphi_j_j_30"),
    "pT_yy_vs_pT_yyj_30": ("pT_yy_reduced", "pT_yyj_30_reduced"),
    "pT_yy_vs_yAbs_yy": ("pT_yy_reduced", "yAbs_yy_reduced"),
}

branches = {
    "pT_yy_reduced": "pT_yy",
    "pT_yyj_30_reduced": "pT_yyj_30",
    "VBF_pT_j1_30": "pT_j1_30",
    "m_yy": "m_yy",
    "VBF_pT_yyjj_30": "pT_yyjj_30",
    "yAbs_yy_reduced": "yAbs_yy",
}
for quantity in quantities:
    if quantity not in branches:
        branches[quantity] = quantity


regions_quantities_analysis = [
    ("Diphoton", "constant"),
    ("Diphoton", "pT_yy"),
    ("Diphoton", "yAbs_yy"),
    ("Diphoton", "N_j_30"),
    ("Diphoton", "rel_pT_y1"),
    ("Diphoton", "rel_pT_y2"),
    ("Diphoton", "pT_j1_30"),
    ("Diphoton", "pT_yyj_30"),
    ("Diphoton", "pT_yyjj_30"),
    ("Diphoton", "HT_30"),
    ("Diphoton", "maxTau_yyj_30"),
    ("Diphoton", "sumTau_yyj_30"),
    ("Diphoton", "m_yyj_30"),
    ("Diphoton", "m_jj_30"),
    ("Diphoton", "Dphi_yy_jj_30"),
    ("Diphoton", "Dphi_j_j_30_signed"),
    ("Diphoton", "Dphi_j_j_30"),
    ("Diphoton", "pT_yy_JV_30"),
    ("Diphoton", "pT_yy_JV_40"),
    ("Diphoton", "pT_yy_JV_50"),
    ("Diphoton", "pT_yy_JV_60"),
    ("VBF", "constant"),
    ("VBF", "abs_Zepp"),
    # TODO: check and update VBF variables
    ("VBF", "Dphi_j_j_30_signed"),
    ("VBF", "pT_j1_30"),
    ("VBF", "VBF_pT_j1_30"),
    ("VBF", "VBF_pT_yyjj_30"),
    ("VBF", "pT_yyjj_30"),
    ("Lepton", "constant"),
    ("MET", "constant"),
    # 2D
    ("VBF", "VBF_pT_j1_30_vs_Dphi_j_j_30"),
    ("Diphoton", "pT_yy_vs_pT_yyj_30"),
    ("Diphoton", "pT_yy_vs_yAbs_yy"),
]

weight_final = {
    ("Diphoton", "constant"): ["weight"],
    ("Diphoton", "pT_yy"): ["weight"],
    ("Diphoton", "yAbs_yy"): ["weight"],
    ("Diphoton", "N_j_30"): ["weight", "weightJvt_30"],
    ("Diphoton", "rel_pT_y1"): ["weight"],
    ("Diphoton", "rel_pT_y2"): ["weight"],
    ("Diphoton", "pT_j1_30"): ["weight", "weightJvt_30"],
    ("Diphoton", "pT_yyj_30"): ["weight", "weightJvt_30"],
    ("Diphoton", "pT_yyjj_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "HT_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "maxTau_yyj_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "sumTau_yyj_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "m_yyj_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "m_jj_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "Dphi_yy_jj_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "Dphi_j_j_30_signed"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "Dphi_j_j_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "pT_yy_JV_40"): ["weightJvt_30"],  # TODO: check
    ("Diphoton", "pT_yy_JV_30"): ["weightJvt_30"],  # TODO: check
    ("Diphoton", "pT_yy_JV_50"): ["weightJvt_30"],  # TODO: check
    ("Diphoton", "pT_yy_JV_60"): ["weightJvt_30"],  # TODO: check
    ("VBF", "constant"): list(["weight", "weightJvt_30"]),
    # TODO: check and update VBF variables
    ("VBF", "abs_Zepp"): list(["weight", "weightJvt_30"]),
    ("VBF", "Dphi_j_j_30_signed"): list(["weight", "weightJvt_30"]),
    ("VBF", "pT_j1_30"): list(["weight", "weightJvt_30"]),
    ("VBF", "VBF_pT_j1_30"): list(["weight", "weightJvt_30"]),
    ("VBF", "VBF_pT_yyjj_30"): list(["weight", "weightJvt_30"]),
    ("VBF", "pT_yyjj_30"): list(["weight", "weightJvt_30"]),
    ("Lepton", "constant"): list(["weight", "weightCatXS_lepton"]),
    ("MET", "constant"): list(["weight"]),  # TODO: fix
    ("Diphoton", "pT_yy_vs_pT_yyj_30"): list(["weight", "weightJvt_30"]),
    ("Diphoton", "pT_yy_vs_yAbs_yy"): list(["weight"]),
    ("VBF", "VBF_pT_j1_30_vs_Dphi_j_j_30"): list(["weight", "weightJvt_30"]),
}

MeV = 1
GeV = 1000 * MeV

# units in mxAOD / uxAOD
units_root = {
    "pT_yy": MeV,
    "pT_yy_reduced": MeV,
    "yAbs_yy": 1,
    "N_j_30": 1,
    "rel_pT_y1": 1,
    "rel_pT_y2": 1,
    "pT_j1_30": MeV,
    "VBF_pT_j1_30": MeV,
    "pT_yyj_30": MeV,
    "pT_yyj_30_reduced": MeV,
    "pT_yyjj_30": MeV,
    "pT_yy_JV_30": MeV,
    "pT_yy_JV_40": MeV,
    "pT_yy_JV_50": MeV,
    "pT_yy_JV_60": MeV,
    "HT_30": MeV,
    "maxTau_yyj_30": MeV,
    "sumTau_yyj_30": MeV,
    "m_yyj_30": MeV,
    "m_jj_30": MeV,
    "Dphi_yy_jj_30": 1,
    "Dphi_j_j_30_signed": 1,
    "Dphi_j_j_30": 1,
    "abs_Zepp": 1,
    "VBF_pT_yyjj_30": MeV,
    "yAbs_yy_reduced": 1,
    "m_yy": MeV,
    "constant": 1,
}

units_final = {
    "pT_yy": GeV,
    "pT_yy_reduced": GeV,
    "yAbs_yy": 1,
    "N_j_30": 1,
    "rel_pT_y1": 1,
    "rel_pT_y2": 1,
    "pT_j1_30": GeV,
    "VBF_pT_j1_30": GeV,
    "pT_yyj_30": GeV,
    "pT_yyj_30_reduced": GeV,
    "pT_yyjj_30": GeV,
    "pT_yy_JV_30": GeV,
    "pT_yy_JV_40": GeV,
    "pT_yy_JV_50": GeV,
    "pT_yy_JV_60": GeV,
    "HT_30": GeV,
    "maxTau_yyj_30": GeV,
    "sumTau_yyj_30": GeV,
    "m_yyj_30": GeV,
    "m_jj_30": GeV,
    "Dphi_yy_jj_30": 1,
    "Dphi_j_j_30_signed": 1,
    "Dphi_j_j_30": 1,
    "abs_Zepp": 1,
    "VBF_pT_yyjj_30": GeV,
    "yAbs_yy_reduced": 1,
    "m_yy": GeV,
    "constant": 1,
}

m_yy_nbins = 220
m_yy_min = 105
m_yy_max = 160
m_yy_nparray = np.linspace(m_yy_min, m_yy_max, m_yy_nbins + 1)

binnings = {
    "m_yy": array("f", m_yy_nparray),  # GeV
    "pT_yy": array(
        "f",
        [
            0,
            5,
            10,
            15,
            20,
            25,
            30,
            35,
            45,
            60,
            80,
            100,
            120,
            140,
            170,
            200,
            250,
            300,
            450,
            650,
            1000,
        ],
    ),  # GeV
    "pT_yy_reduced": array("f", [0, 45, 120, 350]),  # GeV
    "yAbs_yy": array("f", [0, 0.15, 0.30, 0.45, 0.60, 0.75, 0.90, 1.2, 1.6, 2.0, 2.5]),
    "yAbs_yy_reduced": array("f", [0, 0.5, 1.0, 1.5, 2.5]),
    "N_j_30": array("f", [0, 1, 2, 3]),
    "rel_pT_y1": array("f", [0.35, 0.45, 0.5, 0.55, 0.6, 0.65, 0.75, 0.85, 0.95]),  # p_T^y1/m_yy
    "rel_pT_y2": array("f", [0.25, 0.35, 0.4, 0.45, 0.5, 0.55, 0.65, 0.75, 0.85]),  # p_T^y2/m_yy
    "pT_j1_30": array("f", [30, 45, 60, 90, 120, 350]),  # GeV
    "VBF_pT_j1_30": array("f", [30, 120, 500]),  # GeV
    "pT_yyj_30": array("f", [0, 30, 60, 120, 350]),  # GeV
    "pT_yyj_30_reduced": array("f", [0, 30, 60, 350]),  # GeV
    "pT_yyjj_30": array("f", [0, 30, 60, 120, 350]),  # GeV
    "pT_yy_JV_30": array("f", [0, 5, 10, 15, 20, 30, 40, 50, 100, 200]),
    "pT_yy_JV_40": array("f", [0, 5, 10, 15, 20, 30, 40, 50, 60, 100, 200]),
    "pT_yy_JV_50": array("f", [0, 5, 10, 15, 20, 30, 40, 50, 60, 70, 100, 200]),
    "pT_yy_JV_60": array("f", [0, 5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 100, 200]),
    "HT_30": array("f", [30, 70, 140, 200, 500]),  # GeV
    "maxTau_yyj_30": array("f", [0, 5, 15, 25, 40, 400]),  # GeV
    "sumTau_yyj_30": array("f", [5, 15, 25, 40, 80, 400]),  # GeV
    "m_yyj_30": array("f", [120, 220, 300, 400, 600, 900]),  # GeV
    "m_jj_30": array("f", [0, 120, 450, 3000]),  # GeV
    "Dphi_yy_jj_30": array("f", [0.0, 2.5, 3.0, 3.15]),
    "Dphi_j_j_30": array("f", [0, 1.04, 2.08, 3.15]),
    "Dphi_j_j_30_signed": array(
        "f", [-3.15, -1.5708, 0, 1.5708, 3.15]
    ),  # TODO: check, in particular the 2D
    "abs_Zepp": array("f", [0, 1, 2, 6]),
    "VBF_pT_yyjj_30": array("f", [0, 30]),
    "constant": array("f", [0, 1]),
}

for q12, (q1, q2) in quantities_2d.items():
    nb1 = len(binnings[q1])
    nb2 = len(binnings[q2])
    n = (nb1 + 1) * (nb2 + 1)
    binnings[q12] = (n, 0, n)

nbins_noextraflow = {k: len(binnings[k]) - 1 for k in binnings}

latex_labels = {
    "Dphi_j_j_30": r"$|\Delta\phi_{\gamma\gamma jj}^{30}|$",
    "Dphi_j_j_30_signed": r"$\Delta\phi_{\gamma\gamma jj}^{30}$",
    "Dphi_yy_jj_30": r"$\Delta\phi_{jj}^{(30)}$",
    "HT_30": r"$H_T^{j\geq 30}$",
    "N_j_30": r"$N_j^{(30)}$",
    "m_jj_30": r"$m_{jj}^{(30)}$",
    "m_yyj_30": r"$m_{\gamma\gamma j}$",
    "maxTau_yyj_30": r"$\tau_1$",
    "pT_j1_30": r"$p_{T}^{j1}$",
    "VBF_pT_j1_30": r"$p_{T}^{j1}$",
    "pT_yy": r"$p_{T}^{\gamma\gamma}$",
    "pT_yy_reduced": r"$p_{T}^{\gamma\gamma}$",
    "pT_yyj_30": r"$p_T^{\gamma\gamma j}$",
    "pT_yyj_30_reduced": r"$p_T^{\gamma\gamma j}$",
    "pT_yyjj_30": r"$p_T^{\gamma\gamma jj}$",
    "pT_yy_JV_30": r"$p_T^{\gamma\gamma} N_j^{30}=0$",
    "pT_yy_JV_40": r"$p_T^{\gamma\gamma} N_j^{40}=0$",
    "pT_yy_JV_50": r"$p_T^{\gamma\gamma} N_j^{50}=0$",
    "pT_yy_JV_60": r"$p_T^{\gamma\gamma} N_j^{60}=0$",
    "rel_pT_y1": r"$p_T^{\gamma 1}/m_{\gamma\gamma}$",
    "rel_pT_y2": r"$p_T^{\gamma 2}/m_{\gamma\gamma}$",
    "sumTau_yyj_30": r"$\tau$",
    "yAbs_yy": r"$|y_{\gamma\gamma}|$",
    "abs_Zepp": r"$|\eta_{Zep}|$",
    "VBF_pT_yyjj_30": r"$p_T^{\gamma\gamma jj}$",
    "constant": "inclusive",
    "pT_yy_vs_pT_yyj_30": r"$p_{T}^{\gamma\gamma} \otimes p_T^{\gamma\gamma j}$",
    "pT_yy_vs_yAbs_yy": r"$p_{T}^{\gamma\gamma} \otimes |y_{\gamma\gamma}|$",
    "VBF_pT_j1_30_vs_Dphi_j_j_30": r"$p_{T}^{j1} \otimes \Delta\phi_{\gamma\gamma jj}^{30}$",
}


show_overflows = {
    "Dphi_j_j_30_signed": False,
    "Dphi_j_j_30": False,
    "Dphi_yy_jj_30": False,
    "HT_30": True,
    "N_j_30": True,
    "m_jj_30": True,
    "m_yyj_30": True,
    "maxTau_yyj_30": False,
    "pT_j1_30": True,
    "VBF_pT_j1_30": True,
    "pT_yy": False,
    "pT_yyj_30": False,
    "pT_yyjj_30": False,
    "pT_yy_JV_30": False,
    "pT_yy_JV_40": False,
    "pT_yy_JV_50": False,
    "pT_yy_JV_60": False,
    "rel_pT_y1": True,
    "rel_pT_y2": True,
    "sumTau_yyj_30": False,
    "yAbs_yy": False,
    "abs_Zepp": True,
    "VBF_pT_yyjj_30": True,
    "constant": False,
    "pT_yy_vs_pT_yyj_30": False,
    "pT_yy_vs_yAbs_yy": False,
    "VBF_pT_j1_30_vs_Dphi_j_j_30": False,
    "pT_yy_reduced": False,
}

show_underflows = {
    "Dphi_j_j_30_signed": True,
    "Dphi_j_j_30": False,
    "Dphi_yy_jj_30": True,
    "HT_30": True,
    "N_j_30": False,
    "m_jj_30": True,
    "m_yyj_30": True,
    "maxTau_yyj_30": True,
    "pT_j1_30": False,
    "VBF_pT_j1_30": False,
    "pT_yy": False,
    "pT_yy_JV_30": False,
    "pT_yy_JV_40": False,
    "pT_yy_JV_50": False,
    "pT_yy_JV_60": False,
    "pT_yyj_30": True,
    "pT_yyjj_30": True,
    "rel_pT_y1": False,
    "rel_pT_y2": False,
    "sumTau_yyj_30": True,
    "yAbs_yy": False,
    "abs_Zepp": False,
    "VBF_pT_yyjj_30": False,
    "constant": False,
    "pT_yy_vs_pT_yyj_30": False,
    "pT_yy_vs_yAbs_yy": False,
    "VBF_pT_j1_30_vs_Dphi_j_j_30": False,
    "pT_yy_reduced": False,
}

do_normalizations = {
    "Dphi_j_j_30_signed": True,
    "Dphi_j_j_30": True,
    "Dphi_yy_jj_30": True,
    "HT_30": True,
    "N_j_30": True,
    "m_jj_30": True,
    "m_yyj_30": True,
    "maxTau_yyj_30": True,
    "pT_j1_30": True,
    "VBF_pT_j1_30": True,
    "pT_yy": True,
    "pT_yyj_30": True,
    "pT_yyjj_30": True,
    "rel_pT_y1": True,
    "rel_pT_y2": True,
    "pT_yy_JV_30": True,
    "pT_yy_JV_40": True,
    "pT_yy_JV_50": True,
    "pT_yy_JV_60": True,
    "sumTau_yyj_30": True,
    "yAbs_yy": True,
    "abs_Zepp": True,
    "VBF_pT_yyjj_30": True,
    "constant": False,
    "pT_yy_vs_pT_yyj_30": False,
    "pT_yy_vs_yAbs_yy": False,
    "VBF_pT_j1_30_vs_Dphi_j_j_30": False,
}

do_logs_x = {
    "Dphi_j_j_30_signed": False,
    "Dphi_j_j_30": False,
    "Dphi_yy_jj_30": True,
    "HT_30": True,
    "N_j_30": False,
    "m_jj_30": True,
    "m_yyj_30": True,
    "maxTau_yyj_30": True,
    "pT_j1_30": True,
    "VBF_pT_j1_30": True,
    "pT_yy": True,
    "pT_yyj_30": True,
    "pT_yyjj_30": True,
    "rel_pT_y1": False,
    "rel_pT_y2": False,
    "sumTau_yyj_30": True,
    "pT_yy_JV_30": True,
    "pT_yy_JV_40": True,
    "pT_yy_JV_50": True,
    "pT_yy_JV_60": True,
    "yAbs_yy": False,
    "abs_Zepp": False,
    "VBF_pT_yyjj_30": False,
    "constant": False,
    "pT_yy_vs_pT_yyj_30": False,
    "pT_yy_vs_yAbs_yy": False,
    "VBF_pT_j1_30_vs_Dphi_j_j_30": False,
}


def _create_bin_label(binning, float_format="%d"):
    fmt = "[{fmt} - {fmt})".format(fmt=float_format)
    labels = (
        [r"$< {fmt}$".format(fmt=float_format) % binning[0]]
        + [fmt % (a, b) for a, b in pairwise(binning)]
        + [r"$\geq {fmt}$".format(fmt=float_format) % binning[-1]]
    )
    return labels


# with extra-flow
bin_labels = {
    "pT_yy": _create_bin_label(binnings["pT_yy"]),
    "pT_yy_reduced": _create_bin_label(binnings["pT_yy_reduced"]),
    "yAbs_yy": _create_bin_label(binnings["yAbs_yy"], "%.1f"),
    "N_j_30": _create_bin_label(binnings["N_j_30"]),
    "rel_pT_y1": _create_bin_label(binnings["rel_pT_y1"], "%.2f"),
    "rel_pT_y2": _create_bin_label(binnings["rel_pT_y2"], "%.2f"),
    "pT_j1_30": _create_bin_label(binnings["pT_j1_30"]),
    "VBF_pT_j1_30": _create_bin_label(binnings["VBF_pT_j1_30"]),
    "pT_yyj_30": _create_bin_label(binnings["pT_yyj_30"]),
    "pT_yyj_30_reduced": _create_bin_label(binnings["pT_yyj_30_reduced"]),
    "yAbs_yy_reduced": _create_bin_label(binnings["yAbs_yy_reduced"], "%.1f"),
    "pT_yyjj_30": _create_bin_label(binnings["pT_yyjj_30"], "%.1f"),
    "HT_30": _create_bin_label(binnings["HT_30"], "%.1f"),
    "maxTau_yyj_30": _create_bin_label(binnings["maxTau_yyj_30"], "%.1f"),
    "sumTau_yyj_30": _create_bin_label(binnings["sumTau_yyj_30"], "%.1f"),
    "m_yyj_30": _create_bin_label(binnings["m_yyj_30"], "%.1f"),
    "m_jj_30": _create_bin_label(binnings["m_jj_30"], "%.1f"),
    "Dphi_yy_jj_30": _create_bin_label(binnings["Dphi_yy_jj_30"], "%.2f"),
    "Dphi_j_j_30_signed": _create_bin_label(binnings["Dphi_j_j_30_signed"], "%.2f"),
    "Dphi_j_j_30": _create_bin_label(binnings["Dphi_j_j_30"], "%.2f"),
    "abs_Zepp": _create_bin_label(binnings["abs_Zepp"], "%.2f"),
    "VBF_pT_yyjj_30": _create_bin_label(binnings["VBF_pT_yyjj_30"], "%.2f"),
    "pT_yy_JV_30": _create_bin_label(binnings["pT_yy_JV_30"], "%.1f"),
    "pT_yy_JV_40": _create_bin_label(binnings["pT_yy_JV_40"], "%.1f"),
    "pT_yy_JV_50": _create_bin_label(binnings["pT_yy_JV_50"], "%.1f"),
    "pT_yy_JV_60": _create_bin_label(binnings["pT_yy_JV_60"], "%.1f"),
    "constant": ["underflow", "all", "overflow"],
}

for quantity in quantities_2d:
    l12 = []
    l1 = bin_labels[quantities_2d[quantity][0]]
    l2 = bin_labels[quantities_2d[quantity][1]]
    for ll1, ll2 in product(l1, l2):
        l12.append("%s - %s" % (ll1, ll2))
    bin_labels[quantity] = ["underflow"] + l12 + ["overflow"]


y_limits_xsection_plots = {
    "Dphi_j_j_30_signed": 2.75,
    "Dphi_j_j_30": 2.75,
    "Dphi_yy_jj_30": 100,
    "HT_30": 1,
    "maxTau_yyj_30": 1,
    "m_jj_30": 0.1,
    "m_yyj_30": 1,
    "N_j_30": 45,
    "pT_j1_30": 5,
    "VBF_pT_j1_30": 5,
    "pT_yy": 1,
    "pT_yyj_30": 5,
    "pT_yyjj_30": 1,
    "rel_pT_y1": 400,
    "rel_pT_y2": 400,
    "sumTau_yyj_30": 5,
    "yAbs_yy": 60,
    "pT_yy_JV_30": 1,
    "pT_yy_JV_40": 1,
    "pT_yy_JV_50": 1,
    "pT_yy_JV_60": 1,
}


def get_region_quantities(proposed_regions, proposed_quantities):
    result = regions_quantities_analysis[:]
    if proposed_regions is not None:
        result = [q for q in result if q[0] in proposed_regions]
    if proposed_quantities is not None:
        result = [q for q in result if q[1] in proposed_quantities]
    return result


global_yield_systematics = {
    "ATLAS_lumi_run2": [1.7e-2, "logn", "yield"],
}

global_scale_systematics = {"ATLAS_MSS_HIGGS_MASS": [0.00192, "gaus", "shape"]}
