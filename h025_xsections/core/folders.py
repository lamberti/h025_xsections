import string
import os


patterns = {
    "signal_parameters": "signal_parameters/signal_parameters_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL/{signal_model_type}/{region}/{quantity}/bin_{bin}/resonance_paramList.txt",
    "merged_files_folder": "output_histo_norm_merged",
    "cfactor_sys_folder": "systematics/sys_yield/bin_by_bin/{region}/{quantity}",
    "matrix_sys_folder": "systematics/sys_yield/matrix/{region}/{quantity}",
    "cfactory_sys": "systematics/sys_yield/bin_by_bin/{region}/{quantity}/{variation}.csv",
    "matrix_sys": "systematics/sys_yield/matrix/{region}{quantity}/{variation}.csv",
    "xml_folder": "workspace/{method}/{region}/{quantity}/{blinded}/",
    "workspace_folder": "workspace/{method}/{region}/{quantity}/{blinded}",
    "workspace_fitted_folder": "workspace_fitted/{method}/{region}/{quantity}",
    "workspace": "workspace/{method}/{region}/{quantity}/{blinded}/XSectionWS_{region}_{quantity}.root",
    "workspace_fitted": "workspace_fitted/{method}/{region}/{quantity}/XSectionWS_{region}_{quantity}-fitted-{dataset}-{sys}.root",
    "results": "results/{method}/{dataset}/{region}/{quantity}",
    "data_folder": "data/{blinded}",
    "bkg_prefit_parameters_folder": "bkg_prefit_parameters",
    # plots
    "signal_plot": "plots/signal/",
    "workspace_plots_folder": "plots/workspace/{method}/{dataset}/{region}/{quantity}",
    "final_plots_folder": "plots/results/{method}/{dataset}/{region}/{quantity}",
    "check_workspace_plot": "plots/check_workspace/{method}/{dataset}/{region}/{quantity}/comparison_{region}_{quantity}.png",
    "cfactor_sys_plot_folder": "plots/cfactor_sys",
    "cfactor_sys_plot": "plots/cfactor_sys/plotsys_cfactor_{region}_{quantity}_{direction}.png",
    "pull_plot": "plots/results/{method}/{dataset}/{region}/{quantity}/pull_{region}_{quantity}.png",
    "result_plot": "plots/results/{method}/{dataset}/{region}/{quantity}/plot_{region}_{quantity}.png"
}

def get_path(basedir, parameter:str, **kwargs) -> str:

    pattern = patterns[parameter]
    field_names = set([v[1] for v in string.Formatter().parse(pattern) if v[1] is not None])
    if 'blinded' in kwargs:
        kwargs['blinded'] = 'blinded' if kwargs['blinded'] else 'unblinded'
    for field_name in field_names:
        if field_name not in list(kwargs.keys()):
            raise ValueError('"%s" argument not specified' % field_name)
    return os.path.join(basedir, pattern.format(**kwargs))
