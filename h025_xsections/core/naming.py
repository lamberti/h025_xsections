import os
from functools import total_ordering
from itertools import product

MU_CB, SIGMA_CB, ALPHA_LOW, NCB_LOW, ALPHA_HI, NCB_HI = (
    "mu_cb",
    "sigma_cb",
    "alpha_low",
    "ncb_low",
    "alpha_hi",
    "ncb_hi",
)


@total_ordering
class FileHistoInfo:
    def __init__(self, mc, prod, skim, sysgroup, sys):
        self.mc = mc
        self.prod = prod
        self.skim = skim
        self.sysgroup = sysgroup
        self.sys = sys

    def __lt__(self, other):
        return (self.mc, self.prod, self.skim, self.sysgroup, self.sys) < (
            other.mc,
            other.prod,
            other.skim,
            other.sysgroup,
            other.sys,
        )

    def __hash__(self):
        return hash((self.mc, self.prod, self.skim, self.sysgroup, self.sys))

    def __eq__(self, other):
        return (
            self.mc == other.mc
            and self.prod == other.prod
            and self.skim == other.skim
            and self.sysgroup == other.sysgroup
            and self.sys == other.sys
        )

    def format_tags(self):
        return "{mc}.{prod}.{skim}.{sysgroup}.{sys}".format(
            mc=self.mc, prod=self.prod, skim=self.skim, sysgroup=self.sysgroup, sys=self.sys
        )

    def get_line_csv(self):
        return "\t".join((self.mc, self.prod, self.skim, self.sysgroup, self.sys))

    def asdict(self):
        return {
            "mc": self.mc,
            "prod": self.prod,
            "skim": self.skim,
            "sysgroup": self.sysgroup,
            "sys": self.sys,
        }


class Catalogue(dict):
    @staticmethod
    def create_catalogue(fns):
        catalogue = Catalogue()

        for fn in fns:
            sample_info = parse_histo_filename(fn)
            if sample_info in catalogue:
                raise ValueError("duplicate: %s" % sample_info.get_line_csv())
            catalogue[sample_info] = fn

        catalogue.update_all()
        return catalogue

    def __init__(self, *args, **kwargs):
        super(Catalogue, self).__init__(*args, **kwargs)
        self._all_production = []
        self._all_mc = []
        self._common_tags = FileHistoInfo(mc=None, prod=None, skim=None, sysgroup=None, sys=None)

    def all_mc(self):
        return self._all_mc

    def all_production(self):
        return self._all_production

    def missings(self, mc=None, prod=None, skim=None, sysgroup=None, sys=None):
        if mc is None:
            mc = [None]
        if prod is None:
            prod = [None]
        if skim is None:
            skim = [None]
        if sysgroup is None:
            sysgroup = [None]
        if sys is None:
            sys = [None]

        missings = []

        for _mc, _prod, _skim, _sysgroup, _sys in product(mc, prod, skim, sysgroup, sys):
            sub_cat = self.sub_catalogue(_mc, _prod, _skim, _sysgroup, _sys)
            if not sub_cat:
                sample_missing = FileHistoInfo(
                    prod=_prod, mc=_mc, skim=_skim, sysgroup=_sysgroup, sys=_sys
                )
                missings.append(sample_missing)

        return missings

    def update_all(self):
        self._all_mc = set([s.mc for s in self])
        self._all_production = set([s.prod for s in self])
        if len(self._all_mc) == 1:
            self._common_tags.mc = list(self._all_mc)[0]
        if len(self._all_production) == 1:
            self._common_tags.prod = list(self._all_production)[0]
        skims = set([s.skim for s in self])
        if len(skims) == 1:
            self._common_tags.skim = list(skims)[0]
        sysgroups = set([s.sysgroup for s in self])
        if len(sysgroups) == 1:
            self._common_tags.sysgroup = list(sysgroups)[0]
        syss = set([s.sys for s in self])
        if len(syss) == 1:
            self._common_tags.sys = list(syss)[0]

    def sub_catalogue(self, mc=None, prod=None, skim=None, sysgroup=None, sys=None, function=None):
        catalogue = Catalogue()
        for sample_info, sample_fn in self.items():
            if mc is not None and sample_info.mc != mc:
                continue
            if prod is not None and sample_info.prod != prod:
                continue
            if skim is not None and sample_info.skim != skim:
                continue
            if sysgroup is not None and sample_info.sysgroup != sysgroup:
                continue
            if sys is not None and sample_info.sys != sys:
                continue
            if function is not None and not function(sample_info):
                continue
            catalogue[sample_info] = sample_fn
        catalogue.update_all()
        return catalogue

    def common_tags(self):
        return self._common_tags

    def check_mc_prod(self):
        # check that for a fixed mc-period, prod and sys there is only 1 file
        msgs = []
        for mc_period in self.all_mc():
            for prod in self.all_production():
                samples = [s for s in self if (s.mc == mc_period and s.prod == prod)]
                if not samples:
                    msg = "no sample for prod=%s and mc-period=%s" % (prod, mc_period)
                    msgs.append(msg)
        return msgs


def parse_histo_filename(fn):
    fn1 = os.path.split(fn)[1]
    els = fn1.replace("histo_", "").replace(".root", "").split(".")
    sample = FileHistoInfo(prod=els[1], mc=els[0], skim=els[2], sysgroup=els[3], sys=els[4])
    return sample


def parse_efficiency_filename(fn):
    fn1 = os.path.split(fn)[1]
    els = fn1.replace("efficiencies_", "").split(".")
    sample = FileHistoInfo(prod=els[1], mc=els[0], skim=els[2], sysgroup=els[3], sys=els[4])
    return sample


def get_bin_suffix(region, quantity, ibin):
    return "{region}_{quantity}_{ibin}".format(region=region, quantity=quantity, ibin=ibin)


def get_category_name(region, quantity, ibin):
    suffix = get_bin_suffix(region, quantity, ibin)
    return "category_{suffix}".format(suffix=suffix)


def get_truebin_name(region, quantity, ibin):
    suffix = get_bin_suffix(region, quantity, ibin)
    return suffix
