from typing import List

class BackgroundExpression:  # pylint: disable=too-few-public-methods
    @classmethod
    def get(cls, name:str, obs:str, par_basename:str, par_values:List=None):
        par_values = par_values or cls._default_par_values  # pylint: disable=no-member
        result = cls._template  # pylint: disable=no-member

        for ipar, par_value in enumerate(par_values, 1):
            par_string = "%f,%f,%f" % (par_value[0], par_value[1], par_value[2])
            template_string = "<par%d>" % (ipar)
            result = result.replace(template_string, par_string)
        result = result.format(name=name, par_basename=par_basename, obs=obs)
        return result


# pylint: disable=too-few-public-methods
# pylint: disable=line-too-long
class Exponential(BackgroundExpression):
    _default_par_values = [(0, -20, 20)]
    _template = "EXPR::{name}('exp(@0 * @1)', {obs}, {par_basename}_1[<par1>])"


class Pow(BackgroundExpression):
    _default_par_values = [(0, -10, 0)]
    _template = "EXPR::{name}('pow(@0/100., @1)', {obs}, {par_basename}_1[<par1>])"


class ExpPoly2(BackgroundExpression):
    _default_par_values = [(-5, -10, 5), (2, 0.001, 6)]
    _template = "EXPR::{name}('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', {obs}, {par_basename}_1[<par1>], {par_basename}_2[<par2>])"


class ExpPoly3(BackgroundExpression):
    _default_par_values = [(-5, -10, 5), (2, 0.001, 6), (2.0, 0.001, 6)]
    _template = "EXPR::{name}('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100 + @3*(@0 - 100)/100*(@0 - 100)/100))', {obs}, {par_basename}_1[<par1>], {par_basename}_2[<par2>], {par_basename}_3[<par3>])"


class Bern3(BackgroundExpression):
    _default_par_values = [(0.6, -10, 10)] * 3
    _template = "RooBernsteinM::{name}({obs}, {{1, {par_basename}_1[<par1>], {par_basename}_2[<par2>], {par_basename}_3[<par3>]}})"


class Bern4(BackgroundExpression):
    _default_par_values = [(0.6, -10, 10)] * 4
    _template = "RooBernsteinM::{name}({obs}, {{1, {par_basename}_1[<par1>], {par_basename}_2[<par2>], {par_basename}_3[<par3>], {par_basename}_4[<par4>]}})"


class Bern5(BackgroundExpression):
    _default_par_values = [(0.6, -10, 10)] * 5
    _template = "RooBernsteinM::{name}({obs}, {{1, {par_basename}_1[<par1>], {par_basename}_2[<par2>], {par_basename}_3[<par3>], {par_basename}_4[<par4>], {par_basename}_5[<par5>]}})"


_factories = {
    name: the_class
    for name, the_class in locals().items()
    if (
        isinstance(the_class, type)
        and issubclass(the_class, BackgroundExpression)
        and the_class is not BackgroundExpression
    )
}

VALID_BACKGROUND_FUNCTIONS = list(_factories.keys())


def background_factory(bkg_name:str, bkg_function:str, obs:str, par_basename:str, par_values:List=None):
    if bkg_function in _factories:
        return _factories[bkg_function].get(bkg_name, obs, par_basename, par_values)
    else:
        raise ValueError("cannot understand functional form %s" % bkg_function)