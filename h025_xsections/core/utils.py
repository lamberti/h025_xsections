import sys
import os

if sys.version_info.major == 2:
    from itertools import tee, izip

    def pairwise_(iterable):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        a, b = tee(iterable)
        next(b, None)
        return izip(a, b)


else:
    from itertools import tee

    def pairwise_(iterable):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        a, b = tee(iterable)
        next(b, None)
        return zip(a, b)


pairwise = pairwise_


def create_dir_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def create_dir_for_file(fn):
    folder = os.path.dirname(fn)
    create_dir_if_not_exists(folder)


def loop_iterator(iterator):
    object = iterator.Next()
    while object:
        yield object
        object = iterator.Next()


def iter_collection(rooAbsCollection):
    iterator = rooAbsCollection.createIterator()
    return loop_iterator(iterator)


def silence_roofit(ROOT):
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.NumIntegration)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Fitting)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Minimization)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.InputArguments)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Eval)
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.DataHandling)
    ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)
    ROOT.RooMsgService.instance().setSilentMode(True)


def short_array_repr(iterable, fmt="%s"):
    def repr(el):
        return fmt % el

    l = list(iterable)
    if not l:
        return "[ ]"
    elif len(iterable) < 10:
        return ", ".join(map(repr, l))
    else:
        l1 = ", ".join(map(repr, l[:4]))
        l2 = ", ".join(map(repr, l[-3:]))
        return l1 + ", ..., " + l2


def extract_info_from_fitresult(fit_results, const=False):
    if not const:
        fit_results_RooArgSet = fit_results.floatParsFinal()
    else:
        fit_results_RooArgSet = fit_results.constPars()
    fit_results_collection = iter_collection(fit_results_RooArgSet)

    output = {}
    for fit_elem in fit_results_collection:
        output[fit_elem.GetName()] = (fit_elem.getVal(), fit_elem.getError())
    return output
