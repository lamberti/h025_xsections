from typing import List
import os
import logging

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


class Slide:
    def __init__(self, title="MyTitle", subtitle=""):
        self.title = title
        self.subtitle = subtitle
        self.content = []

    def add_content(self, content):
        self.content.append(content)

    def render(self):
        content_rendered = "\n".join([c.render() for c in self.content])
        return (
            r"""
\begin{frame}{<title>}{<subtitle>}
<content>
\end{frame}
""".replace(
                "<title>", self.title
            )
            .replace("<subtitle>", self.subtitle)
            .replace("<content>", content_rendered)
        )


class Document:
    def __init__(self, title="", subtitle="", authors=""):
        self.title = title
        self.authors = authors
        self.slides = []
        self.subtitle = subtitle

    def add_slide(self, slide: Slide):
        self.slides.append(slide)

    def add_slides(self, slides: List[Slide]):
        for slide in slides:
            self.add_slide(slide)

    def render(self):
        content = "\n".join(slide.render() for slide in self.slides)
        return (
            r"""
\documentclass[10pt, mathsans, compress, xcolor=dvipsnames, table]{beamer}

\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=,urlcolor=links}

\usepackage[utf8x]{inputenc}
\usepackage[separate-uncertainty=true,multi-part-units=single]{siunitx}
\usepackage{booktabs}

\usepackage{etoolbox}
\makeatletter

\patchcmd{\beamer@section}{{#2}{\the\c@page}}{{#1}{\the\c@page}}{}{}
\patchcmd{\beamer@section}{{\the\c@section}{\secname}}{{\the\c@section}{#1}}{}{}
\makeatother

\setbeamertemplate{blocks}[rounded][shadow=true]

\useoutertheme{infolines}
\usetheme[height=9mm]{Rochester}
\setbeamertemplate{navigation symbols}{}
\usecolortheme[RGB={51,153,255}]{structure}

\definecolor{lbcolor}{rgb}{0.9,0.9,0.9}
\definecolor{mydarkgray}{cmyk}{0.43, 0.35, 0.35,0.01}

\title{<title>}
\subtitle{<subtitle>}
\author{<authors>}

\begin{document}

\begin{frame}[plain]
  \maketitle
\end{frame}

<slides>
\end{document}

""".replace(
                "<slides>", content
            )
            .replace("<title>", self.title)
            .replace("<subtitle>", self.subtitle)
            .replace("<authors>", self.authors)
        )


def escape(s):
    return s.replace("_", "\_")


class RawLatex:
    def __init__(self, latex: str):
        self.latex = latex

    def render(self):
        return self.latex


class Graphics:
    def __init__(self, path: str, options=None):
        self.path = path
        self.options = options

    def render(self):
        options = "[%s]" % self.options if self.options is not None else ""
        if os.path.exists(self.path):
            return r"\includegraphics{options}{{{path}}}".format(options=options, path=self.path)
        else:
            logging.error("cannot find image %s", self.path)
            return r"{missing image %s}" % self.path.replace("_", r"\_")


class Figure:
    def __init__(self, graphics: List[Graphics] = None, caption: str = None):
        self.content = graphics or []
        self.caption = caption

    def add_figure(self, figure):
        self.content.append(figure)

    def render(self):
        latex = r"\begin{figure}" + "\n" + r"\centering" + "\n"
        latex_figures = [g.render() for g in self.content]
        latex_figures = "\n".join(latex_figures)
        latex += latex_figures + "\n"
        if self.caption is not None:
            latex += r"\caption{%s}" % self.caption
        latex += r"\end{figure}" + "\n"
        return latex
