import numpy as np
from matplotlib import pyplot as plt

# ************ Convert ROOT TH into numpy array **********
def TH1F2np(histo, overflow=True, underflow=True):
    content_ = histo.GetArray()
    content_.reshape((histo.GetNbinsX() + 2,))
    if histo.ClassName() == "TH1F":
        dtype = np.float32
    elif histo.ClassName() == "TH1D":
        dtype = np.float64
    content = np.frombuffer(content_, dtype=dtype, count=histo.GetNbinsX() + 2).copy()
    if not overflow:
        content = content[:-1]
    if not underflow:
        content = content[1:]

    if histo.GetXaxis().IsVariableBinSize():
        binning = np.array(histo.GetXaxis().GetXbins())
    else:
        binning = np.linspace(
            histo.GetXaxis().GetXmin(), histo.GetXaxis().GetXmax(), histo.GetXaxis().GetNbins() + 1
        )
    if overflow:
        binning = np.append(binning, [np.inf])
    if underflow:
        binning = np.insert(binning, 0, [-np.inf])
    return content, binning


def TH2F2np(histo, underflow=True, overflow=True):
    content = np.zeros((histo.GetNbinsX() + 2, histo.GetNbinsY() + 2))

    # dump everything (under/over-flow included)
    for xbin in range(0, histo.GetNbinsX() + 2):
        for ybin in range(0, histo.GetNbinsY() + 2):
            content[xbin, ybin] = histo.GetBinContent(xbin, ybin)

    if not overflow:
        content = content[:-1, :-1]
    if not underflow:
        content = content[1:, 1:]

    if histo.GetXaxis().IsVariableBinSize():
        binningx = np.array(histo.GetXaxis().GetXbins())
    else:
        binningx = np.linspace(
            histo.GetXaxis().GetXmin(), histo.GetXaxis().GetXmax(), histo.GetXaxis().GetNbins() + 1
        )
    if histo.GetYaxis().IsVariableBinSize():
        binningy = np.array(histo.GetYaxis().GetXbins())
    else:
        binningy = np.linspace(
            histo.GetYaxis().GetXmin(), histo.GetYaxis().GetXmax(), histo.GetYaxis().GetNbins() + 1
        )

    if overflow:
        binningx = np.append(binningx, [np.inf])
        binningy = np.append(binningy, [np.inf])
    if underflow:
        binningx = np.append([-np.inf], binningx)
        binningy = np.append([-np.inf], binningy)

    return content, binningx, binningy


# *********** Histograms manipulations ***************
def get_binning_for_plot(binning):
    binning = np.copy(binning)
    if binning[-1] == np.inf:
        binning[-1] = (binning[-2] - binning[-3]) + binning[-2]
    if binning[0] == -np.inf:
        binning[0] = binning[1] - (binning[2] - binning[1])
    return binning


def remove_under_overflow(data, do_underflow, do_overflow):
    """
    given an array return a copy removing the first and/or the last element,
    depending on the flags
    """
    data = np.copy(data)
    if not do_overflow:
        data = data[:-1]
    if not do_underflow:
        data = data[1:]
    return data


def get_content_binning_from_file(f, name):
    """
    return the content and the binning of an histogram with name name from ROOT file f
    """
    h = f.Get(name)
    if not h:
        raise ValueError("cannot find histogram %s in file %s" % (name, f))
    content, binning = TH1F2np(h)
    return content, binning


# ************ Plotting functions **************
def plot1D(
    binning,
    content,
    bin_labels_with_extraflow,
    title,
    latex_label,
    show_underflow,
    show_overflow,
    do_normalization,
    do_log_x,
    ax=None,
    color="red",
):
    """
    binning: edges without under/over-flow
    content: full content, including under/over-flow
    """

    # remove under/overflow if needed
    content_plot = remove_under_overflow(
        content, do_underflow=show_underflow, do_overflow=show_overflow
    )

    # add -inf, +inf to the binning if needed
    binning_with_extraflow = np.copy(np.array(binning))
    if show_underflow:
        binning_with_extraflow = np.append(-np.inf, binning_with_extraflow)
    if show_overflow:
        binning_with_extraflow = np.append(binning_with_extraflow, np.inf)

    # create special bins for under/over-flow
    binning_plot = get_binning_for_plot(binning_with_extraflow)

    bincenters = 0.5 * (binning_plot[1:] + binning_plot[:-1])
    binwidths = binning_plot[1:] - binning_plot[:-1]

    bin_labels_plot = bin_labels_with_extraflow[:]

    # add labels for under/over-flow
    bin_labels_plot = remove_under_overflow(
        bin_labels_plot, do_overflow=show_overflow, do_underflow=show_underflow
    )

    if ax is None:
        fig, ax = plt.subplots()
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)

    ax.bar(
        bincenters,
        content_plot / binwidths if do_normalization else content_plot,
        binwidths,
        color=color,
    )
    ax.set_xlabel(latex_label)
    # ax.text(binning[-7], 0.150, r'$H\rightarrow\gamma\gamma$', fontsize=13)
    # ax.text(binning[-7], 0.140, r'$\sqrt{s} = 13$ TeV', fontsize=13)
    # ax.text(binning[-7], 0.130, r'$140$ fb$^{-1}$', fontsize=13)
    if do_normalization:
        ax.set_ylabel("dN / d%s" % str(latex_label.split("[")[0]))
    else:
        ax.set_ylabel("Entries")

    ax.set_title(title)
    if do_log_x:
        if binning_plot[0] == 0:
            ax.set_xlim(binning_plot[0] + binwidths[0] * 0.25)
        ax.set_xscale("log")

    if bin_labels_plot is not None:
        ax.set_xticks(bincenters)
        ax.set_xticklabels(bin_labels_plot, rotation=90)

    return fig


def plot2D(content2d, latex_label, bin_labelx, bin_labely, v_min, v_max, f_mt, ax=None):
    import seaborn as sns

    if ax is None:
        fig, ax = plt.subplots(figsize=(10, 10))

    hm = sns.heatmap(
        content2d,
        xticklabels=bin_labelx,
        yticklabels=bin_labely,
        annot=True,
        ax=ax,
        square=True,
        vmin=v_min,
        vmax=v_max,
        cmap="YlGnBu",
        fmt=f_mt,
        cbar_kws={"shrink": 0.82},
        annot_kws={"size": 35 / np.sqrt(len(content2d))},
    )

    ax.set_ylabel("true %s" % latex_label, fontsize=18)
    ax.set_xlabel("reco %s" % latex_label, fontsize=18)
    hm.set_yticklabels(hm.get_yticklabels(), va="center", fontsize=13)
    hm.set_xticklabels(hm.get_xticklabels(), rotation=90, fontsize=13)

    return fig, ax


def plot_stack(
    binning,
    histograms,
    labels,
    bin_labels_with_extraflow,
    latex_label,
    show_underflow,
    show_overflow,
    do_normalization,
    do_log_x,
    stacked=True,
    ylabel="N",
    kwargs_bar=None,
):

    kwargs_bar = kwargs_bar or {}

    # add -inf, +inf to the binning if needed
    binning_with_extraflow = np.copy(np.array(binning))
    if show_underflow:
        binning_with_extraflow = np.append(-np.inf, binning_with_extraflow)
    if show_overflow:
        binning_with_extraflow = np.append(binning_with_extraflow, np.inf)

    # create special bins for under/over-flow
    binning_plot = get_binning_for_plot(binning_with_extraflow)

    bincenters = 0.5 * (binning_plot[1:] + binning_plot[:-1])
    binwidths = binning_plot[1:] - binning_plot[:-1]

    bin_labels_plot = bin_labels_with_extraflow[:]

    # add labels for under/over-flow
    bin_labels_plot = remove_under_overflow(
        bin_labels_plot, do_overflow=show_overflow, do_underflow=show_underflow
    )

    contents = histograms[:]
    contents = [
        remove_under_overflow(c, do_underflow=show_underflow, do_overflow=show_overflow)
        for c in contents
    ]

    fig, ax = plt.subplots()
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    content_previous = np.zeros_like(contents[0])

    for content, label in zip(contents, labels):
        ax.bar(
            bincenters,
            content / binwidths if do_normalization else content,
            binwidths,
            bottom=content_previous / binwidths if do_normalization else content_previous,
            label=label,
            **kwargs_bar
        )

        if stacked:
            content_previous += content

    ax.set_xlabel(latex_label)

    if do_normalization:
        ax.set_ylabel("d%s / d%s" % (ylabel, str(latex_label.split("[")[0])))
    else:
        ax.set_ylabel(ylabel)

    if do_log_x:
        if binning_plot[0] == 0:
            ax.set_xlim(binning_plot[0] + binwidths[0] * 0.25)
        ax.set_xscale("log")

    if bin_labels_plot is not None:
        ax.set_xticks(bincenters, minor=False)
        ax.set_xticks([], minor=True)
        ax.set_xticklabels(bin_labels_plot, rotation=90)

    ax.legend()

    return fig
