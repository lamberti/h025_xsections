import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt


def check_samples(fn):
    df = pd.read_csv(fn, sep="\t", header=None)
    df.columns = ["production", "mc", "skim", "sys", "i", "fn", "entries", "reco", "truth"]
    df["reco"] = df["reco"].str.split(",")
    df = df[df['production'] != '?']

    cols = [c for c in df.columns if c != "reco"]

    df2 = (
        df.reco.apply(pd.Series)
        .merge(df, right_index=True, left_index=True)
        .drop(["reco"], axis=1)
        .melt(id_vars=cols, value_name="reco")
        .drop("variable", axis=1)
    )

    df = df.set_index(['mc', 'production', 'skim', 'sys'])
    print(df)
    df_pivot = df['entries'].unstack('sys')

    fig, ax = plt.subplots(figsize=(15, 15))
    sns.heatmap(df_pivot, ax=ax, annot=True)
    fig.savefig("sample_check.png", bbox_inches='tight')


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    check_samples(args.filename)
