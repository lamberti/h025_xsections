import logging
import subprocess
from dataclasses import dataclass
from typing import List, Optional

import ROOT
from h025_xsections.core.utils import iter_collection

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


@dataclass
class FitArgs:
    filename: str
    ws_name: str
    dataset_name: str
    output_fname: str
    min_tolerance: Optional[float] = None
    savews: bool = False
    pois: Optional[List[str]] = None
    fix_np: Optional[List[str]] = None
    name: Optional[str] = None

    def get_command(self) -> str:
        if self.pois is None:
            f = ROOT.TFile.Open(self.filename)
            ws = f.Get(self.ws_name)
            ROOT.SetOwnership(ws, True)
            self.pois = get_poi_list(ws)
        pois = ",".join(self.pois)

        command = "quickFit -f {filename} -w {ws_name} -d {dataset} -p {pois} --saveWS {savews} -o {output_fname}".format(
            filename=self.filename,
            ws_name=self.ws_name,
            pois=pois,
            dataset=self.dataset_name,
            output_fname=self.output_fname,
            # min_tolerance=self.min_tolerance,
            savews=int(self.savews),
        )
        if self.min_tolerance is not None:
            command += " --minTolerance %.20f" % self.min_tolerance
        if self.fix_np is not None:
            command += " --fixNP {NP}".format(NP=",".join(self.fix_np))
        return command


def get_poi_list(ws) -> List[str]:
    return [
        s.GetName()
        for s in iter_collection(ws.obj("ModelConfig").GetParametersOfInterest())
        if "xsec" in s.GetName()
    ]


def run_command(fit_args: FitArgs):
    command = fit_args.get_command()
    subprocess.call(command, shell=True)

    fout = ROOT.TFile.Open(fit_args.output_fname)
    if not fout:
        logging.error("no output created when fitting %s", fit_args.filename)
        return 1
    fr = fout.Get("fitResult")
    if not fr:
        logging.error("no fit result when fitting %s", fit_args.filename)
        return 1
    ROOT.SetOwnership(fr, True)
    if fr.status() != 0:
        logging.warning("Fit result is %d when fitting %s", fr.status(), fit_args.filename)
        return 1
    else:
        logging.info("fit seems OK")
        return 0


def fit_workspace(fit_args: FitArgs):
    command = fit_args.get_command()
    logging.info("running command %s", command)
    subprocess.call(command, shell=True)

    fout = ROOT.TFile.Open(fit_args.output_fname)
    if not fout:
        logging.error("no output created when fitting %s", fit_args.filename)
        return 1
    fr = fout.Get("fitResult")
    if not fr:
        logging.error("no fit result when fitting %s", fit_args.filename)
        return 1
    ROOT.SetOwnership(fr, True)
    if fr.status() != 0:
        logging.warning("Fit result is %d when fitting %s", fr.status(), fit_args.filename)
        return 1
    else:
        logging.info("fit seems OK")
        return 0


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Fit workspace")
    parser.add_argument("workspace")
    parser.add_argument("output")
    parser.add_argument("--ws-name", default="combWS")
    parser.add_argument("--dataset", default="AsimovSB")
    parser.add_argument("--pois")
    parser.add_argument("--fix-np")
    parser.add_argument("--min-tolerance")
    parser.add_argument("--save-ws", action="store_true")
    args = parser.parse_args()
    fit_args = FitArgs(
        args.workspace,
        args.ws_name,
        args.dataset,
        args.output,
        args.min_tolerance,
        args.save_ws,
        args.pois,
        args.fix_np,
    )

    fit_workspace(fit_args)
