#!/usr/bin/env python

import logging
import os
import re
from glob import glob

import pandas as pd
import numpy as np
import ROOT

ROOT.PyConfig.StartGuiThread = False
from matplotlib import pyplot as plt

from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.utils import extract_info_from_fitresult, create_dir_if_not_exists

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def compare(region, quantity, method, input_dir, output_dir, output_dir_plot, dataset):
    logging.info("producing plots sys for %s %s", region, quantity)
    create_dir_if_not_exists(output_dir)
    create_dir_if_not_exists(output_dir_plot)
    all_fns = glob(os.path.join(input_dir, "*.root"))
    if not all_fns:
        logging.error("no ROOT files found in folder %s", input_dir)
    regex = re.compile(
        r"XSectionWS_{region}_{quantity}-fitted-{dataset}-([\w-]+).root".format(
            region=region, quantity=quantity, dataset=dataset
        )
    )
    results = {}

    for fn in all_fns:
        m = regex.match(os.path.split(fn)[1])
        if not m:
            logging.warning("cannot parse %s", fn)
            continue
        sys_suffix = m.group(1)
        grouping = None
        sys_name = None
        if sys_suffix == "full" or sys_suffix == "statonly":
            grouping = False
            sys_name = sys_suffix
        if "fixedgroup" in sys_suffix:
            grouping = True
            sys_name = sys_suffix.replace("fixedgroup-", "")
        else:
            grouping = False
            sys_name = sys_suffix
        f = ROOT.TFile.Open(fn)
        fit_result = f.Get("fitResult")
        ROOT.SetOwnership(fit_result, True)
        fitted_pars = extract_info_from_fitresult(fit_result)
        fitted_pars = {
            par: values[1] for par, values in fitted_pars.items() if par.startswith("mu_xsec")
        }
        results[(grouping, sys_name)] = fitted_pars

    if not results:
        raise IOError("cannot find any fitted workspace in %s" % input_dir)

    errors = pd.DataFrame.from_dict(results, orient="index")
    if True not in errors.index.levels[0]:
        logging.error("cannot find group systematic for %s %s", region, quantity)
        return
    if "full" not in errors.index.levels[1]:
        logging.error("cannot find full fill for %s %s", region, quantity)
        return
    errors.columns = errors.columns.str.replace(
        "mu_xsec_{region}_{quantity}_".format(region=region, quantity=quantity), ""
    ).astype(int)
    errors.sort_index(axis=1, inplace=True)
    errors_partial = errors.loc[(False, "full")] ** 2 - errors ** 2
    errors_partial = errors_partial.where(errors_partial >= 0, 0.0)
    errors_partial = np.sqrt(errors_partial)
    errors_group = errors_partial.loc[True]

    errors_group.to_csv(
        os.path.join(
            output_dir,
            "sysgroup_{method}_{region}_{quantity}.txt".format(
                method=method, region=region, quantity=quantity
            ),
        )
    )

    fractional_error = errors_group / errors_group.sum(axis=0)
    fig, ax = plt.subplots()
    fractional_error.T.plot.bar(stacked=True, ax=ax, width=1, cmap="Accent")
    ax2 = ax.twinx()
    ax2.step(
        errors.loc[False, "full"].index - 1,
        errors.loc[False, "full"].values,
        color="k",
        where="mid",
        linestyle="--",
    )
    ax.set_ylim(0, 1)
    fig.savefig(
        os.path.join(
            output_dir_plot,
            "fraction_sys_errorgroup_{method}_{region}_{quantity}.png".format(
                method=method, region=region, quantity=quantity
            ),
        ),
        bbox_inches="tight",
    )

    plt.close(fig)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="fit over all systematic variations")
    parser.add_argument("input_dir")
    parser.add_argument("output_dir")
    parser.add_argument("--region", nargs="*")
    parser.add_argument("--quantity", nargs="*")
    parser.add_argument("--method")
    parser.add_argument("--dataset", default="AsimovSB")
    args = parser.parse_args()

    compare(args.region, args.quantity, args.method, args.input_dir, args.output_dir, args.output_dir, args.dataset)
