#!/usr/bin/env python

import logging
import os
import re
from typing import List


import ROOT
from h025_xsections.core.utils import (
    create_dir_if_not_exists,
    iter_collection,
)

from h025_xsections.fit_data.fit import get_poi_list, run_command, FitArgs

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def get_NP_list_from_RooArgSet_selection(ws, name_selection):
    NP_list = []
    RooArgSet_selection = ws.allVars().selectByName(name_selection)
    for item in iter_collection(RooArgSet_selection):
        NP_list.append("%s" % item.GetName())
    return NP_list


SYS_GROUPS = {
    "LUMINOSITY": ("ATLAS_lumi",),
    "PER_PES": ("ATLAS_EG_SCALE", "ATLAS_PH_SCALE", "ATLAS_MSS_HIGGS_MASS", "ATLAS_EG_RESOLUTION"),
    "PHOTON_ID": ("ATLAS_PH_EFF_ID", "ATLAS_PH_EFF_ISO"),
    "PILEUP": ("ATLAS_PRW_DATASF",),
    "TRIGGER": ("ATLAS_PH_EFF_TRIGGER",),
    "JET": ("ATLAS_JET",),
    "MUON": ("ATLAS_MUON_",),
    "ELECTRON": ("ATLAS_EL_",),
    "SPURIOUS": ("ATLAS_Hgg_Bias_",),
}


def create_condor_submission(fit_args: List[FitArgs], condor_folder: str):
    create_dir_if_not_exists(condor_folder)
    condor_txt = """
universe                = docker
image                   = gitlab-registry.cern.ch/lamberti/h025_xsections/dev
executable              = wrapper_fit.sh
output                  = {output_dir}/fit.$(ClusterId).$(ProcId).out
error                   = {error_dir}/fit.$(ClusterId).$(ProcId).err
log                     = {log_dir}/fit.$(ClusterId).log

queue command from (
{queue}
)
"""
    queue = []
    for fit_arg in fit_args:
        queue.append(fit_arg.get_command())
    queue = "\n".join(queue)

    condor_txt = condor_txt.format(
        output_dir=condor_folder, error_dir=condor_folder, log_dir=condor_folder, queue=queue
    )

    with (open(os.path.join(condor_folder, "submit.condor"), "w")) as f:
        f.write(condor_txt)


def grouping_NP(np_list, groups):
    results = {}
    for np_input in np_list:
        for group_name, np_regexes in groups.items():
            if any(re.match(np_regex, np_input) for np_regex in np_regexes):
                results.setdefault(group_name, []).append(np_input)
                break
        else:
            logging.error("cannot put systematic %s in any group", np_input)
    return results


def loop_sys(
    filename,
    ws_name,
    data_name,
    output_dir,
    do_stat_and_full=True,
    do_sys_loop=True,
    do_sysgroup_loop=True,
    execution="direct",
):
    create_dir_if_not_exists(output_dir)

    f = ROOT.TFile.Open(filename)
    if not f or f.IsZombie():
        raise IOError("cannot find file %s" % filename)
    ws = f.Get(ws_name)
    ROOT.SetOwnership(ws, True)

    pois = get_poi_list(ws)

    NP_list = get_NP_list_from_RooArgSet_selection(ws, "ATLAS*")
    logging.info("Nuisance Parameters list: %s", NP_list)
    logging.info("POI list: %s", pois)

    prefix_ws = os.path.splitext(os.path.basename(filename))[0]
    fit_args = []

    if do_stat_and_full:
        # full error
        fit_name = "full"
        logging.info("adding fit %s", fit_name)
        output_fname = prefix_ws + "-fitted-{dataset}-{fit_name}.root"
        output_fname = os.path.join(output_dir, output_fname).format(
            dataset=data_name, fit_name=fit_name
        )
        fit_args.append(
            FitArgs(
                filename, ws_name, data_name, output_fname, savews=True, pois=pois, name="full-fit"
            )
        )

        # stat only
        fit_name = "statonly"
        logging.info("adding fit %s", fit_name)
        output_fname = prefix_ws + "-fitted-{dataset}-{fit_name}.root"
        output_fname = os.path.join(output_dir, output_fname).format(
            dataset=data_name, fit_name=fit_name
        )
        fit_args.append(
            FitArgs(
                filename,
                ws_name,
                data_name,
                output_fname,
                savews=True,
                pois=pois,
                fix_np=NP_list,
                name=fit_name,
            )
        )

    # full error minus one
    if do_sys_loop:
        for NP_elem in NP_list:
            fit_name = "fixed-%s" % NP_elem
            logging.info("adding fit %s", fit_name)
            output_fname = prefix_ws + "-fitted-{dataset}-{fit_name}.root"
            output_fname = os.path.join(output_dir, output_fname).format(
                dataset=data_name, fit_name=fit_name
            )
            fit_args.append(
                FitArgs(
                    filename,
                    ws_name,
                    data_name,
                    output_fname,
                    savews=False,
                    pois=pois,
                    fix_np=[NP_elem],
                    name=fit_name,
                )
            )

    if do_sysgroup_loop:
        for group_name, NP_list in grouping_NP(NP_list, SYS_GROUPS).items():
            fit_name = "fixedgroup-%s" % group_name
            logging.info("adding fit %s", fit_name)
            output_fname = prefix_ws + "-fitted-{dataset}-{fit_name}.root"
            output_fname = os.path.join(output_dir, output_fname).format(
                dataset=data_name, fit_name=fit_name
            )
            fit_args.append(
                FitArgs(
                    filename,
                    ws_name,
                    data_name,
                    output_fname,
                    savews=False,
                    pois=pois,
                    fix_np=NP_list,
                    name=fit_name,
                )
            )

    logging.info("submitting %d fits", len(fit_args))

    errors = []
    if execution == "condor":
        logging.warning("condor is not yet supported")
        create_condor_submission(fit_args, "condor")
        return errors

    for fit_arg in fit_args:
        if execution == "direct":
            logging.info("running fit %s", fit_arg.name)
            status = run_command(fit_arg)
            if status != 0:
                errors.append(fit_arg.name)
        elif execution == "none":
            logging.info("command: %s", fit_arg.get_command())

    return errors


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="fit over all systematic variations")
    parser.add_argument("workspace")
    parser.add_argument("--output-dir")
    parser.add_argument("--dataset", default="AsimovSB")
    parser.add_argument("--do-stat-and-full", action="store_true")
    parser.add_argument("--do-sys-loop", action="store_true")
    parser.add_argument("--do-sysgroup-loop", action="store_true")
    parser.add_argument("--execution", choices=["direct", "condor", "none"], default="direct")
    args = parser.parse_args()

    loop_sys(
        args.workspace,
        "combWS",
        args.dataset,
        args.output_dir,
        do_stat_and_full=args.do_stat_and_full,
        do_sys_loop=args.do_sys_loop,
        do_sysgroup_loop=args.do_sysgroup_loop,
        execution=args.execution,
    )
