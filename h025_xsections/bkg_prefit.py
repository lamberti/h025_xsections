#!/usr/bin/env python

import logging
import os
from glob import glob
from typing import List

import numpy as np
import ROOT

ROOT.PyConfig.IgnoreCommandLineOptions = True

from h025_xsections.core import definitions, utils
from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.background import background_factory, VALID_BACKGROUND_FUNCTIONS
from h025_xsections.reader.LPHE import Reader as Reader_LPHE
from h025_xsections.core.utils import loop_iterator, iter_collection

ROOT.gROOT.SetBatch(True)  # Disable histogram display when running
bern_cxx = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "make_workspace/RooBernsteinM.cxx+"
)

ROOT.gROOT.ProcessLine(".L %s" % bern_cxx)

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

NBINS = 220
LOW = 105
HIGH = 160


def extract_info_from_fitresult(fit_results):
    fit_results_RooArgSet = fit_results.floatParsFinal()
    fit_results_collection = iter_collection(fit_results_RooArgSet)

    fit_results_list = []
    for fit_elem in fit_results_collection:
        fit_results_list.append([fit_elem.GetName(), fit_elem.getVal(), fit_elem.getError()])
    return sorted(fit_results_list)


def get_data(data_fn: str, obs: str, ibin: str):
    data_nparray = np.genfromtxt(data_fn)

    histo = ROOT.TH1D("mass_scan_%s" % ibin, "mass_scan_%s" % ibin, NBINS, LOW, HIGH)

    data_nparray = np.atleast_1d(data_nparray)
    for v in data_nparray:
        histo.Fill(v)

    rdh = ROOT.RooDataHist(
        "root_data_hist_%s" % ibin,
        "root data hist_%s" % ibin,
        ROOT.RooArgList(obs),
        histo,
    )

    return rdh


def plot(
    data_histo,
    ws,
    model,
    region: str,
    quantity: str,
    ibin: int,
    bkg_function: str,
    fr_list,
    blind_range,
    output_dir: str,
    interactive: bool,
):
    canvas = ROOT.TCanvas()
    bins_plot = NBINS
    nevents = data_histo.sumEntries()
    if nevents < 1000:
        bins_plot = 55
    elif nevents < 10000:
        bins_plot = 110
    frame = ws.var("m_yy").frame(ROOT.RooFit.Bins(bins_plot))
    data_histo.plotOn(frame, ROOT.RooFit.Binning(bins_plot, LOW, HIGH))
    if blind_range is not None:
        model.plotOn(frame, ROOT.RooFit.NormRange("left,right"), ROOT.RooFit.Range("left,right"))
    else:
        model.plotOn(frame)

    chi2 = model.createChi2(data_histo, ROOT.RooFit.Range("left,right")).getVal()
    if chi2 > NBINS * 2:
        logging.warning("chi2 is big for Region: %s Quantity: %s Bin: %s", region, quantity, ibin)

    frame.GetXaxis().SetTitle(r"m_{\gamma\gamma} [GeV]")
    try:
        unit = {1: "", 1000: "GeV"}[definitions.units_final[quantity]]
    except KeyError:
        unit = ""
    # frame.GetYaxis().SetTitle("Events /1 (GeV)")
    frame.SetTitle(
        "Data background fit Region: %s Quantity: %s Bin: %s (%s %s)"
        % (region, quantity, ibin, definitions.bin_labels[quantity][ibin], unit)
    )
    frame.Draw()
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.03)
    latex.DrawLatex(0.6, 0.85, bkg_function)
    latex.DrawLatex(0.6, 0.80, "#chi^{2} / nbins = %.1f / %d" % (chi2, bins_plot))
    for ipar, par in enumerate(fr_list, 1):
        latex.DrawLatex(0.6, 0.75 - 0.05 * ipar, "%s: %.3f" % (par[0], par[1]))

    canvas.SaveAs(os.path.join(output_dir, "fit_plot.pdf"))

    if interactive:
        print("press enter")
        input()


def prefit(
    output_basedir: str,
    data_files: List[str],
    region: str,
    quantity: str,
    background_functions: List[str],
    blind_range=None,
    verbose: bool = False,
    interactive: bool = False,
):

    if len(data_files) != len(background_functions):
        raise ValueError(
            "the number of background functions (%d) is different from the number of bins (%d)"
            % (len(background_functions), len(data_files))
        )

    ws = ROOT.RooWorkspace()
    mass = ws.factory("m_yy[%f, %f]" % (LOW, HIGH))
    mass.setBins(NBINS)
    if blind_range is not None:
        mass.setRange("left", LOW, blind_range[0])
        mass.setRange("right", blind_range[1], HIGH)

    verbosity = ROOT.RooCmdArg() if verbose else ROOT.RooFit.PrintLevel(-1)

    for data_fn in sorted(data_files):
        logging.debug("Processing file %s", data_fn)

        data_name = data_fn.split()[-1]
        ibin_str = data_name.split("_")[-2]
        ibin = int(ibin_str.replace("bin", ""))

        output_dir = os.path.join(output_basedir, ibin_str)
        utils.create_dir_if_not_exists(output_dir)

        fname_output = os.path.join(output_dir, "fit_parameters_%s.txt" % ibin_str)
        bkg_name = "bkg_%d" % ibin

        background_function = background_functions[ibin]
        if not background_function:
            background_function = "ExpPoly2"
        if background_function not in VALID_BACKGROUND_FUNCTIONS:
            logging.warning(
                "bkg function '%s' is not supported, using ExpPoly2", background_function
            )
            background_function = "ExpPoly2"

        bkg_string = background_factory(
            bkg_name, background_function, mass.GetName(), "par_cat%d" % ibin
        )
        logging.debug("fitting with bkg %s = %s", background_function, bkg_string)

        bkg = ws.factory(bkg_string)

        if not bkg:
            raise ValueError(
                "problem creating background from string %s for bin %s" % (bkg_string, ibin_str)
            )
        nbkg_var = ws.factory("nbkg_cat%d[1, 1500000]" % ibin)
        model = ws.factory(
            "SUM::model_cat{ibin}({nbkg} * {bkg})".format(
                nbkg=nbkg_var.GetName(), bkg=bkg.GetName(), ibin=ibin
            )
        )
        mass_set = ROOT.RooArgSet(mass)
        pars = bkg.getParameters(mass_set)
        par_names = [par.GetName() for par in iter_collection(pars)]

        if os.path.getsize(data_fn) != 0:
            data_histo = get_data(data_fn, ws.obj("m_yy"), ibin_str)

            nbkg_from_data = data_histo.sumEntries()

            nbkg_var.setMin(nbkg_from_data * 0.5)
            nbkg_var.setMax(nbkg_from_data * 2)
            nbkg_var.setVal(nbkg_from_data)

            if blind_range is not None:
                fr = model.fitTo(
                    data_histo,
                    ROOT.RooFit.Range("left,right"),
                    verbosity,
                    ROOT.RooFit.Save(),
                )
            else:
                fr = model.fitTo(data_histo, verbosity, ROOT.RooFit.Save())

            fr_list = extract_info_from_fitresult(fr)

            root_error_level = ROOT.gErrorIgnoreLevel
            ROOT.gErrorIgnoreLevel = ROOT.kWarning
            plot(
                data_histo,
                ws,
                model,
                region,
                quantity,
                ibin,
                background_function,
                fr_list,
                blind_range,
                output_dir,
                interactive,
            )
            ROOT.gErrorIgnoreLevel = root_error_level

        else:
            if ibin == 0 and not definitions.show_underflows[quantity]:
                pass
            elif (
                ibin == len(definitions.binnings[quantity]) - 1
                and definitions.show_overflows[quantity]
            ):
                pass
            else:
                logging.warning("File %s is empty. Fix parameters to initial values.", data_fn)
            fr_list = []
            for par_name in par_names:
                par = ws.obj(par_name)
                if par_name == "nbkg":
                    fr_list.append([par.GetName(), 0.0, 0.0])
                else:
                    fr_list.append([par.GetName(), par.getVal(), par.getError()])

        with open(fname_output, "w") as f_output:
            for par in fr_list:
                f_output.write("%s %f %f\n" % (par[0], par[1], par[2]))
            logging.debug(", ".join("%s: %.4f" % (par[0], par[1]) for par in fr_list))


def loop(
    input_folder,
    output_folder,
    regions,
    quantities,
    background_functions=None,
    no_blind=False,
    interactive=False,
    verbose=False,
):
    if verbose:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        utils.silence_roofit(ROOT)

    logging.info("Input directory:             %s", input_folder)
    logging.info("Output directory:            %s", output_folder)
    logging.info("Regions:                     %s", regions)
    logging.info("Quantities:                  %s", quantities)
    if background_functions is None:
        import h025_xsections

        path_file = os.path.join(h025_xsections.__path__[0], "../external-data/ss_results.txt")
        logging.info("Background functions from file: %s", path_file)
        background_functions = path_file
    else:
        logging.info("Background function:          %s", background_functions)

    regions_quantities_analysis = get_region_quantities(regions, quantities)

    logging.info("Running on %d combination(s)", len(regions_quantities_analysis))

    if not no_blind:
        logging.info("BLIND INTERVAL:              [120, 130] GeV")
    blind_range = (120, 130) if not no_blind else None

    reader = Reader_LPHE(basedir=None, normalize=True)

    for region, quantity in regions_quantities_analysis:

        basedir_output = os.path.join(output_folder, region, quantity)
        utils.create_dir_if_not_exists(basedir_output)

        basedir_data = os.path.join(input_folder, region, quantity)
        if not os.path.isdir(basedir_data):
            raise IOError("cannot find directory %s" % basedir_data)

        data_files = glob(os.path.join(basedir_data, "*.txt"))
        if not data_files:
            raise IOError("empty directory %s" % basedir_data)

        if background_functions and os.path.isfile(background_functions):
            try:
                bkg_functions = reader.get_background_function_old(
                    region, quantity, filename=background_functions
                )
            except ValueError:
                logging.error(
                    "cannot find background function for %s %s, using ExpPoly2", region, quantity
                )
                bkg_functions = ["ExpPoly2"] * len(data_files)
        else:
            if background_functions not in VALID_BACKGROUND_FUNCTIONS:
                raise ValueError("%s is not a valid functions" % background_functions)
            bkg_functions = [background_functions] * len(data_files)

        logging.info(
            "Fitting  (%s, %s) with %s",
            region,
            quantity,
            ", ".join(bkg_functions),
        )

        prefit(
            basedir_output,
            data_files,
            region,
            quantity,
            bkg_functions,
            blind_range,
            verbose,
            interactive,
        )


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        description="Find fit parameters for background function",
        epilog="example: ./find_parameters.py ~/cernbox/h025_xsections/data/blinded output --quantities N_j_30",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument("input_folder")
    parser.add_argument("output_folder")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--quantities", nargs="*")
    parser.add_argument("--background-functions")
    parser.add_argument("--no-blind", action="store_true")
    parser.add_argument("--interactive", action="store_true")
    parser.add_argument("--verbose", action="store_true")
    args = parser.parse_args()

    if args.interactive:
        ROOT.gROOT.SetBatch(False)

    loop(
        args.input_folder,
        args.output_folder,
        args.regions,
        args.quantities,
        args.background_functions,
        args.no_blind,
        args.interactive,
        args.verbose,
    )
