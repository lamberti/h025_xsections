from glob import glob
import os
import ROOT

from h025_xsections.core.naming import Catalogue
from h025_xsections.make_signal.make_signal import create_workspace, get_histogram
from h025_xsections.core import definitions
from reader.LPHE import Reader

ROOT.gROOT.SetStyle("ATLAS")

input_dir = "../tmp-results/output_histo_norm_merged"


all_files = glob(os.path.join(input_dir, "histo_mcAll.*"))
catalogue = Catalogue.create_catalogue(all_files)
catalogue = catalogue.sub_catalogue(mc='mcAll', prod='prodAll', sys='NOMINAL')
assert len(catalogue) == 1

f = list(catalogue.values())[0]
f = ROOT.TFile.Open(f)

histo1 = get_histogram(f, "Diphoton", "pT_yy", 1)
histo1.SetName("histo1")
histo2 = get_histogram(f, "Diphoton", "pT_yy", len(definitions.binnings['pT_yy']) - 1)
histo2.SetName("histo2")

histo1.Rebin(2)
histo2.Rebin(2)

reader = Reader("/home/turra/FullRun2CrossSections")                  
parameters = reader.get_signal_pars("Diphoton", "pT_yy")

pars_first = parameters[1]
pars_second = parameters[20]

canvas = ROOT.TCanvas()

ws = create_workspace()
mass = ws.var("mass")



histo1.SetMarkerColor(ROOT.kViolet)
histo2.SetMarkerColor(ROOT.kCyan)

#histo1.Scale(1./histo1.GetSumOfWeights())
#histo2.Scale(1./histo2.GetSumOfWeights())


rdh1 = ROOT.RooDataHist("root_data_hist1", "root_data_hist1", ROOT.RooArgList(mass), histo1)
rdh2 = ROOT.RooDataHist("root_data_hist2", "root_data_hist2", ROOT.RooArgList(mass), histo2)

canvas = ROOT.TCanvas()
histo1.DrawNormalized("histo")
histo2.DrawNormalized("samehisto")
canvas.SaveAs("bo2.png")

canvas = ROOT.TCanvas()
frame = mass.frame(110, 140, 60)
legend1 = ROOT.TLegend(0.68, 0.59, 0.9, 0.74)
rdh1.plotOn(frame, ROOT.RooFit.MarkerColor(ROOT.kRed), ROOT.RooFit.Rescale(1./histo1.GetSumOfWeights()), ROOT.RooFit.Name("data1"))
legend1.AddEntry(frame.findObject("data1"), "MC", "P")



def set_parameters(ws, parameters):
    ws.var('alpha_hi').setVal(parameters['alphaHigh'])
    ws.var('alpha_low').setVal(parameters['alphaLow'])
    ws.var('mu_cb').setVal(parameters['mu_offset'] + 125)
#    ws.var('ncb_hi')
#    ws.var('ncb_low')
    ws.var('sigma_cb').setVal(parameters['sigma_offset'])

print(pars_first)
print(pars_second)
set_parameters(ws, pars_first)
ws.var('ncb_hi').setVal(98.83)
ws.var('ncb_low').setVal(7.42)

ws.pdf("signal").plotOn(frame, ROOT.RooFit.LineColor(ROOT.kRed), ROOT.RooFit.Normalization(1./histo1.GetSumOfWeights()), ROOT.RooFit.Name("pdf1"))
legend1.AddEntry(frame.findObject("pdf1"), "model", "L")

# second
legend2 = ROOT.TLegend(0.68, 0.3, 0.9, 0.46)
rdh2.plotOn(frame, ROOT.RooFit.MarkerColor(ROOT.kBlue), ROOT.RooFit.Rescale(1./histo2.GetSumOfWeights()), ROOT.RooFit.MarkerStyle(24), ROOT.RooFit.Name("data2"))
legend2.AddEntry(frame.findObject("data2"), "MC", "P")
set_parameters(ws, pars_second)
ws.var('ncb_low').setVal(6.88)
ws.var('ncb_hi').setVal(8.12)
ws.pdf("signal").plotOn(frame, ROOT.RooFit.LineColor(ROOT.kBlue), ROOT.RooFit.Normalization(1./histo2.GetSumOfWeights()), ROOT.RooFit.Name("pdf2"))
legend2.AddEntry(frame.findObject("pdf2"), "model", "L")

    
frame.Draw()
frame.GetYaxis().SetTitle("Fraction of events / 0.5 GeV")
frame.GetXaxis().SetTitle("m_{\gamma\gamma}\,\, \mathrm{[GeV]}")
frame.SetMaximum(0.25)

label = ROOT.TLatex(0.20, 0.87, "#bf{#it{ATLAS}} Internal")
label.SetNDC()
label.Draw()

label2 = ROOT.TLatex(0.60, 0.87, r"H\to\gamma\gamma, m_\mathrm{H} = 125\, \mathrm{GeV}")
label2.SetNDC()
label2.Draw()

label3 = ROOT.TLatex(0.68, 0.77, "p_{T}^{\gamma\gamma} < 5\,\mathrm{GeV}")
label4 = ROOT.TLatex(0.68, 0.49, "p_{T}^{\gamma\gamma} > 650\,\mathrm{GeV}")
label3.SetNDC()
label4.SetNDC()

label3.Draw()
label4.Draw()

legend1.SetBorderSize(0)
legend2.SetBorderSize(0)

legend1.SetTextSizePixels(10)
legend2.SetTextSizePixels(10)

legend1.Draw()
legend2.Draw()

canvas.SaveAs("signal_example.eps")

