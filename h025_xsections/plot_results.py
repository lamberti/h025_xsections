#!/usr/bin/env python

import logging
import os

import ROOT

# ROOT.PyConfig.StartGUIThread = False
ROOT.gROOT.SetBatch(True)

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns


from h025_xsections.core import definitions
from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.histo_utils import get_binning_for_plot, remove_under_overflow
from h025_xsections.core.utils import (
    create_dir_if_not_exists,
    extract_info_from_fitresult,
)
from h025_xsections.reader.milano import Reader


logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def plot_xs(output_directory, xs_SM, mus, mus_stat, mus_full, region, quantity):
    latex_label = definitions.latex_labels[quantity]
    bin_labels = definitions.bin_labels[quantity]  # without under/over-flow

    if "_vs_" in quantity:
        bin_info = definitions.binnings[quantity]
        binning = np.linspace(bin_info[1], bin_info[2], bin_info[0] + 1)

        do_normalization = False
        do_log_x = False

        unit = 1

    else:
        binning = np.array(definitions.binnings[quantity])  # without under/over-flow

        do_normalization = definitions.do_normalizations[quantity]
        do_log_x = definitions.do_logs_x[quantity]

        unit = definitions.units_root[quantity]
        if unit == "MeV":
            binning /= 1000.0

    show_overflow = definitions.show_overflows[quantity]
    show_underflow = definitions.show_underflows[quantity]

    # add -inf, +inf to the binning if needed
    binning_with_extraflow = np.copy(np.array(binning))
    binning_with_extraflow = np.append(-np.inf, binning_with_extraflow)
    binning_with_extraflow = np.append(binning_with_extraflow, np.inf)

    # create special bins for under/over-flow
    binning_plot = get_binning_for_plot(binning_with_extraflow)

    bincenters = 0.5 * (binning_plot[1:] + binning_plot[:-1])
    binwidths = binning_plot[1:] - binning_plot[:-1]

    # add labels for under/over-flow
    this_remove_under_overflow = lambda x: remove_under_overflow(
        x, do_overflow=show_overflow, do_underflow=show_underflow
    )
    bin_labels_plot = this_remove_under_overflow(bin_labels)

    prediction = np.copy(xs_SM)

    prediction = this_remove_under_overflow(prediction)
    measured = this_remove_under_overflow(mus * xs_SM)
    measured_full = this_remove_under_overflow(mus_full * xs_SM)
    measured_stat = this_remove_under_overflow(mus_stat * xs_SM)
    bincenters = this_remove_under_overflow(bincenters)
    binwidths = this_remove_under_overflow(binwidths)

    if do_normalization:
        prediction /= binwidths
        measured /= binwidths
        measured_full /= binwidths
        measured_stat /= binwidths
    fig, (ax_top, ax_bottom) = plt.subplots(2, 1, sharex=True)
    fig.subplots_adjust(hspace=0)
    ax_top.errorbar(
        bincenters,
        prediction,
        linestyle="None",
        marker=None,
        xerr=[binwidths / 2, binwidths / 2],
        label="SM",
        color="C0",
    )

    # measured xsections
    ax_top.errorbar(
        bincenters,
        measured,
        yerr=measured_full,
        linestyle="None",
        marker=None,
        color="0.1",
        label="data",
    )
    # measured xsection stat-only
    ax_top.errorbar(
        bincenters,
        measured,
        yerr=measured_stat,
        capsize=0,
        markersize=4,
        line=None,
        lw=5,
        fmt="o",
        color="0.1",
        ecolor="0.4",
        label="data (stat-only)",
    )

    ax_top.legend(loc=0, fontsize=10)

    xlabel = latex_label
    ylabel = r"$d\sigma/d%s$" % latex_label.replace("$", "")

    if definitions.units_final[quantity] != 1:
        xlabel += " [GeV]"
        ylabel += " [pb / GeV]"
    else:
        ylabel += " [pb]"
    ax_top.set_xlabel(xlabel)
    ax_top.set_ylabel(ylabel)

    mus = this_remove_under_overflow(mus)
    mus_full = this_remove_under_overflow(mus_full)
    mus_stat = this_remove_under_overflow(mus_stat)

    # measured xsections
    ax_bottom.errorbar(
        bincenters,
        mus,
        yerr=mus_full,
        linestyle="None",
        marker=None,
        color="0.1",
    )
    # measured xsection stat-only
    ax_bottom.errorbar(
        bincenters,
        mus,
        yerr=mus_stat,
        capsize=0,
        markersize=4,
        line=None,
        lw=5,
        fmt="o",
        color="0.1",
        ecolor="0.4",
    )

    xlabel = latex_label
    ylabel = "data / SM"

    if definitions.units_final[quantity] != 1:
        xlabel += " [GeV]"
    ax_bottom.set_xlabel(xlabel)
    ax_bottom.set_ylabel(ylabel)
    ax_bottom.axhline(1, color="C0")

    for ax in ax_top, ax_bottom:
        if do_log_x:
            if binning_plot[0] == 0:
                ax.set_xlim(binning_plot[0] + binwidths[0] * 0.25)
            ax.set_xscale("log")

        if bin_labels_plot is not None:
            ax.set_xticks([], minor=True)
            ax.set_xticks(bincenters, minor=False)
            ax.set_xticklabels(bin_labels_plot, rotation=90)

    fig.savefig(
        os.path.join(
            output_directory,
            "plot_{region}_{quantity}.png".format(region=region, quantity=quantity),
        ),
        bbox_inches="tight",
    )

    plt.close(fig)


def plot_pulls(pull_names, pulls, output_fn, title):
    fig, ax = plt.subplots(figsize=(int(0.3 * len(pulls)), 5))

    idx_sorting = np.argsort(pull_names)
    pull_names = [pull_names[i] for i in idx_sorting]
    pulls = [pulls[i] for i in idx_sorting]

    y = [pull[0] for pull in pulls]
    ey = [pull[1] for pull in pulls]
    x = np.arange(len(ey))

    ax.errorbar(x, y, ey, color="k", fmt="o", capsize=8, capthick=1.5)
    ax.axhspan(-2, +2, color="gold", alpha=0.7)
    ax.axhspan(-1, +1, color="springgreen", alpha=0.7)
    ax.axhline(0, ls=":", color="Black")
    ax.set_ylim(-2.2, 2.2)

    ax.set_xticks(x)
    ax.set_xticklabels(pull_names, rotation=90)

    ax.set_title(title)
    fig.savefig(output_fn, bbox_inches="tight")
    plt.close(fig)


def plot_correlation(output_directory, output_img, correlation, region, quantity, title):

    show_overflow = definitions.show_overflows[quantity]
    show_underflow = definitions.show_underflows[quantity]

    a = None if show_underflow else 1
    b = None if show_overflow else -1

    correlation_to_plot = correlation[a:b, a:b]

    fig, ax = plt.subplots(figsize=(12, 10))

    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    mask = np.triu(np.ones_like(correlation_to_plot, dtype=np.bool))

    bin_labels = definitions.bin_labels[quantity]  # without under/over-flow

    this_remove_under_overflow = lambda x: remove_under_overflow(
        x, do_overflow=show_overflow, do_underflow=show_underflow
    )
    bin_labels_plot = this_remove_under_overflow(bin_labels)

    sns.heatmap(
        correlation_to_plot * 100,
        square=True,
        vmin=-40,
        vmax=40,
        center=0,
        annot=True,
        mask=mask,
        cmap=cmap,
        fmt=".1f",
        xticklabels=bin_labels_plot,
        yticklabels=bin_labels_plot,
        cbar_kws={"shrink": 0.82},
        ax=ax,
    )
    ax.set_title(title)

    fig.savefig(
        os.path.join(output_directory, output_img),
        bbox_inches="tight",
    )

    plt.close(fig)


def plot_results(
    ws_path_statonly,
    ws_path_fullfit,
    reader,
    region,
    quantity,
    method,
    dataset,
    output_directory,
    output_directory_plots,
):
    create_dir_if_not_exists(output_directory)

    if quantity in definitions.quantities_2d:
        return  # TODO: fix

    xs_SM = reader.get_xsections_fiducial(region, quantity).values

    if xs_SM.sum == 0:
        logging.warning("all the xs are zero!")

    f_statonly = ROOT.TFile.Open(ws_path_statonly)
    f_fullfit = ROOT.TFile.Open(ws_path_fullfit)

    if not f_statonly:
        logging.error("cannot find %s", ws_path_statonly)
        return
    if not f_fullfit:
        logging.error("cannot find %s", ws_path_fullfit)
        return

    fit_results_statonly = f_statonly.Get("fitResult")
    if not fit_results_statonly:
        logging.error(
            "cannot find fit result for region: %s, quantity: %s from file %s",
            region,
            quantity,
            ws_path_statonly,
        )
        return
    fit_results_fullfit = f_fullfit.Get("fitResult")
    if not fit_results_fullfit:
        logging.error(
            "cannot find fit result for region: %s, quantity: %s from file %s",
            region,
            quantity,
            ws_path_fullfit,
        )
        return

    df_fullfit = extract_info_from_fitresult(fit_results_fullfit)
    df_statonly = extract_info_from_fitresult(fit_results_statonly)

    mus = []
    mus_stat = []
    mus_full = []
    if quantity not in definitions.quantities_2d:
        nbins = len(definitions.binnings[quantity]) + 1
    else:
        nbins = definitions.binnings[quantity][0]
    correlation = np.ones((nbins, nbins)) * np.nan
    correlation_statonly = np.ones((nbins, nbins)) * np.nan

    for ibin in range(nbins):
        mu_name = "mu_xsec_{region}_{quantity}_{ibin}".format(
            region=region, quantity=quantity, ibin=ibin
        )
        if mu_name in df_fullfit:
            mus.append(df_fullfit[mu_name][0])
            mus_stat.append(df_statonly[mu_name][1])
            mus_full.append(df_fullfit[mu_name][1])
        else:
            mus.append(0)
            mus_stat.append(0)
            mus_full.append(0)
        for jbin in range(nbins):
            mu_name2 = "mu_xsec_{region}_{quantity}_{ibin}".format(
                region=region, quantity=quantity, ibin=jbin
            )
            if mu_name in df_fullfit and mu_name2 in df_fullfit:
                correlation[ibin, jbin] = fit_results_fullfit.correlation(mu_name, mu_name2)
                correlation_statonly[ibin, jbin] = fit_results_statonly.correlation(
                    mu_name, mu_name2
                )

    create_dir_if_not_exists(output_directory)
    logging.info("plotting %s %s", region, quantity)
    np.savetxt(
        os.path.join(
            output_directory,
            "relative_errors_statonly_{region}_{quantity}.csv".format(
                region=region, quantity=quantity
            ),
        ),
        [mus_stat],
        delimiter=",",
    )
    np.savetxt(
        os.path.join(
            output_directory,
            "relative_errors_full_{region}_{quantity}.csv".format(region=region, quantity=quantity),
        ),
        [mus_full],
        delimiter=",",
    )

    plot_xs(output_directory_plots, xs_SM, mus, mus_stat, mus_full, region, quantity)

    plot_correlation(
        output_directory_plots,
        "correlation_{region}_{quantity}_full.png".format(region=region, quantity=quantity),
        correlation,
        region,
        quantity,
        "region: {region} quantity: {quantity}".format(
            region=region, quantity=definitions.latex_labels[quantity]
        ),
    )
    plot_correlation(
        output_directory_plots,
        "correlation_{region}_{quantity}_staonly.png".format(region=region, quantity=quantity),
        correlation_statonly,
        region,
        quantity,
        "region: {region} quantity: {quantity}".format(
            region=region, quantity=definitions.latex_labels[quantity]
        ),
    )

    pulls = []
    pull_names = []
    for pull_name, pull_values in df_fullfit.items():
        if pull_name.startswith("ATLAS_"):
            pulls.append(pull_values)
            pull_names.append(pull_name)
    plot_pulls(
        pull_names,
        pulls,
        os.path.join(
            output_directory_plots,
            "pull_{region}_{quantity}.png".format(region=region, quantity=quantity),
        ),
        "region: {region} quantity: {quantity}".format(
            region=region, quantity=definitions.latex_labels[quantity]
        ),
    )


if __name__ == "__main__":
    import argparse
    from h025_xsections.core.folders import get_path

    parser = argparse.ArgumentParser(description="Compute quantities related to yield.")
    parser.add_argument("basedir")
    parser.add_argument("output_directory", default="output")
    parser.add_argument("--ws-statonly")
    parser.add_argument("--ws-full")
    parser.add_argument("--quantity")
    parser.add_argument("--region")
    parser.add_argument("--blinded", action="store_true")
    parser.add_argument("--method", choices=["bin_by_bin", "matrix"])
    parser.add_argument("--dataset", default="AsimovSB")
    args = parser.parse_args()

    reader = Reader(args.basedir)

    if args.ws_statonly is None:
        args.ws_statonly = get_path(
            args.basedir,
            "workspace_fitted",
            region=args.region,
            quantity=args.quantity,
            dataset=args.dataset,
            sys="statonly",
        )

    if args.ws_full is None:
        args.ws_full = get_path(
            args.basedir,
            "workspace_fitted",
            region=args.region,
            quantity=args.quantity,
            dataset=args.dataset,
            sys="full",
        )

    plot_results(
        args.ws_statonly,
        args.ws_full,
        reader,
        args.region,
        args.quantity,
        args.method,
        args.dataset,
        args.output_directory,
        args.output_directory
    )
