#!/usr/bin/env python

import os
import logging
import re
import pandas as pd
import ROOT
import numpy as np
from matplotlib import pyplot as plt

from h025_xsections.core import definitions
from h025_xsections.reader import Reader
from h025_xsections.core.utils import iter_collection, create_dir_for_file
from h025_xsections.core.histo_utils import plot_stack

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def get_nreco_from_workspace(ws: ROOT.RooWorkspace, region: str, quantity: str):
    # for matrix
    # yield__Diphoton_pT_yy_7_category_Diphoton_pT_yy_21 + ...
    # yield__pT_yy_notFiducialnotDalitz_category_Diphoton_pT_yy_21 +
    # yield__pT_yy_isDalitz_category_Diphoton_pT_yy_21
    wildcard = "yield__*{quantity}_*_category_{region}_{quantity}_*".format(
        region=region, quantity=quantity
    )
    all_function_yields = ws.allFunctions().selectByName(wildcard)

    r = re.compile(
        r"yield__({region}_)?{quantity}_(?P<bin_true>\w+)_category_{region}_{quantity}_(?P<cat_reco>\w+)".format(
            quantity=quantity,
            region=region,
        )
    )

    results = []

    for func in iter_collection(all_function_yields):
        name = func.GetName()
        m = r.match(name)
        if not m:
            raise ValueError("cannot parse %s with %s" % (name, r.pattern))
        bin_true = m.group("bin_true")
        cat_reco = m.group("cat_reco")
        results.append((bin_true, cat_reco, func.getVal()))

    results = pd.DataFrame(results, columns=["true_bin", "reco_cat", "yield"])
    results["reco_cat"] = results["reco_cat"].astype(int)
    results = results.set_index(["true_bin", "reco_cat"])["yield"]
    results = results.unstack("reco_cat")

    return results


def plot_comparison(
    n_workspace: np.array, n_expected: np.array, method: str, region: str, quantity: str, output_fn: str
):

    binning = definitions.binnings[quantity]
    if isinstance(binning, tuple) and len(binning) == 3:
        binning = np.linspace(binning[1], binning[2], binning[0] + 1)
    else:
        binning = np.array(binning)

    ratio = (n_workspace / n_expected - 1) * 100

    fig = plot_stack(
        binning,
        [ratio.values],
        ratio.index,
        bin_labels_with_extraflow=definitions.bin_labels[quantity],
        latex_label=definitions.latex_labels[quantity],
        show_underflow=definitions.show_underflows[quantity],
        show_overflow=definitions.show_overflows[quantity],
        do_normalization=False,
        do_log_x=definitions.do_logs_x[quantity],
        stacked=False,
        ylabel="ratio yield (workspace / theory - 1) [%]",
        kwargs_bar={"fill": False},
    )

    fig.axes[0].set_ylim(-0.03, 0.03)

    fig.axes[0].set_title(
        "method: {method} region: {region} quantity: {quantity}".format(
            method=method, region=region, quantity=definitions.latex_labels[quantity]
        )
    )
    fig.savefig(output_fn, bbox_inches="tight")
    plt.close(fig)


def check_workspace(
    fn_workspace: str,
    region: str,
    quantity: str,
    method: str,
    reader: Reader,
    output_fn: str,
    ws_name: str = "combWS",
):
    nreco_expected = reader.get_nreco(region, quantity, mc="mcAll", prod="prodAll")
    if not os.path.exists(fn_workspace):
        raise IOError("file %s does not exists" % fn_workspace)
    logging.info("opening workspace %s from file %s", ws_name, fn_workspace)
    f = ROOT.TFile.Open(fn_workspace)
    ws = f.Get(ws_name)
    ROOT.SetOwnership(ws, True)
    nreco_ws = get_nreco_from_workspace(ws, region, quantity)
    create_dir_for_file(output_fn)
    plot_comparison(nreco_ws.sum(axis=0), nreco_expected, method, region, quantity, output_fn)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("data_folder")
    parser.add_argument("output_fn", default="check.png")
    parser.add_argument("--ws-name", default="combWS")
    parser.add_argument("--region", nargs="*")
    parser.add_argument("--quantity", nargs="*")
    parser.add_argument("--method", choices=["bin_by_bin", "matrix"])

    args = parser.parse_args()

    reader = Reader(args.data_folder)

    fn_workspace = (
        "workspace/{method}/{region}/{quantity}/blinded/XSectionWS_{region}_{quantity}.root"
    )
    fn_workspace = fn_workspace.format(
        method=args.method, region=args.region, quantity=args.quantity
    )
    fn_workspace = os.path.join(args.data_folder, fn_workspace)
    check_workspace(
        fn_workspace,
        args.regions,
        args.quantity,
        args.method,
        reader,
        args.output_fn,
        args.ws_name,
    )
