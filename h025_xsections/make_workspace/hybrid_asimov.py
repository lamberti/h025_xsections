import logging
import os
from typing import List, Optional

import ROOT
from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.folders import get_path
from h025_xsections.core.naming import FileHistoInfo

ROOT.PyConfig.IgnoreCommandLineOptions = True
logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def create_hybrid_asimov(
    ws: ROOT.RooWorkspace,
    asimovB_name: str,
    signal_histo_2d: ROOT.TH2,
    cat_name: str,
    obs_name: str,
    dataset_name: str = None,
) -> ROOT.RooDataSet:
    """
    histogram:
       * x-axis: quantity
       * y-axis: myy
       * content: expected yield, normalized to luminosity
    """
    asimovB = ws.data(asimovB_name)
    cat = ws.cat(cat_name)
    ncat = cat.size()
    nentries = asimovB.numEntries()
    dataset_name = dataset_name or "AsimovSB_with_signal_template"

    # check binnings
    for icat in range(ncat):
        cat.setIndex(icat)
        category_name = cat.getLabel()
        category_index = int(category_name.split("_")[-1])
        obs = ws.var(obs_name.format(icat=category_index))
        assert obs.getMin() == signal_histo_2d.GetYaxis().GetXmin()
        assert obs.getMax() == signal_histo_2d.GetYaxis().GetXmax()
        assert obs.numBins() == signal_histo_2d.GetYaxis().GetNbins()

    asimov_hybrid = asimovB.emptyClone(dataset_name, "s(template) + b(asimov)")

    for ientry in range(nentries):
        values = asimovB.get(ientry)
        weight_b = asimovB.weight()

        cat_this_event = values.find(cat.GetName())
        category_name = cat_this_event.getLabel()
        category_index = int(category_name.split("_")[-1])
        obs_this_event = values.find(obs_name.format(icat=category_index))
        myy_value = obs_this_event.getVal()

        myy_bin = signal_histo_2d.GetYaxis().FindBin(myy_value)
        entries_histo = signal_histo_2d.GetBinContent(category_index, myy_bin)

        weight_sb = entries_histo + weight_b
        asimov_hybrid.add(values, weight_sb)

    return asimov_hybrid


def patch_workspace_filename_histogram(
    fn_ws,
    obs_name,
    fn_histo,
    histo_name,
    ws_name="combWS",
    abs_normalization: Optional[List[float]] = None,
    dataset_name: str = None,
):
    f_ws = ROOT.TFile.Open(fn_ws)
    ws = f_ws.Get(ws_name)
    f_histo = ROOT.TFile.Open(fn_histo)
    histo = f_histo.Get(histo_name)

    if abs_normalization is not None:
        nx, ny = histo.GetNbinsX() + 2, histo.GetNbinsY() + 2

        if nx != len(abs_normalization):
            raise ValueError(
                "histogram has %d bins, but the normalization are %d" % (nx,
                len(abs_normalization)),
            )
        histo = histo.Clone()

        for ix, n in enumerate(abs_normalization):
            k = sum(histo.GetBinContent(ix, iy) for iy in range(ny))
            if k == 0:
                continue
            for iy in range(ny):
                new_value = histo.GetBinContent(ix, iy) * n / k
                histo.SetBinContent(ix, iy, new_value)

    asimov = create_hybrid_asimov(ws, "AsimovB", histo, "channellist", obs_name, dataset_name)
    getattr(ws, "import")(asimov)
    ws.writeToFile(fn_ws)


def patch_workspace_filename(
    fn_ws: str,
    basedir: str,
    region: str,
    quantity: str,
    tags_histogram: FileHistoInfo = None,
    abs_normalization: Optional[List[float]] = None,
    dataset_name: str = None,
):
    if tags_histogram is None:
        tags_histogram = FileHistoInfo(
            "mcAll", "prodAll", "isPass_or_isFiducial", "Nominal", "NOMINAL"
        )
    folder_histo = get_path(basedir, "merged_files_folder")
    tags = tags_histogram.format_tags()
    fn_histo = "histo_%s.norm.merged.root" % tags
    fn_histo = os.path.join(folder_histo, fn_histo)
    histo_name = "histo2D_m_yy_{quantity}_isPassed{region}".format(quantity=quantity, region=region)
    obs_name = "atlas_invMass_category_%s_%s_{icat}" % (region, quantity)
    logging.info("creating hybrid Asimov for workspace %s with histogram %s", fn_ws, fn_histo)
    patch_workspace_filename_histogram(
        fn_ws,
        obs_name,
        fn_histo,
        histo_name,
        "combWS",
        abs_normalization,
        dataset_name,
    )


def patch_workspace(
    basedir: str,
    region: str,
    quantity: str,
    method: str,
    blinded: bool = True,
    tags_histogram: FileHistoInfo = None,
    abs_normalization: Optional[List[float]] = None,
    dataset_name: str = None,
):
    fn_ws = get_path(
        basedir, "workspace", region=region, quantity=quantity, blinded=blinded, method=method
    )
    patch_workspace_filename(
        fn_ws,
        basedir,
        region,
        quantity,
        tags_histogram,
        abs_normalization,
        dataset_name,
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Make workspace")
    parser.add_argument("basedir")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--quantities", nargs="*")
    parser.add_argument("--method")
    parser.add_argument("--blinded", action="store_true")
    parser.add_argument("--dataset-name")
    args = parser.parse_args()

    regions_quantities = get_region_quantities(args.regions, args.quantities)
    for region, quantity in regions_quantities:
        patch_workspace(
            args.basedir, region, quantity, args.method, args.blinded, args.dataset_name
        )
