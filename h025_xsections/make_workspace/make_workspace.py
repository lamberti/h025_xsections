#!/usr/bin/env python

import logging
import subprocess
import os
from typing import List

from h025_xsections.core.definitions import get_region_quantities

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def make_workspace_from_xml(folder: str) -> str:
    if not os.path.exists(os.path.join(folder, "combWS.xml")):
        raise IOError("cannot find combWS.xml in folder %s" % folder)
    subprocess.run("XMLReader -x combWS.xml".split(), cwd=folder)
    import xml.etree.ElementTree as ET
    tree = ET.parse(os.path.join(folder, 'combWS.xml'))
    fn = tree.getroot().attrib['OutputFile']
    return os.path.join(folder, fn)


def loop(basedir, regions: List[str], quantities: List[str], blinded: bool):
    regions_quantities = get_region_quantities(regions, quantities)
    for region, quantity in regions_quantities:
        logging.info("making workspace for %s %s", region, quantity)
        folder = os.path.join(
            basedir, region, quantity, "blinded" if blinded else "unblinded"
        )
        try:
            make_workspace_from_xml(folder)
        except IOError as err:
            logging.error("error when making workspace for %s %s: %s", region, quantity, str(err))
            continue

def get_parser():
    import argparse

    parser = argparse.ArgumentParser(description="Make workspace")
    parser.add_argument("basedir")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--quantities", nargs="*")
    parser.add_argument("--blinded", action="store_true")

    return parser


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()
    loop(args.basedir, args.regions, args.quantities, args.blinded)
