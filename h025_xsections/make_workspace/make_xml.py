#!/usr/bin/env python

import logging
import os
from collections import OrderedDict
from glob import glob
from typing import List

import h025_xsections
import numpy as np
import pandas as pd
from h025_xsections.core import definitions, naming
from h025_xsections.core.background import VALID_BACKGROUND_FUNCTIONS, background_factory
from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.utils import create_dir_if_not_exists, short_array_repr
from h025_xsections.reader import Reader
from h025_xsections.reader.LPHE import Reader as Reader_LPHE
from lxml import etree
from lxml.etree import Element

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

doctype_combination = "<!DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'>"
doctype_channel = "<!DOCTYPE Channel SYSTEM 'AnaWSBuilder.dtd'>"
doctype_model = "<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>"
doctype_SampleItems = "<!DOCTYPE SampleItems SYSTEM 'AnaWSBuilder.dtd'>"


def make_combination(
    true_bin_names: List[str],
    reco_names: List[str],
    output_ws_name: str,
    base_dir: str,
    blind: bool,
    do_fit: bool,
):
    root = Element("Combination")
    root.set("DataName", "combData")
    root.set("OutputFile", output_ws_name)
    root.set("WorkspaceName", "combWS")
    root.set("ModelConfigName", "ModelConfig")
    if blind:
        root.set("Blind", "True")

    # Inputs & POI definitions
    mu_list = ["mu", "mu_yy", "mu_BR_yy"]
    for true_bin_name in true_bin_names:
        mu_list.append("mu_xsec_%s" % true_bin_name)

    for reco_name in reco_names:
        etree.SubElement(root, "Input").text = "config/channel/%s.xml" % reco_name

    logging.info("POI: %s", ", ".join(mu_list))
    etree.SubElement(root, "POI").text = ",".join(mu_list)
    root.append(etree.Comment(" <Correlate>mu,mH,lumi_run2</Correlate> "))

    # Asimov & AsimovSB definitions
    if blind:
        AsimovB = etree.SubElement(root, "Asimov")
        AsimovB.set("Name", "AsimovB")
        AsimovB.set("Setup", "mu=0")
        AsimovB.set(
            "Action", "fixsyst:fit:genasimov:float" if do_fit else "fixsyst:genasimov:float"
        )
        AsimovSB = etree.SubElement(root, "Asimov")
        AsimovSB.set("Name", "AsimovSB")
        AsimovSB.set("Setup", "mu=1")
        AsimovSB.set("Action", "genasimov:savesnapshot")
        AsimovSB.set("SnapshotAll", "prefit")
    else:
        AsimovSB = etree.SubElement(root, "Asimov")
        AsimovSB.set("Name", "AsimovSB")
        AsimovSB.set("Setup", "mu=1")
        AsimovSB.set(
            "Action", "fixsyst:fit:genasimov:float" if do_fit else "fixsyst:genasimov:float"
        )
        AsimovSB.set("SnapshotAll", "prefit")
        AsimovB = etree.SubElement(root, "Asimov")
        AsimovB.set("Name", "AsimovB")
        AsimovB.set("Setup", "mu=0")
        AsimovB.set("Action", "genasimov:savesnapshot:reset")

    text = etree.tostring(
        root, pretty_print=True, xml_declaration=False, doctype=doctype_combination
    )
    with open(os.path.join(base_dir, "combWS.xml"), "wb") as f:
        f.write(text)


def create_data_xml_element(root, reco_category, nmass_bins, blind, data_file):
    data = etree.SubElement(root, "Data")
    data.set(
        "Observable", "atlas_invMass_{reco_category}[105,160]".format(reco_category=reco_category)
    )
    data.set("InjectGhost", "1")
    data.set("Binning", "%d" % nmass_bins)
    data.set("InputFile", data_file)

    if blind:
        data.set("BlindRange", "120,130")


def make_background_sample(reader, region, quantity, category_name, ireco, channel_el, lumi):

    bkg_pars = reader.get_background_prefit_pars(region, quantity, ireco)
    nbkg_prefit = bkg_pars[0][1]
    nbkg_prefit_error = bkg_pars[0][2]

    sample = etree.SubElement(channel_el, "Sample")
    sample.set("Name", "background")
    sample.set("InputFile", "config/model/background_%s.xml" % category_name)
    sample.set("XSection", "1")
    sample.set("SelectionEff", "1")
    sample.set("MultiplyLumi", "1")
    nf = etree.SubElement(sample, "NormFactor")
    nf.set(
        "Name",
        "bkg_{name}[{value},{value_min},{value_max}]".format(
            name=category_name,
            value=nbkg_prefit,
            value_min=max(0, nbkg_prefit * 0.9 - 5 * nbkg_prefit_error)
            if nbkg_prefit != 0
            else 0.0,
            value_max=nbkg_prefit * 1.5 + 5 * nbkg_prefit_error if nbkg_prefit != 0 else 100.0,
        ),
    )
    nf = etree.SubElement(sample, "NormFactor")
    nf.set(
        "Name",
        "EXPR::adjust_LUMI('@0/@1',lumi_run2[%.10f],ref_lumi[%.10f])"
        % (lumi, definitions.luminosity["mcAll"]),
    )
    nf = etree.SubElement(sample, "NormFactor", {"Name": "NFcontBkg[1.0]", "Correlate": "1"})


def make_spurious_sample(
    input_dir, region, quantity, category_name, ireco, channel_el, lumi, ss_value
):
    sample = etree.SubElement(channel_el, "Sample")
    sample.set("Name", "spurious")
    sample.set("InputFile", "config/model/signal_%s.xml" % category_name)
    sample.set("ImportSyst", ":self:")
    sample.set("SharePdf", "commonSig")
    sample.set("XSection", "1")
    sample.set("SelectionEff", "1")
    sample.set("MultiplyLumi", "1")
    syst_element = etree.SubElement(sample, "Systematic")
    syst_element.set("Name", "ATLAS_Hgg_Bias_%s" % category_name)
    syst_element.set("Constr", "gaus")
    syst_element.set("CentralValue", "0")
    syst_element.set("Mag", "%.10f" % ss_value)
    syst_element.set("WhereTo", "yield")
    nf = etree.SubElement(sample, "NormFactor")
    nf.set(
        "Name",
        "expr::ss_scaling('@0 / @1', lumi_run2[%.10f], ref_lumi[%.10f])"
        % (lumi, definitions.luminosity["mcAll"]),
    )
    # nf.set('Correlate', '1')
    nf = etree.SubElement(sample, "NormFactor", {"Name": "NFspurious[1.0]", "Correlate": "1"})


def add_global_yield_systematics_to_channel(channel_el, global_yield_systematics):
    channel_el.append(etree.Comment("Common Yield Systematics"))
    for syst_name in global_yield_systematics:
        syst_element = etree.SubElement(channel_el, "Systematic")
        syst_element.set("Name", syst_name)
        syst_element.set("Constr", global_yield_systematics[syst_name][1])
        syst_element.set("CentralValue", "1")
        syst_element.set("Mag", str(global_yield_systematics[syst_name][0]))
        syst_element.set("WhereTo", "yield")


def add_energy_sys_to_channel_element(
    channel_el,
    global_scale_systematics,
    sys_scale_names: List[str],
    sys_resolution_names: List[str],
    category_name,
):
    channel_el.append(etree.Comment("Common Scale Systematics"))
    for syst_name in global_scale_systematics:
        syst_element = etree.SubElement(channel_el, "Systematic")
        syst_element.set("Name", syst_name)
        syst_element.set("Constr", global_scale_systematics[syst_name][1])
        syst_element.set("CentralValue", "1")
        syst_element.set("Mag", str(global_scale_systematics[syst_name][0]))
        syst_element.set("WhereTo", "shape")

    shape_sampleItems = etree.SubElement(channel_el, "ImportItems")
    shape_sampleItems.set("FileName", "config/syst/shape/SampleItems_%s.xml" % (category_name))

    all_mss_syst_names = sys_scale_names + list(global_scale_systematics.keys())
    resp_MSS = (
        "prod::resp_MSS("
        + ",".join(["response::%s" % sys_name for sys_name in sorted(all_mss_syst_names)])
        + ")"
    )
    resp_MRS = (
        "prod::resp_MRS("
        + ",".join(["response::%s" % sys_name for sys_name in sorted(sys_resolution_names)])
        + ")"
    )

    for item_str in resp_MSS, resp_MRS:
        item = etree.SubElement(channel_el, "Item")
        item.set("Name", item_str)


def make_channel_matrix(
    reader,
    region,
    quantity,
    category_index,
    true_bins_name,
    lumi,
    xsections,
    efficiencies,
    br_dalitz,
    global_yield_systematics,
    global_scale_systematics,
    input_dir,
    base_dir,
    blind,
    sys_scale_names: List[str],
    sys_resolution_names: List[str],
    nmass_bins=220,
    mH_value=definitions.mH
):

    assert len(true_bins_name) == len(xsections)
    assert len(true_bins_name) == len(efficiencies)

    suffix_category = naming.get_bin_suffix(region, quantity, category_index)
    category_name = naming.get_category_name(region, quantity, category_index)

    channel_el = Element("Channel", {"Name": category_name, "Type": "shape", "Lumi": "1.00"})

    # Define dataset, observable and eventual blinded region
    data_txt_filename = "data/mass_points_{quantity}_bin{ibin}_{region}.txt".format(
        quantity=quantity, ibin=category_index, region=region
    )

    create_data_xml_element(channel_el, category_name, nmass_bins, blind, data_txt_filename)

    item_mass = etree.SubElement(channel_el, "Item")
    item_mass.set("Name", "mH[%.10f]" % mH_value)

    add_global_yield_systematics_to_channel(channel_el, global_yield_systematics)

    add_energy_sys_to_channel_element(
        channel_el, global_scale_systematics, sys_scale_names, sys_resolution_names, category_name
    )

    for itrue, true_bin_name in enumerate(true_bins_name):
        efficiency = efficiencies[itrue]
        if efficiency < 0.0:
            logging.warning(
                "efficiency for reco=%s, true=%s is negative: %s",
                category_name,
                true_bin_name,
                efficiency,
            )
            channel_el.append(
                etree.Comment("Skipping true-bin %s, eff:%f" % (true_bin_name, efficiency))
            )
            continue

        if xsections[itrue] == 0:
            logging.info("cross section for true=%s is 0, skipping", true_bin_name)
            channel_el.append(etree.Comment("Skipping true-bin %s, xsection:0" % true_bin_name))
            continue

        sample = etree.SubElement(channel_el, "Sample")
        sample.set("Name", true_bin_name)
        sample.set("InputFile", "config/model/signal_%s.xml" % category_name)
        sample.set("ImportSyst", ":common:")
        sample.set("SharePdf", "commonSig")
        sample.set("XSection", "1")
        sample.set("SelectionEff", "1")
        sample.set("MultiplyLumi", "1")

        yield_sampleItems = etree.SubElement(sample, "ImportItems")
        yield_sampleItems.set(
            "FileName",
            "config/syst/yield/SampleItems_true_%s__reco_%s.xml" % (true_bin_name, category_name),
        )

        norm_factors = [
            ("mu", 1.0),
            ("mu_yy", 1.0),
            ("lumi_run2", lumi),
            ("XS_%s" % true_bin_name, xsections[itrue]),
            ("eff_reco_%s__true_%s" % (category_name, true_bin_name), efficiency),
        ]
        if itrue == 0:  # not fiducial and not dalitz
            norm_factors += [("mu_BR_yy", 1.0)]
            norm_factors += [("Br", definitions.Br)]
        elif itrue == 1:  # dalitz
            norm_factors += [("Br_dalitz", br_dalitz)]
        else:  # fiducial
            norm_factors += [("mu_BR_yy", 1.0)]
            norm_factors += [("Br", definitions.Br)]
            norm_factors += [("mu_xsec_%s" % true_bin_name, 1.0)]

        for name, value in norm_factors:
            nf_element = etree.SubElement(sample, "NormFactor")
            nf_element.set("Name", "%s[%.10f]" % (name, value))
            nf_element.set("Correlate", "1")

    path_file = os.path.join(h025_xsections.__path__[0], "../external-data/ss_results.txt")
    reader_LPHE = Reader_LPHE(basedir=None, normalize=True)
    try:
        ss_value = reader_LPHE.get_ss_over_sref_over_lumi(
            region, quantity, category_index, path_file
        )
    except:
        logging.error("cannot find spurious signal for %s %s", region, quantity)
        ss_value = 0

    ss_value *= reader.get_nreco(region, quantity)[category_index] * lumi / 1000.0

    make_spurious_sample(
        input_dir, region, quantity, category_name, category_index, channel_el, lumi, ss_value
    )

    make_background_sample(
        reader, region, quantity, category_name, category_index, channel_el, lumi
    )

    text = etree.tostring(
        channel_el, pretty_print=True, xml_declaration=False, doctype=doctype_channel
    )
    with open(os.path.join(base_dir, "config/channel/%s.xml" % category_name), "wb") as f:
        f.write(text)


def make_channel_bin_by_bin(
    reader,
    region,
    quantity,
    category_index,
    lumi,
    xsection,
    cfactor,
    global_yield_systematics,
    global_scale_systematics,
    input_dir,
    base_dir,
    blind,
    sys_scale_names: List[str],
    sys_resolution_names: List[str],
    nmass_bins=220,
    mH_value=definitions.mH
):
    suffix_category = naming.get_bin_suffix(region, quantity, category_index)
    category_name = naming.get_category_name(region, quantity, category_index)

    channel_el = Element("Channel", {"Name": category_name, "Type": "shape", "Lumi": "1.00"})

    # Define dataset, observable and eventual blinded region
    data_txt_filename = "data/mass_points_{quantity}_bin{ibin}_{region}.txt".format(
        quantity=quantity, ibin=category_index, region=region
    )

    create_data_xml_element(channel_el, category_name, nmass_bins, blind, data_txt_filename)

    item_mass = etree.SubElement(channel_el, "Item")
    item_mass.set("Name", "mH[%.10f]" % mH_value)

    add_global_yield_systematics_to_channel(channel_el, global_yield_systematics)

    add_energy_sys_to_channel_element(
        channel_el, global_scale_systematics, sys_scale_names, sys_resolution_names, category_name
    )

    # Sample definition
    sample = etree.SubElement(channel_el, "Sample")
    sample.set("Name", suffix_category)
    sample.set("InputFile", "config/model/signal_%s.xml" % category_name)
    sample.set("ImportSyst", ":common:")
    sample.set("SharePdf", "commonSig")
    sample.set("XSection", "1")
    sample.set("SelectionEff", "1")
    sample.set("MultiplyLumi", "1")

    yield_sampleItems = etree.SubElement(sample, "ImportItems")
    yield_sampleItems.set("FileName", "config/syst/yield/SampleItems_%s.xml" % (category_name))

    # NormFactors for Samples definition

    norm_factors = OrderedDict(
        [
            ("lumi_run2", lumi),
            ("mu", 1.0),
            ("mu_BR_yy", 1.0),
            ("mu_yy", 1.0),
            ("mu_xsec_%s" % suffix_category, 1.0),
            ("c_factor_%s" % suffix_category, cfactor),
            ("XS_%s" % suffix_category, xsection),
            ("BR_yy", definitions.Br),
        ]
    )
    for name, value in norm_factors.items():
        nf = etree.SubElement(sample, "NormFactor")
        nf.set("Name", "%s[%.10f]" % (name, value))
        nf.set("Correlate", "1")

    path_file = os.path.join(h025_xsections.__path__[0], "../external-data/ss_results.txt")
    reader_LPHE = Reader_LPHE(basedir=None, normalize=True)
    try:
        ss_value = reader_LPHE.get_ss_over_sref_over_lumi(
            region, quantity, category_index, path_file
        )
    except:
        logging.error("cannot find spurious signal for %s %s", region, quantity)
        ss_value = 0

    ss_value *= reader.get_nreco(region, quantity)[category_index] * lumi / 1000.0

    make_spurious_sample(
        input_dir, region, quantity, category_name, category_index, channel_el, lumi, ss_value
    )

    make_background_sample(
        reader, region, quantity, category_name, category_index, channel_el, lumi
    )

    text = etree.tostring(
        channel_el, pretty_print=True, xml_declaration=False, doctype=doctype_channel
    )
    with open(os.path.join(base_dir, "config/channel/%s.xml" % category_name), "wb") as f:
        f.write(text)


def make_shape_SampleItems(
    region,
    quantity,
    reco_category,
    input_dir,
    base_dir,
    ireco,
    sys_scale_values: pd.Series,
    sys_resolution_values: pd.Series,
):
    root = Element("SampleItems")

    # scale on muCB
    root.append(etree.Comment("Signal Scale Systematics"))
    sys_scale_values = sys_scale_values.unstack("direction").sort_index()
    for sys_name, sys_value in sys_scale_values.iterrows():
        sys_value_up, sys_value_down = sys_value["up"], sys_value["down"]
        sys_value = np.sign(sys_value_up) * 0.5 * (abs(sys_value_up) + abs(sys_value_down))
        systematic = etree.SubElement(root, "Systematic")
        systematic.set("Name", "%s" % sys_name)
        systematic.set("Constr", "gaus")
        systematic.set("Mag", "%f, %f" % (-sys_value, sys_value))
        systematic.set("CentralValue", "1")
        systematic.set("WhereTo", "shape")

    # resolution on sigmaCB
    root.append(etree.Comment("Signal Resolution Systematics"))
    sys_resolution_values = sys_resolution_values.unstack("direction").sort_index()
    for sys_name, sys_value in sys_resolution_values.iterrows():
        sys_value_up, sys_value_down = sys_value["up"], sys_value["down"]
        systematic = etree.SubElement(root, "Systematic")
        systematic.set("Name", "%s" % sys_name)
        systematic.set("Constr", "asym")
        systematic.set("Mag", "%f, %f" % (sys_value_down, sys_value_up))
        systematic.set("CentralValue", "1")
        systematic.set("WhereTo", "shape")
    text = etree.tostring(
        root, pretty_print=True, xml_declaration=False, doctype=doctype_SampleItems
    )
    f = open(os.path.join(base_dir, "config/syst/shape/SampleItems_%s.xml" % reco_category), "wb")
    f.write(text)


def make_systematics_bin_by_bin(reco_category: str, base_dir: str, sys_yield_values: pd.DataFrame):
    """
    sys_yield_values example:
    sys                               direction
    ATLAS_EG_SCALE_ALL                down        -0.000236
                                      up           0.000382
    ATLAS_JET_BJES_Response           down        -0.000931
                                      up           0.001150
    ATLAS_JET_EffectiveNP_Detector1   down        -0.001150
    """
    if sys_yield_values.empty:
        return
    root = Element("SampleItems")
    root.append(etree.Comment("Experimental Yield Systematics"))
    sys_yield_values = sys_yield_values.unstack("direction")
    for sys_name, sys_value in sys_yield_values.iterrows():
        sys_value_up = sys_value["up"]
        sys_value_down = sys_value["down"]
        if sys_value_up == 0 and sys_value_down == 0:
            continue

        if np.isnan(sys_value_up):
            sys_value_up = -sys_value_down
        if np.isnan(sys_value_down):
            sys_value_down = -sys_value_up

        systematic = etree.SubElement(root, "Systematic")
        systematic.set("Name", "%s" % sys_name)
        systematic.set("Constr", "asym")
        systematic.set("Mag", "%f, %f" % (sys_value_down, sys_value_up))
        systematic.set("CentralValue", "1")
        systematic.set("WhereTo", "yield")
    text = etree.tostring(
        root, pretty_print=True, xml_declaration=False, doctype=doctype_SampleItems
    )
    fn = os.path.join(base_dir, "config/syst/yield/SampleItems_%s.xml" % reco_category)
    with open(fn, "wb") as f:
        f.write(text)


def make_systematics_matrix(base_dir, reco_category, true_category, sys_yield_values):
    root = Element("SampleItems")
    root.append(etree.Comment("Experimental Yield Systematics"))
    # Experimental yield systematics definitions
    for sys_name, sys_value in sys_yield_values.items():
        sys_value = np.nan_to_num(sys_value)
        systematic = etree.SubElement(root, "Systematic")
        systematic.set("Name", sys_name)
        systematic.set("Constr", "asym")
        systematic.set("Mag", "%f, %f" % (-sys_value, sys_value))
        systematic.set("CentralValue", "1")
        systematic.set("WhereTo", "yield")
    text = etree.tostring(
        root, pretty_print=True, xml_declaration=False, doctype=doctype_SampleItems
    )
    fn = os.path.join(
        base_dir,
        "config/syst/yield/SampleItems_true_{true_name}__reco_{reco_name}.xml".format(
            true_name=true_category, reco_name=reco_category
        ),
    )
    with open(fn, "wb") as f:
        f.write(text)


def make_signal(
    reader: Reader,
    region: str,
    quantity: str,
    reco_category: str,
    ibin: int,
    output_base_dir: str,
    signal_model_type: str = "DSCB",
):

    if signal_model_type == "DSCB":
        signal_xml = make_signal_DSCB(reader, region, quantity, ibin)
    elif signal_model_type == "gaus":
        signal_xml = make_signal_gaus(reader, region, quantity, ibin)
    else:
        raise ValueError("cannot understand signal type %s" % signal_model_type)

    text = etree.tostring(
        signal_xml, pretty_print=True, xml_declaration=False, doctype=doctype_model
    )
    fn = os.path.join(
        output_base_dir,
        "config/model/signal_{reco_category}.xml".format(reco_category=reco_category),
    )
    with open(fn, "wb") as f:
        f.write(text)


def make_signal_DSCB(reader: Reader, region: str, quantity: str, ibin: int):
    params = reader.get_signal_pars(region, quantity, ibin)
    if params.isnull().any():
        logging.error("no signal parameter for %s %s bin %s", region, quantity, ibin)

    root = Element("Model")
    root.set("Type", "UserDef")
    root.set("CacheBinning", "100000")
    etree.SubElement(root, "Item").set(
        "Name",
        "expr::muCBFn( '(@0 + (@1-125.0))', muCBNom[%f], mH)" % params["mu_cb"],
    )
    etree.SubElement(root, "Item").set("Name", "sigmaCBFn[%f]" % params["sigma_cb"])
    etree.SubElement(root, "Item").set("Name", "alphaCBLo[%f]" % params["alpha_low"])
    etree.SubElement(root, "Item").set("Name", "alphaCBHi[%f]" % params["alpha_hi"])
    etree.SubElement(root, "Item").set("Name", "nCBLo[%f]" % params["ncb_low"])
    etree.SubElement(root, "Item").set("Name", "nCBHi[%f]" % params["ncb_hi"])
    etree.SubElement(root, "ModelItem").set(
        "Name",
        "RooTwoSidedCBShape::signal(:observable:, prod::muCB(muCBFn, resp_MSS), prod::sigmaCB(sigmaCBFn, resp_MRS), alphaCBLo, nCBLo, alphaCBHi, nCBHi)",
    )
    return root


def make_signal_gaus(reader: Reader, region: str, quantity: str, ibin: int):
    params = reader.get_signal_pars(region, quantity, ibin, signal_model_type='gaus')
    if params.isnull().any():
        logging.error("no signal parameter for %s %s bin %s", region, quantity, ibin)

    root = Element("Model")
    root.set("Type", "UserDef")
    root.set("CacheBinning", "100000")
    etree.SubElement(root, "Item").set(
        "Name",
        "expr::muGausFn( '(@0 + (@1-125.0))', muCBNom[%f], mH)" % params["mu_G"],
    )
    etree.SubElement(root, "Item").set("Name", "sigmaGausFn[%f]" % params["sigma_G"])
    etree.SubElement(root, "ModelItem").set(
        "Name",
        "RooGaussian::signal(:observable:, prod::muGaus(muGausFn, resp_MSS), prod::sigmaGaus(sigmaGausFn, resp_MRS))",
    )
    return root


def make_background(reader, region, quantity, reco_category, ibin, bkg_function, output_base_dir):

    bkg_func_params = reader.get_background_prefit_pars(region, quantity, ibin)[
        1:
    ]  # the first is the number of bkg events

    etree.Comment("DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'")
    root = Element("Model")
    root.set("Type", "UserDef")

    background_expression = background_factory(
        "bkg_category_{quantity}_{ibin}".format(quantity=quantity, ibin=ibin),
        bkg_function,
        ":observable:",
        "par_{quantity}_{ibin}".format(quantity=quantity, ibin=ibin),
        [(par[1], par[1] - 3 * abs(par[2]), par[1] + 3 * abs(par[2])) for par in bkg_func_params],
    )

    # TODO: FIX
    background_expression = background_expression.replace("RooBernsteinM", "RooBernstein")
    etree.SubElement(root, "ModelItem").set("Name", background_expression)

    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_model)
    f = open(
        os.path.join(
            output_base_dir,
            "config/model/background_{reco_category}.xml".format(reco_category=reco_category),
        ),
        "wb",
    )
    f.write(text)


def create_subdir_structure(basedir):
    from shutil import copy

    dtd_filename = os.path.join(os.path.dirname(__file__), "AnaWSBuilder.dtd")
    copy(dtd_filename, basedir)
    for subdir in "config/channel", "config/model", "config/syst/shape", "config/syst/yield":
        create_dir_if_not_exists(os.path.join(basedir, subdir))
        copy(dtd_filename, os.path.join(basedir, subdir))


def copy_link_to_data(data_directory: str, output_directory: str, copy: bool = False):

    if not os.path.isdir(data_directory):
        raise IOError("cannot find directory with data %s" % data_directory)

    destination = os.path.join(output_directory, "data")
    if copy:
        import shutil

        if os.path.exists(destination) and os.path.isdir(destination):
            shutil.rmtree(destination)
        shutil.copytree(data_directory, destination)
    else:
        try:
            os.unlink(os.path.join(output_directory, "data"))
        except:
            pass
        os.symlink(os.path.relpath(data_directory, os.path.join(destination, "..")), destination)


def prune_vector(x, idx_to_remove):
    if not idx_to_remove:
        return x
    return [x[i] for i in range(len(x)) if i not in idx_to_remove]


def prune_matrix(matrix, idx_to_remove):
    if not idx_to_remove:
        return matrix
    result = np.copy(matrix)
    idx_to_remove = np.array(idx_to_remove)
    result = np.delete(result, idx_to_remove + 2, 0)  # true axis
    result = np.delete(result, idx_to_remove, 1)  # reco axis
    return result


def get_sys_scale(reader: Reader, region: str, quantity: str, index_to_remove) -> pd.DataFrame:
    sys_scale_values = reader.get_pes(region, quantity).drop(columns=index_to_remove)
    if sys_scale_values.empty:
        logging.warning("no scale sys found")
        return sys_scale_values
    nsys_before = len(sys_scale_values.index.levels[0])

    ignore_sys_mask = sys_scale_values.index.map(lambda x: "EG_SCALE_ALL" in x[0] or "AF2" in x[0])
    sys_scale_values.rename(
        lambda x: "ATLAS_" + x if not x.startswith("ATLAS_") else x,
        axis=0,
        level="sys_name",
        inplace=True,
    )
    sys_scale_values = sys_scale_values.drop(sys_scale_values.index[ignore_sys_mask])
    sys_scale_values = sys_scale_values.fillna(0)
    small_scale_mask = sys_scale_values.abs().mean(axis=1).max(axis=0, level="sys_name") < 0.0001

    small_scale_names = small_scale_mask.index[small_scale_mask]
    large_scale_names = small_scale_mask.index[~small_scale_mask]

    for sys_name in small_scale_names:
        logging.info("Skipping systematic on scale %s --> too small", sys_name)

    sys_scale_values = sys_scale_values.loc[large_scale_names]
    sys_scale_values.index = sys_scale_values.index.remove_unused_levels()
    logging.info(
        "scale systematics before pruning: %d, after: %d",
        nsys_before,
        len(sys_scale_values.index.levels[0]),
    )

    return sys_scale_values


def get_sys_resolution(reader: Reader, region: str, quantity: str, index_to_remove):
    sys_resolution_values = reader.get_per(region, quantity).drop(columns=index_to_remove)
    if sys_resolution_values.empty:
        return sys_resolution_values
    nsys_before = len(sys_resolution_values.index.levels[0])

    ignore_sys_mask = sys_resolution_values.index.map(
        lambda x: "RESOLUTION_ALL" in x[0] or "AF2" in x[0]
    )
    sys_resolution_values.rename(
        lambda x: "ATLAS_" + x if not x.startswith("ATLAS_") else x,
        axis=0,
        level="sys_name",
        inplace=True,
    )
    sys_resolution_values = sys_resolution_values.drop(sys_resolution_values.index[ignore_sys_mask])
    sys_resolution_values = sys_resolution_values.fillna(0)
    small_res_mask = sys_resolution_values.abs().mean(axis=1).max(axis=0, level="sys_name") < 0.0001

    small_res_sys_names = small_res_mask.index[small_res_mask]
    large_res_sys_names = small_res_mask.index[~small_res_mask]

    for sys_name in small_res_sys_names:
        logging.info("Skipping systematic on resolution %s --> too small", sys_name)

    sys_resolution_values = sys_resolution_values.loc[large_res_sys_names]
    sys_resolution_values.index = sys_resolution_values.index.remove_unused_levels()
    logging.info(
        "resolution systematics before pruning: %d, after: %d",
        nsys_before,
        len(sys_resolution_values.index.levels[0]),
    )

    return sys_resolution_values


def get_sys_cfactors(reader: Reader, region: str, quantity: str, index_to_remove) -> pd.DataFrame:
    sys_cfactors = reader.get_cfactors_sys(region, quantity)
    if sys_cfactors.empty:
        return sys_cfactors
    sys_cfactors = sys_cfactors.drop(columns=index_to_remove)
    sys_cfactors = sys_cfactors.fillna(0)
    logging.info("number of experimental sys on cfactors = %d", len(sys_cfactors.index.levels[0]))
    # sys is small if c1 and c2
    # c1: max[between up/down] mean[across bins] abs(sys) < ...
    # c2: max[between up/down] max[across bins] abs(sys) < ...
    c1 = sys_cfactors.abs().mean(axis=1).max(axis=0, level="sys") < 0.0005
    c2 = sys_cfactors.abs().max(axis=1).max(axis=0, level="sys") < 0.003
    small_mask = c1 & c2
    small_sys_names = small_mask.index[small_mask]
    large_sys_names = small_mask.index[~small_mask]

    for sys_name in small_sys_names:
        logging.info("Skipping systematic on c-factors %s --> too small", sys_name)

    sys_cfactors = sys_cfactors.loc[large_sys_names]
    sys_cfactors.index = sys_cfactors.index.remove_unused_levels()
    logging.info(
        "number of experimental sys on cfactors after pruning = %d",
        len(sys_cfactors.index.levels[0]),
    )
    return sys_cfactors


def get_sys_yield(input_dir, region, quantity, unfolding):
    sys_yield_values = {}
    for sys_fn in glob(
        os.path.join(
            input_dir, "systematics", "sys_yield", unfolding, region, quantity, "*__1up.csv"
        )
    ):
        sys_name = os.path.basename(sys_fn).split("__1")[0].split(".")[-1]
        sys_values = np.genfromtxt(sys_fn, delimiter=",")
        assert sys_name not in sys_yield_values
        sys_yield_values[sys_name] = sys_values

    return sys_yield_values


def create_xmls(
    region: str,
    quantity: str,
    input_dir: str,
    data_dir: str,
    output_dir: str,
    unfolding: str,
    blinded: bool,
    lumi: float,
    do_fit: bool,
    copy_data: bool,
    no_data: bool = False,
    signal_model_type: str = "DSCB",
    mH_value: float = definitions.mH
):
    logging.info("Creating xml files for (%s, %s)", region, quantity)
    reader = Reader(input_dir)
    blinding_suffix = "blinded" if blinded else "unblinded"

    logging.info("Output directory: %s", output_dir)
    create_dir_if_not_exists(output_dir)

    create_subdir_structure(output_dir)
    base_data_dir = os.path.abspath(
        os.path.join(input_dir, "data") if data_dir is None else data_dir
    )
    data_path = os.path.join(
        base_data_dir,
        blinding_suffix,
        region,
        quantity,
    )
    if not no_data:
        copy_link_to_data(data_path, output_dir, copy=copy_data)

    output_ws_name = os.path.join(
        # output_dir.split("/")[-1],
        "XSectionWS_{region}_{quantity}.root".format(region=region, quantity=quantity),
    )
    logging.info("Output ws name: %s", output_ws_name)

    # The idea is to first define which categories/truth-bins we want to use.
    # We are not using bins where the xsection is 0. This will remove bins that
    # make no sense (e.g. pT<0)
    # So we are defining vector of quantities containing values only for the
    # categories/truth-bins we want. Still the nomenclature will remain the original,
    # for example the underflow bin will be bin0. All the input files uses this
    # convention (underflow is the bin 0, event when it make no sense to have an
    # underflow).
    # Do not confuse the index you use to iterate with the index of the categories.
    # For example if you prune the underflow the name of the first bin will be bin1,
    # but since it is the first when you iterate it will have index 0.

    # Get xsections array
    xsections_no_pruning = reader.get_xsections_fiducial(region, quantity)
    logging.info(
        "xsections (%d): %s",
        len(xsections_no_pruning),
        short_array_repr(xsections_no_pruning, "%.2f"),
    )

    # The name of the bins will follow the original numbering
    true_bins_name = [
        naming.get_truebin_name(region, quantity, ibin) for ibin in range(len(xsections_no_pruning))
    ]
    logging.info("True bins (%d): %s", len(true_bins_name), short_array_repr(true_bins_name))

    # prune input (remove under/overflow and true bins with null cross section when needed)
    index_to_remove = [i for i in range(len(xsections_no_pruning)) if xsections_no_pruning[i] == 0]
    logging.info("Categories with null cross section: %s", index_to_remove)

    xsections = prune_vector(xsections_no_pruning, index_to_remove)
    true_bins_name = prune_vector(true_bins_name, index_to_remove)
    logging.info("xsections (%d): %s", len(xsections), short_array_repr(xsections, "%.2f"))

    # Get categories
    if "_vs_" in quantity:
        bin_info = definitions.binnings[quantity]
        nbins_noextraflow = len(np.linspace(bin_info[1], bin_info[2], bin_info[0]))
    else:
        nbins_noextraflow = definitions.nbins_noextraflow[quantity]

    # Also the naming of the reco-category follow the original numbering
    reco_bins_name = [
        naming.get_category_name(region, quantity, ibin) for ibin in range(nbins_noextraflow + 2)
    ]  # +2 to add under/over-flow
    reco_bins_number = list(range(nbins_noextraflow + 2))
    logging.info("Reco bins (%d): %s", len(reco_bins_name), short_array_repr(reco_bins_name))

    reco_bins_name = prune_vector(reco_bins_name, index_to_remove)
    logging.info("Reco bins (%d): %s", len(reco_bins_name), short_array_repr(reco_bins_name))
    # this list will keep the index (in the original form) of the bin that are used
    reco_bins_number = prune_vector(reco_bins_number, index_to_remove)

    make_combination(
        true_bins_name,
        reco_bins_name,
        output_ws_name=output_ws_name,
        base_dir=output_dir,
        blind=blinded,
        do_fit=do_fit,
    )

    sys_scale_values = get_sys_scale(reader, region, quantity, index_to_remove)
    sys_resolution_values = get_sys_resolution(reader, region, quantity, index_to_remove)
    sys_scale_names = sys_scale_values.index.levels[0].tolist()
    sys_resolution_names = sys_resolution_values.index.levels[0].tolist()

    if unfolding == "matrix":
        br_dalitz = 0.000148
        logging.info("Branching Ratio dalitz: %f", br_dalitz)
        acceptance_fiducial = 0.50447351811  # TODO: fix
        logging.info("Fiducial acceptance: %f", acceptance_fiducial)

        folding_matrix = reader.get_matrix(region, quantity).values

        logging.info("Folding matrix has shape %s", folding_matrix.shape)
        folding_matrix = prune_matrix(folding_matrix, index_to_remove)
        logging.info("Folding matrix (after pruning) has shape %s", folding_matrix.shape)

        xsection_non_fiducial = np.sum(xsections) * (1 - acceptance_fiducial) / acceptance_fiducial
        xsection_dalitz = np.sum(xsections) + xsection_non_fiducial
        xsections = np.array([xsection_non_fiducial, xsection_dalitz] + list(xsections))
        logging.info(
            "Adding cross section for non-fiducial and dalitz (%d): %s",
            len(xsections),
            short_array_repr(xsections, "%.2f"),
        )

        true_bins_name = [
            "%s_notFiducialnotDalitz" % quantity,
            "%s_isDalitz" % quantity,
        ] + true_bins_name
        logging.info(
            "true bin names (%d): %s", len(true_bins_name), short_array_repr(true_bins_name)
        )

        sys_yield_values = get_sys_yield(input_dir, region, quantity, "matrix")
        sys_yield_values = {
            k: np.nan_to_num(prune_matrix(v, index_to_remove)) for k, v in sys_yield_values.items()
        }
        logging.info("number of experimental sys on matrix = %d", len(sys_yield_values))
        small_systematics = []
        for sys_name, values in sys_yield_values.items():
            small_eff = folding_matrix < 0.01

            if (
                np.abs(values[~small_eff]).mean() < 0.002
                and np.abs(values[~small_eff]).max() < 0.02
            ):
                logging.info("Skipping systematic on matrix %s --> too small", sys_name)
                small_systematics.append(sys_name)
        sys_yield_values = {k: v for k, v in sys_yield_values.items() if k not in small_systematics}
        logging.info(
            "number of experimental sys on matrix after pruning = %d", len(sys_yield_values)
        )

        for ireco, reco_category in enumerate(reco_bins_name):
            make_channel_matrix(
                reader,
                region,
                quantity,
                reco_bins_number[ireco],
                true_bins_name,
                lumi=lumi,
                xsections=xsections,
                efficiencies=folding_matrix[:, ireco],
                br_dalitz=br_dalitz,
                global_yield_systematics=definitions.global_yield_systematics,
                global_scale_systematics=definitions.global_scale_systematics,
                input_dir=input_dir,
                base_dir=output_dir,
                blind=blinded,
                sys_scale_names=sys_scale_names,
                sys_resolution_names=sys_resolution_names,
                mH_value=mH_value
            )

            for itrue, true_category in enumerate(true_bins_name):
                sys_yield_values_this_cat_bin = {
                    k: v[itrue, ireco] for k, v in sys_yield_values.items()
                }

                make_systematics_matrix(
                    output_dir,
                    reco_category,
                    true_category,
                    sys_yield_values_this_cat_bin,
                )

    elif unfolding == "bin_by_bin":
        # Get cfactors array
        cfactors = reader.get_cfactors(region, quantity)

        logging.info("cfactors (%d): %s", len(cfactors), short_array_repr(cfactors, "%.2f"))

        cfactors = prune_vector(cfactors, index_to_remove)
        logging.info(
            "cfactors after pruning (%d): %s", len(cfactors), short_array_repr(cfactors, "%.2f")
        )
        if any(np.isnan(cfactors)):
            logging.error("some cfactors are null for %s %s", region, quantity)

        sys_cfactors = get_sys_cfactors(reader, region, quantity, index_to_remove)

        for ireco, reco_category in enumerate(reco_bins_name):
            make_channel_bin_by_bin(
                reader,
                region,
                quantity,
                reco_bins_number[ireco],
                xsection=xsections[ireco],
                cfactor=cfactors[ireco],
                global_yield_systematics=definitions.global_yield_systematics,
                global_scale_systematics=definitions.global_scale_systematics,
                input_dir=input_dir,
                base_dir=output_dir,
                lumi=lumi,
                blind=blinded,
                sys_scale_names=sys_scale_names,
                sys_resolution_names=sys_resolution_names,
                mH_value=mH_value
            )

            sys_cfactors_this_cat = sys_cfactors[reco_bins_number[ireco]]
            make_systematics_bin_by_bin(reco_category, output_dir, sys_cfactors_this_cat)

    path_file = os.path.join(h025_xsections.__path__[0], "../external-data/ss_results.txt")
    reader_LPHE = Reader_LPHE(basedir=None, normalize=True)
    try:
        bkg_functions = reader_LPHE.get_background_function_old(
            region, quantity, filename=path_file
        )
    except ValueError:
        logging.error("cannot find background function for %s %s, using ExpPoly2", region, quantity)
        bkg_functions = None

    for ireco, reco_category in zip(reco_bins_number, reco_bins_name):
        make_signal(
            reader,
            region,
            quantity,
            reco_category,
            ireco,
            output_base_dir=output_dir,
            signal_model_type=signal_model_type,
        )
        bkg_function = "ExpPoly2" if bkg_functions is None else bkg_functions[ireco]
        if bkg_function not in VALID_BACKGROUND_FUNCTIONS:
            bkg_function = "ExpPoly2"

        sys_resolution_values_this_cat = sys_resolution_values[ireco]
        sys_scale_values_this_cat = sys_scale_values[ireco]

        make_background(
            reader,
            region,
            quantity,
            reco_category,
            ireco,
            bkg_function,
            output_base_dir=output_dir,
        )
        make_shape_SampleItems(
            region,
            quantity,
            reco_category,
            input_dir,
            output_dir,
            ireco,
            sys_scale_values_this_cat,
            sys_resolution_values_this_cat,
        )


if __name__ == "__main__":

    import argparse
    from argparse import RawTextHelpFormatter

    parser = argparse.ArgumentParser(
        description="Create xml for XMLAnabuilder workspace",
        formatter_class=RawTextHelpFormatter,
        epilog="Example: ./make_workspace.py ~/cernbox/h025_xsections/v0.6.0 out_xml --quantities pT_yy --unfolding bin_by_bin --blinded",
    )
    parser.add_argument("input_dir")
    parser.add_argument("output_dir")
    parser.add_argument(
        "--data-dir", help="alternative directory to find data, otherwise input_dir is used"
    )
    parser.add_argument("--region")
    parser.add_argument("--quantity")
    parser.add_argument(
        "--unfolding",
        choices=["bin_by_bin", "matrix"],
        help="Unfolding method (bin_by_bin or matrix)",
        required=True,
    )
    parser.add_argument("--blinded", action="store_true")
    parser.add_argument("--fit", action="store_true")
    parser.add_argument(
        "--luminosity",
        help="luminosity in 1/pb",
        type=float,
        default=definitions.luminosity["mcAll"],
    )
    parser.add_argument("--copy-data", action="store_true")
    parser.add_argument("--no-data", action="store_true")
    parser.add_argument("--signal-model-type", choices=("DSCB", "gaus"), default="DSCB")
    parser.add_argument("--mH-value", type=float, default=definitions.mH)
    args = parser.parse_args()

    logging.info("Input directory: %s", args.input_dir)
    logging.info("Output directory: %s", args.output_dir)
    logging.info("Unfolding method: %s", args.unfolding)

    logging.info("Luminosity: %s /pb", args.luminosity)

    blinding_suffix = "blinded" if args.blinded else "unblinded"
    output_dir = os.path.join(args.output_dir, args.region, args.quantity, blinding_suffix)
    create_xmls(
        args.region,
        args.quantity,
        args.input_dir,
        args.data_dir,
        args.output_dir,
        args.unfolding,
        args.blinded,
        args.luminosity,
        args.fit,
        args.copy_data,
        args.no_data,
        args.signal_model_type,
        args.mH_value
    )
