import subprocess
import os
import pandas as pd
from glob import glob
from typing import List

import logging

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

import h025_xsections
from h025_xsections.core import definitions
from h025_xsections.core.folders import get_path
from h025_xsections.version import __version__
from h025_xsections.core.latex import Document, Slide, Figure, Graphics, RawLatex, escape

def make_samples() -> List[Slide]:

    path_file = os.path.join(
        h025_xsections.__path__[0], "samples/samples_h025_uxAOD-xrootd-proof.txt"
    )
    df = pd.read_csv(path_file, sep="\t")
    df.columns = ["generator", "prod", "mc", "skim", "ifile", "path", "nevents", "sys", "truth"]
    df = df[df["sys"] == "HGamEventInfo"]
    df = df[(df["prod"] != "?") & (df["mc"] != "?")]
    df = df.drop(columns=["sys", "truth"])
    df["path"] = df["path"].str.split("uxAOD/").map(lambda x: x[1])
    df = df.sort_values(by=df.columns.tolist(), axis='index')

    slides = []
    n_per_pages = 30
    npages = len(df) // 30 + 1
    for ipage in range(npages):
        slide = Slide("Nominal samples / %d" % (ipage + 1))

        latex_table = df.iloc[ipage * n_per_pages : (ipage + 1) * n_per_pages].to_latex(index=False)

        latex = r"""
            \begin{table}
        \centering
        \resizebox{0.95\textwidth}{!}{
            <table>
        }
        \end{table}
        """.replace(
            "<table>", latex_table
        )

        slide.add_content(RawLatex(latex))
        slides.append(slide)

    return slides


def make_luminosity() -> Slide:
    slide = Slide("Luminosity")
    slide.add_content(
        RawLatex(
            r"""
    \begin{table}
    \centering
    \begin{tabular}{ll}
        \toprule
        Period & Luminosity [\si{\pico\barn^{-1}}] \\
        \midrule
        2015+16 & %.2f \\
        2017 & %.2f \\
        2018 & %.2f \\
        \hline
        Total & %.2f \\
        \bottomrule
    \end{tabular}
    \end{table}
    """
            % (
                definitions.luminosity["mc16a"],
                definitions.luminosity["mc16d"],
                definitions.luminosity["mc16e"],
                definitions.luminosity["mcAll"],
            )
        )
    )
    return slide


def make_weight_final() -> Slide:
    slide = Slide("Weight final")
    slide.add_content(
        RawLatex(
            "To build the weight-final the following branches are used, depending on the quantity"
        )
    )
    weight_final_table = {}
    for region_quantity, weights in definitions.weight_final.items():
        if len(weights) == 0:
            weights += ["-"]
        weight_final_table[region_quantity] = weights

    table = "\n".join(
        [
            r"{region} & {quantity} & {weight} \\".format(
                region=region_quantity[0],
                quantity=definitions.latex_labels[region_quantity[1]],
                weight=r" $\times$ ".join(
                    [r"\texttt{{{weight}}}".format(weight=escape(weight)) for weight in weights]
                ),
            )
            for region_quantity, weights in weight_final_table.items()
        ]
    )

    slide.add_content(
        RawLatex(
            r"""
    \begin{table}
    \centering
    \resizebox{0.4\textwidth}{!}{
    \begin{tabular}{lll}
        \toprule
        <table>
        \bottomrule
    \end{tabular}
    }
    \end{table}
    """.replace(
                "<table>", table
            )
        )
    )
    return slide


def make_binning() -> Slide:
    slide = Slide("Binning")
    slide.add_content(
        RawLatex(
            "To build the weight-final the following branches are used, depending on the quantity"
        )
    )
    table = "\n".join(
        [
            "{quantity} & {nbins} & {binning} \\\\".format(
                quantity=escape(quantity),
                nbins=len(definitions.binnings[quantity]) - 1,
                binning=", ".join(map(lambda x: "%.2f" % x, definitions.binnings[quantity])),
            )
            for quantity in definitions.quantities
        ]
    )
    slide.add_content(
        RawLatex(
            r"""
    \begin{table}
    \centering
    \resizebox{\textwidth}{!}{
    \begin{tabular}{lll}
        \toprule
        <table>
        \bottomrule
    \end{tabular}
    }
    \end{table}
    """.replace(
                "<table>", table
            )
        )
    )
    return slide


def make_cross_section(basedir, region) -> Slide:
    slide = Slide("Fiducial cross sections for region %s" % region)
    latex = open(
        os.path.join(
            basedir,
            "tables/xsections",
            "table_xsection_mcmcAll_prodprodAll_region{region}.tex".format(region=region),
        )
    ).read()
    slide.add_content(
        RawLatex(
            r"""
    \begin{table}
    \centering
    \resizebox{\textwidth}{!}{
    <table>
    }
    \end{table}""".replace(
                "<table>", latex
            )
        )
    )
    return slide


def make_yield(basedir, region, quantity) -> Slide:
    slide = Slide(
        "Signal yield %s for region %s fiducial" % (definitions.latex_labels[quantity], region)
    )
    slide.add_content(
        RawLatex(
            r"True includes only fiducial and not-Dalitz. Normalized to \SI{%.2f}{\pico\barn^{-1}}"
            % definitions.luminosity["mcAll"]
        )
    )

    freco = os.path.join(
        basedir,
        "efficiencies",
        "efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL",
        "nreco_isPassed",
        region,
        quantity,
        "nreco_isPassed_{region}_{quantity}_mcAll_prodprodAll_NOMINAL.csv".format(
            region=region, quantity=quantity
        ),
    )
    df_reco = pd.read_csv(freco, header=None)

    ftrue = os.path.join(
        basedir,
        "efficiencies",
        "efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL",
        "ntrue_isFiducial_noDalitz",
        region,
        quantity,
        "ntrue_isFiducial_noDalitz_{region}_{quantity}_mcAll_prodprodAll_NOMINAL.csv".format(
            region=region, quantity=quantity
        ),
    )
    df_true = pd.read_csv(ftrue, header=None)

    fcfactor = os.path.join(
        basedir,
        "efficiencies",
        "efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL",
        "cfactors",
        region,
        quantity,
        "cfactors_{region}_{quantity}_mcAll_prodprodAll_NOMINAL.csv".format(
            region=region, quantity=quantity
        ),
    )
    df_cfactors = pd.read_csv(fcfactor, header=None)

    df = pd.DataFrame({"true": df_true[0], "reco": df_reco[0], "cfactor": df_cfactors[0]})
    table = df.to_latex(float_format="%.2f")

    if len(df) > 10:
        table = RawLatex(
            r"""
            \begin{table}
            \resizebox*{!}{0.8\textheight}{
            <table>
            }
            \end{table}
            """.replace(
                "<table>", table
            )
        )

    else:
        table = RawLatex(
            r"""
            \begin{table}
            <table>
            \end{table}
            """.replace(
                "<table>", table
            )
        )

    fig_path = get_path(
        basedir, "cfactor_sys_plot", region=region, quantity=quantity, direction="up"
    )

    figure = Figure([Graphics(fig_path, r"width=\textwidth")])

    columns = RawLatex(
        r"""
    \begin{columns}
    \begin{column}{0.5\textwidth}
    <table>
    \end{column}
    \begin{column}{0.5\textwidth}
    <figure>
    \end{column}
    \end{columns}
    """.replace(
            "<table>", table.render()
        ).replace(
            "<figure>", figure.render()
        )
    )

    slide.add_content(columns)

    return slide


def make_matrix(basedir, region, quantity) -> Slide:
    slide = Slide(
        "Confusion and migration for %s for %s fiducial"
        % (definitions.latex_labels[quantity], region)
    )
    fig1 = os.path.join(
        basedir,
        "efficiencies",
        "efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL",
        "folding_matrices",
        region,
        quantity,
        "plot",
        "yield2d_mcAll_prodAll_sysNOMINAL.pdf",
    )
    fig2 = os.path.join(
        basedir,
        "efficiencies",
        "efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL",
        "folding_matrices",
        region,
        quantity,
        "plot",
        "folding_mcAll_prodAll_sysNOMINAL.pdf",
    )

    fn_condition_number = os.path.join(
        basedir,
        "efficiencies",
        "efficiencies_mcAll.prodAll.isPass_or_isFiducial.Nominal.NOMINAL",
        "cond_number",
        region,
        quantity,
        "cond_{region}_{quantity}_mcAll_prodprodAll_NOMINAL.csv".format(
            region=region, quantity=quantity
        ),
    )

    condition_number = float(open(fn_condition_number).read().strip())
    figure = Figure(
        [Graphics(fig1, r"width=0.45\textwidth"), Graphics(fig2, r"width=0.45\textwidth")]
    )
    latex = RawLatex(
        r"""
Condition number of isFiducial $\times$ isPassed submatrix: %.4f
"""
        % condition_number
    )
    slide.add_content(figure)
    slide.add_content(latex)
    return slide


def make_sigmaCB(basedir, region) -> Slide:
    slide = Slide("$\sigma_{CB}$ [GeV] of signal shape %s fiducial" % (region))

    fn = os.path.join(
        basedir,
        "tables/signal_pars",
        "table_signal parameters_mcmcAll_prodprodAll_region{region}_parametersigma_cb.tex".format(
            region=region
        ),
    )

    latex = open(fn).read()

    slide.add_content(
        RawLatex(
            r"""
    \begin{table}
    \centering
    \resizebox{\textwidth}{!}{
    <table>
    }
    \end{table}""".replace(
                "<table>", latex
            )
        )
    )
    return slide


def make_signal_shape_sys(basedir, region, quantity) -> Slide:
    slide = Slide(
        "Signal shape systematics for %s for %s fiducial"
        % (definitions.latex_labels[quantity], region)
    )
    fig1 = os.path.join(
        basedir,
        "plot_signal_shape_sys",
        "plotsys_muCBNom_{region}_{quantity}_up.png".format(region=region, quantity=quantity),
    )
    fig2 = os.path.join(
        basedir,
        "plot_signal_shape_sys",
        "plotsys_sigmaCBNom_{region}_{quantity}_up.png".format(region=region, quantity=quantity),
    )
    fig3 = os.path.join(
        basedir,
        "plot_signal_shape_sys",
        "plotsys_muCBNom_{region}_{quantity}_down.png".format(region=region, quantity=quantity),
    )
    fig4 = os.path.join(
        basedir,
        "plot_signal_shape_sys",
        "plotsys_sigmaCBNom_{region}_{quantity}_down.png".format(region=region, quantity=quantity),
    )

    figure = Figure(
        [
            Graphics(fig1, r"width=0.45\textwidth"),
            Graphics(fig2, r"width=0.45\textwidth"),
            Graphics(fig3, r"width=0.45\textwidth"),
            Graphics(fig4, r"width=0.45\textwidth"),
        ]
    )

    slide.add_content(figure)
    return slide


def make_cfactors_sys(basedir, region, quantity) -> Slide:
    slide = Slide("C-factors systematics UP %s %s" % (region, definitions.latex_labels[quantity]))
    fig_path = get_path(
        basedir, "cfactor_sys_plot", region=region, quantity=quantity, direction="up"
    )
    fig = Figure([Graphics(fig_path, r"width=0.9\textwidth")])
    slide.add_content(fig)
    return slide


def make_cfactors(basedir, region) -> Slide:
    slide = Slide("C-factors summary %s fiducial" % (region))

    fn = os.path.join(
        basedir,
        "tables/cfactors",
        "table_cfactors_mcmcAll_prodprodAll_region{region}.tex".format(region=region),
    )

    latex = open(fn).read()

    slide.add_content(
        RawLatex(
            r"""
    \begin{table}
    \centering
    \resizebox{\textwidth}{!}{
    <table>
    }
    \end{table}
    """.replace(
                "<table>", latex
            )
        )
    )
    return slide


def make_chi2(basedir) -> Slide:
    slide = Slide("$\chi^2$ values summary")

    fn_binbybin = os.path.join(
        basedir,
        "tables/chi2/bin_by_bin",
        "table_chi2 bin by bin_prodprodAll_mcmcAll.tex",
    )

    latex_binbybin = open(fn_binbybin).read()

    fn_matrix = os.path.join(
        basedir,
        "tables/chi2/matrix",
        "table_chi2 matrix_prodprodAll_mcmcAll.tex",
    )

    latex_matrix = open(fn_matrix).read()

    slide.add_content(
        RawLatex(
            r"""
            \begin{center}
            $\chi^2$ values with the Bin by Bin unfolding method
            \begin{table}
            \centering
            \resizebox{\textwidth}{!}{
            <table_binbybin>
            }
            \end{table}
            \vspace{0.5cm}
            $\chi^2$ values with the Inverse Matrix unfolding method
            \begin{table}
            \centering
            \resizebox{\textwidth}{!}{
            <table_matrix>
            }
            \end{table}
            \end{center}""".replace(
                "<table_binbybin>", latex_binbybin
            ).replace(
                "table_matrix", latex_matrix
            )
        )
    )
    return slide


def make_sidebands_fit_plot(basedir, region, quantity, ncat) -> Slide:
    slide = Slide(
        "Data sidebands fit plots for %s for %s fiducial"
        % (definitions.latex_labels[quantity], region)
    )
    folder = get_path(
        basedir,
        "workspace_folder",
        quantity=quantity,
        region=region,
        method="bin_by_bin",
        blinded=True,
    )
    fig = os.path.join(
        folder,
        "XSectionWS_%s_%s.pdf" % (region, quantity),
    )
    if not os.path.exists(fig):
        logging.error("cannot find %s", fig)
    figure = Figure()

    for i in range(ncat):
        width = r"0.8" if definitions.latex_labels[quantity] == "inclusive" else r"0.18"
        options = r"page=%s, width=%s\textwidth" % (i + 1, width)
        figure.add_figure(Graphics(fig, options))

    slide.add_content(figure)
    return slide


def make_pulls(basedir, region, quantity) -> Slide:
    slide = Slide(
        "Signal yield systematic pulls for %s %s fiducial"
        % (definitions.latex_labels[quantity], region)
    )
    fig_pulls = get_path(
        basedir,
        "pull_plot",
        method="bin_by_bin",
        dataset="AsimovSB",
        region=region,
        quantity=quantity,
    )

    figure = Figure([Graphics(fig_pulls, r"width=0.9\textwidth")])
    slide.add_content(figure)
    return slide


def make_plot_results(basedir, region, quantity) -> Slide:
    slide = Slide(
        "Fitted expected values for %s for %s fiducial"
        % (definitions.latex_labels[quantity], region)
    )

    slide.add_content(RawLatex(r"Bin by bin (top), matrix (bottom)"))

    folder = get_path(
        basedir,
        "final_plots_folder",
        region=region,
        quantity=quantity,
        method="bin_by_bin",
        dataset="AsimovSB",
    )

    fig_binbybin = get_path(
        basedir,
        "result_plot",
        region=region,
        quantity=quantity,
        method="bin_by_bin",
        dataset="AsimovSB",
    )

    correlation_binbybin_stat = os.path.join(
        folder,
        "correlation_{region}_{quantity}_staonly.png".format(region=region, quantity=quantity),
    )

    correlation_binbybin_full = os.path.join(
        folder,
        "correlation_{region}_{quantity}_full.png".format(region=region, quantity=quantity),
    )

    folder = get_path(
        basedir,
        "final_plots_folder",
        region=region,
        quantity=quantity,
        method="matrix",
        dataset="AsimovSB",
    )
    fig_matrix = os.path.join(
        folder,
        "plot_{region}_{quantity}.png".format(region=region, quantity=quantity),
    )

    correlation_matrix_stat = os.path.join(
        folder,
        "correlation_{region}_{quantity}_staonly.png".format(region=region, quantity=quantity),
    )

    correlation_matrix_full = os.path.join(
        folder,
        "correlation_{region}_{quantity}_full.png".format(region=region, quantity=quantity),
    )

    options = r"width=0.29\textwidth"
    figure = Figure(
        [
            Graphics(fig_binbybin, options),
            Graphics(correlation_binbybin_stat, options),
            Graphics(correlation_binbybin_full, options),
            RawLatex(r"\\"),
            Graphics(fig_matrix, options),
            Graphics(correlation_matrix_stat, options),
            Graphics(correlation_matrix_full, options),
        ]
    )
    slide.add_content(figure)
    return slide


def make_plot_fit(basedir, region, quantity, num_categories) -> Slide:
    slide = Slide("Fit plots for %s for %s fiducial" % (definitions.latex_labels[quantity], region))
    figure = Figure()

    folder = get_path(
        basedir,
        "workspace_plots_folder",
        dataset="AsimovSB",
        region=region,
        quantity=quantity,
        method="bin_by_bin",
    )

    for i in range(num_categories):
        fig = os.path.join(
            folder,
            "XSectionWS_{region}_{quantity}_{icat}.pdf".format(
                quantity=quantity, region=region, icat=i
            ),
        )
        width = "0.5" if definitions.latex_labels[quantity] == "inclusive" else "0.18"
        figure.add_figure(Graphics(fig, r"width=%s\textwidth" % width))

    slide.add_content(figure)
    return slide


def make_report(basedir, no_pdf=False):
    document = Document(
        title="Cross sections automatic report",
        subtitle="v%s" % __version__,
        authors="Mario Lamberti, Ruggero Turra",
    )

    logging.info("CREATING SAMPLES")
    document.add_slides(make_samples())
    logging.info("CREATING LUMINOSITY")
    document.add_slide(make_luminosity())
    logging.info("CREATING WEIGHT FINAL")
    document.add_slide(make_weight_final())
    logging.info("CREATING BINNING")
    document.add_slide(make_binning())
    logging.info("CREATING RESOLUTION")
    for region in definitions.fiducial_regions:
        document.add_slide(make_sigmaCB(basedir, region))
    for region in definitions.fiducial_regions:
        document.addslide(make_resolution(basedir, region))
    logging.info("CREATING SIGNAL SHAPE SYSTEMATICS")
    for region, quantity in definitions.regions_quantities_analysis:
        document.add_slide(make_signal_shape_sys(basedir, region, quantity))
    # logging.info("CREATING SIGNAL SHAPE SYSTEMATICS 1UP")
    # for region, quantity in definitions.regions_quantities_analysis:
    #    document.add_slide(make_signal_shape_sys_up(basedir, region, quantity))
    logging.info("CREATING CROSS SECTIONS")
    for region in definitions.fiducial_regions:
        document.add_slide(make_cross_section(basedir, region))
    logging.info("CREATING YIELD")
    for region, quantity in definitions.regions_quantities_analysis:
        document.add_slide(make_yield(basedir, region, quantity))
    logging.info("CREATING C-FACTORS SYS")
    for region, quantity in definitions.regions_quantities_analysis:
        document.add_slide(make_cfactors_sys(basedir, region, quantity))
    logging.info("CREATING C-FACTORS SUMMARY")
    for region in definitions.fiducial_regions:
        document.add_slide(make_cfactors(basedir, region))
    logging.info("CREATING FOLDING MATRIX")
    for region, quantity in definitions.regions_quantities_analysis:
        document.add_slide(make_matrix(basedir, region, quantity))

    logging.info("CREATING CHI^2")
    try:
        document.add_slide(make_chi2(basedir))
    except FileNotFoundError:
        logging.error("problem in chi2")

    for region, quantity in definitions.regions_quantities_analysis:
        folder = get_path(
            basedir,
            "workspace_plots_folder",
            region=region,
            quantity=quantity,
            method="bin_by_bin",
            dataset="AsimovSB",
        )
        num_categories = len(glob(os.path.join(folder, "*.pdf")))
        if num_categories == 0:
            logging.error("found 0 category for %s %s in %s", region, quantity, folder)
        document.add_slide(make_sidebands_fit_plot(basedir, region, quantity, num_categories))

    for region, quantity in definitions.regions_quantities_analysis:
        folder = get_path(
            basedir,
            "workspace_plots_folder",
            region=region,
            quantity=quantity,
            method="bin_by_bin",
            dataset="AsimovSB",
        )
        num_categories = len(glob(os.path.join(folder, "*.pdf")))
        document.add_slide(make_plot_fit(basedir, region, quantity, num_categories))

    logging.info("CREATING PULLLS")
    for region, quantity in definitions.regions_quantities_analysis:
        document.add_slide(make_pulls(basedir, region, quantity))

    logging.info("CREATING PLOT RESULTS")
    for region, quantity in definitions.regions_quantities_analysis:
        document.add_slide(make_plot_results(basedir, region, quantity))

    latex = document.render()

    document_fn = "report.tex"
    with open(document_fn, "w") as f:
        f.write(latex)

    if not no_pdf:
        subprocess.call(
            (
                "pdflatex %s" % document_fn
            ).split()  # , stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"),
        )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Make full PDF report")
    parser.add_argument(
        "basedir",
        help="The directory with all the results (e.g. ~/cernbox_h025/v0.0.3)",
    )
    parser.add_argument("--no-pdf", action="store_true")

    args = parser.parse_args()

    make_report(args.basedir, args.no_pdf)
