#!/usr/bin/env python

import papermill as pm
from glob import glob
import os
import logging

from h025_xsections.core.utils import create_dir_if_not_exists
from h025_xsections.core import definitions

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def run(input_directory, output_directory, output_directory_notebook):
    all_root_fn = glob(
        os.path.join(input_directory, "histo_mcAll.prodAll.norm.merged.root")
    )  # the nominal one
    # all_root_fn += glob(os.path.join(input_directory, "**/*1up*.root"))

    if not all_root_fn:
        logging.error("cannot find any suitable file in folder %s", input_directory)

    for i, fn in enumerate(all_root_fn):
        logging.info("%d. %s" % (i, fn))

    notebook_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../notebooks/plot_to_unfolding.ipynb")
    notebook_path = os.path.normpath(notebook_path)

    create_dir_if_not_exists(output_directory)
    create_dir_if_not_exists(output_directory_notebook)

    for quantity in definitions.quantities:
        for fn in all_root_fn:

            logging.info("running on quantity %s fn %s" % (quantity, fn))

            output_notebook = "plot_to_unfolding_{quantity}__{fn}.ipynb".format(
                quantity=quantity, fn=os.path.splitext(os.path.basename(fn))
            )
            output_notebook = os.path.join(output_directory_notebook, output_notebook)
            do_plots = True

            pm.execute_notebook(
                notebook_path,
                output_notebook,
                parameters=dict(
                    quantity=quantity, file_name=fn, do_plots=do_plots, BASEDIR_OUTPUT=output_directory
                ),
            )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Compute quantities related to yield.")
    parser.add_argument("input_directory", help="path to the merged normalized histograms")
    parser.add_argument("output_directory", default="output")
    parser.add_argument("output_directory_notebook", default="output")

    args = parser.parse_args()
    run(args.input_directory, args.output_directory, args.output_directory_notebook)
