#!/usr/bin/env python

import papermill as pm
from glob import glob
import os

import logging
logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

from h025_xsections.core import definitions, utils


def run(input_directory, output_directory, output_directory_notebook, quantity, region):
    notebook_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../notebooks/plot_cfactor_errors.ipynb")
    notebook_path = os.path.normpath(notebook_path)
    utils.create_dir_if_not_exists(output_directory)
    utils.create_dir_if_not_exists(output_directory_notebook)
    
    print("running on (%s, %s)" % (region, quantity))
    output_notebook = "plot_cfactor_errors_{region}_{quantity}.ipynb".format(region=region, quantity=quantity)
    output_notebook = os.path.join(output_directory_notebook, output_notebook)
    pm.execute_notebook(notebook_path,
                        output_notebook,
                        parameters=dict(input_dir=input_directory, output_dir=output_directory, region=region, quantity=quantity),
    )

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot cfactors with total systematic uncertainties.")
    parser.add_argument("input_directory", help="path to the cfactors .csv files")
    parser.add_argument("output_directory", default="output")
    parser.add_argument("output_directory_notebook", default="output")
    parser.add_argument("--quantities", nargs="*")
    parser.add_argument("--regions", nargs="*")
    args = parser.parse_args()


    if args.quantities == "ALL" and args.regions == "ALL":
        args.quantities = definitions.quantities
        args.regions = definitions.fiducial_regions
        regions_quantities_analysis = [(region, quantity) for region in args.regions for quantity in args.quantities]
    if args.quantities == "ALL" and args.regions:
        args.quantities = definitions.quantities
        regions_quantities_analysis = [(region, quantity) for region in args.regions for quantity in args.quantities]
    if args.quantities and args.regions == "ALL":
        args.regions = definitions.fiducial_regions
        regions_quantities_analysis = [(region, quantity) for region in args.regions for quantity in args.quantities]
    if args.quantities is None and args.regions is None:
        regions_quantities_analysis = definitions.regions_quantities_analysis
    else:
        raise IOError("Invalid input")

    logging.info("SELECTED COMBINATIONS:")
    map(logging.info, regions_quantities_analysis)
    for region, quantity in regions_quantities_analysis:
        run(args.input_directory, args.output_directory, args.output_directory_notebook, quantity, region)
