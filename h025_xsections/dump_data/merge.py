#!/usr/bin/env python

import argparse
import logging
import os
from itertools import groupby

from h025_xsections.core import definitions
from h025_xsections.core.utils import create_dir_if_not_exists

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")

parser = argparse.ArgumentParser(description="merge txt files")

parser.add_argument("directory")
parser.add_argument("output_directory")
args = parser.parse_args()

create_dir_if_not_exists(args.output_directory)

all_observables_list = definitions.quantities + ["constant"]

for root, dirs, files in os.walk(args.directory):
    if dirs:
        new_root = root.replace(
            args.directory.replace("/", ""), args.output_directory.replace("/", "")
        )
        for folder in dirs:
            new_dir = os.path.join(new_root, folder)
            print("creating new dir %s" % new_dir)
            create_dir_if_not_exists(new_dir)
    else:
        for k, v in groupby(sorted(files), lambda x: os.path.basename(x).split("__")[0]):
            output_filename = os.path.join(
                root.replace(
                    args.directory.replace("/", ""), args.output_directory.replace("/", "")
                ),
                k + ".txt",
            )
            logging.info("merging %s", output_filename)
            command = "cat %s > %s" % (
                " ".join(os.path.join(root, vv) for vv in v),
                output_filename,
            )
            os.system(command)
