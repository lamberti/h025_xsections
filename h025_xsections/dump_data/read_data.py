#!/usr/bin/env python

import argparse
import datetime
import logging
import os
from argparse import RawTextHelpFormatter
from array import array

import numpy as np
import ROOT

from h025_xsections.core import definitions
from h025_xsections.core.utils import create_dir_if_not_exists

logging.basicConfig(
    level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s"
)


def create_emtpy_files(output_folder, regions, quantities, split_directories, basename):
    folder = output_folder
    all_out_files = {}
    for quantity in quantities:
        nedges = len(all_binnings[quantity])
        for region in regions:
            if (region, quantity) not in definitions.regions_quantities_analysis:
                continue
            if split_directories:
                folder = os.path.join(output_folder, region, quantity)
                create_dir_if_not_exists(folder)

            # include file for underflow and overflow
            for ibin in range(0, nedges + 1):
                output_filename = "mass_points_%s_bin%d_%s__%s.txt" % (
                    quantity,
                    ibin,
                    region,
                    basename,
                )
                output_filename = os.path.join(folder, output_filename)
                all_out_files[(quantity, region, ibin)] = open(output_filename, "w")
    return all_out_files


def define_branch(tree, quantity_branch):
    try:
        root_type = tree.GetBranch(quantity_branch).GetTitle().split("/")[1].strip()
    except ReferenceError:
        logging.error("cannot find branch %s", quantity_branch)
        raise
    quantity_type = {"F": "f", "I": "l"}[root_type]
    quantity_value = array(quantity_type, [0])

    logging.debug("setting branch %s" % quantity_branch)
    tree.SetBranchStatus(quantity_branch, True)
    tree.SetBranchAddress(quantity_branch, quantity_value)
    return quantity_value


def read_file(
    fname,
    all_binnings,
    n_events=None,
    output_folder="output",
    split_directories=False,
    blind=False,
    regions=None,
):

    quantities = list(all_binnings.keys())
    regions = regions or definitions.fiducial_regions
    additional_regions = [region for region in regions if region != "Diphoton"]

    region_quantity_to_do = [
        rq
        for rq in definitions.regions_quantities_analysis
        if (rq[0] in regions and rq[1] in quantities)
    ]

    all_out_files = {}
    create_dir_if_not_exists(output_folder)
    all_out_files = create_emtpy_files(
        output_folder,
        regions,
        quantities,
        split_directories,
        os.path.splitext(os.path.basename(fname))[0],
    )

    logging.info("number of empty txt file created: %d" % len(all_out_files))

    f = ROOT.TFile.Open(fname)
    if not f:
        raise IOError("cannot find file %s" % fname)
    tree = f.Get("CollectionTree")
    tree.SetBranchStatus("*", False)  # disable all branches

    m_yy_value = array("f", [0])
    isPassed_value = array("b", [0])

    m_yy_branch = "HGamEventInfoAuxDyn.m_yy"
    isPassed_branch = "HGamEventInfoAuxDyn.isPassed"
    additional_isPassed = {
        "VBF": "HGamEventInfoAuxDyn.catXS_VBF",
        "Lepton": "HGamEventInfoAuxDyn.catXS_lepton",
        "MET": "HGamEventInfoAuxDyn.catXS_MET",
    }
    additional_isPassed_branches = {
        region: additional_isPassed[region] for region in additional_regions
    }
    additional_isPassed_values = {
        region: array("b", [0]) for region in additional_regions
    }

    # enable needed branches
    tree.SetBranchStatus(m_yy_branch, True)
    tree.SetBranchStatus(isPassed_branch, True)
    for branch in additional_isPassed_branches.values():
        tree.SetBranchStatus(branch, True)

    # set branches address
    tree.SetBranchAddress(m_yy_branch, m_yy_value)
    tree.SetBranchAddress(isPassed_branch, isPassed_value)
    for name_additional in additional_regions:
        tree.SetBranchAddress(
            additional_isPassed_branches[name_additional],
            additional_isPassed_values[name_additional],
        )

    # defining branches for quantities
    quantities_value = {}

    for q in quantities:
        if q == "constant":
            continue
        if q in definitions.quantities_2d:
            q1, q2 = definitions.quantities_2d[q]
            b1, b2 = definitions.branches[q1], definitions.branches[q2]
            if b1 not in quantities_value:
                quantities_value[b1] = define_branch(
                    tree, "HGamEventInfoAuxDyn.%s" % b1
                )
            all_binnings[q1] = definitions.binnings[q1]
            if b2 not in quantities_value:
                quantities_value[b2] = define_branch(
                    tree, "HGamEventInfoAuxDyn.%s" % b2
                )
            all_binnings[q2] = definitions.binnings[q2]
        else:
            b = definitions.branches[q]
            if b not in quantities_value:
                quantities_value[b] = define_branch(tree, "HGamEventInfoAuxDyn.%s" % b)

    if n_events is None:
        n_events = tree.GetEntries()

    # change unit of binnings
    for q in all_binnings:
        if q == "constant":
            continue
        if (
            q not in definitions.quantities_2d and definitions.units_final[q] == 1000
        ):  # GeV
            all_binnings[q] = np.array(all_binnings[q]) * 1000.0

    logging.info("events to be processed: %d" % n_events)

    start = datetime.datetime.now()
    for i in range(n_events):
        if i % 5000 == 0 and i != 0:
            speed = i / (datetime.datetime.now() - start).total_seconds()
            eta = datetime.timedelta(seconds=(n_events - i) / speed)
            logging.info("event processed: %d (%.0f evt/s eta: %s)", i, speed, eta)
        tree.GetEvent(i)

        if blind:
            if 120e3 < m_yy_value[0] < 130e3:
                continue

        if not isPassed_value[0]:
            continue

        for q in quantities:
            if q == "constant":
                ibin = 1
            elif q in definitions.quantities_2d:
                q1, q2 = definitions.quantities_2d[q]
                b1, b2 = definitions.branches[q1], definitions.branches[q2]
                ibin1 = int(np.digitize(quantities_value[b1], all_binnings[q1]))
                ibin2 = int(np.digitize(quantities_value[b2], all_binnings[q2]))
                ibin = ibin1 * (len(all_binnings[q2]) + 1) + ibin2 + 1
            else:
                b = definitions.branches[q]
                ibin = int(np.digitize(quantities_value[b], all_binnings[q]))

            for region in regions:
                if (region, q) not in definitions.regions_quantities_analysis:
                    continue

                if region == "Diphoton":
                    all_out_files[(q, "Diphoton", ibin)].write(
                        "%s\n" % (m_yy_value[0] / 1000)
                    )
                else:
                    if additional_isPassed_values[region][0]:
                        all_out_files[(q, region, ibin)].write(
                            "%s\n" % (m_yy_value[0] / 1000)
                        )

    logging.info("all events processed")

    return n_events


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="scan data for workspace",
        formatter_class=RawTextHelpFormatter,
        epilog="Example: ./read_data.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/uxAOD/h025/data/data15.root --split-folders",
    )

    parser.add_argument("filenames", nargs="+")
    parser.add_argument("--quantities", nargs="*", help="quantities name")
    parser.add_argument("--output-folder", default="output")
    parser.add_argument("--split-folders", action="store_true")
    parser.add_argument("--nevents", type=int)
    parser.add_argument("--blind", action="store_true")
    args = parser.parse_args()

    all_observables_list = definitions.quantities + list(
        definitions.quantities_2d.keys()
    )

    if args.quantities is None:
        logging.info(
            "observables not speficied: using all of them: %s"
            % ",".join(all_observables_list)
        )
        args.quantities = all_observables_list + ["constant"]
    logging.info("quantities: %s" % args.quantities)

    all_binnings = {
        quantity: (
            np.linspace(
                definitions.binnings[quantity][1],
                definitions.binnings[quantity][2],
                definitions.binnings[quantity][0] + 1,
            )
            if quantity in definitions.quantities_2d
            else definitions.binnings[quantity]
        )
        for quantity in args.quantities
    }

    for quantity in all_binnings:
        nedges = len(all_binnings[quantity])
        logging.info(
            "quantity %s binning (%d): %s",
            quantity,
            nedges - 1,
            list(all_binnings[quantity]),
        )

    nprocessed = 0
    start = datetime.datetime.now()
    for i, filename in enumerate(args.filenames, 1):
        logging.info("runnig on file (%d/%d): %s", i, len(args.filenames), filename)

        try:
            nprocessed += read_file(
                filename,
                all_binnings,
                args.nevents,
                args.output_folder,
                args.split_folders,
                args.blind,
            )
        except KeyboardInterrupt:
            logging.warning("interrupted by the user")
            break

    logging.info(
        "process nevents %d (%f evt/sec)",
        nprocessed,
        nprocessed / (datetime.datetime.now() - start).total_seconds(),
    )
