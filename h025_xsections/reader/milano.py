import logging
import os
import re
from functools import lru_cache
from glob import glob
from itertools import product
from typing import List, Dict

import numpy as np
import pandas as pd
from h025_xsections.core import definitions
from h025_xsections.core.utils import iter_collection
from h025_xsections.core.folders import get_path, patterns
from h025_xsections.core.naming import parse_efficiency_filename

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def read_all_csv(
    folder_scheme: str, regions: List[str], quantities: List[str], mcs: List[str], prods: List[str]
):
    all_df = {}
    if "{bin}" not in folder_scheme:
        for region, quantity, mc, prod in product(regions, quantities, mcs, prods):
            fn = folder_scheme.format(region=region, quantity=quantity, mc=mc, prod=prod)
            try:
                df = pd.read_csv(fn, header=None)
                df.index.name = "bin"
                all_df[(region, quantity, mc, prod)] = df
            except IOError:
                logging.warning("cannot find file %s", fn)
                continue
        if not all_df:
            return None
        df = pd.concat(all_df, names=["region", "quantity", "mc", "prod"])
        return df
    else:
        raise NotImplementedError


def read_signal_parameters(
    basedir: str,
    regions: List[str],
    quantities: List[str],
    mcs: List[str],
    prods: List[str],
    signal_model_type: str = "DSCB",
):
    all_df = {}
    for region, quantity, mc, prod in product(regions, quantities, mcs, prods):

        if "_vs_" in quantity:
            bin_info = definitions.binnings[quantity]
            nbins = len(np.linspace(bin_info[1], bin_info[2], bin_info[0])) + 1
        else:
            nbins = definitions.nbins_noextraflow[quantity] + 2

        all_df_bins = {}
        for ibin in range(nbins):
            fn = get_path(
                basedir,
                "signal_parameters",
                signal_model_type=signal_model_type,
                region=region,
                quantity=quantity,
                bin=ibin,
            )
            try:
                df = pd.read_csv(fn, header=None, sep=" ").set_index(0)[1]
                df.index.name = "parameter"
                all_df_bins[ibin] = df

            except FileNotFoundError:
                if (
                    (ibin != 0 or definitions.show_underflows[quantity] is True)
                    and (ibin != (nbins - 1) or definitions.show_overflows[quantity] is True)
                    and "_vs_" not in quantity
                ):
                    logging.error("cannot find file %s", fn)
                continue
        if not all_df_bins:
            logging.error(
                "cannot find any signal parameter info for any bin %s for %s %s %s %s",
                list(range(nbins)),
                region,
                quantity,
                mc,
                prod,
            )
            continue
        df_all_bins = pd.concat(all_df_bins, names=["bin"])
        all_df[(region, quantity, mc, prod)] = df_all_bins
    df = pd.concat(all_df, names=["region", "quantity", "mc", "prod"])
    return df


def get_sys_scale(input_dir: str, region: str, quantity: str, direction: str = "up") -> Dict:
    sys_scale_values = {}
    for sys_fn in glob(
        os.path.join(
            input_dir, "systematics", "sys_scale", region, quantity, "*SCALE*__1%s.csv" % direction
        )
    ):
        sys_name = os.path.basename(sys_fn).split("__1")[0].split(".")[-1]
        sys_values = np.genfromtxt(sys_fn, delimiter=",")
        if "EG_SCALE_ALL" in sys_name or "AF2" in sys_name:
            continue
        assert sys_name not in sys_scale_values
        sys_scale_values[sys_name] = sys_values

    return sys_scale_values


def get_sys_resolution(input_dir: str, region: str, quantity: str, direction: str = "up") -> Dict:
    sys_resolution_values = {}
    for sys_fn in glob(
        os.path.join(
            input_dir,
            "systematics",
            "sys_resolution",
            region,
            quantity,
            "*RESOLUTION*__1%s.csv" % direction,
        )
    ):
        sys_name = os.path.basename(sys_fn).split("__1")[0].split(".")[-1]
        sys_values = np.genfromtxt(sys_fn, delimiter=",")
        if "EG_RESOLUTION_ALL" in sys_name or "AF2" in sys_name:
            continue
        assert sys_name not in sys_resolution_values
        sys_resolution_values[sys_name] = sys_values

    return sys_resolution_values


class Reader:
    def __init__(self, basedir: str):
        self.basedir = basedir
        if not os.path.exists(basedir):
            raise IOError("path %s does not exist" % basedir)

    def get_variations(self, only_yield=False, only_shape=False):
        folder = get_path(self.basedir, "merged_files_folder")
        all_files = glob(os.path.join(folder, "*.root"))
        infos = map(parse_efficiency_filename, all_files)
        s = set()
        for info in infos:
            if only_yield and "PhotonAllSys" in info.sysgroup:
                continue
            if only_shape and "PhotonAllSys" not in info.sysgroup:
                continue
            s.add((info.sysgroup, info.sys))
        return list(s)

    @lru_cache(maxsize=32)
    def _get_pes_all(self, region: str, quantity: str):
        nbins = len(definitions.binnings[quantity]) + 1
        idx_bins = pd.RangeIndex(nbins)
        df_up = pd.DataFrame(get_sys_scale(self.basedir, region, quantity, "up"), index=idx_bins).T
        df_down = pd.DataFrame(
            get_sys_scale(self.basedir, region, quantity, "down"), index=idx_bins
        ).T
        df = pd.concat({"up": df_up, "down": df_down})
        df.index.names = ["direction", "sys_name"]
        df.index = df.index.swaplevel()
        df.columns.names = ["bin"]
        df.index.levels[0].str.replace("ATLAS_", "")
        if df.empty:
            return df
        df.index = df.index.set_levels(df.index.levels[0].str.replace("ATLAS_", ""), level=0)
        return df

    @lru_cache(maxsize=32)
    def _get_per_all(self, region: str, quantity: str):
        nbins = len(definitions.binnings[quantity]) + 1
        idx_bins = pd.RangeIndex(nbins)
        df_up = pd.DataFrame(
            get_sys_resolution(self.basedir, region, quantity, "up"), index=idx_bins
        ).T
        df_down = pd.DataFrame(
            get_sys_resolution(self.basedir, region, quantity, "down"), index=idx_bins
        ).T
        df = pd.concat({"up": df_up, "down": df_down})
        df.index.names = ["direction", "sys_name"]
        df.index = df.index.swaplevel()
        df.columns.names = ["bin"]
        if df.empty:
            return df
        df.index = df.index.set_levels(df.index.levels[0].str.replace("ATLAS_", ""), level=0)
        return df

    def _get_signal_pars_all(
        self, region: str, quantity: str, basedir: str = None, signal_model_type: str = "DSCB"
    ):
        basedir = basedir or self.basedir
        df = read_signal_parameters(
            self.basedir,
            [region],
            [quantity],
            ["mcAll"],
            ["prodAll"],
            signal_model_type=signal_model_type,
        )

        return df

    def get_pes(self, region: str, quantity: str, sys: str = None, direction: str = None):
        df = self._get_pes_all(region, quantity)

        if sys is not None:
            df = df.xs(sys, level="sys_name")
        if direction is not None:
            df = df.xs(direction, level="direction")
        return df

    def get_per(self, region: str, quantity: str, sys: str = None, direction: str = None):
        df = self._get_per_all(region, quantity)

        if sys is not None:
            df = df.xs(sys, level="sys_name")
        if direction is not None:
            df = df.xs(direction, level="direction")
        return df

    def get_background_prefit_pars(
        self, region: str, quantity: str, ibin: int, basedir: str = None
    ):
        basedir = basedir or os.path.join(self.basedir, "bkg_prefit_parameters")
        folder_scheme = "%s/{region}/{quantity}/bin{ibin}/fit_parameters_bin{ibin}.txt" % basedir
        parameters_fn = folder_scheme.format(region=region, quantity=quantity, ibin=ibin)
        pars = []
        with open(parameters_fn) as f:
            for line in f:
                line = line.strip()
                if line:
                    ls = line.split()
                    pars.append([ls[0], float(ls[1]), float(ls[2])])
        return pars

    def get_signal_pars(
        self,
        region: str,
        quantity: str,
        ibin: int = None,
        parameter: str = None,
        basedir: str = None,
        signal_model_type: str = "DSCB",
    ):
        df = self._get_signal_pars_all(region, quantity, basedir, signal_model_type)
        df = df.xs([region, quantity, "mcAll", "prodAll"]).unstack("bin")
        nbins = len(definitions.binnings[quantity])
        idx_bins = pd.RangeIndex(nbins + 1)
        df = df.reindex(columns=idx_bins)
        if parameter is not None:
            df = df.xs(parameter)
        if ibin is not None:
            df = df[ibin]
        return df

    @lru_cache(maxsize=32)
    def get_resolution_all(
        self,
        region: str,
        quantity: str,
        mc: str = "mcAll",
        prod: str = "prodAll",
        sysgroup="Nominal",
        sys="NOMINAL",
        signal_model_type: str = "DSCB",
    ) -> pd.DataFrame:
        folder_scheme = (
            "%s/signal_parameters/signal_parameters_{mc}.{prod}.isPass_or_isFiducial.{sysgroup}.{sys}/{signal_model_type}/{region}/{quantity}/bin_{ibin}/resolution.txt"
            % self.basedir
        )
        if "_vs_" in quantity:
            bin_info = definitions.binnings[quantity]
            nbins = len(np.linspace(bin_info[1], bin_info[2], bin_info[0])) + 1
        else:
            nbins = definitions.nbins_noextraflow[quantity] + 2
        dfs = {}
        for ibin in range(nbins):
            fn = folder_scheme.format(
                mc=mc,
                prod=prod,
                sysgroup=sysgroup,
                sys=sys,
                signal_model_type=signal_model_type,
                ibin=ibin,
                region=region,
                quantity=quantity,
            )
            try:
                dfs[ibin] = pd.read_csv(fn, sep="\t", header=None)
                dfs[ibin].columns = ["fraction", "resolution"]
                dfs[ibin] = dfs[ibin].set_index(["fraction"])
            except FileNotFoundError:
                if (
                    (ibin != 0 or definitions.show_underflows[quantity] is True)
                    and (ibin != (nbins - 1) or definitions.show_overflows[quantity] is True)
                    and "_vs_" not in quantity
                ):
                    logging.error("cannot find file %s", fn)
                continue
        df = pd.concat(dfs, names=["bin"])
        return df

    def get_resolution(
        self,
        region: str,
        quantity: str,
        ibin: int = None,
        fraction: float = 0.6827,
        mc: str = "mcAll",
        prod: str = "prodAll",
        sysgroup="Nominal",
        sys="NOMINAL",
        signal_model_type: str = "DSCB",
    ) -> pd.DataFrame:
        df = self.get_resolution_all(region, quantity, mc, prod, sysgroup, sys, signal_model_type).copy()
        if ibin is None:
            ibin = slice(None)
        if fraction is None:
            fraction = slice(None)
        df = df.xs(pd.IndexSlice[ibin, fraction])
        return df

    def get_xsections_fiducial(
        self, region: str, quantity: str, mc: str = "mcAll", prod: str = "prodAll"
    ):
        if prod == "prodAll":
            folder_scheme = (
                "%s/xsections/{region}/{quantity}/xsection_isFiducial_notDalitz_{region}_{quantity}_mcAll_NOMINAL.csv"
                % self.basedir
            )
            df = read_all_csv(folder_scheme, [region], [quantity], [mc], [prod])
            if df is None:
                raise ValueError("data is empty")
            df = df[0]
        else:
            df = pd.read_csv(
                "{basedir}/xsections/{region}/{quantity}/xsection_per_prod_modes_isFiducial_notDalitz_{region}_{quantity}_mcAll_NOMINAL.csv".format(
                    basedir=self.basedir, region=region, quantity=quantity
                )
            )
            df.columns.name = "bin"
            df = df.set_index("prod")
            df.columns = df.columns.astype(int)
            if prod is not None:
                df = df.loc[prod]
            return df

        df = df.xs([region, quantity, mc, prod])
        return df

    def get_cfactors(
        self,
        region: str,
        quantity: str,
        mc: str = "mcAll",
        prod: str = "prodAll",
        sysgroup="Nominal",
        sys="NOMINAL",
    ) -> pd.DataFrame:
        folder_scheme = (
            "%s/efficiencies/efficiencies_{mc}.{prod}.isPass_or_isFiducial.%s.%s/cfactors/{region}/{quantity}/cfactors_{region}_{quantity}_{mc}_prod{prod}_%s.csv"
            % (self.basedir, sysgroup, sys, sys)
        )
        df = read_all_csv(folder_scheme, [region], [quantity], [mc], [prod])[0]

        df = df.xs([region, quantity, mc, prod])
        return df

    def get_matrix(
        self,
        region: str,
        quantity: str,
        mc: str = "mcAll",
        prod: str = "prodAll",
        sysgroup="Nominal",
        sys="NOMINAL",
    ):
        folder_scheme = (
            "%s/efficiencies/efficiencies_{mc}.{prod}.isPass_or_isFiducial.%s.%s/folding_matrices/{region}/{quantity}/folding_matrix_isPassed_for_workspace_{region}_{quantity}_{mc}_prod{prod}_%s.csv"
            % (self.basedir, sysgroup, sys, sys)
        )
        df = read_all_csv(folder_scheme, [region], [quantity], [mc], [prod])

        df = df.xs([region, quantity, mc, prod])
        return df

    @lru_cache(maxsize=32)
    def get_cfactors_sys(self, region: str, quantity: str):
        folder_scheme = "%s/systematics/sys_yield/bin_by_bin/{region}/{quantity}" % self.basedir
        folder = folder_scheme.format(region=region, quantity=quantity)
        all_files = glob(os.path.join(folder, "*"))
        if not all_files:
            logging.warning("no systematic found on cfactor for %s %s", region, quantity)
            nbins = len(definitions.binnings[quantity]) + 1
            idx_bins = pd.RangeIndex(nbins)
            return pd.DataFrame(columns=idx_bins)
        sys_yield_values = {}
        for sys_fn in all_files:
            sys_fn_basename = os.path.splitext(os.path.basename(sys_fn))[0]
            if "__1" in sys_fn_basename:
                sys_name, direction = sys_fn_basename.split("__1")
            else:
                sys_name = sys_fn_basename
                direction = "up"
            sys_values = np.genfromtxt(sys_fn, delimiter=",")
            key = (sys_name, direction)
            assert key not in sys_yield_values
            sys_yield_values[key] = sys_values
        df = pd.DataFrame(sys_yield_values)
        df.columns.names = ["sys", "direction"]
        df.index.name = "bin"
        df = df.T
        df = df.sort_index()
        return df

    def get_nreco(self, region: str, quantity: str, mc: str = "mcAll", prod: str = "prodAll"):
        folder_scheme = (
            "%s/efficiencies/efficiencies_{mc}.{prod}.isPass_or_isFiducial.Nominal.NOMINAL/nreco_isPassed/{region}/{quantity}/nreco_isPassed_{region}_{quantity}_{mc}_prod{prod}_NOMINAL.csv"
            % self.basedir
        )
        df = read_all_csv(folder_scheme, [region], [quantity], [mc], [prod])[0]

        df = df.xs([region, quantity, mc, prod])
        return df

    def _get_mu_errors(self, fit_result, region: str, quantity: str):
        parameters = fit_result.floatParsFinal()
        results = {}
        r = re.compile(
            r"mu_xsec_{region}_{quantity}_([0-9]+)".format(region=region, quantity=quantity)
        )
        for parameter in iter_collection(parameters):
            m = r.match(parameter.GetName())
            if not m:
                continue
            ibin = int(m.group(1))
            results[ibin] = parameter.getError()
        return pd.Series(results).sort_index()

    def _get_mu_values(self, fit_result, region: str, quantity: str):
        parameters = fit_result.floatParsFinal()
        results = {}
        r = re.compile(
            r"mu_xsec_{region}_{quantity}_([0-9]+)".format(region=region, quantity=quantity)
        )
        for parameter in iter_collection(parameters):
            m = r.match(parameter.GetName())
            if not m:
                continue
            ibin = int(m.group(1))
            results[ibin] = parameter.getVal()
        return pd.Series(results).sort_index()

    def get_fit_result(
        self, region: str, quantity: str, method: str, sys="statonly", dataset="AsimovSB"
    ):
        fn = get_path(
            self.basedir,
            "workspace_fitted",
            method=method,
            region=region,
            quantity=quantity,
            dataset=dataset,
            sys=sys,
        )

        import ROOT

        f = ROOT.TFile.Open(fn)
        fit_result = f.Get("fitResult")
        ROOT.SetOwnership(fit_result, True)

        return fit_result

    def get_fit_mus(
        self, region: str, quantity: str, method: str, sys: str = "full", dataset: str = "AsimovSB"
    ):
        fit_result = self.get_fit_result(region, quantity, method, sys=sys, dataset=dataset)
        return self._get_mu_values(fit_result, region, quantity)

    def get_fit_stat_error(
        self, region: str, quantity: str, method: str, dataset: str = "AsimovSB"
    ):
        fit_result = self.get_fit_result(region, quantity, method, sys="statonly", dataset=dataset)
        return self._get_mu_errors(fit_result, region, quantity)

    def get_fit_full_error(
        self, region: str, quantity: str, method: str, dataset: str = "AsimovSB"
    ):
        fit_result = self.get_fit_result(region, quantity, method, sys="full", dataset=dataset)
        return self._get_mu_errors(fit_result, region, quantity)
