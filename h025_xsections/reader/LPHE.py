import logging
import os
import sys
from copy import copy
import re
from numpy.lib.arraysetops import isin
import yaml

import numpy as np
import pandas as pd

from h025_xsections.core import definitions

if sys.version_info[0] < 3:
    from StringIO import StringIO
else:
    from io import StringIO

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def parse_perpes(filename):
    lines = open(filename).readlines()
    # remove the non-needed lines and make the file a csv
    lines = "".join([l.replace(":", "") for l in lines if ("up" in l or "down" in l)])
    lines = StringIO(lines)

    df = pd.read_csv(lines, sep=" ", header=None).rename(columns={0: "sys_name"})
    df["direction"] = df["sys_name"].apply(lambda name: name.split("_")[-1].rstrip(":"))
    df["sys_name"] = df["sys_name"].str.replace("_up", "").str.replace("_down", "")
    df = df.set_index(["sys_name", "direction"])
    return df


def parse_signalmodel(filename):
    lines = open(filename).readlines()

    lines = "".join([l.replace(":", "") for l in lines if l.startswith("DSCB_")])

    lines = StringIO(lines)
    df = pd.read_csv(lines, sep=" ", header=None).rename(columns={0: "parameter"})
    df[["parameter", "bin"]] = df["parameter"].str.rsplit("_", 1, expand=True)
    df["bin"] = df["bin"].str.replace("bin", "")
    df["parameter"] = df["parameter"].str.replace("DSCB_", "")
    df["bin"] = df["bin"].astype(int)
    df = df.set_index(["parameter", "bin"])
    df.columns = ["min", "max", "value", "error"]
    df = df.unstack("bin")
    return df


class Reader:
    def __init__(self, basedir, normalize=True):
        self.basedir = basedir
        self.normalize = normalize

        self._df_per = {}
        self._df_pes = {}
        self._df_signal_pars = {}
        self._background_functions = {}
        self._background_ss = {}
        self._punchcards = {}

    def read_pes_per(self, filename):
        df_perpes = parse_perpes(filename)

        df_perpes.index = df_perpes.index.set_levels(
            df_perpes.index.levels[0].str.replace("__ETABN", "__ETABIN"), level=0
        )

        mask_per = df_perpes.index.map(lambda x: "EG_RESOLUTION" in x[0])
        mask_pes = df_perpes.index.map(lambda x: "EG_SCALE" in x[0] or "PH_SCALE" in x[0])

        mask_unknown = ~(mask_per.to_numpy(dtype=bool) | mask_pes.to_numpy(dtype=bool))
        mask_unknown = pd.Index(mask_unknown)
        if len(df_perpes.loc[mask_unknown]) != 0:
            logging.warning(
                "can't understand if per or pes: %s",
                ", ".join(df_perpes.loc[mask_unknown].index.remove_unused_levels().levels[0]),
            )

        df_pes, df_per = df_perpes[mask_pes], df_perpes[mask_per]
        df_pes.index = df_pes.index.remove_unused_levels()
        df_per.index = df_per.index.remove_unused_levels()

        return df_pes, df_per

    def get_punchcard_filename(self, region, quantity):
        if quantity == "constant":
            if region == "Diphoton":
                fn = "isPassed.punch"
            else:
                fn = "catXS_%s.punch" % region
        else:
            fn = quantity + ".punch"
            if region != "Diphoton":
                fn = region + "_" + fn
        fn = os.path.join("HGamFitter/punchcards/LPNHE", fn)
        return os.path.join(self.basedir, fn)

    def read_pesper_punchcard(self, sys_names, punchcard, add_underflow, add_overflow):
        data = {}
        for sys_name in sys_names:
            for direction in ["up", "down"]:
                row = sys_name + "_" + direction
                if row not in punchcard:
                    raise KeyError("cannot find systematic %s in punchcard" % row)
                values = punchcard[row]
                values = list(np.atleast_1d(values))
                if add_underflow:
                    values = [np.nan] + values
                if add_overflow:
                    values = values + [np.nan]
                data[(sys_name.replace("ETABN", "ETABIN"), direction)] = values
        df = pd.DataFrame(data).T
        df.index.names = ["sys_name", "direction"]

        return df

    def read_pes_punchcard(self, punchcard, add_underflow, add_overflow):
        names_sys_pes = punchcard["EnScaleNames"]
        return self.read_pesper_punchcard(names_sys_pes, punchcard, add_underflow, add_overflow)

    def read_per_punchcard(self, punchcard, add_underflow, add_overflow):
        names_sys_per = punchcard["EnResNames"]
        return self.read_pesper_punchcard(names_sys_per, punchcard, add_underflow, add_overflow)

    def get_pes(self, region, quantity, sys=None, direction=None):
        if (region, quantity) not in self._punchcards:
            self._punchcards[(region, quantity)] = self.read_punch_card(region, quantity)

        punchcard = self._punchcards[(region, quantity)]
        add_underflow = self.normalize and not definitions.show_underflows[quantity]
        add_overflow = self.normalize and not definitions.show_overflows[quantity]

        df = self.read_pes_punchcard(punchcard, add_underflow, add_overflow)

        if sys is not None:
            df = df.xs(sys, label="sys_name")
        if direction is not None:
            df = df.xs(direction, level="direction")

        return df

    def get_per(self, region, quantity, sys=None, direction=None):
        if (region, quantity) not in self._punchcards:
            self._punchcards[(region, quantity)] = self.read_punch_card(region, quantity)

        punchcard = self._punchcards[(region, quantity)]
        add_underflow = self.normalize and not definitions.show_underflows[quantity]
        add_overflow = self.normalize and not definitions.show_overflows[quantity]

        df = self.read_per_punchcard(punchcard, add_underflow, add_overflow)

        if sys is not None:
            df = df.xs(sys, label="sys_name")
        if direction is not None:
            df = df.xs(direction, level="direction")

        return df

    def get_signal_pars(self, region, quantity, parameter=None, value="value"):
        if quantity == "constant":
            if region == "Diphoton":
                fn = "PlotSignalModels_params", "isPassed"
            else:
                fn = "PlotSignalModels_params_catXS", region
        else:
            fn = ("PlotSignalModels_params", region if region != "Diphoton" else "", quantity)
        fn = "_".join([_ for _ in fn if _]) + ".record"
        filename = os.path.join(self.basedir, "SignalModels/LPNHE", fn)

        if (region, quantity) not in self._df_signal_pars:
            self._df_signal_pars[(region, quantity)] = parse_signalmodel(filename)
        df = self._df_signal_pars[(region, quantity)]

        if parameter is not None:
            df = df.xs(parameter)
        if value is not None:
            df = df[value]
        return df

    def get_xsections_fiducial(self, region, quantity, mc="mcAll", prod="prodAll"):
        if mc != "mcAll":
            raise NotImplementedError
        prod_dir = {
            "prodAll": "all",
            "ggH": "PowhegPy8_NNLOPS_ggH125",
            "VBF": "PowhegPy8EG_NNPDF30_VBFH125",
            "WmH": "PowhegPy8_WmH125J",
            "WpH": "PowhegPy8_WpH125J",
            "ZH": "PowhegPy8_ZH125J",
            "ggZH": "PowhegPy8_ggZH125",
            "ttH": "PowhegPy8_ttH125",
        }.get(prod, prod)
        fn = "cross_sections/LPNHE/{prod_dir}/{quantity}_fid.txt".format(
            quantity=quantity, prod_dir=prod_dir
        )
        fn = os.path.join(self.basedir, fn)
        xs = pd.read_csv(fn, header=None, squeeze=True)
        xs = xs.tolist()

        if self.normalize:
            if not definitions.show_underflows[quantity]:
                xs = [0] + xs
            if not definitions.show_overflows[quantity]:
                xs = xs + [0]

        return pd.Series(xs) / 1000.0

    def read_background_functions(self, filename):
        txt = open(filename).read()
        splitted = txt.split("Variable: ")
        results_bkg_functions = {}
        regions_bkg_ss = {}
        for lines_block in splitted:
            region = "Diphoton"  # TODO: fix
            lines = lines_block.split("\n")
            variable = lines[0].strip()
            for line in lines:
                if not line:
                    continue
                elif "BkgFunctions:" in line:
                    l = [x.strip() for x in line.replace("BkgFunctions:", "").split(" ")]
                    l = [x for x in l if x]
                    results_bkg_functions[(region, variable)] = l
                elif "max(S)/Sref:" in line:
                    l = [x.strip() for x in line.replace("max(S)/Sref:", "").split(" ")]
                    l = [float(x) for x in l if x]
                    regions_bkg_ss[(region, variable)] = l
        return results_bkg_functions, regions_bkg_ss

    def read_punch_card_fn(self, filename):
        results = {}
        raw_data = yaml.safe_load(open(filename))
        for k, v in raw_data.items():
            if isinstance(v, str):
                values = re.split(" +", v)
                try:
                    values = [float(v) for v in values]
                except ValueError:
                    pass
                if len(values) == 1:
                    values = values[0]
                results[k] = values
            else:
                results[k] = v

        return results

    def read_punch_card(self, region, quantity):
        fn = self.get_punchcard_filename(region, quantity)
        return self.read_punch_card_fn(fn)

    def get_spurious_signal(self, region, quantity, ibin=None):
        if not (region, quantity) in self._punchcards:
            self._punchcards[(region, quantity)] = self.read_punch_card(region, quantity)

        spurious_signal = self._punchcards[(region, quantity)]["Nspur"] * definitions.luminosity

        if self.normalize:
            if not definitions.show_underflows[quantity]:
                spurious_signal = [0.0] + spurious_signal
            if not definitions.show_overflows[quantity]:
                spurious_signal = spurious_signal + [0.0]

        if ibin is not None:
            spurious_signal = spurious_signal[ibin]

        return abs(spurious_signal)

    def get_background_function(self, region, quantity, ibin=None):
        if not (region, quantity) in self._punchcards:
            self._punchcards[(region, quantity)] = self.read_punch_card(region, quantity)

        background_functions = self._punchcards[(region, quantity)]["BkgFunctions"]

        if self.normalize:
            if not definitions.show_underflows[quantity]:
                background_functions = [""] + background_functions
            if not definitions.show_overflows[quantity]:
                background_functions = background_functions + [""]

        if ibin is not None:
            background_functions = background_functions[ibin]
        return background_functions

    def get_background_function_old(self, region, quantity, ibin=None, filename=None):
        if filename is None:
            filename = "SpuriousSignal/ss_results.txt"
            filename = os.path.join(self.basedir, filename)

        if not self._background_ss or not self._background_functions:
            self._background_functions, self._background_ss = self.read_background_functions(
                filename
            )
        try:
            background_function = copy(self._background_functions[region, quantity])
        except KeyError:
            raise ValueError("cannot find background function %s %s" % (region, quantity))

        if self.normalize:
            if not definitions.show_underflows[quantity]:
                background_function = [""] + background_function
            if not definitions.show_overflows[quantity]:
                background_function = background_function + [""]

        if ibin is not None:
            background_function = background_function[ibin]
        return background_function

    def get_ss_over_sref_over_lumi(self, region, quantity, ibin=None, filename=None):
        if filename is None:
            filename = "SpuriousSignal/ss_results.txt"
            filename = os.path.join(self.basedir, filename)
        if not self._background_ss or not self._background_functions:
            self._background_functions, self._background_ss = self.read_background_functions(
                filename
            )

        ss = copy(self._background_ss[region, quantity])

        if self.normalize:
            if not definitions.show_underflows[quantity]:
                ss = [0] + ss
            if not definitions.show_overflows[quantity]:
                ss = ss + [0]

        if ibin is not None:
            ss = ss[ibin]
        return abs(ss)

    def get_result_cfactors_errors(self, region, quantity, ibin=None, filename=None):
        if filename is None:
            filename = "HGamUnfolder/LPNHE/results_CorrectionFactors/{quantity}.data".format(
                quantity=quantity
            )
            filename = os.path.join(self.basedir, filename)

        data_punchcard = self.read_punch_card_fn(filename)

        def select_err_from_name(name):
            if not name.startswith("%s.error" % quantity):
                return False
            if not any(
                prefix in name
                for prefix in (
                    "errorPH_",
                    "errorFT_",
                    "errorMUON_",
                    "errorMET_",
                    "errorEL_",
                    "errorJET_",
                    "errorPRW_",
                )
            ):
                return False
            if any(prefix in name for prefix in ("errorMET_up", "errorMET_down")):
                return False
            return True

        df = {}
        for k, v in data_punchcard.items():
            if select_err_from_name(k):
                k = k.replace("%s.error" % quantity, "")
                sysname, direction = k.rsplit('_', 1)
                direction = direction.lower()
                if direction not in ('up', 'down'):
                    direction = 'up'
                    sysname = k
                if not sysname.startswith("ATLAS_"):
                    sysname = "ATLAS_" + sysname

                if self.normalize:
                    if not definitions.show_underflows[quantity]:
                        v = [0] + v
                    if not definitions.show_overflows[quantity]:
                        v = v + [0]

                df[(sysname, direction)] = v


        df = pd.DataFrame(df).T.sort_index()
        df.index.names = ['sys', 'direction']
        df.columns.name = 'bin'
        return df
