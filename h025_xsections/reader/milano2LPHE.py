#!/usr/bin/env python

import logging
import os
from typing import List

import numpy as np
import pandas as pd
from h025_xsections.core import definitions
from h025_xsections.core.definitions import get_region_quantities
from h025_xsections.core.histo_utils import remove_under_overflow
from h025_xsections.core.utils import create_dir_if_not_exists
from h025_xsections.reader import Reader

logging.basicConfig(level=logging.INFO, format="%(module)-25s %(levelname)-10s %(message)s")


def convert_pes_per(reader: Reader, region: str, quantity: str, output_dir: str):
    lines = []

    all_per = set()
    all_pes = set()
    show_overflows = definitions.show_overflows[quantity]
    show_underflows = definitions.show_underflows[quantity]

    df_per = reader.get_per(region, quantity).sort_index()
    for index, values in df_per.iterrows():
        sysname = index[0].replace("__ETABIN", "__ETABN")
        all_per.add(sysname)
        k = "{sys}_{dir}".format(sys=sysname, dir=index[1])
        values = remove_under_overflow(values, show_underflows, show_overflows)
        v = " ".join(["%.7f" % v for v in values])
        line = "%s: %s" % (k, v)
        lines.append(line)

    df_pes = reader.get_pes(region, quantity).sort_index()
    for index, values in df_pes.iterrows():
        sysname = index[0].replace("__ETABIN", "__ETABN")
        all_pes.add(sysname)
        k = "{sys}_{dir}".format(sys=sysname, dir=index[1])
        values = remove_under_overflow(values, show_underflows, show_overflows)
        v = " ".join(["%.7f" % v for v in values])
        line = "%s: %s" % (k, v)
        lines.append(line)

    lines += ["\n"]

    lines += ["EnScaleNames: %s" % " ".join(all_pes)]
    lines += ["EnResNames: %s" % " ".join(all_per)]

    lines += ["\n\n"]

    pes_sumq_up = np.sqrt((df_pes.loc[pd.IndexSlice[:, "up"], :] ** 2).sum(axis=0))
    pes_sumq_up = remove_under_overflow(pes_sumq_up, show_underflows, show_overflows)
    lines += ["SCALE_up: %s" % ' '.join(['%.7f' % v for v in pes_sumq_up])]
    pes_sumq_down = np.sqrt((df_pes.loc[pd.IndexSlice[:, "down"], :] ** 2).sum(axis=0))
    pes_sumq_down = remove_under_overflow(pes_sumq_down, show_underflows, show_overflows)
    lines += ["SCALE_down: %s" % ' '.join(['%.7f' % v for v in pes_sumq_down])]

    per_sumq_up = np.sqrt((df_per.loc[pd.IndexSlice[:, "up"], :] ** 2).sum(axis=0))
    per_sumq_up = remove_under_overflow(per_sumq_up, show_underflows, show_overflows)
    lines += ["RES_up: %s" % ' '.join(['%.7f' % v for v in per_sumq_up])]
    per_sumq_down = np.sqrt((df_per.loc[pd.IndexSlice[:, "down"], :] ** 2).sum(axis=0))
    per_sumq_down = remove_under_overflow(per_sumq_down, show_underflows, show_overflows)
    lines += ["RES_down: %s" % ' '.join(['%.7f' % v for v in per_sumq_down])]

    lines += ['\n']

    txt = "\n".join(lines)
    fn = os.path.join(output_dir, "PESPER_{quantity}.txt".format(quantity=quantity))
    with open(fn, 'w') as f:
        f.write(txt)


def convert(reader: Reader, output_dir, region: str, quantity: str):
    create_dir_if_not_exists(output_dir)
    convert_pes_per(reader, region, quantity, output_dir)


def convert_all(base_dir: str, output_dir: str, regions: List[str], quantities: List[str]):
    reader = Reader(base_dir)
    regions_quantities_analysis = get_region_quantities(regions, quantities)
    for region, quantity in regions_quantities_analysis:
        if region != 'Diphoton':
            logging.warning("skipping non-Diphoton")
            continue
        logging.info("converting %s %s", region, quantity)
        convert(reader, output_dir, region, quantity)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="convert to LPHE")
    parser.add_argument("input_dir")
    parser.add_argument("output_dir")
    parser.add_argument("--regions", nargs="*")
    parser.add_argument("--quantities", nargs="*")
    args = parser.parse_args()
    convert_all(args.input_dir, args.output_dir, args.regions, args.quantities)
