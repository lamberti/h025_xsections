#!/bin/bash

source menu.sh


RED='\033[0;31m'
NC='\033[0m' # No Color
LINK='\e]8;;'

if [ -z "$1" ]
then
    INPUT_BASEDIR="${HOME}/cernbox/h025_xsections/v0.6.0"
    echo "default"
else
    INPUT_BASEDIR=$1
fi
DIR_RAW_HISTOGRAM="${INPUT_BASEDIR}/output_histo"

OUTPUT_DIR="./tmp-results"
DIR_NORMALIZED=$OUTPUT_DIR/output_histo_normalized
DIR_MERGED=$OUTPUT_DIR/output_histo_norm_merged
DIR_SIGNAL=$OUTPUT_DIR/signal_parameters
DIR_EFF=$OUTPUT_DIR/efficiencies
DIR_SYS=$OUTPUT_DIR/systematics

DIR_TABLES=$OUTPUT_DIR/tables


AUTO_OPT=false

declare -a instructions=("VERSION: 0.6.1"
                         "INPUT_BASEDIR=$INPUT_BASEDIR");

declare -a options=(" None"
                    " Normalize & merge histograms"
					" Compute signal model plus systematics"
					" Compute yield"
					" Compute yield systematics"
                    " Pre-fit for background parameters"
                    " Build XML for workspace"
                    " Build workspaces ['xmlAnaWSBuilder' tool needed]"
                    " Fit workspaces  ['quickFit' tool needed]"
                    "Plot fits"
                    "Create final plots for cross-sections"
                    "Fit workspaces for sys ['quickFit' tool needed]"
					"Sys plots"
                    "Make tables"
                    "Exit");

declare -a unfolding_options=("Bin by bin unfolding"
			      "Inverse matrix unfolding");

# selectedOption (option)
function selectedOption() {
    echo -e "${RED}SELECTED: ${1}${NC}"
  }


for arg in "$@"; do
    shift
    case "$arg" in
        "--help")
            set -- "$@" "-h";;
        "--auto")
            set -- "$@" "-a";;
        *)
            set -- "$@" "$arg"
    esac
done

while getopts ":ah" option; do
    case ${option} in
        "a")
            AUTO_OPT=true
            ;;
        "h") 
            echo "USAGE: Add the flag '-a' [auto] to run the complete analysis automatically (no selection menu will be shown)."
            exit 1
            ;;
        *)
            echo "WARNING: Illegal option. Use '-h' for usage."
            exit 1
            ;;
    esac
done

if [[ ${AUTO_OPT} = true ]]; then
    echo "not supported"

else
    trap "exit" INT # Exit from the entire script when ctrl+C
    
    while true; do
	# Generate ordered instructions
	generateDialog "instructions" "'h025_xsections' Analysis [L = 139 fb^-1 @ sqrt(s) = 13 TeV]" \
		       "Mario Lamberti, Ruggero Turra" "${instructions[@]}"

	# Generate options menu
	generateDialog "options" "SELECT AN OPTION" "" "${options[@]}"

	read CHOICE


	start=$SECONDS #Start script timer
	case $CHOICE in
  	    2) # Normalize & merge histograms
  		selectedOption "Normalize & merge histograms"
  		echo -e "${RED}Running normalization of histograms${NC}"
  		h025_xsections/merging/normalize_histos.py $DIR_RAW_HISTOGRAM $DIR_NORMALIZED

  		echo -e "${RED}Running merging of histograms${NC}"
  		h025_xsections/merging/merge_histos.py $DIR_NORMALIZED $DIR_MERGED

		break
  		;;

	    3) # Compute signal model plus systematics
		selectedOption "Compute signal model plus systematics"
  		echo -e "${RED}Computing signal model${NC}"
		./run.py --basedir $OUTPUT_DIR signal_model_nominal
		./run.py --basedir $OUTPUT_DIR signal_model_systematic
		echo -e "${RED}Extracting shape systematics${NC}"
		h025_xsections/make_signal/compute_systematics.py $DIR_SIGNAL $DIR_SYS --output-dir-plots $OUTPUT_DIR/plots/plot_signal_shape_sys

		break
  		;;

  	    4) # Compute signal yield
  		selectedOption "Compute signal yield"
  		echo -e "${RED}Computing yields${NC}"
		if [ ! -d $DIR_MERGED ]
		then
			DIR_MERGED=$INPUT_BASEDIR/output_histo_norm_merged
			echo "directory does not exists, using $DIR_MERGED"
		fi
  		h025_xsections/efficiency/get_efficiency_all_sys.py $DIR_MERGED/ $DIR_EFF

		break
		;;

		5) # Compute yield systematics

		echo -e "${RED}Extracting yield systematics${NC}"
		./run.py --basedir $OUTPUT_DIR matrix_sys
		./run.py --basedir $OUTPUT_DIR cfactor_sys

		break
  		;;

  	    6) # Pre-fit background parameters
  		selectedOption "Pre-fit for background parameters"
  		echo -e "${RED}Extracting bkg fit parameters from sidebands${NC}"
		./run.py --basedir $OUTPUT_DIR background_prefit --blinded

		break
  		;;

  	    7) # Build XML for workspaces
  		selectedOption "Build XML for workspaces"
		
		while true; do
  		    generateDialog "options" "SELECT UNFOLDING METHOD" "" "${unfolding_options[@]}"
		    
  		    read CHOICE
		
  		    case $CHOICE in
  			1)  # Bin by bin
  			    selectedOption "Bin by bin unfolding${NC}"
  			    echo -e "${RED}Creating xml for building the blinded workspaces${NC}"
  			    ./run.py --basedir $OUTPUT_DIR make_xml --unfolding bin_by_bin --blinded
			    break
  			    ;;

  			2) # Inverse Matrix
  			    selectedOption "Inverse matrix unfolding"
  			    echo -e "${RED}Creating xml for building the blinded workspaces${NC}"

  			    ./run.py --basedir $OUTPUT_DIR make_xml --unfolding matrix --blinded
			    break
  			    ;;

  			*) # Illegal arguments
  			    echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
  		    esac
		done
		break
  		;;

  	    8) # Fit data sidebands for Asimov dataset
  		selectedOption "Creating workspace"

		while true; do
  		    generateDialog "options" "SELECT UNFOLDING METHOD" "" "${unfolding_options[@]}"

  		    read CHOICE

  		    case $CHOICE in
  			1) # Bin by bin
  			    selectedOption "Bin by bin unfolding"
				./run.py --basedir $OUTPUT_DIR make_workspace --unfolding bin_by_bin --blinded
			    break
  			    ;;

  			2) # Inverse Matrix
  			    selectedOption "Inverse matrix unfolding"
  			    ./run.py --basedir $OUTPUT_DIR make_workspace --unfolding matrix --blinded
			    break
  			    ;;

  			*) # Illegal arguments
  			    echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
  			    
  		    esac
		done
		break
  		;;

  	    9) # Create fit plots from AsimovSB dataset
  		selectedOption "Fit Asimov"

		while true; do
  		    generateDialog "options" "SELECT UNFOLDING METHOD" "" "${unfolding_options[@]}"
		
  		    read CHOICE

  		    case $CHOICE in
  			1) # Bin by bin
  			    selectedOption "Bin by bin unfolding"
  			    echo -e "${RED}Fitting Asimov dataset${NC}"
				./run.py --basedir $OUTPUT_DIR fit_nominal --unfolding bin_by_bin --blinded
				./run.py --basedir $OUTPUT_DIR check_workspace --unfolding bin_by_bin

			    break
  			    ;;

  			2) # Inverse matrix
  			    selectedOption "Inverse matrix unfolding"
  			    echo -e "${RED}Fitting Asimov dataset${NC}"
  			    ./run.py --basedir $OUTPUT_DIR fit_nominal --unfolding matrix --blinded
				./run.py --basedir $OUTPUT_DIR check_workspace --unfolding bin_by_bin

			    break
			    ;;

  			*) # Illegal arguments
  			    echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
  		     
  		    esac
		done
		break
  		;;

		10)
		selectedOption "Plot workspace"
		while true; do
			generateDialog "options" "SELECT UNFOLDING METHOD" "" "${unfolding_options[@]}"
			read CHOICE
  			case $CHOICE in
  			1) # Bin by bin
  			    selectedOption "Bin by bin unfolding"
				./run.py --basedir $OUTPUT_DIR plot_workspace --unfolding bin_by_bin --blinded
				break
  			    ;;
			2) # Inverse matrix
  			    selectedOption "Inverse matrix unfolding"
				./run.py --basedir $OUTPUT_DIR plot_workspace --unfolding matrix --blinded
				break
  			    ;;
			*) # Illegal arguments
  			    echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
  		     
  		    esac
		done
		break
  		;;

  	    11) # Create final plots for cross-sections
  		selectedOption "Create final plots for cross-sections"

		while true; do
  		    generateDialog "options" "SELECT UNFOLDING METHOD" "" "${unfolding_options[@]}"

  		    read CHOICE

  		    case $CHOICE in
  			1) # Bin by bin
  			    selectedOption "Bin by bin unfolding"
  			    echo -e "${RED}Creating final cross section plots${NC}"
				./run.py --basedir $OUTPUT_DIR plot_results --unfolding bin_by_bin
			    break
  			    ;;

  			2) # Inverse matrix
  			    selectedOption "Inverse matrix unfolding"
  			    echo -e "${RED}Creating final cross section plots${NC}"
  			    ./run.py --basedir $OUTPUT_DIR plot_results --unfolding matrix
			    break
  			    ;;

  			*) # Illegal arguments
  			    echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
			    
  		    esac
		done
		break
  		;;


  	    12) # Fit for each sys group
  		selectedOption "Fit Asimov for each sys group"

		while true; do
  		    generateDialog "options" "SELECT UNFOLDING METHOD" "" "${unfolding_options[@]}"
		
  		    read CHOICE

  		    case $CHOICE in
  			1) # Bin by bin
  			    selectedOption "Bin by bin unfolding"
  			    echo -e "${RED}Fitting Asimov dataset${NC}"
  			    ./run.py --basedir $OUTPUT_DIR fit_sysgroup --unfolding bin_by_bin --blinded

			    break
  			    ;;

  			2) # Inverse matrix
  			    selectedOption "Inverse matrix unfolding"
  			    echo -e "${RED}Fitting Asimov dataset${NC}"
  			    ./run.py --basedir $OUTPUT_DIR fit_sysgroup --unfolding matrix --blinded
			    break
			    ;;

  			*) # Illegal arguments
  			    echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
  		     
  		    esac
		done
		break
  		;;

		13) # systematic plots
		selectedOption "Systematic plot"
		while true; do
  		    generateDialog "options" "SELECT UNFOLDING METHOD" "" "${unfolding_options[@]}"

  		    read CHOICE

  		    case $CHOICE in
  			1) # Bin by bin
  			    selectedOption "Bin by bin unfolding"
            	./run.py --basedir $OUTPUT_DIR compare_fit_sys --unfolding bin_by_bin
			    break
  			    ;;

  			2) # Inverse matrix
  			    selectedOption "Inverse matrix unfolding"
            	./run.py --basedir $OUTPUT_DIR compare_fit_sys --unfolding matrix
			    break
  			    ;;

  			*) # Illegal arguments
  			    echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
			    
  		    esac
		done
		break
  		;;



  	    14) # Make tables
  		selectedOption "Make tables"
  		echo -e "${RED}Doing tables${NC}"
  		python3 h025_xsections/make_tables.py --base-dir $INPUT_BASEDIR --output-dir $DIR_TABLES
		
		break
  		;;

  	    0) # Exit code
  		echo -e "${RED}Exit code${NC}"
  		exit 1
  		;;

  	    *) # Illegal arguments
  		echo -e "${RED}WARNING: Option not valid. Choose again.${NC}"
		
	esac
    done
fi

# Timer
if (( $SECONDS > 3600 )) ; then
    let "hours=SECONDS/3600"
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "**************************************************************************"
    echo "* Completed in $hours hour(s), $minutes minute(s) and $seconds second(s) *"
    echo "**************************************************************************"
elif (( $SECONDS > 60 )) ; then
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "**********************************************************"
    echo "* Completed in $minutes minute(s) and $seconds second(s) *"
    echo "**********************************************************"
else
    echo "*********************************"
    echo "* Completed in $SECONDS seconds *"
    echo "*********************************"
fi
